--1. Creates categories

delete from Recommendation
delete from Product_Category_mapping
delete from product_manufacturer_mapping
delete from product
delete from Category

set identity_insert  Category on

insert into Category (id,name,description,Parentcategoryid,published,deleted,displayorder,createdonutc,UpdatedOnUtc,CategoryTemplateId,PictureId,PageSize,AllowCustomersToSelectPageSize,ShowOnHomePage,HasDiscountsApplied)
select id,categoryname,categoryname,ISNULL(parentid, 0),
case hidden when 'F' then 1 else 0 end
,radio,sequencenumber,getutcdate(),getutcdate(),1,1,1,0,0,0 from mn_category order by id

set identity_insert  Category off



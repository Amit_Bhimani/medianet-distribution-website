SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WebCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NOT NULL,
	[HighPriority] [bit] NOT NULL,
 CONSTRAINT [PK_WebCategory] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

set identity_insert WebCategory on

insert into WebCategory (id,Name,HighPriority)  values (1,'General News',1)
insert into WebCategory (id,Name,HighPriority)  values (2,'Aviation',0)
insert into WebCategory (id,Name,HighPriority)  values (3,'Marketing & Advertising',0)
insert into WebCategory (id,Name,HighPriority)  values (4,'Banking',0)
insert into WebCategory (id,Name,HighPriority)  values (5,'Charities/Aid/Welfare',0)
insert into WebCategory (id,Name,HighPriority)  values (6,'Community',0)
insert into WebCategory (id,Name,HighPriority)  values (7,'Information Technology',0)
insert into WebCategory (id,Name,HighPriority)  values (8,'Conventions & Clubs',0)
insert into WebCategory (id,Name,HighPriority)  values (9,'Education & Training',0)
insert into WebCategory (id,Name,HighPriority)  values (10,'Entertainment',1)
insert into WebCategory (id,Name,HighPriority)  values (11,'Environment',0)
insert into WebCategory (id,Name,HighPriority)  values (12,'Ethnic',0)
insert into WebCategory (id,Name,HighPriority)  values (13,'Farm & Rural',0)
insert into WebCategory (id,Name,HighPriority)  values (14,'Fashion',0)
insert into WebCategory (id,Name,HighPriority)  values (15,'Food & Beverages',0)
insert into WebCategory (id,Name,HighPriority)  values (16,'Gay & Lesbian',0)
insert into WebCategory (id,Name,HighPriority)  values (17,'Government- Federal',0)
insert into WebCategory (id,Name,HighPriority)  values (18,'Architecture',0)
insert into WebCategory (id,Name,HighPriority)  values (19,'Indigenous',0)
insert into WebCategory (id,Name,HighPriority)  values (20,'Insurance',0)
insert into WebCategory (id,Name,HighPriority)  values (21,'Legal',0)
insert into WebCategory (id,Name,HighPriority)  values (23,'Medical, Health & Aged Care',0)
insert into WebCategory (id,Name,HighPriority)  values (24,'National News & Current Affairs',0)
insert into WebCategory (id,Name,HighPriority)  values (25,'Oil, Mining & Resources',0)
insert into WebCategory (id,Name,HighPriority)  values (26,'House & Real Estate',0)
insert into WebCategory (id,Name,HighPriority)  values (27,'Results & Statistics',0)
insert into WebCategory (id,Name,HighPriority)  values (28,'Retail',0)
insert into WebCategory (id,Name,HighPriority)  values (29,'Sport & Recreation',1)
insert into WebCategory (id,Name,HighPriority)  values (30,'Taxation',0)
insert into WebCategory (id,Name,HighPriority)  values (31,'Telecommunication',0)
insert into WebCategory (id,Name,HighPriority)  values (33,'Transport & Automotive',0)
insert into WebCategory (id,Name,HighPriority)  values (34,'Union',0)
insert into WebCategory (id,Name,HighPriority)  values (32,'Travel & Tourism',0)
insert into WebCategory (id,Name,HighPriority)  values (35,'Finance & Investment',0)
insert into WebCategory (id,Name,HighPriority)  values (36,'Government- NSW',0)
insert into WebCategory (id,Name,HighPriority)  values (37,'Government- NT',0)
insert into WebCategory (id,Name,HighPriority)  values (38,'Government- QLD',0)
insert into WebCategory (id,Name,HighPriority)  values (39,'Government- SA',0)
insert into WebCategory (id,Name,HighPriority)  values (40,'Government- TAS',0)
insert into WebCategory (id,Name,HighPriority)  values (41,'Government- VIC',0)
insert into WebCategory (id,Name,HighPriority)  values (42,'Government- WA',0)
insert into WebCategory (id,Name,HighPriority)  values (43,'Foreign Affairs & Trade',0)
insert into WebCategory (id,Name,HighPriority)  values (45,'Crime & Emergency Services',0)
insert into WebCategory (id,Name,HighPriority)  values (47,'Utilities',0)
insert into WebCategory (id,Name,HighPriority)  values (50,'Police',0)
insert into WebCategory (id,Name,HighPriority)  values (51,'Gaming',0)
insert into WebCategory (id,Name,HighPriority)  values (52,'Science',0)
insert into WebCategory (id,Name,HighPriority)  values (53,'Women',0)
insert into WebCategory (id,Name,HighPriority)  values (54,'Business & Company News',1)
insert into WebCategory (id,Name,HighPriority)  values (55,'Media',0)
insert into WebCategory (id,Name,HighPriority)  values (56,'Local Government',0)
insert into WebCategory (id,Name,HighPriority)  values (57,'Manufacturing',0)
insert into WebCategory (id,Name,HighPriority)  values (58,'Religion',0)
insert into WebCategory (id,Name,HighPriority)  values (116,'Federal Election',0)
insert into WebCategory (id,Name,HighPriority)  values (60,'Political',0)
insert into WebCategory (id,Name,HighPriority)  values (61,'Defence',0)
insert into WebCategory (id,Name,HighPriority)  values (64,'Veterans'' Affairs',0)
insert into WebCategory (id,Name,HighPriority)  values (65,'Employment',0)
insert into WebCategory (id,Name,HighPriority)  values (66,'International News',0)
insert into WebCategory (id,Name,HighPriority)  values (69,'Biotechnology',0)
insert into WebCategory (id,Name,HighPriority)  values (70,'Animal',0)
insert into WebCategory (id,Name,HighPriority)  values (71,'Youth',0)
insert into WebCategory (id,Name,HighPriority)  values (73,'Industrial Relations',0)
insert into WebCategory (id,Name,HighPriority)  values (74,'Building & Construction',0)
insert into WebCategory (id,Name,HighPriority)  values (77,'Regional & Country Services',0)
insert into WebCategory (id,Name,HighPriority)  values (78,'Research & Development',0)
insert into WebCategory (id,Name,HighPriority)  values (79,'Energy',0)
insert into WebCategory (id,Name,HighPriority)  values (80,'Art',0)
insert into WebCategory (id,Name,HighPriority)  values (117,'Federal Budget',0)
insert into WebCategory (id,Name,HighPriority)  values (82,'Hospitality',0)
insert into WebCategory (id,Name,HighPriority)  values (83,'Boating',0)
insert into WebCategory (id,Name,HighPriority)  values (86,'NSW Election',0)
insert into WebCategory (id,Name,HighPriority)  values (111,'Music',0)
insert into WebCategory (id,Name,HighPriority)  values (92,'Professional Associations',0)
insert into WebCategory (id,Name,HighPriority)  values (94,'Engineering',0)
insert into WebCategory (id,Name,HighPriority)  values (97,'Niche Product Marketing',0)
insert into WebCategory (id,Name,HighPriority)  values (99,'On Line Retail',0)
insert into WebCategory (id,Name,HighPriority)  values (100,'Fishing',0)
insert into WebCategory (id,Name,HighPriority)  values (103,'Australia Day',0)
insert into WebCategory (id,Name,HighPriority)  values (107,'CeBIT',0)
insert into WebCategory (id,Name,HighPriority)  values (109,'Immigration',0)
insert into WebCategory (id,Name,HighPriority)  values (110,'Animal Welfare/Rights',0)
insert into WebCategory (id,Name,HighPriority)  values (88,'Childcare',0)
insert into WebCategory (id,Name,HighPriority)  values (93,'Human Resources',0)
insert into WebCategory (id,Name,HighPriority)  values (104,'Emergency Services',0)

set identity_insert WebCategory off
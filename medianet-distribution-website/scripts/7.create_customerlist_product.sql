-----0. Default

declare @recipient_product_id int
declare @customer_contact_product_id int
declare @csv_database_product_id int

set @recipient_product_id = 9000000
set @customer_contact_product_id = 9000001
set @csv_database_product_id = 9000002

-----1. Insert service Products

delete from product where id = @recipient_product_id;
delete from product where id = @customer_contact_product_id;
delete from product where id = @csv_database_product_id;

-----2. Insert service Products

set identity_insert product on

insert into product (
id,
name,shortdescription,fulldescription,admincomment,
producttemplateid,
showonhomepage,
metakeywords,metadescription,metatitle,
approvedRatingSum,allowcustomerreviews,notapprovedratingsum,approvedtotalreviews,notapprovedtotalreviews,deleted,published,
owner,
distributiontype,
isPackage,
Addresses,
createdonutc,updatedonutc
)
select 
@recipient_product_id,
'Single recipient','Single recipient','Single recipient','Single recipient',
1,
0,
'','','',
0,0,0,0,0,0,1,
'',
'',
0,
0,
getutcdate(),
getutcdate() 



insert into product (
id,
name,shortdescription,fulldescription,admincomment,
producttemplateid,
showonhomepage,
metakeywords,metadescription,metatitle,
approvedRatingSum,allowcustomerreviews,notapprovedratingsum,approvedtotalreviews,notapprovedtotalreviews,deleted,published,
owner,
distributiontype,
isPackage,
Addresses,
createdonutc,updatedonutc
)
select 
@customer_contact_product_id,
'Customer Contact','Customer Contact','Customer Contact','Customer Contact',
1,
0,
'','','',
0,0,0,0,0,0,1,
'',
'',
0,
0,
getutcdate(),
getutcdate() 

insert into product (
id,
name,shortdescription,fulldescription,admincomment,
producttemplateid,
showonhomepage,
metakeywords,metadescription,metatitle,
approvedRatingSum,allowcustomerreviews,notapprovedratingsum,approvedtotalreviews,notapprovedtotalreviews,deleted,published,
owner,
distributiontype,
isPackage,
Addresses,
createdonutc,updatedonutc
)
select 
@csv_database_product_id,
'Mail merge contacts database','Mail merge contacts database','Mail merge contacts database','Mail merge contacts database',
1,
0,
'','','',
0,0,0,0,0,0,1,
'',
'',
0,
0,
getutcdate(),
getutcdate() 
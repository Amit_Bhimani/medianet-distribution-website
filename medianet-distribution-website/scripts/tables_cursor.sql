declare @table_name varchar(255)
DECLARE cursi CURSOR for select name from sysobjects where xtype = 'U';
open cursi;

fetch next from cursi into @table_name;
WHILE @@FETCH_STATUS = 0 
BEGIN

	--print @table_name
	exec ('select count(*) as ' + @table_name + ' from ' + @table_name + ' having count(*) > 0')

	fetch next from cursi into @table_name
END

close cursi
deallocate cursi
-----0. Alter ShoppingCartItem
Alter TABLE ShoppingCartItem add  [Included] [bit] NULL

-----1. Default

declare @web_posting int

set @web_posting = 9000003

-----2. Insert service Products

delete from product where id = @web_posting;

-----3. Insert service Products

set identity_insert product on

insert into product (
id,
name,shortdescription,fulldescription,admincomment,
producttemplateid,
showonhomepage,
metakeywords,metadescription,metatitle,
approvedRatingSum,allowcustomerreviews,notapprovedratingsum,approvedtotalreviews,notapprovedtotalreviews,deleted,published,
owner,
distributiontype,
isPackage,
Addresses,
createdonutc,updatedonutc
)
select 
@web_posting,
'Web posting','Web posting','Web posting','Web posting',
1,
0,
'','','',
0,0,0,0,0,0,1,
'',
'',
0,
0,
getutcdate(),
getutcdate() 

/****** Object:  UserDefinedFunction [dbo].[get_package_address_count]    Script Date: 08/22/2012 14:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[get_package_address_count]
(
	@packageid int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @count int

	select @count = sum(addresses) from mn_service inner join mn_package_service_xref on mn_service.id = mn_package_service_xref.serviceid
	where
	packageid =  @packageid

	-- Return the result of the function
	RETURN @count

END


Alter TABLE [dbo].[ProductVariant] alter column [Sku] [nvarchar](400) NULL
Alter TABLE [dbo].[ProductVariant] alter column [ManufacturerPartNumber] [nvarchar](400) NULL
Alter TABLE [dbo].[ProductVariant] alter column [Gtin] [nvarchar](400) NULL
Alter TABLE [dbo].[ProductVariant] alter column [IsGiftCard] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [GiftCardTypeId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [RequireOtherProducts] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [RequiredProductVariantIds] [nvarchar](1000) NULL
Alter TABLE [dbo].[ProductVariant] alter column [AutomaticallyAddRequiredProductVariants] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [IsDownload] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [DownloadId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [UnlimitedDownloads] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [MaxNumberOfDownloads] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [DownloadExpirationDays] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [DownloadActivationTypeId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [HasSampleDownload] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [SampleDownloadId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [HasUserAgreement] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [UserAgreementText] [nvarchar](max) NULL
Alter TABLE [dbo].[ProductVariant] alter column [IsRecurring] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [RecurringCycleLength] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [RecurringCyclePeriodId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [RecurringTotalCycles] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [IsShipEnabled] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [IsFreeShipping] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [AdditionalShippingCharge] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [IsTaxExempt] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [TaxCategoryId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [ManageInventoryMethodId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [StockQuantity] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [DisplayStockAvailability] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [DisplayStockQuantity] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [MinStockQuantity] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [LowStockActivityId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [NotifyAdminForQuantityBelow] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [BackorderModeId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [AllowBackInStockSubscriptions] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [OrderMinimumQuantity] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [OrderMaximumQuantity] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [AllowedQuantities] [nvarchar](1000) NULL
Alter TABLE [dbo].[ProductVariant] alter column [DisableBuyButton] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [DisableWishlistButton] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [AvailableForPreOrder] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [CallForPrice] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [Price] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [OldPrice] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [ProductCost] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [SpecialPrice] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [SpecialPriceStartDateTimeUtc] [datetime] NULL
Alter TABLE [dbo].[ProductVariant] alter column [SpecialPriceEndDateTimeUtc] [datetime] NULL
Alter TABLE [dbo].[ProductVariant] alter column [CustomerEntersPrice] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [MinimumCustomerEnteredPrice] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [MaximumCustomerEnteredPrice] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [HasTierPrices] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [HasDiscountsApplied] [bit] NULL
Alter TABLE [dbo].[ProductVariant] alter column [Weight] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [Length] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [Width] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [Height] [decimal](18, 4) NULL
Alter TABLE [dbo].[ProductVariant] alter column [PictureId] [int] NULL
Alter TABLE [dbo].[ProductVariant] alter column [AvailableStartDateTimeUtc] [datetime] NULL
Alter TABLE [dbo].[ProductVariant] alter column [AvailableEndDateTimeUtc] [datetime] NULL
Alter TABLE [dbo].[ProductVariant] add  [ServiceId] [int] NULL 

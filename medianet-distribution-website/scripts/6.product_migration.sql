----0. defaults

declare @package_identity int
set @package_identity = 10000000

----1. Delete tables
delete from Product_Category_mapping
delete from product_manufacturer_mapping
delete from product

-----2. Insert service Products

set identity_insert product on

insert into product (
id,
name,shortdescription,fulldescription,admincomment,
producttemplateid,
showonhomepage,
metakeywords,metadescription,metatitle,
approvedRatingSum,allowcustomerreviews,notapprovedratingsum,approvedtotalreviews,notapprovedtotalreviews,deleted,published,
owner,
distributiontype,
isPackage,
Addresses,
createdonutc,updatedonutc
)
select 
id,
selectionDescription,selectionDescription,selectionDescription,introduction,
1,
0,
'','','',
0,0,0,0,0,0,
case Hidden 
	when 'T' then 0
	else 1
end,
owner,
distributiontype,
0,
isnull(Addresses,0),
getutcdate(),
getutcdate() 

from mn_service where category is not null and SystemType='M' order by id

-----3. Insert package products

insert into product (
id,name,shortdescription,fulldescription,admincomment,
producttemplateid,
showonhomepage,
metakeywords,metadescription,metatitle,
approvedRatingSum,allowcustomerreviews,notapprovedratingsum,approvedtotalreviews,notapprovedtotalreviews,deleted,
published,
owner,
distributiontype,
isPackage,
Addresses,
createdonutc,updatedonutc
)
select 
id+@package_identity,selectionDescription,selectionDescription,selectionDescription,introduction,
1,
0,
'','','',
0,0,0,0,0,0,
case hidden
	when 'T' then 0
	else 1
end,
1,
'',
1,
[dbo].[get_package_address_count](id),
getutcdate(),
getutcdate() 

from mn_package 
where SystemType='M' order by id
	
set identity_insert product off

-----4. Create category mapping for sevices and packages

insert into Product_Category_mapping (ProductId,CategoryId,isFeaturedProduct,displayOrder)
select id,category,0,SequenceNumber from mn_service where category is not null
	
insert into Product_Category_mapping (ProductId,CategoryId,isFeaturedProduct,displayOrder)
select id+@package_identity,category1,0,SequenceNumber from mn_package where category1 is not null

insert into Product_Category_mapping (ProductId,CategoryId,isFeaturedProduct,displayOrder)
select id+@package_identity,category2,0,SequenceNumber from mn_package where category2 is not null

-----5. Create manufacturer mapping for services and packages

insert into Product_Manufacturer_mapping (ProductId,ManufacturerId,isFeaturedProduct,displayOrder)
select id,1,0,SequenceNumber from mn_service where category is not null order by id

insert into Product_Manufacturer_mapping (ProductId,ManufacturerId,isFeaturedProduct,displayOrder)
select id+@package_identity,2,0,SequenceNumber from mn_package order by id
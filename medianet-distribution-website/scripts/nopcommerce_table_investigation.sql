----------------------------------------------------------------------------------------------  Parametric tables
Country --keep
Setting --keep
Currency --keep
StateProvince --delete and generate for australia - done
Language --keep 
LocaleStringResource --keep
MeasureDimension --keep
MeasureWeight --keep
MessageTemplate --keep ?
ShippingMethod --delete and create one for MediaNet , perhaps own delivery method ?

---------------------------------------------------------------------------------------------- Related to products

Category --delete and import - done
CategoryTemplate --delete and create one - done
Picture --delete and create a new picture 
Product --delete and import , deleted
ProductTemplate --delete, deleted
ProductAttribute --delete and create new attributes ?, deleted
Product_Category_Mapping --delete and generate, deleted
Product_Manufacturer_Mapping --delete,deleted
Product_Picture_Mapping --delete,deleted
ProductReview --delete, deleted
Product_SpecificationAttribute_Mapping --delete, deleted
Product_ProductTag_Mapping --delete, deleted
ProductVariant_ProductAttribute_Mapping --delete, deleted
ProductVariantAttributeValue --delete, deleted
ProductVariant --delete, deleted
RelatedProduct --delete , deleted
ProductTag --delete -- done, deleted
Discount --delete,deleted
TaxCategory --delete, deleted
TierPrice --delete , deleted
SpecificationAttribute --delete ,deleted 
SpecificationAttributeOption --delete , deleted
GenericAttribute --delete ,deleted
Manufacturer --delete -- done
ManufacturerTemplate --delete -- done

---------------------------------------------------------------------------------------------- Related to customers
Customer --keep
CustomerRole --keep
Customer_CustomerRole_Mapping --keep
CustomerAddresses --delete -- done
EmailAccount --delete -- done
Address --delete -- done	

---------------------------------------------------------------------------------------------- CMS
delete from Poll --delete -- done
delete from PollAnswer --delete --done
delete from BlogPost --delete --done
delete from Topic --keep ? --done
delete from Forums_Forum --delete --done
delete from Forums_Group --delete --done
delete from Forums_Post --delete  --done
delete from News --delete --done

---------------------------------------------------------------------------------------------- INTERNAL
ScheduleTask --delete some ** INVESTIGATE **
Log --delete
PermissionRecord --keep
PermissionRecord_Role_Mapping -- keep
ActivityLogType --keep
Download --delete
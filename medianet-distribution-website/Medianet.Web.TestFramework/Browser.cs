﻿using System;
using System.Configuration;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace Medianet.Web.TestFramework
{
    public class Browser
    {
        public enum BrowserType
        {
            Firefox = 0,
            Explorer = 1,
            Chrome = 2
        }

        private static readonly Browser _instance = new Browser();

        /// <summary>
        /// Defines the interface to control the browser.
        /// </summary>
        private IWebDriver _webSearchDriver = null;

        /// <summary>
        /// This is a private constructor, meaning no outsiders have access.
        /// </summary>
        private Browser()
        {
            //_webSearchDriver = new FirefoxDriver();
            //WebSearchDriver = new InternetExplorerDriver();
            //WebSearchDriver = new ChromeDriver();
            //WebSearchDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
        }

        /// <summary>
        /// Access SiteStructure.Instance to get the singleton object.
        /// Then call methods on that instance.
        /// </summary>
        public static Browser Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Gets the title of the current browser window.
        /// </summary>
        public string Title
        {
            get { return _webSearchDriver.Title; }
        }

        /// <summary>
        /// Gets the interface to search for elements in the browser.
        /// </summary>
        public ISearchContext SearchDriver
        {
            get { return _webSearchDriver; }
        }

        /// <summary>
        /// Gets the driver.
        /// </summary>
        public IWebDriver Driver
        {
            get
            {
                return _webSearchDriver;
            }
        }

        /// <summary>
        /// Gets the Javascript executor.
        /// </summary>
        public IJavaScriptExecutor JavaScriptExecutor
        {
            get
            {
                return _webSearchDriver as IJavaScriptExecutor;
            }
        }

        /// <summary>
        /// Navigates the browser to <paramref name="url"/> parameter passed in.
        /// </summary>
        /// <param name="url">
        /// The url.
        /// </param>
        public void GoTo(string url)
        {
            _webSearchDriver.Url = url;
            WaitForPageToLoad();
        }

        /// <summary>
        /// Closes the current window in the browser and the browser if it's the last window in the browser.
        /// </summary>
        public void Close()
        {
            _webSearchDriver.Close();
            _webSearchDriver = null;
        }

        /// <summary>
        /// Opens a new browser window.
        /// </summary>
        public void Open()
        {
            var browserType = ConfigurationManager.AppSettings["Browser"];

            if (string.IsNullOrWhiteSpace(browserType))
                browserType = "FirefoxDriver";

            if (_webSearchDriver != null)
                _webSearchDriver.Close();

            switch (browserType)
            {
                case "InternetExplorerDriver":
                    _webSearchDriver = new InternetExplorerDriver();
                    break;
                case "ChromeDriver":
                    _webSearchDriver = new ChromeDriver();
                    break;
                default:
                    _webSearchDriver = new FirefoxDriver();
                    break;
            }
        }

        private static void WaitForPageToLoad()
        {
            var wait = new WebDriverWait(Browser.Instance.Driver, TimeSpan.FromSeconds(10));
            //wait.Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
            wait.Until(d =>
            {
                var state = ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState");
                if (state.Equals("complete"))
                    return true;
                else
                    return false;
            });
        }
    }
}

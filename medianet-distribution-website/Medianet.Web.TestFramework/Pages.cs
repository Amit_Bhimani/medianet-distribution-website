﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Configuration;

namespace Medianet.Web.TestFramework
{

    /// <summary>
    /// The pages.
    /// </summary>
    public class Pages
    {
        /// <summary>
        /// The base url of the curation web front end.
        /// </summary>
        public static string BaseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["BaseUrl"];
            }
        }
        /// <summary>
        /// The base title of all pages.
        /// </summary>
        public const string BasePageTitle = "Medianet Distribution";
    }
}
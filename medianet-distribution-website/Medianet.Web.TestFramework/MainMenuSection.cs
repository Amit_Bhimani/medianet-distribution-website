﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Medianet.Web.TestFramework
{
    /// <summary>
    /// The main menu on every page.
    /// </summary>
    public class MainMenuSection
    {
        public static void Logout()
        {
            var container = Browser.Instance.SearchDriver.FindElements(By.ClassName("user-details"));

            if (container != null && container.Count > 1)
            {
                //logout link is in the second container with the class name "user-details"
                var link = container[1].FindElement(By.TagName("a"));

                if (link != null)
                    link.Click();
            }
        }

        public static bool IsLoggedIn()
        {
            var container = Browser.Instance.SearchDriver.FindElements(By.ClassName("user-details"));

            return (container != null && container.Count > 0);
        }
    }
}

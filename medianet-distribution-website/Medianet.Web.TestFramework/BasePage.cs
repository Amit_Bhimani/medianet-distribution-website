﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Medianet.Web.TestFramework
{
    public abstract class BasePage
    {
        protected static bool IsAt(string pageTitle)
        {
            var wait = new WebDriverWait(Browser.Instance.Driver, TimeSpan.FromSeconds(10));

            try
            {
                wait.Until(d =>
                {
                    return (Browser.Instance.Title.Equals(pageTitle, System.StringComparison.CurrentCultureIgnoreCase));
                });

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected static void SetCheckbox(string id, bool check)
        {
            var checkbox = Browser.Instance.SearchDriver.FindElement(By.Id(id));
            var attrib = checkbox.GetAttribute("checked");

            if (!string.IsNullOrWhiteSpace(attrib) && 
                (attrib.Equals("checked", StringComparison.CurrentCultureIgnoreCase) || 
                attrib.Equals("true", StringComparison.CurrentCultureIgnoreCase)))
            {
                if (!check) checkbox.Click();
            }
            else
            {
                if (check) checkbox.Click();
            }
        }

        protected static bool IsTokenInputDisabled(string tokenInputName)
        {
            var tokenTextbox = Browser.Instance.SearchDriver.FindElement(By.Id("token-input-" + tokenInputName));
            return (!tokenTextbox.Enabled);
        }

        protected static void SetTokenInput(string tokenInputName, string selectText)
        {
            var tokenInputFullName = "token-input-" + tokenInputName;
            var tokenInputDropdownName = tokenInputFullName + "-token-input-dropdown-mediapeople";

            // Find the Token Input text box
            WaitForElementById(tokenInputFullName);
            var tokenTextbox = Browser.Instance.SearchDriver.FindElement(By.Id(tokenInputFullName));
            tokenTextbox.SendKeys(selectText);

            // Locate the div containing the drop down unordered list once it's ul tag has been created
            WaitForElementByCssSelector(string.Format("#{0} ul", tokenInputDropdownName));
            var tokenInputDropdownDiv = Browser.Instance.SearchDriver.FindElement(By.Id(tokenInputDropdownName));

            // fetch an array of all list items in the drop down menu
            var listItems = tokenInputDropdownDiv.FindElements(By.TagName("li"));

            foreach (var item in listItems)
            {
                // find the item we are looking for, click it, and exit the loop.
                if (item.Text == selectText)
                {
                    item.Click();
                    break;
                }
            }
        }

        protected static void SetDropdown(string id, string value)
        {
            var dropdown = new SelectElement(Browser.Instance.SearchDriver.FindElement(By.Id(id)));
            dropdown.SelectByValue(value);
        }

        protected static void ClickButton(string id)
        {
            var button = Browser.Instance.SearchDriver.FindElement(By.Id(id));

            button.Click();
        }

        protected static void WaitForElementById(string elementId)
        {
            var wait = new WebDriverWait(Browser.Instance.Driver, TimeSpan.FromSeconds(30));
            wait.Until(d =>
            {
                var elements = Browser.Instance.SearchDriver.FindElements(By.Id(elementId));
                if (elements.Count > 0)
                    return elements[0];
                else
                    return null;
            });
        }

        protected static void WaitForWaitingIconToDisappear(string id)
        {
            var wait = new WebDriverWait(Browser.Instance.Driver, TimeSpan.FromSeconds(30));
            wait.Until(d =>
            {
                var element = Browser.Instance.SearchDriver.FindElement(By.Id(id));
                var display = element.GetCssValue("display");

                if (display.Equals("none", StringComparison.CurrentCultureIgnoreCase))
                    return element; // It's not visible so return something to exit the wait.
                else
                    return null; // It's still visible so return null to keep waiting.
            });
        }

        protected static void WaitForElementByCssSelector(string css)
        {
            var wait = new WebDriverWait(Browser.Instance.Driver, TimeSpan.FromSeconds(30));
            wait.Until(d =>
            {
                var elements = Browser.Instance.SearchDriver.FindElements(By.CssSelector(css));
                if (elements.Count > 0)
                    return elements[0];
                else
                    return null;
            });
        }

        protected static IWebElement GetModalError(string id, string className)
        {
            var modalMessagebox = Browser.Instance.SearchDriver.FindElements(By.Id(id));

            if (modalMessagebox[0] != null && modalMessagebox.Count > 0 && modalMessagebox[0].GetAttribute("class") == className)
                return modalMessagebox[0];
            else
                return null;
        }

        protected static string GetModalErrorHeading(string id, string className, string tagName)
        {
            var modalError = GetModalError(id, className);

            if (modalError != null)
            {
                var modalMessagebox = Browser.Instance.SearchDriver.FindElements(By.TagName(tagName));

                if (modalMessagebox[0] != null && modalMessagebox.Count > 0)
                    return modalMessagebox[0].Text;
            }

            return string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Medianet.Web.TestFramework
{
    /// <summary>
    /// The login page.
    /// </summary>
    public class LoginPage : BasePage
    {
        private static readonly string PageUrl = "/account/logon";
        private const string PageTitle = Pages.BasePageTitle + " - Login";

        // The ID's for the logon page controls
        private const string EmailAddressTextId = "EmailAddress";
        private const string PasswordTextId = "Password";
        private const string LoginButtonId = "logon";

        // default username/password constants
        public const string default_emailAddress = "ahatter@aap.com.au";
        public const string default_password = "bulldogs2004";

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public LoginPage()
        {
        }

        /// <summary>
        /// Navigates to this page in the Browser.
        /// </summary>
        public static void GoTo()
        {
            Browser.Instance.GoTo(Pages.BaseUrl + PageUrl);
        }

        /// <summary>
        /// Validate that this page is currently being displayed.
        /// </summary>
        /// <returns></returns>
        public static bool IsAt()
        {
            return IsAt(PageTitle);
        }

        /// <summary>
        /// Executes the login request.
        /// </summary>
        public static void Login(string emailaddress, string password)
        {
            var emailAddressTextbox = Browser.Instance.SearchDriver.FindElement(By.Id(EmailAddressTextId));
            var passwordTextbox = Browser.Instance.SearchDriver.FindElement(By.Id(PasswordTextId));
            var loginButton = Browser.Instance.SearchDriver.FindElement(By.Id(LoginButtonId));

            emailAddressTextbox.SendKeys(emailaddress);
            passwordTextbox.SendKeys(password);
            loginButton.Click();
        }

        public static bool IsShowingUsernameEmptyValidationError()
        {
            var errors = Browser.Instance.SearchDriver.FindElements(By.ClassName("field-validation-error"));
            foreach (var e in errors)
            {
                if (e.Text.IndexOf("The Email Address field is required", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    return true;
            }

            return false;
        }

        public static bool IsShowingPasswordEmptyValidationError()
        {
            var errors = Browser.Instance.SearchDriver.FindElements(By.ClassName("field-validation-error"));
            foreach (var e in errors)
            {
                if (e.Text.IndexOf("The Password field is required", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    return true;
            }

            return false;
        }

        public static bool IsShowingInvalidLoginDetailsError()
        {
            var error = Browser.Instance.SearchDriver.FindElements(By.ClassName("error-message"));

            if (error != null && error.Count > 0)
                return (error[0].Text.IndexOf("Invalid login details", StringComparison.CurrentCultureIgnoreCase) >= 0);
            else
                return false;

        }
    }
}

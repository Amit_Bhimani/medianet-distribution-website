﻿using System;
using NUnit.Framework;
using Medianet.Web.TestFramework;
using Medianet.Web.Tests;

namespace Medianet.Web.Tests.UI_Tests
{
    [TestFixture]
    public class LoginTests
    {
        #region Setup

        [TestFixtureSetUp]
        public static void SetupAssembly() {
            Common.SetupAssembly();
        }

        [TestFixtureTearDown]
        public static void CleanupAssembly() {
            Common.CleanupAssembly();
        }

        [SetUp]
        public void SetupTest()
        {
            if (MainMenuSection.IsLoggedIn())
                MainMenuSection.Logout();
        }

        #endregion

        [Test]
        public void LoginWithValidCredintials_GoesToMediaSearchQuickPage()
        {
            LoginPage.GoTo();
            LoginPage.Login(LoginPage.default_emailAddress, LoginPage.default_password);
            Assert.IsTrue(MainMenuSection.IsLoggedIn());
        }

        [Test]
        public void LoginWithNoPassword_ShowsError()
        {
            LoginPage.GoTo();
            LoginPage.Login(LoginPage.default_emailAddress, string.Empty);
            Assert.IsTrue(LoginPage.IsShowingPasswordEmptyValidationError());
        }

        [Test]
        public void LoginWithNoEmail_ShowsError()
        {
            LoginPage.GoTo();
            LoginPage.Login(string.Empty, LoginPage.default_password);
            Assert.IsTrue(LoginPage.IsShowingUsernameEmptyValidationError());
        }

        [Test]
        public void Logout_ShowsLoginPage()
        {
            LoginPage.GoTo();
            LoginPage.Login(LoginPage.default_emailAddress, LoginPage.default_password);
            Assert.IsTrue(MainMenuSection.IsLoggedIn());
            MainMenuSection.Logout();
            Assert.IsTrue(!MainMenuSection.IsLoggedIn());
        }
    }
}

﻿using System;
using Medianet.Web.Models;
using NUnit.Framework;
using System.ComponentModel.DataAnnotations;

namespace Medianet.Web.Tests.Model
{
    [TestFixture]
    [Category("ModelTests")]
    public class WireModelTests
    {
        
        [Test]
        public void FormatTweet_ConvertsHandleToUrl()
        {
            var model = new WireModel();
            var result = "testing <a href=\"http://twitter.com/handle\" target=\"_blank\">@handle</a>";

            model.Tweet = "testing @handle";

            Assert.AreEqual(result, model.FormatedTweet());
        }

        [Test]
        public void FormatTweet_DoesntConvertEmailToUrl() {
            var model = new WireModel();
            var result = "testing email@hotmail.com";

            model.Tweet = result;

            Assert.AreEqual(result, model.FormatedTweet());
        }

        [Test]
        public void FormatTweet_ConvertsHashtagToUrl() {
            var model = new WireModel();
            var result = "testing <a href=\"http://twitter.com/search?q=%23hashtag\" target=\"_blank\">#hashtag</a>";

            model.Tweet = "testing #hashtag";

            Assert.AreEqual(result, model.FormatedTweet());
        }
    }
}


﻿using System;
using NUnit.Framework;
using Medianet.Web.Helpers;
using Medianet.Web.Models;

namespace Medianet.Web.Tests
{
    [TestFixture]
    [Category("ControllerTests")]
    public class SearchControllerTests
    {
        [Test]
        public void Test_If_Test_Works()
        {
            Assert.AreEqual(1,1);
        }

        [Test]
        public void HubSpot_Register_Works()
        {
            var hubHelper = new HubSpotHelper();
            var model = new RegisterDetailModel
            {
                Email = "durquhart@mediality.com.au",
                FirstName = "David",
                Lastname = "Urquhart",
                Company = "AAP",
                Industry = "Media",
                Position = "CEO",
                Phone = "0293228000",
                Address = "3 Rider Bvd",
                Address2 = "Rhodes Waterside",
                Suburb = "Rhodes",
                PostCode = "2000",
                State = "NSW",
                Country = "Australia"
            };

            hubHelper.PostRegistration(model, "https://www.medianet.com.au/Account/RegisterDetails");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medianet.Web.TestFramework;

namespace Medianet.Web.Tests
{
    public static class Common
    {
        public static void SetupAssembly()
        {
            Browser.Instance.Open();
        }

        public static void CleanupAssembly()
        {
            if (MainMenuSection.IsLoggedIn())
                MainMenuSection.Logout();

            Browser.Instance.Close();
        }
    }
}

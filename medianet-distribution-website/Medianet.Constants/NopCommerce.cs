﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.Constants
{
    public class NopCommerce
    {
        public const string Register_Key = "register";

        public const string RegisterFree_Key = "registerfree";

        public const string Account_Key = "account";

        public const string AutoLogin_Key = "autologin";

        public const string LastRelease_Key = "lastrelease";

        public const int Sms_TextLimit = 160;

        public const string EmailList_key = "email_list";
        
        public const string SmsList_key = "sms_list";

        public const string Audio_Key = "audio";

        public const string Wire_Key = "wire";

        public const string Free_Key = "free";

        public const string Sms_Key = "sms";
        
        public const string Video_Key = "video";

        public const string MultiMedia_Key = "multimedia";

        public const string Session_Key = "customer";

        public const string Report_Key = "report";

        public const string CartItem_Package = "PACKAGE";

        public const string CartItem_Service = "SERVICE";

        public const string CartItem_CustomerList = "CUSTOMERLIST";

        public const string CartItem_SingleRecipient = "RECEIPIENT";

        public const string CartItem_Mailmerge = "MAILMERGE";
        public const string CartItem_Attachment = "ATTACHMENTCHARGE";

        public const string CartItem_ANR = "AUDIONEWSRELEASECHARGE";

        public const string CartItem_SWC = "ADDITIONALWEBCATEGORYCHARGE";

        public const string CartItem_Priority = "PRIORITY";
        public const string CartDistType_Fax = "F";

        public const string CartDistType_Email = "E";

        public const string CartDistType_Internet = "I";

        public const string CartDistType_Wire = "W";

        public const string CartDistType_Sms = "S";

        public const string CartDistType_Twitter = "T";

        public const string CartDistType_AttachmentFee = "A";

        public const string CartDistType_AnrFee = "R";

        public const string CartDistType_AWCFee = "C";

        public const string CartDistType_PriorityFee = "H";

        public const int Recipient_Product_Id = 9000000;

        public const int CustomerList_Product_Id = 9000001;

        public const int MailMerge_Product_Id = 9000002;

        public const int NewsHubPosting_Product_Id = 2;

        public const int TwitterPosting_Product_Id = 1;

        public const int JournalistPosting_Product_Id = 3;

        public const int AudioNewsReleaseCharge_Product_Id = 9000006;

        public const int SecondaryWebCategoryCharge_Product_Id = 9000008;

        public const int AttachmentCharge_Product_Id = 9000007;

        public const int HighPriorityCharge_Product_Id = 9000009;

        //public const int AllStates_Product_Id = 100001;

        public const int GlobalDB_Product_Id = 19751;

        public const int Default_Product_Id = 10000002;

        public const int Default_TwitterCategoryId = 1;

        public const int Max_Shopping_Cart_Recommendations = 3;

        public const int Max_Have_You_Consider = 5;

        public const int Specials_Category = 21;

        public const int Online_Category = 104;
        
        public const int International_Distribution = 105;

        public const int AudioFiles_Page_Size = 5;

        public const int Report_Page_Size = 30;

        public const int Recent_Max_Items = 10;

        public const string Medianet_Sender = "aapmedianet@aapmedianet.com.au";

        public const string Medianet_Company_Sender = "Medianet Press Release";

        public const string Registration_Default_Country = "Australia";

        public const int Check_Session_TimeSlice = 5;

        public const string Application_Ticker = "ticker";

        public const string Application_Global_Stats = "stats";        

        public const int Application_Ticker_Length = 200;

        public static int[] Radio_Top_Categories = new int[3] { 68, 69, 71 };

        public const int Report_Pager_Size = 5;

        public static List<string> DEBTOR_NUMBERS_INTERNAL = new List<string>()
        {
            "10",
            "20",
            "32"
        };
    }
}

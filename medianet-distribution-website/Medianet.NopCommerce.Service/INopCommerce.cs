﻿using System;
using System.Text;
using System.ServiceModel;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Nop.Data;
using Nop.Services.Catalog;
using Nop.Services.Customers;

using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.QuickRecommendations;
using Medianet.NopCommerce.Service.DTO;

namespace Medianet.NopCommerce.Service
{
    [ServiceContract]
    interface INopCommerce
    {
        [OperationContract]
        IList<ProductDTO> GetRadioProducts(int CategoryId);

        [OperationContract]
        IList<ProductDTO> GetRecentProducts(int CustomerId);

        [OperationContract]
        ProductDTO GetProduct(int ProductId);

        [OperationContract]
        IList<Category> GetTopCategoryTree();

        [OperationContract]
        IList<Category> GetTopRadioCategories();

        [OperationContract]
        IList<Category> GetChildRadioCategories(int ParentCategoryId);

        [OperationContract]
        IList<Category> GetChildCategories(int ParentCategoryId);

        [OperationContract]
        IList<Category> GetCatalogTree();

        [OperationContract]
        IList<CategoryDTO> GetWireCategories();

        [OperationContract]
        IList<Product> GetWireProductsByCategory(int CategortyId);

        [OperationContract]
        IList<Product> GetSpecialProducts();

        [OperationContract]
        IList<Product> GetOnlineProducts();

        [OperationContract]
        IList<Product> GetInternationalDistribution();

        [OperationContract]
        IList<WebCategory> GetWebCategoryList();

        [OperationContract]
        int RegisterOrSignUpinNopCommerce(Customer customer);

        [OperationContract]
        IList<ProductDTO> SearchProducts(string criteria, int cant);

        [OperationContract]
        IList<RecommendationDTO> GetRecommendations(int WebCategoryId,int CustomerId);

        [OperationContract]
        IList<RelatedProductDTO> GetShoppingCartRecommendations(int CustomerId);

        [OperationContract]
        bool AddDatabaseListToCart(int CustomerId, string Name, int addresses, string distributionType);

        [OperationContract]
        bool AddCustomerListToCart(int CustomerId, int ServiceId, string Name, int addresses, string distributionType);

        [OperationContract]
        bool AddReceipientToCart(int CustomerId, string address, string receipientType);

        [OperationContract]
        bool AddProductToCart(int CustomerId, int ProductId, string productType);

        [OperationContract]
        string UpdateCartProducts(int CustomerId, IList<ShoppingCartItemDTO> products);

        [OperationContract]
        IList<ShoppingCartItemDTO> GetCart(int CustomerId);

        [OperationContract]
        string RemoveProductFromCart(int ShoppingCartId);

        [OperationContract]
        bool CleanCartAndCreateOrders(int CustomerId,decimal Total);

        [OperationContract]
        bool CleanCart(int CustomerId);

        [OperationContract]
        bool UpdateCartItemAttributes(int CustomerId, ShoppingCartItemDTO cartItem);

    }    
}
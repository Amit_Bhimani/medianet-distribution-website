﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Nop.Data;
using Nop.Core.Events;
using System.Configuration;
using Nop.Core.Infrastructure;

namespace Medianet.NopCommerce.Service
{
    public abstract class BusinessBase
    {
        #region Private

        protected internal EventPublisher _eventPublisher;
        protected internal Nop.Data.NopObjectContext _nopDbContext;

        #endregion

        # region Constructor

        public BusinessBase()
        {
            this._eventPublisher = new EventPublisher(new SubscriptionService());
            this._nopDbContext = new Nop.Data.NopObjectContext(ConfigurationManager.ConnectionStrings["nopcommerceshop"].ToString());
            this._nopDbContext.Configuration.ProxyCreationEnabled = false;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.NopCommerce.Service.DTO
{
    public class CategoryDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public int CategoryID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long Count { get; set; }
    }
}

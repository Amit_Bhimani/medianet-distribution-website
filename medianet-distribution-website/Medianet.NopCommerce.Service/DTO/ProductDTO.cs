﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.NopCommerce.Service.DTO
{
    public class ProductDTO
    {
        /// <summary>
        /// NopCommerce Variant Product Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// NopCommerce Product Id 
        /// </summary>
        public int ProductId { get; set; }
        
        /// <summary>
        /// Medianet Service Id
        /// </summary>
        public int ServiceId { get; set; }

        /// <summary>
        /// is a package ?
        /// </summary>
        public bool IsPackage { get; set; }

        public bool HasEmailServices { get; set; }
        public bool HasNewsHubServices { get; set; }
        public bool HasJournalistsServices { get; set; }
        public bool HasWireServices { get; set; }
        public bool HasTwitterServices { get; set; }
        public bool HasAapOneServices { get; set; }

        public string ChildProductIds { get; set; }

        /// <summary>
        /// Is customer list ?
        /// </summary>
        public bool IsCustomerList {get;set;}

        /// <summary>
        /// is an extra recipient
        /// </summary>
        public bool IsExtraRecipient { get; set; }

        /// <summary>
        /// Medianet Distribution Type
        /// </summary>
        public string DistributionType { get; set; }

        /// <summary>
        /// Product Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Addresses { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsPublished { get; set; }
    }
}

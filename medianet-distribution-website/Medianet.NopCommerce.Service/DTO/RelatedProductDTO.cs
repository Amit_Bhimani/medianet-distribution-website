﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.NopCommerce.Service.DTO
{
    public class RelatedProductDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Cant { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DistributionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsPackage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsCustomerList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Addresses {get;set;}
    }
}
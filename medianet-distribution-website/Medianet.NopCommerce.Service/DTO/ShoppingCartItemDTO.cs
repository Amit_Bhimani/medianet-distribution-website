﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.NopCommerce.Service.DTO
{
    public class ShoppingCartItemDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ProductVariantId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? ServiceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AttributesXml { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsPackage { get; set; }

        public bool HasEmailServices { get; set; }
        public bool HasNewsHubServices { get; set; }
        public bool HasJournalistsServices { get; set; }
        public bool HasWireServices { get; set; }
        public bool HasTwitterServices { get; set; }
        public bool HasAapOneServices { get; set; }

        public string ChildProductIds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Included { get; set; }
    }
}

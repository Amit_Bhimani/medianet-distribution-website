﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Medianet.NopCommerce.Service.DTO
{
    public class CustomerDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int ShipppingAddressId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int BillingAddressId { get; set; }
    }
}

﻿using System;
using System.Text;
using System.Linq;
using System.ServiceModel;
using System.Data.Linq.SqlClient;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Infrastructure;

using Nop.Data;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Security;
using Nop.Services.Localization;
using Nop.Core.Domain.QuickRecommendations;
using Medianet.NopCommerce.Service.DTO;
using Medianet.Constants;


namespace Medianet.NopCommerce.Service
{
    public class NopCommerceService : BusinessBase ,INopCommerce
    {
        private const string CartAttributesFormatString = "{0},{1},{2}";

        public NopCommerceService()
            : base()
        { 
        }

        #region Catalog

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        public IList<ProductDTO> GetRadioProducts(int CategoryId)
        {
            #region Repositories

            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(this._nopDbContext);
            EfRepository<ProductCategory> productCategoryRepository = new EfRepository<ProductCategory>(this._nopDbContext);

            #endregion

            var query = from p in productRepository.Table
                        join pv in productVariantRepository.Table on p.Id equals pv.ProductId
                        join pc in productCategoryRepository.Table on p.Id equals pc.ProductId
                        join c in categoryRepository.Table on pc.CategoryId equals c.Id
                        where c.Id == CategoryId
                        select new ProductDTO()
                            {
                                Id = p.Id,
                                ProductId = p.Id,
                                ServiceId = pv.ServiceId.Value,
                                DistributionType = p.DistributionType,
                                Name = p.Name,
                                FullDescription = p.FullDescription,
                                ShortDescription = p.ShortDescription,
                                Addresses = p.Addresses,
                                IsPackage = p.isPackage, 
                                HasEmailServices = p.HasEmailServices,
                                HasNewsHubServices = p.HasNewsHubServices,
                                HasJournalistsServices = p.HasJournalistsServices,
                                HasWireServices = p.HasWireServices,
                                HasTwitterServices = p.HasTwitterServices,
                                HasAapOneServices = p.HasAapOneServices,
                                ChildProductIds = p.ChildProductIds,
                                IsCustomerList = false,
                                IsExtraRecipient = false,
                                IsPublished = p.Published
                            };

            return query.ToList();            
        }

        /// <summary>
        /// Get radio top categories
        /// </summary>
        /// <returns></returns>
        public IList<Category> GetTopRadioCategories()
        {
            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);

            var query = from c in categoryRepository.Table
                        where Medianet.Constants.NopCommerce.Radio_Top_Categories.Contains(c.Id) && c.Published
                        orderby c.DisplayOrder
                        select c;

            return query.ToList();
        }

        /// <summary>
        /// Get radio child category collection
        /// </summary>
        /// <param name="ParentCategoryId"></param>
        /// <returns></returns>
        public IList<Category> GetChildRadioCategories(int ParentCategoryId)
        {
            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);

            var query = from c in categoryRepository.Table
                        where c.ParentCategoryId == ParentCategoryId
                              && c.Published
                        orderby c.DisplayOrder
                        select c;

            return query.ToList();        
        }

        /// <summary>
        /// Get top parent categories
        /// </summary>
        /// <returns></returns>
        public IList<Category> GetTopCategoryTree()
        {
            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);

            var query = from c in categoryRepository.Table
                        where c.ParentCategoryId == 0
                              && c.Published == true
                              && !c.Deleted
                              && c.Id != Constants.NopCommerce.Online_Category
                        orderby c.DisplayOrder
                        select c;

            return query.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ParentCategoryId"></param>
        /// <returns></returns>
        public IList<Category> GetChildCategories(int ParentCategoryId)
        {
            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);

            var query = from c in categoryRepository.Table
                        where c.ParentCategoryId == ParentCategoryId && c.Published && !c.Deleted 
                        orderby c.DisplayOrder
                        select c;

            return query.ToList();
        }

        /// <summary>
        /// Get all the categories for Wire distribution type
        /// </summary>
        /// <returns></returns>
        public IList<CategoryDTO> GetWireCategories()
        {
            #region Repositories

            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ProductCategory> mappingRepository = new EfRepository<ProductCategory>(this._nopDbContext);

            #endregion

            var query = from pcm in mappingRepository.Table
                        join c in categoryRepository.Table on pcm.CategoryId equals c.Id
                        join p in productRepository.Table on pcm.ProductId equals p.Id
                        where p.DistributionType == "W"
                              && p.Published && !c.Deleted
                              && c.Id != Constants.NopCommerce.Online_Category
                        orderby c.DisplayOrder
                        group c by new {c.Id, c.Name}
                        into categoryGroup
                        select new CategoryDTO
                            {
                                CategoryID = categoryGroup.Key.Id,
                                Name = categoryGroup.Key.Name,
                                Count = categoryGroup.Count()
                            };

            return query.ToList();
        }

        /// <summary>
        /// AG:TODO:Deprecated ?
        /// </summary>
        /// <returns></returns>
        public IList<Category> GetCatalogTree()
        {            
            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);

            var query = from pc in categoryRepository.Table
                        join c in categoryRepository.Table on pc.Id equals c.ParentCategoryId
                        where !c.Deleted && c.Published && !c.Deleted
                        orderby pc.DisplayOrder, c.DisplayOrder
                        select c;

            return query.ToList();
        }

        /// <summary>
        /// Gets a list of products that belong to the special category
        /// </summary>
        /// <returns></returns>
        public IList<Product> GetSpecialProducts()
        {
            #region Repositories

            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ProductCategory> mappingRepository = new EfRepository<ProductCategory>(this._nopDbContext);

            #endregion

            var query = from p in productRepository.Table
                        join pcm in mappingRepository.Table on p.Id equals pcm.ProductId
                        where pcm.CategoryId == Constants.NopCommerce.Specials_Category
                        && p.DistributionType != Constants.NopCommerce.CartDistType_Fax 
                        && p.Addresses > 0
                        && p.Published == true
                        && p.Deleted == false
                        orderby p.Name
                        select p;

            return query.ToList();
        }

        /// <summary>
        /// Returns a list of Online products
        /// </summary>
        /// <returns></returns>
        public IList<Product> GetOnlineProducts()
        {
            #region Repositories

            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ProductCategory> mappingRepository = new EfRepository<ProductCategory>(this._nopDbContext);

            #endregion

            var query = from p in productRepository.Table
                        join pcm in mappingRepository.Table on p.Id equals pcm.ProductId
                        where pcm.CategoryId == Constants.NopCommerce.Online_Category
                        && p.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        //&& p.Addresses > 0
                        && p.Published == true
                        && p.Deleted == false
                        orderby pcm.DisplayOrder
                        select p;

            return query.ToList();
        }


        /// <summary>
        /// Returns a list of International Distribution 
        /// </summary>
        /// <returns></returns>
        public IList<Product> GetInternationalDistribution()
        {
            #region Repositories

            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ProductCategory> mappingRepository = new EfRepository<ProductCategory>(this._nopDbContext);

            #endregion

            var query = from p in productRepository.Table
                        join pcm in mappingRepository.Table on p.Id equals pcm.ProductId
                        where pcm.CategoryId == Constants.NopCommerce.International_Distribution
                        && p.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        //&& p.Addresses > 0
                        && p.Published == true
                        && p.Deleted == false
                        orderby pcm.DisplayOrder
                        select p;

            return query.ToList();
        }


        /// <summary>
        /// Returns a list of products given a category.
        /// </summary>
        /// <param name="CategortyId"></param>
        /// <returns></returns>
        public IList<Product> GetWireProductsByCategory(int CategortyId)
        {
            #region Repositories

            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(this._nopDbContext);
            EfRepository<ProductCategory> mappingRepository = new EfRepository<ProductCategory>(this._nopDbContext);

            #endregion

            var query = from p in productRepository.Table
                        join pcm in mappingRepository.Table on p.Id equals pcm.ProductId
                        join pv in productVariantRepository.Table on p.Id equals pv.ProductId
                        where pcm.CategoryId == CategortyId
                        && p.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        && p.Published == true
                        && p.Deleted == false
                        && p.Addresses > 0
                        orderby pv.DisplayOrder
                        select p;

            return query.ToList();
        }

        /// <summary>
        /// Searchs product
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="cant"></param>
        /// <returns></returns>
        public IList<ProductDTO> SearchProducts(string criteria, int cant)
        {
            #region Repositories

            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<Category> categoryRepository = new EfRepository<Category>(this._nopDbContext);
            EfRepository<ProductCategory> mappingRepository = new EfRepository<ProductCategory>(this._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(this._nopDbContext);

            #endregion

            var query = from product in productRepository.Table
                        join pv in productVariantRepository.Table on product.Id equals pv.ProductId
                        join pcm in mappingRepository.Table on product.Id equals pcm.ProductId
                        join c in categoryRepository.Table on pcm.CategoryId equals c.Id
                        where product.Name.Contains(criteria)
                        && product.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        && !product.Deleted
                        && product.Published
                        && (product.Addresses > 0)
                        && !c.Deleted
                        orderby product.Name,pv.DisplayOrder
                        select new ProductDTO()
                            {
                                Id = product.Id,
                                ProductId = pv.ProductId,
                                ServiceId = pv.ServiceId.Value,
                                DistributionType = product.DistributionType,
                                Name = product.Name,
                                FullDescription = product.FullDescription,
                                ShortDescription = product.ShortDescription,
                                Addresses = product.Addresses,
                                IsPackage = product.isPackage, 
                                HasEmailServices = product.HasEmailServices,
                                HasNewsHubServices = product.HasNewsHubServices,
                                HasJournalistsServices = product.HasJournalistsServices,
                                HasWireServices = product.HasWireServices,
                                HasTwitterServices = product.HasTwitterServices,
                                HasAapOneServices = product.HasAapOneServices,
                                ChildProductIds = product.ChildProductIds,
                                IsCustomerList = (pv.ProductId == Constants.NopCommerce.CustomerList_Product_Id),
                                IsExtraRecipient = (pv.ProductId == Constants.NopCommerce.Recipient_Product_Id),
                                IsPublished = product.Published
                            }; 

            return query.Take(cant).ToList();
        }

        #endregion

        #region Authentication

        /// <summary>
        /// Registers or sign up a customer in nopCommerce 
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public int RegisterOrSignUpinNopCommerce(Customer customer)
        {
            #region Repostories

            ICustomerService customerService = NopCommerceHelper.GetCustomerService(this);
            EfRepository<Address> addressRepository = new EfRepository<Address>(base._nopDbContext);

            #endregion

            Customer fetch_customer = customerService.GetCustomerByUsername(customer.Username);

            if ( fetch_customer == null )
            {
                Address address = new Address();
                    address.FirstName = customer.SystemName;
                    address.CreatedOnUtc = DateTime.UtcNow;
                    addressRepository.Insert(address);

                customer.BillingAddress = address;
                customer.ShippingAddress = address;
                customerService.InsertCustomer(customer);
            }
            else
                return fetch_customer.Id;

            return customer.Id;
        }

        #endregion

        #region Recommendations

        /// <summary>
        /// Returns the top 10 recent products bought by a given customer
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<ProductDTO> GetRecentProducts(int CustomerId)
        {
            #region Repositories

            EfRepository<Customer> customerRepository = new EfRepository<Customer>(base._nopDbContext);
            EfRepository<Order> orderRepository = new EfRepository<Order>(base._nopDbContext);
            EfRepository<OrderProductVariant> orderProductVariantRepository = new EfRepository<OrderProductVariant>(base._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);

            #endregion

            var query = from o in orderRepository.Table
                        join opv in orderProductVariantRepository.Table on o.Id equals opv.OrderId
                        join pv in productVariantRepository.Table on opv.ProductVariantId equals pv.Id
                        join p in productRepository.Table on pv.ProductId equals p.Id
                        where o.CustomerId == CustomerId
                        && p.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        && pv.ServiceId != null
                        && p.Published == true
                        && p.Deleted == false
                        orderby pv.DisplayOrder
                        select new ProductDTO()
                            {
                                Id = p.Id,
                                ProductId = pv.ProductId,
                                ServiceId = pv.ServiceId.Value,
                                DistributionType = p.DistributionType,
                                Name = p.Name,
                                FullDescription = p.FullDescription,
                                ShortDescription = p.ShortDescription,
                                Addresses = p.Addresses,
                                IsPackage = p.isPackage, 
                                HasEmailServices = p.HasEmailServices,
                                HasNewsHubServices = p.HasNewsHubServices,
                                HasJournalistsServices = p.HasJournalistsServices,
                                HasWireServices = p.HasWireServices,
                                HasTwitterServices = p.HasTwitterServices,
                                HasAapOneServices = p.HasAapOneServices,
                                ChildProductIds = p.ChildProductIds,
                                IsCustomerList = (pv.ProductId == Medianet.Constants.NopCommerce.CustomerList_Product_Id),
                                IsExtraRecipient = (pv.ProductId == Medianet.Constants.NopCommerce.Recipient_Product_Id),
                                IsPublished = p.Published
                            };

            return query.Distinct().Take(Medianet.Constants.NopCommerce.Recent_Max_Items).ToList();
        }

        /// <summary>
        /// Returns a list of web categories
        /// </summary>
        /// <returns></returns>
        public IList<WebCategory> GetWebCategoryList()
        {
            #region Repositories

            EfRepository<WebCategory> webCategoryRepository = new EfRepository<WebCategory>(base._nopDbContext);

            #endregion

            var query = from webcategory in webCategoryRepository.Table
                        orderby webcategory.Name
                        select webcategory;

            return query.ToList();
        }
        
        /// <summary>
        /// Gets a list of recommendations based on the medianet plugin
        /// </summary>
        /// <param name="WebCategoryID"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<RecommendationDTO> GetRecommendations(int WebCategoryID,int CustomerId)
        {
            #region Repositories

            EfRepository<Recommendation> recommendationRepository = new EfRepository<Recommendation>(base._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<ShoppingCartItem> cartRepository = new EfRepository<ShoppingCartItem>(this._nopDbContext);

            #endregion            

            IList<ShoppingCartItemDTO> cart = this.GetCart(CustomerId);
            IList<RecommendationDTO> result = new List<RecommendationDTO>();

            var query = from recommendation in recommendationRepository.Table
                        join p in productRepository.Table on recommendation.RecommendedProductId equals p.Id
                        where recommendation.WebCategoryId == WebCategoryID
                        && p.Published == true
                        && p.Deleted == false
                        select new RecommendationDTO() 
                        { 
                            ProductId = recommendation.RecommendedProductId, 
                            DistributionType = p.DistributionType, 
                            Name = p.Name ,
                            IsPackage = p.isPackage,
                            IsCustomerList = (recommendation.RecommendedProductId == Medianet.Constants.NopCommerce.CustomerList_Product_Id),
                            Addresses = p.Addresses,
                            ShortDescription = p.ShortDescription
                        };            

            foreach (RecommendationDTO recommendation in query.ToList())
            { 
                if ( cart.Count(c => c.ProductId == recommendation.ProductId) == 0 )
                {
                    result.Add(recommendation);
                }
            }

            return result.Take(Medianet.Constants.NopCommerce.Max_Shopping_Cart_Recommendations).ToList();
        }

        /// <summary>
        /// Uses nop commerce linked products to get a list of recommendations based on the items on the cart
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<RelatedProductDTO> GetShoppingCartRecommendations(int CustomerId)
        {
            #region Repositories

            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(this._nopDbContext);
            EfRepository<RelatedProduct> relatedProductRepository = new EfRepository<RelatedProduct>(this._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);

            #endregion

            IList<ShoppingCartItemDTO> cart = this.GetCart(CustomerId);
            IList<RelatedProductDTO> result = new List<RelatedProductDTO>();

            var query = from relatedproduct in relatedProductRepository.Table
                        join p in productRepository.Table on relatedproduct.ProductId2 equals p.Id
                        join pv in productVariantRepository.Table on relatedproduct.ProductId1 equals pv.ProductId
                        join sp in shoppingCartRepository.Table on pv.Id equals sp.ProductVariantId
                        orderby p.Name
                        where sp.CustomerId == CustomerId
                        && p.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        && p.Published == true
                        && p.Deleted == false
                        group p by new { p.Name, relatedproduct.ProductId2, p.DistributionType, p.Addresses, p.isPackage, p.HasFaxServices, p.ShortDescription } into relatedProductGroup
                        select new RelatedProductDTO
                        {
                            Name = relatedProductGroup.Key.Name,
                            ProductId = relatedProductGroup.Key.ProductId2,
                            DistributionType = relatedProductGroup.Key.DistributionType,
                            Cant = relatedProductGroup.Count(),
                            Addresses = relatedProductGroup.Key.Addresses,
                            IsPackage = relatedProductGroup.Key.isPackage,
                            IsCustomerList = ( relatedProductGroup.Key.ProductId2 == Constants.NopCommerce.CustomerList_Product_Id ),
                            ShortDescription = relatedProductGroup.Key.ShortDescription
                        };

            foreach (RelatedProductDTO relatedProduct in query.ToList())
            {
                if (cart.Where(c => c.ProductVariantId == relatedProduct.ProductId).Count() == 0)
                {
                    result.Add(relatedProduct);
                }
            }

            return result.Take(Medianet.Constants.NopCommerce.Max_Have_You_Consider).ToList();
        }

        #endregion

        #region Shopping Cart

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="Name"></param>
        /// <param name="addresses"></param>
        /// <param name="distributionType"></param>
        /// <returns></returns>
        public bool AddDatabaseListToCart(int CustomerId, string Name, int addresses, string distributionType)
        {
            #region Repositories

            IShoppingCartService shoppingCartService = NopCommerceHelper.GetShoppingCartService(this);
            ICustomerService customerService = NopCommerceHelper.GetCustomerService(this);
            IProductService productService = NopCommerceHelper.GetProductService(this);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);

            #endregion

            // Checks if the address already exists in the shopping cart
            var query = from sc in shoppingCartRepository.Table
                        join pv in productVariantRepository.Table on sc.ProductVariantId equals pv.Id
                        where sc.CustomerId == CustomerId && pv.Name == Name
                        select sc;

            if (query.Count() > 0)
            {
                return false;
            }

            Customer customer = customerService.GetCustomerById(CustomerId);
            Product receipientProduct = productRepository.GetById(Medianet.Constants.NopCommerce.MailMerge_Product_Id);
            ProductVariant productVariant = new ProductVariant();

            #region Fill in product variant

            productVariant.Price = 0;
            productVariant.Name = Name;
            productVariant.ProductId = Medianet.Constants.NopCommerce.MailMerge_Product_Id;
            productVariant.AdditionalShippingCharge = 0;
            productVariant.AllowBackInStockSubscriptions = false;
            productVariant.AllowedQuantities = "";
            productVariant.AutomaticallyAddRequiredProductVariants = false;
            productVariant.AvailableEndDateTimeUtc = DateTime.UtcNow;
            productVariant.AvailableForPreOrder = false;
            productVariant.AvailableStartDateTimeUtc = DateTime.UtcNow;
            productVariant.CallForPrice = false;
            productVariant.CreatedOnUtc = DateTime.UtcNow;
            productVariant.CustomerEntersPrice = false;
            productVariant.Deleted = false;
            productVariant.DisableBuyButton = true;
            productVariant.DisableWishlistButton = true;
            productVariant.DisplayOrder = 0;
            productVariant.DisplayStockAvailability = false;
            productVariant.DisplayStockQuantity = false;
            productVariant.UpdatedOnUtc = DateTime.UtcNow;

            #endregion

            try
            {
                productVariantRepository.Insert(productVariant);

                // Custom product attribute
                string attributes = string.Format(CartAttributesFormatString, Constants.NopCommerce.CartItem_Mailmerge, distributionType, addresses);

                shoppingCartService.AddToCart(customer, productVariant, attributes, 0);
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        /// <summary>
        /// Adds a customer list service to the cart
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ServiceId"></param>
        /// <param name="Name"></param>
        /// <param name="addresses"></param>
        /// <param name="distributionType"></param>
        /// <returns></returns>
        public bool AddCustomerListToCart(int CustomerId, int ServiceId, string Name, int addresses, string distributionType)
        {
            #region Repositories

            IShoppingCartService shoppingCartService = NopCommerceHelper.GetShoppingCartService(this);
            ICustomerService customerService = NopCommerceHelper.GetCustomerService(this);
            IProductService productService = NopCommerceHelper.GetProductService(this);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);

            #endregion
            
            var query = from sc in shoppingCartRepository.Table
                        join pv in productVariantRepository.Table on sc.ProductVariantId equals pv.Id
                        where sc.CustomerId == CustomerId && pv.ServiceId == ServiceId
                        select sc;

            if (query.Count() > 0)
            {
                return false;
            }

            Customer customer = customerService.GetCustomerById(CustomerId);
            Product receipientProduct = productRepository.GetById(Medianet.Constants.NopCommerce.CustomerList_Product_Id);
            ProductVariant productVariant = new ProductVariant();

            #region Fill in product variant

            productVariant.ServiceId = ServiceId;
            productVariant.Price = 0;
            productVariant.Name = Name;
            productVariant.ProductId = Medianet.Constants.NopCommerce.CustomerList_Product_Id;
            productVariant.AdditionalShippingCharge = 0;
            productVariant.AllowBackInStockSubscriptions = false;
            productVariant.AllowedQuantities = "";
            productVariant.AutomaticallyAddRequiredProductVariants = false;
            productVariant.AvailableEndDateTimeUtc = DateTime.UtcNow;
            productVariant.AvailableForPreOrder = false;
            productVariant.AvailableStartDateTimeUtc = DateTime.UtcNow;
            productVariant.CallForPrice = false;
            productVariant.CreatedOnUtc = DateTime.UtcNow;
            productVariant.CustomerEntersPrice = false;
            productVariant.Deleted = false;
            productVariant.DisableBuyButton = true;
            productVariant.DisableWishlistButton = true;
            productVariant.DisplayOrder = 0;
            productVariant.DisplayStockAvailability = false;
            productVariant.DisplayStockQuantity = false;
            productVariant.UpdatedOnUtc = DateTime.UtcNow;

            #endregion

            try
            {
                productVariantRepository.Insert(productVariant);

                // Custom product attribute
                string attributes = string.Format(CartAttributesFormatString, Constants.NopCommerce.CartItem_CustomerList, distributionType, addresses);

                shoppingCartService.AddToCart(customer, productVariant, attributes, 0);
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        /// <summary>
        /// Adds a recipient to the cart
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="address"></param>
        /// <param name="receipientType"></param>
        /// <returns></returns>
        public bool AddReceipientToCart(int CustomerId, string address, string receipientType)
        {
            #region Repositories

            IShoppingCartService shoppingCartService = NopCommerceHelper.GetShoppingCartService(this);
            ICustomerService customerService = NopCommerceHelper.GetCustomerService(this);
            IProductService productService = NopCommerceHelper.GetProductService(this);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);

            #endregion

            // Checks if the address already exists in the shopping cart
            var query = from sc in shoppingCartRepository.Table
                        join pv in productVariantRepository.Table on sc.ProductVariantId equals pv.Id
                        where sc.CustomerId == CustomerId && pv.Name == address
                        select sc;

            if ( query.Count() > 0 )
            {
                return false;
            }

            Customer customer = customerService.GetCustomerById(CustomerId);
            Product receipientProduct = productRepository.GetById(Medianet.Constants.NopCommerce.Recipient_Product_Id);
            ProductVariant productVariant = new ProductVariant();

            #region Fill in product variant

            productVariant.Price = 0;
            productVariant.Name = address;
            productVariant.ProductId = Medianet.Constants.NopCommerce.Recipient_Product_Id;
            productVariant.AdditionalShippingCharge = 0;
            productVariant.AllowBackInStockSubscriptions = false;
            productVariant.AllowedQuantities = "";
            productVariant.AutomaticallyAddRequiredProductVariants = false;
            productVariant.AvailableEndDateTimeUtc = DateTime.UtcNow;
            productVariant.AvailableForPreOrder = false;
            productVariant.AvailableStartDateTimeUtc = DateTime.UtcNow;
            productVariant.CallForPrice = false;
            productVariant.CreatedOnUtc = DateTime.UtcNow;
            productVariant.CustomerEntersPrice = false;
            productVariant.Deleted = false;
            productVariant.DisableBuyButton = true;
            productVariant.DisableWishlistButton = true;
            productVariant.DisplayOrder = 0;
            productVariant.DisplayStockAvailability = false;
            productVariant.DisplayStockQuantity = false;
            productVariant.UpdatedOnUtc = DateTime.UtcNow;
               
            #endregion

            try
            {
                productVariantRepository.Insert(productVariant);

                // Custom product attribute
                string attributes = string.Format(CartAttributesFormatString, Constants.NopCommerce.CartItem_SingleRecipient, receipientType, 0);
               
                shoppingCartService.AddToCart(customer, productVariant, attributes, 0);

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool UpdateCartItemAttributes(int CustomerId, ShoppingCartItemDTO cartItem)
        {
            #region Repositories
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);
            #endregion


            ShoppingCartItem shoppingCartItemToUpdate = shoppingCartRepository.GetById(cartItem.Id);
            if (shoppingCartItemToUpdate != null)
            {
                shoppingCartItemToUpdate.AttributesXml = cartItem.AttributesXml;
                shoppingCartRepository.Update(shoppingCartItemToUpdate);
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Returns a single product
        /// </summary>
        /// <param name="ProductId"></param>
        /// <returns></returns>
        public ProductDTO GetProduct(int ProductId)
        {
            #region Repositories

            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);

            #endregion

            var query = from p in productRepository.Table
                        join pv in productVariantRepository.Table on p.Id equals pv.ProductId
                        where p.Id == ProductId
                        select new ProductDTO()
                            {
                                Id = p.Id, 
                                ProductId = p.Id, 
                                ServiceId = pv.ServiceId ?? -1, 
                                DistributionType = p.DistributionType,
                                Name = p.Name,
                                FullDescription = p.FullDescription,
                                ShortDescription = p.ShortDescription,
                                Addresses = p.Addresses,
                                IsPackage = p.isPackage, 
                                HasEmailServices = p.HasEmailServices,
                                HasNewsHubServices = p.HasNewsHubServices,
                                HasJournalistsServices = p.HasJournalistsServices,
                                HasWireServices = p.HasWireServices,
                                HasTwitterServices = p.HasTwitterServices,
                                HasAapOneServices = p.HasAapOneServices,
                                ChildProductIds = p.ChildProductIds,
                                IsCustomerList = (pv.ProductId == Medianet.Constants.NopCommerce.CustomerList_Product_Id),
                                IsExtraRecipient = (pv.ProductId == Medianet.Constants.NopCommerce.Recipient_Product_Id),
                                IsPublished = p.Published
                            };

            if ( query.Count() == 0 )
            {
                return null;
            }
            else
                return query.First();
        }

        /// <summary>
        /// Adds a product to cart [service,package or customer list] ! not single recipient.
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ProductId"></param>
        /// <param name="productType"></param>
        public bool AddProductToCart(int CustomerId, int ProductId, string productType)
        {
            #region Repositories

            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);
            IShoppingCartService shoppingCartService = NopCommerceHelper.GetShoppingCartService(this);
            ICustomerService customerService = NopCommerceHelper.GetCustomerService(this);
            IProductService productService = NopCommerceHelper.GetProductService(this);

            #endregion

            var query = from sc in shoppingCartRepository.Table
                        join pv in productVariantRepository.Table on sc.ProductVariantId equals pv.Id
                        where sc.CustomerId == CustomerId && pv.ProductId == ProductId
                        select sc;

            // Check product already exists
            if ( query.Count() > 0)
            {
                return false;
            }

            Customer customer = customerService.GetCustomerById(CustomerId);
            ProductVariant productVariant = (from pv in productVariantRepository.Table where pv.ProductId == ProductId select pv).FirstOrDefault();
            Product product = productRepository.GetById(ProductId);
            int addresses = product.Addresses;
            
            // Custom product attribute
            string attributes = string.Format(CartAttributesFormatString, productType, product.DistributionType, addresses);

            shoppingCartService.AddToCart(customer, productVariant, attributes, 0);

            return true;
        }

        /// <summary>
        /// This is meant to be called once the pricing services on medianet as been called and it returns a pricing for each product in the cart
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="products"></param>
        /// <returns></returns>
        public string UpdateCartProducts(int CustomerId, IList<ShoppingCartItemDTO> products)
        {
            #region Repositories

            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);
            IShoppingCartService shoppingCartService = NopCommerceHelper.GetShoppingCartService(this);
            ICustomerService customerService = NopCommerceHelper.GetCustomerService(this);
            IProductService productService = NopCommerceHelper.GetProductService(this);

            #endregion

            Customer customer = customerService.GetCustomerById(CustomerId);

            foreach (ShoppingCartItemDTO shoppingCartItem in products)
            {
                ShoppingCartItem shoppingCartItemToUpdate = shoppingCartRepository.GetById(shoppingCartItem.Id);

                shoppingCartItemToUpdate.CustomerEnteredPrice = shoppingCartItem.Price;
                shoppingCartRepository.Update(shoppingCartItemToUpdate);                                        
            }
            
            return "";
        }
        
        /// <summary>
        /// Removes a shopping cart item.
        /// </summary>
        /// <param name="ShoppingCartId"></param>
        /// <returns></returns>
        public string RemoveProductFromCart(int ShoppingCartId)
        {
            #region Repositories

            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);
            EfRepository<ProductVariant> productVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);

            #endregion

            try
            {
                ShoppingCartItem item = shoppingCartRepository.GetById(ShoppingCartId);
                ProductVariant productVariant = productVariantRepository.GetById(item.ProductVariantId);

                // We are creating a product variant for every receipient on the cart, when the shopping cart item is removed, so is the product variant
                if ( 
                        (productVariant.ProductId == Medianet.Constants.NopCommerce.Recipient_Product_Id)    ||
                        (productVariant.ProductId == Medianet.Constants.NopCommerce.CustomerList_Product_Id)
                )
                {
                    productVariantRepository.Delete(productVariant); // -> this will also cascade deleted it from shopping cart item.
                }
                else
                    shoppingCartRepository.Delete(item);
            }
            catch (Exception e)
            {
                //return e.Message;
                return e.StackTrace.ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Returns the shopping cart
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<ShoppingCartItemDTO> GetCart(int CustomerId)
        {
            #region Repositories
            
            IShoppingCartService shoppingCartService = NopCommerceHelper.GetShoppingCartService(this);
            EfRepository<ShoppingCartItem> shoppingCartRepository = new EfRepository<ShoppingCartItem>(base._nopDbContext);
            EfRepository<Customer> customerRepository = new EfRepository<Customer>(base._nopDbContext);
            EfRepository<ProductVariant> produtVariantRepository = new EfRepository<ProductVariant>(base._nopDbContext);
            EfRepository<Product> productRepository = new EfRepository<Product>(base._nopDbContext);

            #endregion

            var query = from i in shoppingCartRepository.Table
                        join customer in customerRepository.Table on i.CustomerId equals customer.Id
                        join productVariant in produtVariantRepository.Table on i.ProductVariantId equals productVariant.Id
                        join product in productRepository.Table on productVariant.ProductId equals product.Id
                        where i.CustomerId == CustomerId
                            && product.DistributionType != Constants.NopCommerce.CartDistType_Fax
                        select new ShoppingCartItemDTO()
                            {
                                Id = i.Id,
                                ProductId = product.Id,
                                ProductVariantId = productVariant.Id,
                                ServiceId = (productVariant.ServiceId.HasValue) ? productVariant.ServiceId.Value : 0,
                                Name = productVariant.Name, 
                                Price = i.CustomerEnteredPrice, 
                                AttributesXml = i.AttributesXml, 
                                IsPackage = product.isPackage, 
                                HasEmailServices = product.HasEmailServices,
                                HasNewsHubServices = product.HasNewsHubServices,
                                HasJournalistsServices = product.HasJournalistsServices,
                                HasWireServices = product.HasWireServices,
                                HasTwitterServices = product.HasTwitterServices,
                                HasAapOneServices = product.HasAapOneServices,
                                ChildProductIds = product.ChildProductIds,
                                Included = i.Included
                            };

            return query.ToList();
        }

        /// <summary>
        /// Cleans the shopping cart of a customer and transform it in an order.
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="Total"></param>
        /// <returns></returns>
        public bool CleanCartAndCreateOrders(int CustomerId,decimal Total)
        {
            #region Repositories

            EfRepository<Customer> customerRepository = new EfRepository<Customer>(base._nopDbContext);
            EfRepository<Address> addressRepository = new EfRepository<Address>(base._nopDbContext);
            EfRepository<Order> orderRepository = new EfRepository<Order>(base._nopDbContext);            
            EfRepository<OrderProductVariant> orderProductVariantRepository = new EfRepository<OrderProductVariant>(base._nopDbContext);            

            #endregion

            #region Customer

            var query = from c in customerRepository.Table
                        join sa in addressRepository.Table on c.ShippingAddress.Id equals sa.Id
                        join ba in addressRepository.Table on c.BillingAddress.Id equals ba.Id
                        where c.Id == CustomerId
                        select new CustomerDTO() { Id = c.Id, ShipppingAddressId = sa.Id, BillingAddressId = ba.Id };

            CustomerDTO customer = query.First();

            #endregion

            #region Create order

            // This will help to have a list of recent products bought by the customer           
            Order order = new Order();
                order.OrderGuid = new Guid();
                order.CustomerId = customer.Id;
                order.ShippingAddressId = customer.ShipppingAddressId;
                order.BillingAddressId = customer.BillingAddressId;
                order.OrderStatusId = 0;
                order.ShippingStatusId = 0;
                order.PaymentStatusId = 0;
                order.CustomerTaxDisplayTypeId = 0;
                order.OrderSubTotalDiscountExclTax = 0;
                order.OrderSubTotalDiscountInclTax = 0;
                order.OrderSubtotalExclTax = 0;
                order.OrderSubtotalInclTax = 0;
                order.OrderTax = 0;
                order.OrderShippingExclTax = 0;
                order.OrderShippingInclTax = 0;
                order.PaymentMethodAdditionalFeeExclTax = 0;
                order.PaymentMethodAdditionalFeeInclTax = 0;
                order.OrderTax = 0;
                order.OrderDiscount = 0;
                order.OrderTotal = Total;
                order.RefundedAmount = 0;
                order.RewardPointsWereAdded = false;
                order.CustomerLanguageId = 1;
                order.AllowStoringCreditCardNumber = false;
                order.Deleted = false;
                order.CreatedOnUtc = DateTime.UtcNow;                
            
            orderRepository.Insert(order);

            #endregion

            IList<ShoppingCartItemDTO> shoppingCart = this.GetCart(CustomerId);

            foreach (ShoppingCartItemDTO cartItem in shoppingCart)
            {
                #region Create order cart item

                OrderProductVariant orderProduct = new OrderProductVariant();
                    orderProduct.OrderProductVariantGuid = new Guid();
                    orderProduct.OrderId = order.Id;
                    orderProduct.ProductVariantId = cartItem.ProductVariantId;
                    orderProduct.Quantity = 1;
                    orderProduct.UnitPriceExclTax = cartItem.Price;
                    orderProduct.UnitPriceInclTax = cartItem.Price;
                    orderProduct.PriceExclTax = cartItem.Price;
                    orderProduct.PriceInclTax = cartItem.Price;
                    orderProduct.DiscountAmountExclTax = 0;
                    orderProduct.DiscountAmountInclTax = 0;
                    orderProduct.AttributeDescription = "";
                    orderProduct.AttributesXml = cartItem.AttributesXml;
                    orderProduct.DownloadCount = 0;
                    orderProduct.IsDownloadActivated = false;
                orderProductVariantRepository.Insert(orderProduct);

                #endregion

                this.RemoveProductFromCart(cartItem.Id);
            }

            return true;
        }

        /// <summary>
        /// Clean the shopping cart to start fresh.
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public bool CleanCart(int CustomerId)
        {
            IList<ShoppingCartItemDTO> shoppingCart = this.GetCart(CustomerId);

            foreach (ShoppingCartItemDTO cartItem in shoppingCart)
            {
                this.RemoveProductFromCart(cartItem.Id);
            }

            return true;
        }

        #endregion
    }
}
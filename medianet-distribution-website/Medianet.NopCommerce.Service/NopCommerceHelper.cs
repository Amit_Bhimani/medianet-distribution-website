﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Nop.Data;
using Nop.Services.Common;
using Nop.Services.Catalog;
using Nop.Services.Customers;

using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Common;
using Nop.Services.Directory;
using Nop.Core.Domain.Directory;
using Nop.Core.Plugins;
using Nop.Core.Infrastructure;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Security;
using Nop.Core;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Logging;
using Nop.Core.Domain.Localization;
using Nop.Services.Configuration;
using Nop.Core.Domain.Configuration;

namespace Medianet.NopCommerce.Service
{
    public class NopCommerceHelper
    {
        /// <summary>
        /// Gets a generic attribute service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static IGenericAttributeService GetGenericAttributeService(BusinessBase baseclass)
        {
            EfRepository<GenericAttribute> genericAttributeRepository = new EfRepository<GenericAttribute>(baseclass._nopDbContext);

            return new GenericAttributeService(null,genericAttributeRepository,baseclass._eventPublisher);
        }

        /// <summary>
        /// Gets a customer service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static ICustomerService GetCustomerService(BusinessBase baseclass)
        {
            EfRepository<Customer> customerRepository = new EfRepository<Customer>(baseclass._nopDbContext);
            EfRepository<CustomerRole> customerRoleRepository = new EfRepository<CustomerRole>(baseclass._nopDbContext);
            EfRepository<GenericAttribute> gaRepository = new EfRepository<GenericAttribute>(baseclass._nopDbContext);

            return new CustomerService(null, customerRepository, customerRoleRepository, gaRepository, GetGenericAttributeService(baseclass) , baseclass._eventPublisher);
        }

        /// <summary>
        /// Gets a category service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static ICategoryService GetCategoryService(BusinessBase baseclass)
        {            
            EfRepository<Nop.Core.Domain.Catalog.Category> categoryRepository = new EfRepository<Nop.Core.Domain.Catalog.Category>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.ProductCategory> productCategoryRepository = new EfRepository<Nop.Core.Domain.Catalog.ProductCategory>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.Product> productRepository = new EfRepository<Nop.Core.Domain.Catalog.Product>(baseclass._nopDbContext);
            
            return new CategoryService(null, categoryRepository, productCategoryRepository, productRepository, baseclass._eventPublisher);
        }

        /// <summary>
        /// Gets a product service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static IProductService GetProductService(BusinessBase baseclass)
        {
            #region Repositories

            EfRepository<Nop.Core.Domain.Catalog.Category> categoryRepository = new EfRepository<Nop.Core.Domain.Catalog.Category>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.ProductCategory> productCategoryRepository = new EfRepository<Nop.Core.Domain.Catalog.ProductCategory>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.Product> productRepository = new EfRepository<Nop.Core.Domain.Catalog.Product>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.ProductVariant> productVariantRepository = new EfRepository<Nop.Core.Domain.Catalog.ProductVariant>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.RelatedProduct> relatedProducttRepository = new EfRepository<Nop.Core.Domain.Catalog.RelatedProduct>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.CrossSellProduct> crossSellProductRepository = new EfRepository<Nop.Core.Domain.Catalog.CrossSellProduct>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.TierPrice> tierPriceRepository = new EfRepository<Nop.Core.Domain.Catalog.TierPrice>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.ProductPicture> productPictureRepository = new EfRepository<Nop.Core.Domain.Catalog.ProductPicture>(baseclass._nopDbContext);
            EfRepository<Nop.Core.Domain.Catalog.ProductSpecificationAttribute> productSpecificationAttributeRepository = new EfRepository<Nop.Core.Domain.Catalog.ProductSpecificationAttribute>(baseclass._nopDbContext);

            #endregion

            return new ProductService(null, productRepository, productVariantRepository, relatedProducttRepository, crossSellProductRepository, tierPriceRepository, productPictureRepository, null, productSpecificationAttributeRepository, null, null, null, null, null, baseclass._nopDbContext, null, null, baseclass._eventPublisher);         
        }

        /// <summary>
        /// Gets a currencty service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static ICurrencyService GetCurrencyService(BusinessBase baseclass)
        {
            EfRepository<Currency> currencyRepository = new EfRepository<Currency>(baseclass._nopDbContext);
            CurrencySettings currencySettings = new CurrencySettings();
            ITypeFinder typeFinder = new WebAppTypeFinder(new Nop.Core.Configuration.NopConfig());
            PluginFinder pluginFinder = new PluginFinder(typeFinder);

            return new CurrencyService(null, currencyRepository, NopCommerceHelper.GetCustomerService(baseclass), currencySettings, pluginFinder, baseclass._eventPublisher);
        }

        /// <summary>
        /// Get a product attribute service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static IProductAttributeService GetProductAttributeService(BusinessBase baseclass)
        {
            #region Repositories

            EfRepository<ProductAttribute> productAttribute = new EfRepository<ProductAttribute>(baseclass._nopDbContext);
            EfRepository<ProductVariantAttribute> productVariantAttribute = new EfRepository<ProductVariantAttribute>(baseclass._nopDbContext);
            EfRepository<ProductVariantAttributeCombination> productVariantAttributeCombination = new EfRepository<ProductVariantAttributeCombination>(baseclass._nopDbContext);
            EfRepository<ProductVariantAttributeValue> productVariantAttributeValue = new EfRepository<ProductVariantAttributeValue>(baseclass._nopDbContext);

            #endregion

            return new ProductAttributeService(null, productAttribute, productVariantAttribute, productVariantAttributeCombination, productVariantAttributeValue, baseclass._eventPublisher);
        }

        /// <summary>
        /// Get a checkout attributes service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static ICheckoutAttributeService GetCheckoutAttributeService(BusinessBase baseclass)
        { 
            #region Repositories

            EfRepository<CheckoutAttribute> checkoutAttribute = new EfRepository<CheckoutAttribute>(baseclass._nopDbContext);
            EfRepository<CheckoutAttributeValue> checkoutAttributeValue = new EfRepository<CheckoutAttributeValue>(baseclass._nopDbContext);

            #endregion

            return new CheckoutAttributeService(null, checkoutAttribute, checkoutAttributeValue, baseclass._eventPublisher);
        }

        /// <summary>
        /// Get a shopping cart service instance
        /// </summary>
        /// <param name="baseclass"></param>
        /// <returns></returns>
        public static IShoppingCartService GetShoppingCartService(BusinessBase baseclass)
        {
            #region Repositories

            EfRepository<ShoppingCartItem> sciRepository = new EfRepository<ShoppingCartItem>(baseclass._nopDbContext);
            EfRepository<PermissionRecord> permissionRepository = new EfRepository<PermissionRecord>(baseclass._nopDbContext);

            //IWorkContext workContext = EngineContext.Current.Resolve<IWorkContext>();
            ICurrencyService currencyService = NopCommerceHelper.GetCurrencyService(baseclass);
            IProductService productService = NopCommerceHelper.GetProductService(baseclass);
            //ILocalizationService localizationService = NopCommerceHelper.GetLocalizationService(baseclass);
            IProductAttributeParser productAttributeParser = new ProductAttributeParser(NopCommerceHelper.GetProductAttributeService(baseclass));
            ICheckoutAttributeService checkoutAttributeService = NopCommerceHelper.GetCheckoutAttributeService(baseclass);
            ICheckoutAttributeParser checkoutAttributeParser = new CheckoutAttributeParser(checkoutAttributeService);
            IPriceFormatter priceFormatter = new PriceFormatter(null, currencyService, null, new Nop.Core.Domain.Tax.TaxSettings());
            ICustomerService customerService = NopCommerceHelper.GetCustomerService(baseclass);
            ShoppingCartSettings shoppingCartSettings = new ShoppingCartSettings();
            IPermissionService permissionService = new PermissionService(permissionRepository, customerService, null, null);

            #endregion

            return new ShoppingCartService(sciRepository, null, currencyService, productService, null,
                                            productAttributeParser, checkoutAttributeService, checkoutAttributeParser,
                                            priceFormatter, customerService, shoppingCartSettings, baseclass._eventPublisher, permissionService);
        }
    }
}
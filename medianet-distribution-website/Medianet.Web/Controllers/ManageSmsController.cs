﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class ManageSmsController : ManageListController<SmsListModel>
    {
        public ManageSmsController()
            : base(NopCommerce.SmsList_key)
        {
        }
    }
}

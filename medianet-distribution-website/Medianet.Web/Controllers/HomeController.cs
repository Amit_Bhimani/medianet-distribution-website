﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using System.Net.Mail;
using log4net;
using Medianet.Web.MedianetReleaseService;
using SystemType = Medianet.Web.MedianetCustomerService.SystemType;
using System.Web.Configuration;

namespace Medianet.Web.Controllers
{
    public class HomeController : Controller
    {
        private List<ReleaseStatusType> _lastReportStatuses = new List<ReleaseStatusType> {
                                                ReleaseStatusType.Invoiced,
                                                ReleaseStatusType.Accounts,
                                                ReleaseStatusType.Resulted,
                                                ReleaseStatusType.Transmitted};

        public ActionResult Index()
        {
            HomeModel model = new HomeModel();

            model.session = (SessionModel)Session[NopCommerce.Session_Key];
            model.GetHomePageContent();
            model.GetTrainingCourses();
            model.GetAgendaItems();
            model.GetUsers();
            model.GetSalesRegion();
            model.GetTopPerformingLists();
            model.GetTopValuePackages();
            model.GetCustomerStatistics();

            return View("Index", model);
        }
        
        public ActionResult Terms()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];

            return View("Terms", model);
        }

        public ActionResult About()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];

            return View("About", model);
        }

        public ActionResult Contact()
        {
            ContactModel model = new ContactModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];

            return View("Contact", model);
        }

        [HttpGet]
        public PartialViewResult LastReport()
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            ReportModel model = new ReportModel();
            Release mostRecentReport;

            model.session = session;
            model.Prepare();

            mostRecentReport = model.Reports.Releases.FirstOrDefault(r => _lastReportStatuses.Contains(r.Status));

            return PartialView("_lastReport", mostRecentReport);
        }

        [HttpPost]
        public ActionResult Send(ContactModel sentForm)
        {
            if (ModelState.IsValid)
            {
                string SmtpHost = WebConfigurationManager.AppSettings["SmtpHost"].ToString();
                int SmtPort = int.Parse(WebConfigurationManager.AppSettings["SmtpPort"].ToString());
                string FromEmailAddress = WebConfigurationManager.AppSettings["SmtpMailFromAddress"].ToString();

                try
                {
                    using (var client = new SmtpClient(SmtpHost, SmtPort))
                    {
                        client.Send(FromEmailAddress, sentForm.eMail, String.Format("Contact requested from :{0} {1} - {2} - {3}", sentForm.Name, sentForm.LastName, sentForm.Company, sentForm.PhoneNumber), sentForm.Message);
                    };
                }
                catch (Exception ex)
                {
                    ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    log.Error(null, ex);
                }

                sentForm.session = (SessionModel)Session[NopCommerce.Session_Key];

                return View("contacted", sentForm);
            }
            else
                return View("contact", sentForm);
        }

        public ActionResult Database()
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            if (session.HasContactsWebAccess)
                return Redirect(String.Format("{0}?session={1}", WebConfigurationManager.AppSettings["ContactsSite"].ToString(), session.sessionKey));
            else
                return Redirect(WebConfigurationManager.AppSettings["MarketingPage"].ToString());
        }

        /// <summary>
        /// Page not found
        /// </summary>
        /// <returns></returns>
        public ActionResult PageNotFound()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];

            Response.StatusCode = 404;
            return View("404", model);
        }

        /// <summary>
        /// Page exception
        /// </summary>
        /// <returns></returns>
        public ActionResult Error()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];

            Response.StatusCode = 500;
            return View("500", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Editorial()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];
            model.GetEditorialDiary();

            return View("editorial", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Calendar()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];
            model.GetAgendaItems();

            return View("calendar", model);
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        public ActionResult Training()
        {
            HomeModel model = new HomeModel();
            model.session = (SessionModel)Session[NopCommerce.Session_Key];
            model.GetTrainingCourses();

            return View("training", model);
        }

        [HttpPost]
        public JsonResult HelpEmail(string Subject, string Message)
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            bool successStatus = false;
            string errorMessage = string.Empty;

            if (session != null)
            {
                string smtpHost = WebConfigurationManager.AppSettings["SmtpHost"].ToString();
                int smtPort = int.Parse(WebConfigurationManager.AppSettings["SmtpPort"].ToString());
                string fromEmailAddress = WebConfigurationManager.AppSettings["SmtpMailFromAddress"];

                HomeModel model = new HomeModel { session = (SessionModel)Session[NopCommerce.Session_Key] };
                model.GetSalesRegion();
                
                List<string> recipients = new List<string> { WebConfigurationManager.AppSettings["AccountManagerInclude"] };

                recipients.AddRange(model.AccountManagerEmail.Split(new char[] { ',', ' ', ';' }, StringSplitOptions.RemoveEmptyEntries));

                try
                {
                    using (var client = new SmtpClient(smtpHost, smtPort))
                    {
                        client.Send(
                            fromEmailAddress,
                            string.Join(",", recipients),
                            string.Format("Message for {0} - {1}", model.AccountManagerName, Subject),
                                string.Format("User {0} from {1} Account# {2} sent you a message:\r\n\r\n{3}\r\n\r\nContact them on {4}", (session.FirstName + " " + session.LastName).Trim(),
                                                  session.Name, session.debtorNumber, Message, session.userEmailAddress));

                        successStatus = true;
                    }
                }
                catch (Exception ex)
                {
                    ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    log.Error(string.Format("Error sending Help email for User with Id {0} and email adress {1}", session.MedianetUserId, fromEmailAddress), ex);
                }
            }
            else
                errorMessage = "Session not found. Please login again";

            return Json(new { success = successStatus, error = errorMessage }, "text/html");
        }
    }
}

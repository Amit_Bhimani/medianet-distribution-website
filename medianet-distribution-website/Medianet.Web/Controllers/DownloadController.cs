﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.ServiceModel;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetFileService;

namespace Medianet.Web.Controllers
{
    public class DownloadController : Controller
    {
        public ActionResult GenericDownload(int? w, int? j, int? s, string k)
        {
            if (w.HasValue)
                return WebsiteLogo(w.Value);
            else
                return Document(j.Value, s.Value, k);
        }

        public ActionResult WebsiteLogo(int w)
        {
            string fileExtension;
            string fileName;
            int fileId;
            int fileSize;
            int itemCount;
            string contentType;
            Stream fileData;

            try {
                using (var ncc = new ServiceProxyHelper<FileServiceClient, IFileService>()) {
                    contentType = ncc.Proxy.GetWebsiteLogo(w, out fileExtension, out fileId, out fileSize, out itemCount, out fileName, out fileData);
                }

                return File(fileData, contentType, fileName);
            }
            catch (FaultException<FileNotFoundFault>) {
                return new HttpNotFoundResult();
            }
            catch (FaultException<AccessDeniedFault>) {
                return new HttpUnauthorizedResult();
            }
        }

        public ActionResult Document(int j, int s, string k)
        {
            string fileExtension;
            string fileName;
            int fileId;
            int fileSize;
            int itemCount;
            string contentType;
            Stream fileData;

            try {
                using (var ncc = new ServiceProxyHelper<FileServiceClient, IFileService>()) {
                    contentType = ncc.Proxy.GetDocumentContent(null, j, k, s, null, out fileExtension, out fileId, out fileSize, out itemCount, out fileName, out fileData);
                }

                return File(fileData, contentType, fileName);
            }
            catch (FaultException<FileNotFoundFault>) {
                return new HttpNotFoundResult();
            }
            catch (FaultException<AccessDeniedFault>) {
                return new HttpUnauthorizedResult();
            }
        }
    }
}

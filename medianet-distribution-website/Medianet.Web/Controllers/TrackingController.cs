﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Configuration;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetGeneralService;

namespace Medianet.Web.Controllers
{
    public class TrackingController : Controller
    {
        public ActionResult EmailOpened(int j, int t, int? d)
        {
            try {
                using (var gsc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>()) {
                    gsc.Proxy.RecordEmailOpened(j, t, d);
                }
            }
            catch (Exception) { }

            string filePath = Server.MapPath(ConfigurationManager.AppSettings["EmailOpenedImage"].ToString());
            string contentType = MimeExtensionHelper.GetMimeType(filePath);

            return File(filePath, contentType, Path.GetFileName(filePath));
        }
    }
}

﻿using Medianet.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Helpers;
using Medianet.Constants;
using System.ServiceModel;
using Medianet.Web.MedianetChargeBeeService;
using System.Web.Security;
using Medianet.Web.NopCommerceServices;
using System.Net;
using System.Collections.Specialized;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using log4net;
using System.Reflection;

namespace Medianet.Web.Controllers
{
    public class AccountController : Controller
    {
        public static Hashtable SessionErrorCodesToQueryString = ConfigurationManager.GetSection("SessionErrorCodesToQueryString") as Hashtable;

        public static string LoginFailedURL
        {
            get
            {
                return LoginPageURL + SessionErrorCodesToQueryString["LoginFailed"] as string ?? "";
            }
        }

        public static string InvalidSessionURL
        {
            get
            {
                return LoginPageURL + SessionErrorCodesToQueryString["InvalidSession"] as string ?? "";
            }
        }
        public static string InsufficientLogonDetailsURL
        {
            get
            {
                return LoginPageURL + SessionErrorCodesToQueryString["InsuffecientDetails"] as string ?? "";
            }
        }

        public static string LoginPageURL
        {
            get
            {
                return ConfigurationManager.AppSettings["LoginPageUrl"];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOn()
        {
            LogOnModel model = new LogOnModel();

            return View("logon", model);
        }

        /// <summary>
        /// Login only for testing purpose
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {
                    var status = AjaxMultipleAuthenticationViaEmail(model.EmailAddress, model.Password, model.RememberMe);

                    var response = new AjaxResponseModel()
                    {
                        Status = status,
                    };

                    if (status)
                    {
                        var session = (SessionModel)Session[NopCommerce.Session_Key];
                        if (session.IsVerified && string.IsNullOrEmpty(returnUrl))
                        {
                            response.IsVerified = true;
                        }
                    }

                    return Json(response);
                }
                catch (Exception)
                {
                    var response = new AjaxResponseModel()
                    {
                        Status = false,
                    };

                    return Json(response);
                }
            }
            else
            {
                try
                {
                    ActionResult result = MultipleAuthenticationViaEmail(model.EmailAddress, model.Password);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return result;
                    }
                }
                catch (FaultException<InvalidLoginFault>)
                {
                    if (this.HttpContext.Request.UrlReferrer != null && model != null &&
                        this.HttpContext.Request.UrlReferrer.ToString().IndexOf("/Account/LogOn", StringComparison.CurrentCultureIgnoreCase) > 0)
                    {
                        ModelState.AddModelError("CustomError", "Invalid username or password");
                        return View("LogOn", model);
                    }
                    else
                        return Redirect(LoginFailedURL);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }

                return View(model);
            }
        }
        
        public ActionResult AutoLogOn(string email, string token)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(token))
            {
                return new EmptyResult();
            }

            try
            {
                using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                {
                    var dbSession = cs.Proxy.LoginPartialAuthorisation(email, new Guid(token), SystemType.Medianet);
                    if (MultipleAuthenticationWithouCleanRelease(dbSession))
                    {
                        if (dbSession.Customer.IsVerified)
                        {
                            //Redirect verified user to home page
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
            }
            catch (Exception)
            {
                //cookie not valid, clear it
                Response.Cookies.Remove(NopCommerce.AutoLogin_Key);
            }

            return Redirect(InvalidSessionURL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            if (session != null)
            {
                session.CleanReleases();

                Session[NopCommerce.Session_Key] = null;

                using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
                {
                    nc.Proxy.CleanCart(session.nopCommerceCustomerId);
                }

                using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                {
                    cs.Proxy.Logout(session.sessionKey);
                    FormsAuthentication.SignOut();
                }

                //Clear auto login cookie
                Response.Cookies.Remove(NopCommerce.AutoLogin_Key);
            }

            return Redirect(LoginPageURL);
        }

        /// <summary>
        /// Initial register page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            RegisterModel model = new RegisterModel();

            ViewData["ReCaptchaSitekey"] = ConfigurationManager.AppSettings["ReCaptchaSitekey"];

            return View("register", model);
        }

        /// <summary>
        /// Form submit for initial register page.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitRegister(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var recatchpaResp = Request.Form["g-recaptcha-response"];

                if (string.IsNullOrWhiteSpace(recatchpaResp))
                {
                    ViewData["ReCaptchaSitekey"] = ConfigurationManager.AppSettings["ReCaptchaSitekey"];
                    ModelState.AddModelError("", "Please validate you are not a robot.");
                    return View("register", model);
                }

                if (!ValidateCaptchaResponse(recatchpaResp, Request.UserHostAddress))
                {
                    ViewData["ReCaptchaSitekey"] = ConfigurationManager.AppSettings["ReCaptchaSitekey"];
                    ModelState.AddModelError("", "Please validate you are not a robot.");
                    return View("register", model);
                }

                // Convert the first character to uppercase.
                if (model.FirstName != null)
                {
                    model.FirstName = model.FirstName.Trim();
                    if (model.FirstName.Length > 0)
                        model.FirstName = model.FirstName.Substring(0, 1).ToUpper() + (model.FirstName.Length > 1 ? model.FirstName.Substring(1) : "");
                }

                using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                {
                    try
                    {
                        cs.Proxy.AddRegistrationValidation(model.Email, model.FirstName, SystemType.Medianet);

                        return View("CheckEmail", model);
                    }
                    catch (FaultException<NonUniqueFault>)
                    {
                        ModelState.AddModelError("Email", "User with this email address already exists. If this is an error or to reset your password, please call our Customer Service team on 1300 616 813 or via email at team@medianet.com.au.");
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("SubmitRegister failed.", ex);
                        ModelState.AddModelError("", "An unknown error occurred. Pleasse try again.");
                    }
                }
            }

            return View("register", model);
        }

        /// <summary>
        /// Second register page after their email address has been validated.
        /// </summary>
        /// <param name="token">A guid for validating their email address.</param>
        /// <returns></returns>
        public ActionResult RegisterDetails(string token)
        {
            try
            {
                using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                {
                    var g = new Guid(token);
                    var regoValidation = cs.Proxy.GetRegistrationValidation(g);

                    if (regoValidation != null)
                    {
                        var model = new RegisterDetailModel();

                        model.Email = regoValidation.EmailAddress;
                        model.FirstName = regoValidation.FirstName;
                        model.Prepare();

                        return View(nameof(RegisterDetails), model);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("SubmitRegister failed.", ex);
            }

            // Something has failed. Send them back to step 1.
            ModelState.AddModelError("", "Details not found. Please complete the first step again.");
            return View("register", new RegisterModel());
        }

        /// <summary>
        /// Registers a new user.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitDetails(RegisterDetailModel submitModel)
        {
            if (!submitModel.AgreeConditions)
            {
                ModelState.AddModelError("AgreeConditions", "You must to accept the Medianet's terms and conditions.");
            }

            if (ModelState.IsValid)
            {
                if (this.RegisterNewCustomer(submitModel))
                {
                    submitModel.session = (SessionModel)Session[NopCommerce.Session_Key];

                    try
                    {
                        var hubHelper = new HubSpotHelper();
                        hubHelper.PostRegistration(submitModel, Request.Url?.AbsoluteUri);
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(
                            $"Registration for {submitModel.Email} succeeded, but failed to push to HubSpot", ex);
                    }

                    return View("thanks", submitModel);
                }
            }

            submitModel.Prepare();
            return View(nameof(RegisterDetails), submitModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit()
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            if (!session.IsAuthorised)
            {
                return RedirectToAction("Index", "Home");
            }

            EditDetailModel model = new EditDetailModel { session = session };
            model.Prepare();

            return View("edit", model);
        }

        /// <summary>
        /// Sole trader verification prepare
        /// </summary>
        /// <returns></returns>
        public ActionResult Verification()
        {
            if (Session[NopCommerce.Account_Key] == null)
            {
                return RedirectToAction("index");
            }
            else
            {
                RegisterModel model = (RegisterModel)Session[NopCommerce.Account_Key];

                return View("verification", model);
            }
        }

        /// <summary>
        /// Edit account
        /// </summary>
        /// <param name="submitModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitEdit(EditDetailModel submitModel)
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            if (!session.IsAuthorised)
            {
                return RedirectToAction("Index", "Home");
            }

            submitModel.session = session;

            if (ModelState.IsValid)
            {
                if (submitModel.UpdateAccount(session))
                {
                    return View("saved", submitModel);
                }
                else
                {
                    ModelState.AddModelError("", submitModel.ErrorMessage);
                }
            }

            submitModel.Prepare();
            return View("Edit", submitModel);
        }

        /// <summary>
        /// Comming from medianet.com.au
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="CompanyName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Authenticate(string UserName, string CompanyName, string Password)
        {
            try
            {
                return MultipleAuthenticationViaUserCompany(UserName, CompanyName, Password);
            }
            catch (FaultException<MedianetCustomerService.InvalidLoginFault>)
            {
                return Redirect(LoginFailedURL);
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Authenticate failed.", e);
                return Redirect(LoginFailedURL);
            }
        }

        /// <summary>
        /// Comming from medianet.com.au
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AuthenticateViaEmail(string EmailAddress, string Password)
        {
            try
            {
                return MultipleAuthenticationViaEmail(EmailAddress, Password);
            }
            catch (FaultException<InvalidLoginFault>)
            {
                return Redirect(LoginFailedURL);
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AuthenticateViaEmail failed.", e);
                return Redirect(LoginFailedURL);
            }
        }

        /// <summary>
        /// Upgrade account form.
        /// </summary>
        /// <returns></returns>
        public ActionResult Upgrade()
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            if (session.MustUseCreditCard == false && session.accountType == (Medianet.Web.MedianetCustomerService.CustomerBillingType)CustomerBillingType.Creditcard)
            {
                UpgradeModel model = new UpgradeModel { session = session };
                model.Prepare();

                return View("upgrade", model);
            }
            else
                return Redirect("/");
        }

        /// <summary>
        /// Upgrades account to invoice
        /// </summary>
        /// <param name="submitModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitUpgrade(UpgradeModel submitModel)
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            submitModel.session = session;

            if (ModelState.IsValid && !IsPostOfficeBox(submitModel))
            {
                if (submitModel.Upgrade())
                    return View("saved", submitModel);
                else
                    ModelState.AddModelError("", submitModel.ErrorMessage);
            }

            submitModel.Prepare();

            return View("upgrade", submitModel);
        }

        /// <summary>
        /// Return a page to allow a User to change their password.
        /// </summary>
        /// <param name="SessionKey"></param>
        public ActionResult ChangePassword(string SessionKey)
        {
            if (!string.IsNullOrWhiteSpace(SessionKey))
            {
                // This customer hasn't yet logged on, but has given us a session to allow them to change their password.
                using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                {
                    DBSession session = cs.Proxy.ValidateSession(SessionKey);

                    if (session == null)
                        return Redirect(InvalidSessionURL);

                    PoppulateSession(session);
                }
            }
            else
            {
                // This customer should already have logged on and so should have a session.
                SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

                if (session == null)
                    return Redirect(InvalidSessionURL);
            }

            return View(new ChangePasswordModel());
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

                if (session == null)
                    return Redirect(InvalidSessionURL);

                using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                {
                    try
                    {
                        DBSession dbSession = cs.Proxy.ChangePassword(model.NewPassword, session.sessionKey);

                        if (dbSession == null)
                            return Redirect(InvalidSessionURL);

                        // We may have been given a new session, so update it again.
                        PopulateMnSession(dbSession, session.nopCommerceCustomerId);

                        return RedirectReferrerOrDefault();
                    }
                    catch (FaultException<InvalidSessionFault>)
                    {
                        return Redirect(InvalidSessionURL);
                    }
                }
            }
            else
                return View(model);
        }

        /// <summary>
        /// Sends a "forgot password" email and returns success or fail.
        /// </summary>
        /// <param name="EmailAddress">The email address of the User that forgot their password.</param>
        /// <param name="Website">The system they wish to logon to.</param>
        /// <returns>A JSon object with success set to true or false.</returns>
        [HttpPost]
        public ActionResult SendForgotPasswordEmail(string EmailAddress, string Website)
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                bool success = true;

                try
                {
                    cs.Proxy.SendForgotPasswordEmail(EmailAddress, (SystemType)((int)Website[0]));
                }
                catch (FaultException<InvalidLoginFault>)
                {
                    success = false;
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("SendForgotPasswordEmail failed.", ex);
                    return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
                }

                return Json(new { success = success });
            }

        }

        /// <summary>
        /// Logon using a session for this system which is already created for us by someone else.
        /// </summary>
        public ActionResult LogOnUsingSession(string sessionKey, string urlToNavigate)
        {
            if (!string.IsNullOrWhiteSpace(sessionKey) && sessionKey != "0")
            {
                //Validate User here.
                try
                {
                    using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                    {
                        DBSession session = cs.Proxy.LoginUsingSession(sessionKey, SystemType.Medianet);

                        if (session == null)
                            return Redirect(InvalidSessionURL);

                        PoppulateSession(session);
                    }

                    return Redirect(urlToNavigate);
                }
                catch
                {
                    // an error occurred
                    return Redirect(LoginFailedURL);
                }
            }
            else
            {
                //insuffecient login informaion.
                return Redirect(LoginFailedURL);
            }
        }

        /// <summary>
        /// Logon using a session from another system as credentials.
        /// </summary>
        public ActionResult LogOnUsingOtherSession(string session)
        {
            if (!string.IsNullOrWhiteSpace(session) && session != "0")
            {
                //Validate User here.
                try
                {
                    using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
                    {
                        DBSession dbSession = cs.Proxy.LoginUsingSession(session, SystemType.Medianet);

                        if (dbSession == null)
                            return Redirect(LoginFailedURL);

                        PoppulateSession(dbSession);
                    }

                    return RedirectDefaultOrReferrer();
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType).Error("LogOnUsingOtherSession failed.", ex);
                    return Redirect(LoginFailedURL);
                }
            }
            else
            {
                //insuffecient login informaion.
                return Redirect(InsufficientLogonDetailsURL);
            }
        }

        #region Private

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="company"></param>
        /// <param name="password"></param>
        private ActionResult MultipleAuthenticationViaUserCompany(string user, string company, string password)
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                // Medianet Login
                DBSession dbSession = cs.Proxy.Login(user, company, password, SystemType.Medianet);
                return MultipleAuthentication(dbSession);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        private ActionResult MultipleAuthenticationViaEmail(string email, string password)
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                // Medianet Login
                DBSession dbSession = cs.Proxy.LoginUsingEmailAddress(email, password, SystemType.Medianet);
                return MultipleAuthentication(dbSession);
            }
        }

        private bool AjaxMultipleAuthenticationViaEmail(string email, string password, bool rememberMe)
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                try
                {
                    // Medianet Login
                    DBSession dbSession = cs.Proxy.LoginUsingEmailAddress(email, password, SystemType.Medianet);
                    if (rememberMe)
                    {
                        var cookie = new HttpCookie(NopCommerce.AutoLogin_Key);
                        cookie.Value = Server.UrlEncode(email + "|" + dbSession.User.ExpiryToken);
                        cookie.Expires = DateTime.Now.AddYears(1);
                        Response.Cookies.Add(cookie);
                    }
                    return MultipleAuthenticationWithouCleanRelease(dbSession);
                }
                catch (FaultException<MedianetCustomerService.InvalidLoginFault>)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Performs authentication in NopCommerce and Medianet
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        private ActionResult MultipleAuthentication(DBSession dbSession)
        {
            // NopCommerce session.
            PoppulateSession(dbSession);

            return RedirectReferrerOrDefault(dbSession.User.MustChangePassword);
        }

        private bool MultipleAuthenticationWithouCleanRelease(DBSession dbSession)
        {
            if (!dbSession.Customer.UseMedianetRewrite)
            {
                return false;
            }
            int nopCommerceCustomerId = PopulateNcSession(dbSession);
            PopulateMnSession(dbSession, nopCommerceCustomerId, false);
            return true;
        }

        private SessionModel PoppulateSession(DBSession dbSession)
        {
            // NopCommerce session.
            int nopCommerceCustomerId = PopulateNcSession(dbSession);

            /// Medianet session.
            return PopulateMnSession(dbSession, nopCommerceCustomerId);
        }

        private int PopulateNcSession(DBSession dbSession)
        {
            FormsAuthentication.SetAuthCookie(dbSession.User.EmailAddress, true);
            int nopCommerceCustomerId = -1;

            using (var ncc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                /// NopCommerce login
                NopCommerceServices.Customer nopCustomer = new NopCommerceServices.Customer
                {
                    Active = true,
                    Email = dbSession.User.EmailAddress,
                    SystemName = dbSession.User.EmailAddress,
                    Username = dbSession.User.LogonName,
                    PasswordFormatId = 0,
                    TaxDisplayTypeId = 0,
                    IsTaxExempt = true,
                    UseRewardPointsDuringCheckout = false,
                    Deleted = false,
                    CreatedOnUtc = DateTime.UtcNow,
                    LastActivityDateUtc = DateTime.UtcNow,
                    LastLoginDateUtc = null,
                };

                nopCommerceCustomerId = ncc.Proxy.RegisterOrSignUpinNopCommerce(nopCustomer);
            }

            return nopCommerceCustomerId;
        }

        private SessionModel PopulateMnSession(DBSession dbSession, int nopCommerceCustomerId, bool clearReleases = true)
        {
            SessionModel session = new SessionModel();

            session.PopulateFromDatabaseChargeBee(dbSession);
            session.nopCommerceCustomerId = nopCommerceCustomerId;

            if (clearReleases)
                session.CleanReleases();

            Session[NopCommerce.Session_Key] = session;

            return session;
        }

        private ActionResult RedirectReferrerOrDefault(bool mustChangePassword = false)
        {
            string returnUrl = Request.QueryString["returnUrl"];

            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }
            else if (mustChangePassword)
            {
                //return View("ChangePassword", new ChangePasswordModel());
                return RedirectToAction("ChangePassword", "Account");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private bool RegisterNewCustomer(RegisterDetailModel model)
        {
            bool created = false;

            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                NewAccountRequest newCustomer = new NewAccountRequest
                {
                    UserLogonName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.Lastname,
                    CompanyName = model.Company,
                    ABN = model.ABN,
                    IndustryCode = model.Industry,
                    ContactPosition = model.Position,
                    TelephoneNumber = model.Phone,
                    AddressLine1 = model.Address,
                    AddressLine2 = model.Address2,                
                    Postcode = model.PostCode,
                    City = model.Suburb,
                    State = model.State,
                    EmailAddress = model.Email,
                    Password = model.Password,
                    AccountType = CustomerBillingType.Creditcard,
                    Country = model.Country,
                    System = SystemType.Medianet,
                };

                try
                {
                    cs.Proxy.AddChargeBeeAccount(newCustomer);
                    MultipleAuthenticationViaEmail(model.Email, model.Password);
                    created = true;
                }
                catch (FaultException<NonUniqueFault> nonUnique)
                {
                    ModelState.AddModelError(string.Empty, nonUnique.Message);
                }
                catch (FaultException<InvalidParameterFault> invalid)
                {
                    ModelState.AddModelError(string.Empty, invalid.Message);
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("RegisterNewCustomer failed.", ex);
                    ModelState.AddModelError(string.Empty, "Error registering new customer.");//+ " Error Message : " + ex.Message
                }
            }

            return created;
        }

        public bool ValidateCaptchaResponse(string greCaptchaResponse, string ipAddress)
        {
            var reCaptchaUrl = ConfigurationManager.AppSettings["ReCaptchaUrl"];
            StringBuilder postData = new StringBuilder();
            postData.Append("secret=" + HttpUtility.UrlEncode(ConfigurationManager.AppSettings["ReCaptchaSecretkey"]));
            postData.Append("&response=" + HttpUtility.UrlEncode(greCaptchaResponse));
            postData.Append("&remoteip=" + HttpUtility.UrlEncode(ipAddress));

            byte[] postBytes = Encoding.ASCII.GetBytes(postData.ToString());

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(reCaptchaUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)";
            request.ContentLength = postBytes.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();

            if (responseStream != null)
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ReCaptchaResponse));
                var translation = (ReCaptchaResponse)serializer.ReadObject(responseStream);

                return (translation.Success.ToLower() == "true");
            }

            return false;
        }

        private bool IsPostOfficeBox(UpgradeModel submitModel)
        {
            if (IsPostOfficeBox(submitModel.Address))
            {
                ModelState.AddModelError("Address", "Please be aware a physical address is required to be eligible for an invoice account.");
                return true;
            }

            if (IsPostOfficeBox(submitModel.Address2))
            {
                ModelState.AddModelError("Address2", "Please be aware a physical address is required to be eligible for an invoice account.");
                return true;
            }

            return false;
        }

        private bool IsPostOfficeBox(string address)
        {
            if (string.IsNullOrWhiteSpace(address))
                return false;

            if (address.IndexOf("PO Box", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                address.IndexOf("P.O.Box", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                address.IndexOf("P.O. Box", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                address.IndexOf("Post Office Box", StringComparison.CurrentCultureIgnoreCase) >= 0)
                return true;

            if (address.IndexOf("GPO Box", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                address.IndexOf("G.P.O. Box", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                address.IndexOf("G.P.O.Box", StringComparison.CurrentCultureIgnoreCase) >= 0)
                return true;

            if (address.IndexOf("Private Bag", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                address.IndexOf("Private Mail Bag", StringComparison.CurrentCultureIgnoreCase) >= 0)
                return true;

            if (address.IndexOf("Locked Bag", StringComparison.CurrentCultureIgnoreCase) >= 0)
                return true;

            return false;
        }

        private ActionResult RedirectDefaultOrReferrer()
        {
            if (Request.UrlReferrer != null)
            {
                // We are logged in. If passed a redirecturl then redirect to this page.
                NameValueCollection col = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

                if (col["redirect"] != null)
                {
                    return Redirect("~/" + HttpUtility.UrlDecode(col["redirect"].ToString()));
                }
            }

            // Redirect to the search page.
            return Redirect("~/");
        }

        #endregion
    }
}
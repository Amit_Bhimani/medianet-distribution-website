﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;
using Medianet.Web.MedianetListService;
using Medianet.Web.Helpers;
using Medianet.Web.NopCommerceServices;
using System.IO;
using log4net;
using Medianet.Web.MedianetChargeBeeService;

namespace Medianet.Web.Controllers
{
    public class BaseController<T> : Controller
        where T : ReleaseModel, new()
    {
        private string model_key { get; set; }

        public BaseController(string key)
        {
            this.model_key = key;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = (SessionModel)Session[NopCommerce.Session_Key];

            // Switching releases
            if (session != null)
            {
                if (session.currentModel != null)
                {
                    if (
                            (session.SwitchRelease == null) &&
                            (session.currentModel != this.model_key) &&
                            (Session[session.currentModel] != null) &&
                            (((ReleaseModel)Session[session.currentModel]).isPrepared) &&
                            (
                                (SwitchReleaseModel.IsRelease(session.currentModel)) &&
                                (SwitchReleaseModel.IsRelease(model_key))
                            )
                    )
                    {
                        var switchRelease = new SwitchReleaseModel();
                        switchRelease.ModelKeyFrom = session.currentModel;
                        switchRelease.ModelKeyTo = this.model_key;

                        session.SwitchRelease = switchRelease;
                        Session[NopCommerce.Session_Key] = session;
                        filterContext.Result = RedirectToAction("switch");
                    }
                }
            }

            var rel = getWorkingRelease();
            var urlReferrer = ((filterContext.HttpContext).Request).UrlReferrer;
            if (urlReferrer != null && rel != null && !urlReferrer.AbsoluteUri.ToLower().EndsWith("/prepare") &&
                       ((filterContext.ActionDescriptor).ActionName.ToLower() == "next"))
            {
                if (rel.isWire && !rel.ValidReleaseContent())
                {
                    filterContext.Result = RedirectToAction("Prepare", NopCommerce.Wire_Key);
                }
                else if (rel.isSms && !rel.ValidReleaseContent())
                {
                    filterContext.Result = RedirectToAction("Prepare", NopCommerce.Sms_Key);
                }
                else if (rel.isAudio && !rel.ValidReleaseContent())
                {
                    filterContext.Result = RedirectToAction("Prepare", NopCommerce.Audio_Key);
                }
            }

            // This is a bad hack to clear the error & warning messages when transitioning from one page to another.
            // The code is way to tangled up to do it properly.
            if (rel != null)
            {
                rel.Error = string.Empty;
                rel.Warning = string.Empty;
                saveWorkingRelease(rel);
            }
        }

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        private string[] dangerousFileTypes = { ".dll", ".exe", ".cab", ".msi", ".js", ".vbs", ".bat", ".cmd", ".com", ".reg", ".wsc" };

        /// <summary>
        /// 
        /// </summary>
        private const string DangerousFiletypeErrorMessage = @"Invalid file type submitted, please verify!";

        #endregion

        #region State Helpers

        protected internal T getWorkingRelease(bool clean)
        {
            T model;
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            if (Session[this.model_key] != null)
            {
                model = (T)Session[this.model_key];
            }
            else
            {
                model = new T();
            }

            model.session = session;

            if (session != null && session.SwitchRelease == null && !model.isInitialised)
            {
                if (clean)
                {
                    model.Clean(this.model_key);
                }

                model.Initialise();
                model.isInitialised = true;
                Session[NopCommerce.Session_Key] = session;
            }

            //model.Error = String.Empty;
            model.modelState = this.ModelState;

            return model;
        }

        /// <summary>
        /// Restores a release from session
        /// </summary>
        /// <returns></returns>
        protected internal T getWorkingRelease()
        {
            return this.getWorkingRelease(true);
        }

        /// <summary>
        /// Sets which menu options are enabled depending on the workflow and sets the active menu
        /// </summary>
        /// <param name="model"></param>
        private void setMenu(T model)
        {
            if (model.Steps != null)
            {
                bool isPrepared = model.isPrepared;

                foreach (WorkflowStep step in model.Steps)
                {
                    if (step != model.Steps.First())
                    {
                        if (isPrepared)
                        {
                            step.isEnabled = true;
                        }
                        else
                            step.isEnabled = false;
                    }
                    else
                        step.isEnabled = true;

                    step.isDefault = false;
                }

                // Set selected
                WorkflowStep selectedStep = model.Steps.Where(s => s.Route == model.workflowStep.Value.Route).First();

                if (selectedStep != null)
                {
                    selectedStep.isDefault = true;

                    model.CurrentStepOrder = selectedStep.Order;
                }
            }
        }

        /// <summary>
        /// Saves the release in session
        /// </summary>
        /// <param name="model"></param>
        protected internal void saveWorkingRelease(T model)
        {
            setMenu(model);

            Session[this.model_key] = model;
        }

        /// <summary>
        /// Removes a release from session
        /// </summary>
        protected internal void removeWorkingWire()
        {
            Session[this.model_key] = null;
        }

        #endregion

        #region Workflow

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        private ActionResult CanStepHere(T model, LinkedListNode<WorkflowStep> step)
        {
            model.workflowStep = step;

            if (!model.CanTransition())
            {
                model.workflowStep = step.Previous;

                if (model.workflowStep.Value.onAfterAction != null)
                {
                    model.workflowStep.Value.onAfterAction.DynamicInvoke();
                }
                return new RedirectResult(model.workflowStep.Value.Route);
            }
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Switch()
        {
            T model = this.getWorkingRelease(false);

            return View("switch", model);
        }

        /// <summary>
        /// Continues to new release
        /// </summary>
        /// <returns></returns>
        public ActionResult Continue()
        {
            string key = this.Request.Form[1].ToString();
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            session.SwitchRelease = null;
            session.currentModel = null;
            Session[NopCommerce.Session_Key] = session;

            return new RedirectResult(SwitchReleaseModel.GetRouteFromReleaseKey(key));
        }

        /// <summary>
        /// Go back and resume release.
        /// </summary>
        /// <returns></returns>
        public ActionResult GoBack()
        {
            string key = this.Request.Form[0].ToString();
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            session.SwitchRelease = null;
            Session[NopCommerce.Session_Key] = session;

            return new RedirectResult(SwitchReleaseModel.GetRouteFromReleaseKey(key));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="stepName"></param>
        /// <returns></returns>
        public ActionResult CanStepHere(T model, string stepName)
        {
            LinkedListNode<WorkflowStep> step = model.GetWorkflowStepFromName(stepName);

            return CanStepHere(model, step);
        }

        /// <summary>
        /// To be called for each controllers step | except the first one 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult CanStepHere(T model)
        {
            LinkedListNode<WorkflowStep> step = model.GetWorkflowStepFromAction();

            return CanStepHere(model, step);
        }

        /// <summary>
        /// Steps to the next wizard 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Next(T model)
        {
            T workingWire = this.getWorkingRelease();

            workingWire.Error = string.Empty;
            workingWire.Warning = string.Empty;
            workingWire.workflowStep = this.GetCurrentStep(workingWire);

            // In case of something submited | each step must have an onAfterNext action that will take the submited model and merge it with the current.
            if (workingWire.workflowStep.Previous != null && workingWire.workflowStep.Previous.Value != null &&
                workingWire.workflowStep.Previous.Value.onAfterNext != null)
            {
                workingWire.workflowStep.Previous.Value.onAfterNext.DynamicInvoke(model);
            }

            model = this.getWorkingRelease();

            bool canTransition = model.Current();

            if (canTransition)
            {
                // This is ugly, but this project is so tangled up that there was no way to do it neatly.
                // UpdateCartAfterOperation calculates the price again, which can fail if the Prepare page
                // doesn't validate, so I only want to do it after validating.
                if (model.GetType() == typeof(WireModel) && model.workflowStep.Previous != null && model.workflowStep.Previous.Value != null && model.workflowStep.Previous.Value.Route == "prepare")
                {
                    var wModel = (WireModel)(object)model;
                    if (wModel.RecalculateCartAfterPrepare)
                    {
                        wModel.UpdateCartAfterOperation();
                    }
                }

                saveWorkingRelease(model);

                return new RedirectResult(model.workflowStep.Value.Route);
            }
            else
            {
                if (model.workflowStep.Value.onAfterAction != null)
                {
                    model.workflowStep.Value.onAfterAction.DynamicInvoke();
                }

                return View(model.workflowStep.Value.Route, model);
            }
        }

        /// <summary>
        /// Steps to the previous wizard
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Previous(T model)
        {
            T workingWire = this.getWorkingRelease();

            workingWire.Error = string.Empty;
            workingWire.Warning = string.Empty;
            workingWire.workflowStep = this.GetCurrentStep(workingWire);

            // In case of something submited | each step must have an onAfterNext action that will take the submited model and merge it with the current.
            if (
                    (workingWire.workflowStep.Next.Value != null) &&
                    (workingWire.workflowStep.Next.Value.onAfterPrevious != null)
            )
            {
                workingWire.workflowStep.Next.Value.onAfterPrevious.DynamicInvoke(model);
            }

            if (workingWire.workflowStep.Value.onAfterAction != null)
            {
                workingWire.workflowStep.Value.onAfterAction.DynamicInvoke();
            }

            saveWorkingRelease(workingWire);

            ModelState.Clear();

            return RedirectToAction(workingWire.workflowStep.Value.Route);
        }
        
        private LinkedListNode<WorkflowStep> GetCurrentStep(T model)
        {
            var action = Request.Form["action"];

            if (action != null)
            {
                string route = action.ToString().ToLower();

                LinkedListNode<WorkflowStep> node = model.workflowSteps.First;

                while (node != null)
                {
                    if (node.Value.Route.ToLower() == route)
                    {
                        return node;
                    }
                    else
                        node = node.Next;
                }
            }

            return model.workflowStep;
        }

        #endregion

        #region Shopping Cart

        /// <summary>
        /// Gets a couple of product comments and returns in a rendered html view.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetProductsInfo(int Id1, int Id2)
        {
            T model = this.getWorkingRelease();

            ProductInformationModel information = model.GetProductsInformation(Id1, Id2);

            return PartialView("_productPopUp", information);
        }

        /// <summary>
        /// Gets a product comment and returns in a rendered html view.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetProductInfo(int Id)
        {
            T model = this.getWorkingRelease();

            ProductInformationModel information = model.GetProductInformation(Id);

            return PartialView("_productPopUp", information);
        }

        [HttpGet]
        public FileResult DownloadPackageAttachment(int packageId, string fileName)
        {
            using (var rsc = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                Byte[]  fileBytes = rsc.Proxy.GetPackageAttachmentFile(packageId, fileName);

                return File(fileBytes, MimeExtensionHelper.GetMimeType(fileName), fileName);
            }
        }

        /// <summary>
        /// Gets a product comment and returns as json.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetProductInfoJson(int Id)
        {
            T model = this.getWorkingRelease();

            ProductInformationModel information = model.GetProductInformation(Id);

            return Json(new { introduction = information.InformationLists[0].Introduction, comment = information.InformationLists[0].Comment.Comment.Replace("\n", "<br />") }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Shopping cart recommendation based on other products added to the cart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ShoppingCartRecommendations()
        {
            T model = this.getWorkingRelease();

            using (var ncc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                model.RecommendationList = ncc.Proxy.GetShoppingCartRecommendations(model.session.nopCommerceCustomerId);
            }

            return PartialView("_cartRecommendations", model);
        }

        /// <summary>
        /// For services or packages
        /// </summary>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddProductToCart(int ProductId, bool isFromDashboard = false)
        {
            T model = this.getWorkingRelease();

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                model.Error = "Can't add a product to the cart on this instance";
            }
            else if (!isFromDashboard && !model.ValidReleaseContent())
            {
                model.Error = "Your content is Invalid due to a session reset. Please Provide your release again";
            }
            else
            {
                try
                {
                    model.AddToCart(model.session, ProductId, isFromDashboard);
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddProductToCart failed.", e);
                    model.Error = e.Message;
                }
                finally
                {
                    model.ReLoadRecommendations();
                    saveWorkingRelease(model);
                }
            }

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// For secondary web category
        /// </summary>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddSwcProductToCart(bool isFromDashboard = false)
        {
            T model = this.getWorkingRelease();

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                model.Error = "Can't add a product to the cart on this instance";
            }
            else
            {
                try
                {
                    model.AddSWCProduct();
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddSWCProductToCart failed.", e);
                    model.Error = e.Message;
                }
                finally
                {
                    model.ReLoadRecommendations();
                    saveWorkingRelease(model);
                }
            }

            return PartialView("_shoppingCart", model);
        }

        //ChangePriority

        /// <summary>
        /// For change in priority
        /// </summary>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult UpdateCartForPriority(string priority)
        {
            T model = this.getWorkingRelease();

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                model.Error = "Can't update the cart on this instance";
            }
            else
            {
                try
                {
                    model.Priority = (Medianet.Web.MedianetReleaseService.ReleasePriorityType)Enum.Parse(typeof(Medianet.Web.MedianetReleaseService.ReleasePriorityType), priority);
                    model.UpdateCartForPriority(model.Priority == MedianetReleaseService.ReleasePriorityType.High);
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UpdateCartForPriority failed.", e);
                    model.Error = e.Message;
                }
                finally
                {
                    saveWorkingRelease(model);
                }
            }

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// For removing items from the cart
        /// </summary>
        /// <param name="itemIndex"></param>
        /// <returns></returns>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult RemoveFromCart(int ShoppingCartItem)
        {
            T model = this.getWorkingRelease();

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                model.Error = "Can't remove from an item from the cart on this instance";
            }
            else
                model.RemoveFromCart(model.session, ShoppingCartItem);

            model.ReLoadRecommendations();
            this.saveWorkingRelease(model);

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// For removing secondary web category from the cart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult RemoveSwcFromCart()
        {
            T model = this.getWorkingRelease();

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                model.Error = "Can't remove from an item from the cart on this instance";
            }
            else
            {
                var shoppingCartItem = 0;
                if (model.shoppingCart != null &&
                    model.shoppingCart.Any(s => s.ProductId == Medianet.Constants.NopCommerce.SecondaryWebCategoryCharge_Product_Id))
                {
                    shoppingCartItem = model.shoppingCart.First(s => s.ProductId == Medianet.Constants.NopCommerce.SecondaryWebCategoryCharge_Product_Id).Id;
                }
                model.RemoveFromCart(model.session, shoppingCartItem);
            }
            model.ReLoadRecommendations();
            this.saveWorkingRelease(model);

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// For custom lists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns
        [HttpPost]
        public ActionResult AddCustomListToCart(int ServiceId)
        {
            T model = getWorkingRelease();

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                model.Error = "Can't add a product to the cart on this instance";
            }
            else if (!model.ValidReleaseContent())
            {
                model.Error = "Your content is Invalid due to a session reset. Please Provide your release again";
            }
            else
            {
                try
                {
                    using (var svc = new ServiceProxyHelper<ListServiceClient, IListService>())
                    {
                        ServiceList serviceList = svc.Proxy.GetServiceListById(ServiceId, model.session.sessionKey);

                        string distributionType = serviceList.DistributionType.ToString().ToUpper().Substring(0, 1);
                        string description = serviceList.SelectionDescription;
                        int addresses = serviceList.AddressCount;

                        model.AddCustomerListToCart(model.session, ServiceId, description, addresses, distributionType);
                    }

                    saveWorkingRelease(model);
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddCustomListToCart failed.", e);
                    model.Error = e.Message;
                }
                finally
                {
                    model.ReLoadRecommendations();
                }
            }

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// Add extra receipient
        /// </summary>
        /// <param name="address"></param>
        /// <param name="distributionType"></param>
        /// <returns></returns>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddExtraReceipientToCart(string address, string distributionType)
        {
            T model = this.getWorkingRelease();
            bool add = true;

            if (model.hasCheckedOut || model.hasFinalized || model.hasSubmited)
            {
                add = false;
                model.Error = "Can't add a product to the cart on this instance";
            }
            if (!model.ValidReleaseContent())
            {
                add = false;
                model.Error = "Release Content is Invalid due to a session reset. Please Provide your release again";
            }

            if (add)
            {
                try
                {
                    model.AddReceipientToCart(model.session, address, distributionType);
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddExtraReceipientToCart failed.", e);
                    model.Error = e.Message;
                }
                finally
                {
                    model.ReLoadRecommendations();
                    saveWorkingRelease(model);
                }
            }

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult WordCount()
        {
            T model = getWorkingRelease();

            if (model.isAudio)
            {
                return PartialView("_attachmentInfoAudio", model);
            }
            else
                return PartialView("_attachmentInfo", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult ShoppingCart()
        {
            T model = getWorkingRelease();

            return PartialView("_shoppingCart", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qqfile"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadAttachment(string qqfile)
        {
            JsonResult result = Upload(qqfile, AttachmentTypeEnum.Other);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qqfile"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadRelease(string qqfile)
        {
            JsonResult result = Upload(qqfile, AttachmentTypeEnum.Release);

            return result;
        }

        /// <summary>
        /// Deletes an attachment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteAttachment(int id)
        {
            DoDeleteAttachment(id);

            return WordCount();
        }

        protected void DoDeleteAttachment(int id)
        {
            T model = getWorkingRelease();
            int wordsToSubstract = 0;
            int pagestoSubstract = 0;
            int attachmentsToSubtract = 0;

            if (model.Release != null)
            {
                if (model.Release.MedianetAttachmentId == id)
                {
                    wordsToSubstract = model.Release.WordCount;
                    pagestoSubstract = model.Release.Pages;
                    attachmentsToSubtract = 1;
                    model.Release = null;
                }
            }

            if (model.WireRelease != null)
            {
                if (model.WireRelease.MedianetAttachmentId == id)
                {
                    wordsToSubstract = model.WireRelease.WordCount;
                    pagestoSubstract = model.WireRelease.Pages;
                    model.WireRelease = null;
                }
            }

            if (model.PressRelease != null)
            {
                if (model.PressRelease.MedianetAttachmentId == id)
                {
                    wordsToSubstract = model.PressRelease.WordCount;
                    pagestoSubstract = model.PressRelease.Pages;
                    attachmentsToSubtract = 1;
                    model.PressRelease = null;
                }
            }

            if (attachmentsToSubtract == 0)
            {
                foreach (AttachmentModel attachment in model.Attachments)
                {
                    if (attachment.MedianetAttachmentId == id)
                    {
                        wordsToSubstract = attachment.WordCount;
                        pagestoSubstract = attachment.Pages;
                        model.Attachments.Remove(attachment);
                        model.AttachmentCount -= 1;
                        break;
                    }
                }
            }

            model.Pages -= pagestoSubstract;
            model.Words -= wordsToSubstract;
            if (model.isWire)
            {
                model.UpdateAttachmentProductToCart(model.AttachmentCount);
            }

            model.UpdateCartAfterOperation();

            saveWorkingRelease(model);
        }

        /// <summary>
        /// Upload method for release and other.
        ///     When uploading the release set the converttofax true and converttowire false
        ///     when uploading the "email & wire text" html set converttofax to false and converttowire true
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        protected internal JsonResult Upload(string qqfile, AttachmentTypeEnum type)
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            T model = getWorkingRelease();

            string fileName = String.Empty;
            int tempFileId = -1;
            Stream stream;

            /// IE
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                stream = postedFile.InputStream;
                fileName = Path.GetFileName(Request.Files[0].FileName);
            }
            /// Webkit, Mozilla
            else
            {
                stream = Request.InputStream;
                fileName = Path.GetFileName(qqfile);
            }

            if (!IsAllowedType(fileName))
            {
                return Json(new { success = false, id = tempFileId, fileName = fileName, error = DangerousFiletypeErrorMessage }, "text/html");
            }

            try
            {
                tempFileId = model.UploadForConversion(stream, fileName, type, false);
                if (type == AttachmentTypeEnum.Other)
                {
                    model.UpdateAttachmentProductToCart(model.AttachmentCount);
                }
                model.UpdateCartAfterOperation();

                saveWorkingRelease(model);
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Upload failed.", e);
                return Json(new { success = false, id = tempFileId, fileName = fileName, error = e.Message }, "text/html");
            }

            return Json(new { success = true, id = tempFileId, fileName = fileName }, "text/html");
        }

        /// <summary>
        /// Protection to avoid upload of dangerous files to the server.
        /// </summary>
        /// <param name="qqfile"></param>
        /// <returns></returns>
        private bool IsAllowedType(string qqfile)
        {
            string extension = qqfile.Substring(qqfile.LastIndexOf('.'));
            return !dangerousFileTypes.Contains<string>(extension);
        }

        /// <summary>
        /// Confirms an order
        /// </summary>
        /// <returns></returns>
        public ActionResult Confirm()
        {
            T model = getWorkingRelease();

            ActionResult notAllowedAction = CanStepHere(model, "checkout");

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }
            model.Confirm();
            ViewBag.DisplayExcludedCartItems = "false";
            saveWorkingRelease(model);

            return View("Confirm", model);
        }

        /// <summary>
        /// Thanks and submit order
        /// </summary>
        /// <returns></returns>
        public ActionResult Thanks()
        {
            T model = this.getWorkingRelease();

            ActionResult notAllowedAction = this.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            /// Must finalise payment
            if (Request["state"] != null)
            {
                try
                {
                    model.Error = string.Empty;
                    model.Warning = string.Empty;
                    model.accessCode = Request["id"].ToString();
                    model.FinalisePayment();

                    if (model.hasFinalized)
                    {
                        model.SubmitRelease();
                        this.removeWorkingWire();
                    }
                    else
                    {
                        this.saveWorkingRelease(model);

                        return RedirectToAction("payment");
                    }
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Thanks failed.", e);
                    model.Error = e.Message;
                }
            }
            else if (model.hasToPay)
            {
                model.hasSubmited = false;
                model.hasFinalized = false;
                model.Error = "There was a problem processing the payment, please contact an administrator or try again";
            }
            else
            {
                model.Error = string.Empty;
                model.Warning = string.Empty;
                model.SubmitRelease();
                this.removeWorkingWire();
                model.hasFinalized = true;
            }

            if (model.workflowStep != null && !string.IsNullOrWhiteSpace(model.workflowStep.Value.WidgetPage))
                model.WidgetList = BaseModel.GetWidgets(model.workflowStep.Value.WidgetPage);

            return View("Thanks", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Payment()
        {
            T model = this.getWorkingRelease();

            ActionResult notAllowedAction = this.CanStepHere(model, "checkout");

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Payment();
            saveWorkingRelease(model);

            return View("Payment", model);
        }

        /// <summary>
        /// Starts checkout process
        /// </summary>
        /// <returns></returns>
        public ActionResult Checkout()
        {
            T model = this.getWorkingRelease();
            ActionResult notAllowedAction = this.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            return model.Checkout();
        }

        #endregion
    }
}
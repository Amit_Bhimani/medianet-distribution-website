﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Constants;
using Medianet.Web.Models;
using Medianet.Web.Helpers;
using System.IO;

namespace Medianet.Web.Controllers
{
    public class ManageListController<T> : Controller
        where T : ManageListBase, new()
    {
        private string model_key { get; set; }

        public ManageListController(string key)
        {
            this.model_key = key;
        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            T model = GetWorkingModel();
                model.Index();

            return View("Index", model);
        }

        protected internal T GetWorkingModel()
        {
            T model;
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            if (Session[this.model_key] != null)
            {
                model = (T)Session[this.model_key];
            }
            else
            {
                model = new T();
            }

            model.session = session;

            //model.Error = String.Empty;
            model.modelState = this.ModelState;

            return model;
        }

        /// <summary>
        /// Saves the Model in session
        /// </summary>
        /// <param name="model"></param>
        protected internal void SaveWorkingModel(T model)
        {
            Session[this.model_key] = model;
        }

        /// <summary>
        /// Uploads a new .csv file for importing
        /// </summary>
        /// <param name="qqfile"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UploadNewFile(string qqfile, string name)
        {
            T model = GetWorkingModel();
            string fileName = String.Empty;
            Stream stream;

            /// IE
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                stream = postedFile.InputStream;
                fileName = Request.Files[0].FileName;
            }
            /// Webkit, Mozilla
            else
            {
                stream = Request.InputStream;
                fileName = qqfile;
            }

            bool result = model.AddServiceListFromSummary(stream, name);
            model.Index();

            SaveWorkingModel(model);

            return Json(new { success = result, error = model.Error, lists = model.Lists }, "text/html");
        }

        /// <summary>
        /// Replaces a list with another.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ReplaceList(string qqfile, string ListId)
        {
            T model = GetWorkingModel();
            string fileName = String.Empty;
            Stream stream;

            /// IE
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                stream = postedFile.InputStream;
                fileName = Request.Files[0].FileName;
            }
            /// Webkit, Mozilla
            else
            {
                stream = Request.InputStream;
                fileName = qqfile;
            }

            bool result = model.UpdateListFromSummary(stream, int.Parse(ListId));                        
            model.Index();

            SaveWorkingModel(model);

            return Json(new { success = result, error = model.Error, lists = model.Lists }, "text/html");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public FileResult Download(int Id)
        {
            T model = GetWorkingModel();

            Stream result = model.GetRecipientsForExport(Id);

            string fileName = String.Format("{0}.csv", Id);

            return File(result, MimeExtensionHelper.GetMimeType(fileName), fileName);
        }

        /// <summary>
        /// Renames the name of a list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RenameList(int Id, string NewName)
        {
            T model = GetWorkingModel();
            model.RenameServiceList(Id, NewName);
            model.Index();

            SaveWorkingModel(model);
            return Json(new { success = true, lists = model.Lists }, "text/html");
        }

        /// <summary>
        /// Deletes an existing list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteList(int Id)
        {
            T model = GetWorkingModel();
            model.DeleteServiceList(Id);
            model.Index();

            SaveWorkingModel(model);
            return Json(new { success = true, lists = model.Lists }, "text/html");
        }

        /// <summary>
        /// Edits an existing list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditList(int Id)
        {
            T model = GetWorkingModel();
            model.EditListFromSummary(Id);

            SaveWorkingModel(model);

            return Json(new { success = true, lists = model.ListRecipients }, "text/html");
        }

        /// <summary>
        /// Gets a recipient 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost,]
        public ActionResult GetRecipient(int ListId, int RecipientId)
        {
            T model = GetWorkingModel();
            model.GetRecipient(ListId, RecipientId);
            SaveWorkingModel(model); ;

            return PartialView("_form", model);
        }

        /// <summary>
        /// Adss a recipient to an existing list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddRecipient(T submitModel)
        {
            // Call this to get the Session and last ListId selected.
            T model = GetWorkingModel();

            submitModel.session = model.session;
            submitModel.ListId = model.ListId;

            if (ModelState.IsValid)
            {
                submitModel.RecipientId = submitModel.AddServiceListRecipient();
                submitModel.EditListFromSummary(submitModel.ListId.Value);
                submitModel.Error = submitModel.Error;

                // Clear the ModelState. Otherwise the view returns the values cached in the ModelState instead of our new values.
                ModelState.Clear();

                return PartialView("_form", submitModel);
            }
            else
            {
                if (submitModel.ListId.HasValue)
                    submitModel.EditListFromSummary(submitModel.ListId.Value);

                return PartialView("_form", submitModel);
            }
        }

        /// <summary>
        /// Updates an existing list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateRecipient(T submitModel)
        {
            // Call this to get the Session.
            T model = GetWorkingModel();

            submitModel.session = model.session;

            if (ModelState.IsValid &&
                submitModel.ListId.HasValue && submitModel.ListId.Value > 0 &&
                submitModel.RecipientId.HasValue && submitModel.RecipientId.Value > 0)
            {
                submitModel.UpdateServiceListRecipient();

                submitModel.EditListFromSummary(submitModel.ListId.Value);
                submitModel.Error = submitModel.Error;
            }
            else {
                if (submitModel.ListId.HasValue)
                    submitModel.EditListFromSummary(submitModel.ListId.Value);
            }

            return PartialView("_form", submitModel);
        }

        /// <summary>
        /// Deletes an existing recipient from an existing list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteRecipient(int ListId, int RecipientId)
        {
            T model = GetWorkingModel();
            model.DeleteServiceListRecipient(ListId, RecipientId);
            model.EditListFromSummary(ListId);
                
            return PartialView("_form", model);
        }
    }
}

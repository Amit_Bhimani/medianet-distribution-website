﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using log4net;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using Medianet.Web.MedianetGeneralService;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.Models;
using Medianet.Web.Validators;
using InvalidParameterFault = Medianet.Web.MedianetGeneralService.InvalidParameterFault;
using SystemType = Medianet.Web.MedianetCustomerService.SystemType;
using System.Net;

namespace Medianet.Web.Controllers
{
    public class TrainingCourseController : Controller
    {
        private const string Session_Training = "TRAINING";

        /// <summary>
        /// Step 1. Initial training course registration page.
        /// </summary>
        [HttpGet]
        public ActionResult Register(string courseId, int scheduleId, string promoCode)
        {
            TrainingCourseRegisterModel model;

            model = FetchCourseDetails(courseId, scheduleId, promoCode);

            return View("Register", model);
        }

        /// <summary>
        /// Step 2. Display both a creditcard payment page and an invoice option
        /// and allow the user to choose which way they want to pay.
        /// </summary>
        [HttpPost]
        public ActionResult Payment(TrainingCourseRegisterModel model)
        {
            return PaymentSteps(model);
        }

        /// <summary>
        /// Step 2. Redirect after creditcard payment failed.
        /// </summary>
        [HttpGet]
        public ActionResult Payment()
        {
            var model = (TrainingCourseRegisterModel)Session[Session_Training];

            if (model == null)
            {
                model = new TrainingCourseRegisterModel();
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Payment could not find details in the session.");
                ModelState.AddModelError(string.Empty, "Details no longer available. Please return to the first step and start again.");
            }

            model.Prepare();
            return PaymentSteps(model);
        }

        [HttpGet]
        public ActionResult ApplyPromoCode(string courseId, int scheduleId, string promoCode)
        {
            var model = FetchCourseDetails(courseId, scheduleId, promoCode);

            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.BadRequest, "Invalid model");
            }

            var code = model.PromoCodes.FirstOrDefault(c => c.PromoCode.Equals(promoCode, StringComparison.CurrentCultureIgnoreCase));

            if (code == null)
                return new HttpNotFoundResult("Invalid course");
            
            return Json(code, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Step 3. Callback method from eway after attempting to pay by creditcard.
        /// </summary>
        [HttpGet]
        public ActionResult Thanks(string debtorNumber, string AccessCode)
        {
            TrainingCourseRegisterModel model;

            if (string.IsNullOrWhiteSpace(debtorNumber) || string.IsNullOrWhiteSpace(AccessCode))
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("TrainingCourse/Thanks called with missing parameters.");
                return View("~/Views/Home/500.cshtml", new ContactModel());
            }

            model = FinalisePayment(AccessCode, debtorNumber);

            if (!model.HasFinalised)
            {
                return RedirectToAction("Payment");
            }

            return View("Thanks", model);
        }

        ///// <summary>
        ///// Step 3. Method called when paying by invoice. Validate the ABN.
        ///// </summary>
        //[HttpPost]
        //public ActionResult Thanks(string abn)
        //{
        //    var model = (TrainingCourseRegisterModel)Session[Session_Training];
        //    string debtorNumber;

        //    if (model == null)
        //    {
        //        model = new TrainingCourseRegisterModel();
        //        model.PaymentMethod = TrainingCourseRegisterModel.PaymentMethod_Invoice;

        //        LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Payment could not find details in the session.");
        //        ModelState.AddModelError(string.Empty, "Details no longer available. Please return to the first step and start again.");
        //        return View("Payment", model);
        //    }

        //    model.Prepare();
        //    model.ABN = abn;
        //    model.PaymentMethod = TrainingCourseRegisterModel.PaymentMethod_Invoice;

        //    var x = new ABNAttribute();
        //    if (!x.IsValid(model.ABN))
        //    {
        //        ModelState.AddModelError("ABN", "The field ABN must be a string with a maximum length of 11");
        //        return View("Payment", model);
        //    }
 
        //    // Update the customer ABN.
        //    debtorNumber = CreateOrUpdateCustomer(model);

        //    if (!ModelState.IsValid)
        //    {
        //        return View("Payment", model);
        //    }

        //    RegisterForCourse(model, debtorNumber, string.Empty, 0d);

        //    return View("Thanks", model);
        //}

        private void FetchCourseDetails(TrainingCourseRegisterModel model)
        {
            try
            {
                using (var svc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
                {
                    TrainingCourse course = svc.Proxy.GetTrainingCourse(model.CourseId);

                    if (course != null && course.EventType != EventType.Event)
                    {
                        model.CourseTitle = course.Title;
                        model.CourseSummary = course.Summary;
                        model.CourseDescription = course.Description;
                        model.CoursePrice = course.Price;
                        model.CourseTopics = new List<string>();
                        model.PromoCodes = new List<PromoCodeModel>();

                        if (course.PromoCodes != null)
                        {
                            foreach (var code in course.PromoCodes)
                            {
                                var x = new PromoCodeModel();
                                x.PromoCode = code.PromoCode;
                                x.PackageId = code.PackageId;
                                x.Price = code.Price;

                                model.PromoCodes.Add(x);
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(model.PromoCode))
                        {
                            var code = model.PromoCodes.FirstOrDefault(
                                        c => c.PromoCode.Equals(model.PromoCode, StringComparison.CurrentCultureIgnoreCase));

                            if (code == null)
                            {
                                model.PromoCode = string.Empty;
                            }
                            else
                            {
                                model.CoursePrice = code.Price;
                            }
                        }

                        if (course.Topics != null)
                        {
                            foreach (var t in course.Topics)
                            {
                                model.CourseTopics.Add(t.Heading);
                            }
                        }

                        var sched = course.Schedules == null
                            ? null
                            : course.Schedules.FirstOrDefault(s => s.Id == model.ScheduleId);

                        if (sched != null)
                        {
                            var seatsTaken =
                                sched.Enrolments.Count(
                                    e => e.RowStatus == Medianet.Web.MedianetGeneralService.RowStatusType.Active);

                            model.ScheduleDate = sched.CourseDate;
                            model.ScheduleTime = sched.CourseTime;
                            model.ScheduleLocation = sched.LocationId;
                            model.ScheduleAddress = sched.Location.Description.Replace("\n", "<br />");
                            model.TotalSeats = sched.Seats;
                            model.SeatsRemaining = model.TotalSeats - seatsTaken;
                            model.ScheduleAgenda = sched.Agenda;
                            model.ScheduleFormHandler = sched.FormHandler;

                            if (sched.CourseDate < DateTime.Now)
                                ModelState.AddModelError(string.Empty,
                                    "This course is in the past and is no longer taking registrations.");

                            if (sched.Seats <= seatsTaken)
                                ModelState.AddModelError(string.Empty, "This course has reached maximum capacity. However you can check out other course dates, <a href='http://www.medianet.com.au/training_courses'>here</a>.");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "The course schedule could not be found.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "The course could not be found.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("FetchCourseDetails failed.", ex);
                ModelState.AddModelError(string.Empty, "Error fetching course details.");
            }
        }

        private TrainingCourseRegisterModel FetchCourseDetails(string courseId, int scheduleId, string promoCode)
        {
            var model = (TrainingCourseRegisterModel)Session[Session_Training];

            if (model == null || (model.CourseId != courseId && model.ScheduleId != scheduleId))
            {
                model = new TrainingCourseRegisterModel();
                model.CourseId = courseId;
                model.ScheduleId = scheduleId;
            }
            else
            {
                model.Prepare();
            }

            if (!string.IsNullOrWhiteSpace(promoCode))
                model.PromoCode = promoCode;

            FetchCourseDetails(model);

            return model;
        }

        private ActionResult PaymentSteps(TrainingCourseRegisterModel model)
        {
            FetchCourseDetails(model);

            if (ModelState.IsValid)
            {
                string debtorNumber;

                debtorNumber = CreateOrUpdateCustomer(model);

                if (ModelState.IsValid)
                {
                    PreparePayment(model, debtorNumber);

                    if (ModelState.IsValid)
                    {
                        return View("Payment", model);
                    }
                }
            }

            return View("Register", model);
        }

        private string CreateOrUpdateCustomer(TrainingCourseRegisterModel model)
        {
            string debtorNumber = string.Empty;

            try
            {
                using (var cs = new ServiceProxyHelper<CustomerServiceClient, ICustomerService>())
                {
                    var newCustomer = new NewAccountRequest();

                    newCustomer.FirstName = model.FirstName;
                    newCustomer.LastName = model.Lastname;
                    newCustomer.CompanyName = model.CourseId + " - " +  model.Company;
                    newCustomer.ABN = model.ABN;
                    newCustomer.IndustryCode = "H"; // Education & Training
                    newCustomer.ContactPosition = model.Position;
                    newCustomer.TelephoneNumber = model.Phone;
                    newCustomer.AddressLine1 = model.Address;
                    newCustomer.AddressLine2 = String.Empty;
                    newCustomer.Postcode = model.PostCode;
                    newCustomer.City = model.Suburb;
                    newCustomer.State = model.State;
                    newCustomer.EmailAddress = model.Email;
                    newCustomer.AccountType = CustomerBillingType.Creditcard;
                    newCustomer.Country = model.Country;
                    newCustomer.System = SystemType.Medianet;

                    debtorNumber = cs.Proxy.CreateCustomerForCourse(newCustomer);
                }
            }
            catch (FaultException<MedianetCustomerService.InvalidParameterFault> invalid)
            {
                ModelState.AddModelError(string.Empty, invalid.Message);
            }
            catch (FaultException<MedianetCustomerService.UpdateFault> invalid)
            {
                ModelState.AddModelError(string.Empty, invalid.Message);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("RegisterNewCustomer failed.", ex);
                ModelState.AddModelError(string.Empty, "Error registering customer.");
            }

            return debtorNumber;
        }

        private void PreparePayment(TrainingCourseRegisterModel model, string debtorNumber)
        {
            var request = new CoursePaymentStartRequest();

            request.ScheduleId = model.ScheduleId;
            request.DebtorNumber = debtorNumber;
            request.RedirectURL = String.Format("{0}://{1}/TrainingCourse/Thanks?debtorNumber={2}", System.Web.HttpContext.Current.Request.Url.Scheme, System.Web.HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST"), debtorNumber);
            request.IPAddress = string.Empty;
            request.Seats = 1;
            request.PromoCode = model.PromoCode;

            Session[Session_Training] = model;

            try
            {
                using (var cs = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    model.PaymentResponse = cs.Proxy.InitialiseCoursePayment(request);
                }
            }
            catch (FaultException<MedianetCustomerService.InvalidParameterFault> invalid)
            {
                ModelState.AddModelError(string.Empty, invalid.Message);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("InitialiseCoursePayment failed.", ex);
                ModelState.AddModelError(string.Empty, "Error initialising course payment.");
            }
        }

        private TrainingCourseRegisterModel FinalisePayment(string accessCode, string debtorNumber)
        {
            TrainingCourseRegisterModel model;
            string receiptId = string.Empty;
            double totalCost = 0;

            model = (TrainingCourseRegisterModel)Session[Session_Training];

            if (model == null)
            {
                model = new TrainingCourseRegisterModel();
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("FinalisePayment could not find details in the session for DebtorNumber '" + debtorNumber + "'.");
            }

            model.Prepare();
            model.HasFinalised = false;

            try
            {
                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    var response = mpc.Proxy.FinaliseCoursePayment(accessCode, debtorNumber);

                    if (response.PaymentSucceeded)
                    {
                        model.HasFinalised = true;
                        receiptId = response.TransactionRecordId.ToString();
                        totalCost = (double)response.Total;
                    }
                    else
                    {
                        model.PaymentResponse = null;
                        //result.Error = response.ResponseMessage;
                        model.PaymentError = response.ResponseMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("FinalisePayment failed.", ex);
                model.Error = ex.Message;
            }

            if (model.HasFinalised)
            {
                RegisterForCourse(model, debtorNumber, receiptId, totalCost);
            }

            return model;
        }

        private void RegisterForCourse(TrainingCourseRegisterModel model, string debtorNumber, string receiptId, double totalCost)
        {
            try
            {
                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    var regoRequest = new CourseRegistrationRequest();

                    regoRequest.DebtorNumber = debtorNumber;
                    regoRequest.FirstName = model.FirstName;
                    regoRequest.LastName = model.Lastname;
                    regoRequest.CompanyName = model.Company;
                    regoRequest.ABN = model.ABN;
                    regoRequest.ContactPosition = model.Position;
                    regoRequest.Address = model.Address;
                    regoRequest.Suburb = model.Suburb;
                    regoRequest.PostCode = model.PostCode;
                    regoRequest.State = model.State;
                    regoRequest.Country = model.Country;
                    regoRequest.TelephoneNumber = model.Phone;
                    regoRequest.EmailAddress = model.Email;
                    regoRequest.ExperienceLevel = model.ExperienceLevel;
                    regoRequest.DesiredOutcome = model.DesiredOutcome;
                    regoRequest.TopicsPlanned = model.Topics;
                    regoRequest.ReceiptId = receiptId;
                    regoRequest.ScheduleId = model.ScheduleId;
                    regoRequest.TotalCost = totalCost;
                    regoRequest.IsInvoice = (model.PaymentMethod != TrainingCourseRegisterModel.PaymentMethod_Creditcard);

                    mpc.Proxy.RegisterForCourse(regoRequest);

                    // We don't need this in the session any more
                    Session[Session_Training] = null;
                }
            }
            catch (FaultException<MedianetCustomerService.InvalidParameterFault> invalid)
            {
                model.Error = invalid.Message;
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("RegisterForCourse failed.", ex);
                model.Error = ex.Message;
            }
        }
    }
}

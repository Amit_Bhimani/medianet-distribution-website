﻿using Medianet.Web.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.MedianetGeneralService;
using Medianet.Web.Helpers;
using log4net;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.IO;

namespace Medianet.Web.Controllers
{
    public class SMSWebhookController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger("SMSWebhookLogger");
        
        [HttpPost]
        public void SMSWebhook()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string json = new StreamReader(req).ReadToEnd();

            log.Info(json);

            SMSDeliveryReportModel data = new JavaScriptSerializer().Deserialize<SMSDeliveryReportModel>(json);

            if (data.status == MessageMediaStatus.submitted)
                return;

            int sendDuration = (int)new TimeSpan((data.date_received - data.submitted_date).Ticks).TotalSeconds;

            try
            {
                using (var svc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
                {
                    svc.Proxy.UpdateMessageMediaAPIResult(new Result
                    {
                        ReleaseId = data.metadata.JobId,
                        TransactionId = data.metadata.TransactionId,
                        DistributionId = data.metadata.DistributionId,
                        Status = TransactionStatusType.Resulted,
                        SendDuration = sendDuration,
                        TransmittedTime = data.date_received,
                        ErrorCode = data.status == MessageMediaStatus.delivered ? "" : data.error_code,
                        ErrorMessage = data.status == MessageMediaStatus.delivered ? "" : data.status.ToString() //EnumHelper.GetDescription((Enum)Enum.Parse(typeof(MessageMediaError), data.error_code))
                    });
                }
            }
            catch (FaultException<KeyNotFoundFault> ex)
            {
                throw new HttpException(404, ex.Message);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class ManageEmailController : ManageListController<EmailListModel>
    {
        public ManageEmailController()
            : base(NopCommerce.EmailList_key)
        {
        }

        /// <summary>
        /// Creates a new list from a comma delimited addresses 
        /// </summary>
        /// <param name="NameList"></param>
        /// <param name="Addresses"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddPastedList(string NameList, string Addresses)
        {
            EmailListModel model = base.GetWorkingModel();
            Addresses = Addresses.Replace(" ", "");
            model.AddServiceListFromPaste(Addresses.Split(','), NameList);
            model.Index();

            base.SaveWorkingModel(model);
            return Json(new { success = true, error = model.Error, lists = model.Lists }, "text/html");
        }
    }
}

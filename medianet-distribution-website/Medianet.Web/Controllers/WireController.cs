﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Web.NopCommerceServices;
using Medianet.Web.MedianetFileService;
using System.IO;
using Medianet.Constants;
using Medianet.Web.Helpers;
using Medianet.Web.SearchService;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class WireController : BaseController<WireModel> 
    {
        public WireController()
            : base(NopCommerce.Wire_Key)
        {
        }

        #region 1.Prepare release

        /// <summary>
        /// Step 1 
        /// </summary>
        /// <returns></returns>
        public ActionResult Prepare()
        {
            WireModel model = base.getWorkingRelease();
            model.isPrepare = true;
            model.workflowStep = model.workflowSteps.First;
            model.session.currentModel = NopCommerce.Wire_Key;
            model.Prepare();

            base.saveWorkingRelease(model);

            return View("prepare",model);
        }

        [HttpPost]
        public ActionResult Prepare(WireModel model)
        {
            return Next(model);
        }

        #endregion

        #region 2.Select Online

        /// <summary>
        /// Step 2 
        /// </summary>
        /// <returns></returns>
        public ActionResult Online()
        {
            WireModel model = (WireModel)base.getWorkingRelease();
            model.workflowStep = model.workflowSteps.First.Next.Next.Next;

            model.Online();
            base.saveWorkingRelease(model);

            return View("Online", model);
        }

        [HttpPost]
        public ActionResult Online(WireModel model)
        {
            return Next(model);
        }

        #endregion

        #region 3.Select wire

        /// <summary>
        /// Step 3 
        /// </summary>
        /// <returns></returns>
        public ActionResult Wire()
        {
            WireModel model = (WireModel)base.getWorkingRelease();
            model.workflowStep = model.workflowSteps.First.Next.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Wire();
            base.saveWorkingRelease(model);
            
            return View("Wire", model);
        }

        [HttpPost]
        public ActionResult Wire(WireModel model)
        {
            return Next(model);
        }

        /// <summary>
        /// Returns a list of products given a category
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetWireProducts(int CategoryId)
        {
            WireModel model = (WireModel)base.getWorkingRelease();

            using (NopCommerceClient ncc = new NopCommerceClient())
            {
                model.ProductSearchList = ncc.GetWireProductsByCategory(CategoryId);
            }

            return PartialView("_wireproducts", model);
        }

        #endregion

        #region 4.Lists

        /// <summary>
        /// Step 4
        /// </summary>
        /// <returns></returns>
        public ActionResult Lists()
        {
            WireModel model = (WireModel)base.getWorkingRelease();
            model.isPrepare = true;
            model.workflowStep = model.workflowSteps.First.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Lists();
            base.saveWorkingRelease(model);

            return View("Lists", model);
        }

        [HttpPost]
        public ActionResult Lists(WireModel model)
        {
            return Next(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult LoadSearchProducts(string Criteria)
        {
            WireModel model = base.getWorkingRelease();
            model.ProductSearch(Criteria);

            return PartialView("_search", model);
        }

        [HttpPost]
        public PartialViewResult SearchOutlets(string outletId)
        {
            WireModel model = base.getWorkingRelease();
            model.ProductSearchByOutlet(outletId);

            return PartialView("_search", model);
        }

        [HttpGet]
        public JsonResult AutoSuggestOutlets(string query)
        {
            List<AutosuggestResult> result = new List<AutosuggestResult>();
            List<SearchResult> searchResults = null;
            var so = new SearchOptionsAdvanced();
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            so.OutletCountryIDList = new int[] { 210 }; // Australian results only
            so.RecordsToShow = "M"; // MediaDirectory only (not private records)
            so.Type = SearchType.Advanced;
            so.Context = SearchContext.Outlet;

            // Remove brackets as they cause the boolean parser to fail
            so.OutletName = query.Replace("(", " ").Replace(")", " ");

            using (var npc = new ServiceProxyHelper<SearchServiceClient, ISearchService>())
            {
                int totalResults;
                searchResults = npc.Proxy.Search(out totalResults, so, SortColumn.Default, SortDirection.ASC, true, session.MedianetUserId, 0, 15, new RecordIdentifier[0], new RecordIdentifier[0], new RecordIdentifier[0]).ToList();
            }

            if (searchResults != null)
            {
                foreach (var x in searchResults)
                {
                    result.Add(new AutosuggestResult() { Key = x.OutletID, Value = x.OutletName });
                }
            }

            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetChildCategories(int CategoryId)
        {
            IList<Category> childCategories;

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                childCategories = nc.Proxy.GetChildCategories(CategoryId);
            }

            return Json(childCategories);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCategoryProducts(int CategoryId)
        {
            WireModel model = base.getWorkingRelease();

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                model.ProductSearchList = nc.Proxy.GetWireProductsByCategory(CategoryId);
            }

            ServiceListNameFormaterHelper<Product> helper = new ServiceListNameFormaterHelper<Product>();
            model.ProductSearchListFormated = helper.GroupServiceLists((List<Product>)model.ProductSearchList.ToList(), "Name");

            return PartialView("_categoryproducts", model);
        }

        /// <summary>
        /// Get cart recommendations based on web category id
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetRecommendations()
        {
            WireModel model = (WireModel)base.getWorkingRelease();

            model.GetRecommendations();

            return PartialView("_quickRecommendations", model);
        }

        #endregion

        #region 5.Contacts

        /// <summary>
        /// Step 5
        /// </summary>
        /// <returns></returns>
        public ActionResult Contacts()
        {
            WireModel model = (WireModel)base.getWorkingRelease();
            model.isPrepare = true;
            model.workflowStep = model.workflowSteps.First.Next.Next.Next.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Contacts();
            base.saveWorkingRelease(model);

            return View("Contacts", model);
        }

        [HttpPost]
        public ActionResult Contacts(WireModel model)
        {
            return Next(model);
        }
        #endregion

        #region 6.Finalise

        /// <summary>
        /// Step 6
        /// </summary>
        /// <returns></returns>
        public ActionResult Finalise()
        {
            WireModel model = (WireModel)base.getWorkingRelease();
            model.workflowStep = model.workflowSteps.First.Next.Next.Next.Next.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.resetWireText = true;
            model.Finalise();
            base.saveWorkingRelease(model);

            return View("Finalise",model);
        }

        [HttpPost]
        public ActionResult Finalise(WireModel model)
        {
            return Next(model);
        }

        [HttpPost]
        public ActionResult Confirm(WireModel model)
        {
            return Next(model);
        }


        [HttpPost]
        [ValidateInput(false)]
        public PartialViewResult ConfirmWireDetails(bool confirmWire, string wireText)
        {
            WireModel model = (WireModel)base.getWorkingRelease();
            model.UpdateWireContent(wireText, confirmWire);
            base.saveWorkingRelease(model);

            model.UpdateCartAfterOperation();
            return PartialView("_Finalise", model);
        }

        public PartialViewResult FinalisePartial()
        {
            WireModel model = (WireModel)base.getWorkingRelease();

            model.resetWireText = false;
            model.Finalise();

            return PartialView("_Finalise", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FileResult Preview(int id)
        {
            WireModel model = (WireModel)base.getWorkingRelease();

            string fileExtension;
            string fileName;
            int fileSize;
            int itemCount;
            Stream fileData;
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            if (model.Release != null)
            {
                if (model.Release.MedianetAttachmentId == id)
                {
                    fileName = model.Release.Name;
                }
            }

            foreach (AttachmentModel attachment in model.Attachments)
            {
                if (attachment.MedianetAttachmentId == id)
                {
                    fileName = attachment.Name;
                }
            }

            using (var ncc = new ServiceProxyHelper<FileServiceClient, IFileService>())
            {
                string result = ncc.Proxy.GetTempFileContent(ref id, session.sessionKey, out fileExtension, out fileSize, out itemCount, out fileName, out fileData);
            }

            return File(fileData, MimeExtensionHelper.GetMimeType(fileName), fileName);
        }

        #endregion
    }
}
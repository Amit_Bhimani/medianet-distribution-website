﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class VideoController : BaseController<VideoModel>
    {
        public VideoController()
            : base(NopCommerce.Video_Key)
        {
        }

        public ActionResult Index()
        {
            VideoModel model = base.getWorkingRelease();

            return View("Index",model);
        }

        public ActionResult Prepare()
        {
            VideoModel model = base.getWorkingRelease();

            return View("Index", model);
        }
    }
}

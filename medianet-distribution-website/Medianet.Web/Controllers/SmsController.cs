﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

using Medianet.Web.Models;
using Medianet.Constants;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class SmsController : BaseController<SmsModel>
    {
        public SmsController()
            : base(NopCommerce.Sms_Key)
        {
        }

        #region 1.Prepare

        /// <summary>
        /// Step I
        /// </summary>
        /// <returns></returns>
        public ActionResult Prepare()
        {
            SmsModel model = (SmsModel)base.getWorkingRelease();
                model.isPrepare = true;
                model.workflowStep = model.workflowSteps.First;
                model.session.currentModel = NopCommerce.Sms_Key;
                model.Prepare();

            base.saveWorkingRelease(model);

            return View("Prepare", model);
        }

        [HttpPost]
        public ActionResult Prepare(SmsModel model)
        {
            return Next(model);
        }

        #endregion

        #region 2.Contacts

        /// <summary>
        /// Step II
        /// </summary>
        /// <returns></returns>
        public ActionResult Contacts()
        {
            SmsModel model = (SmsModel)base.getWorkingRelease();
                model.isPrepare = false;
                model.workflowStep = model.workflowSteps.First.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Contacts();
            base.saveWorkingRelease(model);

            return View("Contacts", model);
        }

        [HttpPost]
        public ActionResult Contacts(SmsModel model)
        {
            return Next(model);
        }

        #endregion

        #region 3.Finalise

        /// <summary>
        /// Step III
        /// </summary>
        /// <returns></returns>
        public ActionResult Finalise()
        {
            SmsModel model = (SmsModel)base.getWorkingRelease();
            model.isPrepare = false;
            model.workflowStep = model.workflowSteps.First.Next.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Finalise();
            base.saveWorkingRelease(model);

            return View("Finalise", model);
        }

        [HttpPost]
        public ActionResult Finalise(SmsModel model)
        {
            return Next(model);
        }
        [HttpPost]
        public ActionResult Confirm(SmsModel model)
        {
            return Next(model);
        }

        #endregion
    }
}

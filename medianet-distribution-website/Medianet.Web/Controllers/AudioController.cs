﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;
using Medianet.Web.MedianetGeneralService;
using System.IO;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetFileService;
using Medianet.Web.NopCommerceServices;
using log4net;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class AudioController : BaseController<AudioModel> 
    {
        public AudioController()
            : base(NopCommerce.Audio_Key)
        {
        }

        #region 1.Prepare release

        /// <summary>
        /// Step I 
        /// </summary>
        /// <returns></returns>
        public ActionResult Prepare()
        {
            AudioModel model = base.getWorkingRelease();
                model.isPrepare = true;
                model.workflowStep = model.workflowSteps.First;
                model.session.currentModel = NopCommerce.Audio_Key;
                model.Prepare();

            base.saveWorkingRelease(model);

            return View("prepare", model);
        }

        [HttpPost]
        public ActionResult Prepare(AudioModel model)
        {
            return Next(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SelectAudioFile(int Id,string Name,bool Add)
        {
            AudioModel model = base.getWorkingRelease();

            string NameId = String.Format("{0}|{1}",Name,Id.ToString());

            if (Add)
            {
                model.SelectAudioFile(NameId);
            }
            else
                model.UnSelectAudioFile(NameId);

            base.saveWorkingRelease(model);

            return PartialView("_attachmentInfoAudio", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PreviousPage()
        {
            AudioModel model = base.getWorkingRelease();

            /*if (model.PageIndex > 1)
            {
                model.PageIndex--;
            }*/
            if (model.PagerIndex > 0) {
                model.PagerIndex--;
            }

            model.PageIndex = (model.PagerIndex * NopCommerce.Report_Pager_Size) + 1;

            model.LoadAudioFilesPage();
            base.saveWorkingRelease(model);
            return PartialView("_audiofiles", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult NextPage() {
            AudioModel model = base.getWorkingRelease();

            /*if (model.PageIndex < model.TotalPages)
            {
                model.PageIndex++;
            }*/
            if ((model.PagerIndex + 1) < (model.TotalPages / NopCommerce.Report_Pager_Size)) {
                model.PagerIndex++;
            }

            model.PageIndex = (model.PagerIndex * NopCommerce.Report_Pager_Size) + 1;

            model.LoadAudioFilesPage();
            base.saveWorkingRelease(model);
            return PartialView("_audiofiles", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GoToPage(int index)
        {
            AudioModel model = base.getWorkingRelease();

            if (index >= 0 && index <= model.TotalPages) {
                model.PageIndex = index;
            }

            model.PageIndex = index;

            model.LoadAudioFilesPage();
            base.saveWorkingRelease(model);
            return PartialView("_audiofiles", model);
        }

        /// <summary>
        /// Uploads a press release
        /// </summary>
        /// <param name="qqfile"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadPressRelease(string qqfile, MedianetReleaseService.ReleasePriorityType priority)
        {
            AudioModel model = base.getWorkingRelease();

            string fileName = String.Empty;
            Stream stream;

            /// IE
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                stream = postedFile.InputStream;
                fileName = Path.GetFileName(Request.Files[0].FileName);
            }
            /// Webkit, Mozilla
            else
            {
                stream = Request.InputStream;
                fileName = Path.GetFileName(qqfile);
            }

            try
            {
                model.UploadForConversion(stream, fileName, AttachmentTypeEnum.PressRelease, false);
                model.UpdateCartAfterOperation();
                base.saveWorkingRelease(model);
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UploadPressRelease failed.", e);
                return Json(new { success = false, id = -1, fileName = fileName, error = e.Message }, "text/html");
            }

            return Json(new { success = true, id = model.PressRelease.MedianetAttachmentId, fileName = fileName }, "text/html");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qqfile"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadWireRelease(string qqfile, MedianetReleaseService.ReleasePriorityType priority)
        {
            AudioModel model = base.getWorkingRelease();

            string fileName = String.Empty;
            Stream stream;

            /// IE
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                stream = postedFile.InputStream;
                fileName = Path.GetFileName(Request.Files[0].FileName);
            }
            /// Webkit, Mozilla
            else
            {
                stream = Request.InputStream;
                fileName = Path.GetFileName(qqfile);
            }

            try
            {
                model.UploadForConversion(stream, fileName, AttachmentTypeEnum.Release, true);
                model.UpdateCartAfterOperation();
                base.saveWorkingRelease(model);
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UploadWireRelease failed.", e);
                return Json(new { success = false, id = -1, fileName = fileName, error = e.Message }, "text/html");
            }

            return Json(new { success = true, id = model.EmailwireTextTempFileUploadId, fileName = fileName }, "text/html");
        }

        /// <summary>
        /// Uploads an audio file
        /// </summary>
        /// <param name="qqfile"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadAudio(string qqfile, MedianetReleaseService.ReleasePriorityType priority)
        {
            AudioModel model = base.getWorkingRelease();

            int attached =model.Attachments.Count;
            attached += (model.PressRelease != null) ? 1 : 0;

            if (attached == 4)
            {
                return Json(new { success = false, id = -1, fileName = "", error = "Only a maximum of 4 uploads are allowed." }, "text/html");
            }

            string fileName = String.Empty;
            Stream stream;

            /// IE
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase postedFile = Request.Files[0];
                stream = postedFile.InputStream;
                fileName = Path.GetFileName(Request.Files[0].FileName);
            }
            /// Webkit, Mozilla
            else
            {
                stream = Request.InputStream;
                fileName = Path.GetFileName(qqfile);
            }

            try
            {
                model.UploadForConversion(stream, fileName, AttachmentTypeEnum.Other, false);
                model.UpdateCartAfterOperation();
                base.saveWorkingRelease(model);
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UploadAudio failed.", e);
                return Json(new { success = false, id = -1, fileName = fileName, error = e.Message }, "text/html");
            }

            return Json(new { success = true, id = model.Attachments[model.Attachments.Count-1].MedianetAttachmentId, fileName = fileName }, "text/html");
        }

        /// <summary>
        /// Plays an audio file.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Play(int Id)
        {
            AudioModel model = base.getWorkingRelease();
            AudioFileModel file = model.Play(Id);

            return File(file.FileArrayData, "audio/mpeg");
        }

        [HttpPost]
        public ActionResult RenameAudioFile(int Id, string Name) {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            InboundFile file;

            using (var gsc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>()) {
                file = gsc.Proxy.GetInboundFileById(Id, session.sessionKey);

                if (Path.GetExtension(file.OriginalFilename) != Path.GetExtension(Name)) {
                    Name = Path.GetFileNameWithoutExtension(Name) + Path.GetExtension(file.OriginalFilename);
                }

                gsc.Proxy.RenameInboundFileById(Id, Name, session.sessionKey);
            }

            return Json(new { Id = Id, Name = Name });
        }

        [HttpPost]
        public ActionResult DeleteAudioFile(int Id) {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            using (var gsc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>()) {
                gsc.Proxy.DeleteInboundFileById(Id, session.sessionKey);
            }

            return Json(new { Success = true });
        }

        public PartialViewResult FinalisePartial()
        {
            AudioModel model = base.getWorkingRelease();

            model.Finalise();

            return PartialView("_Finalise", model);
        }

        #endregion

        #region 2.Online

        /// <summary>
        /// Step II 
        /// </summary>
        /// <returns></returns>
        public ActionResult Online()
        {
            AudioModel model = (AudioModel)base.getWorkingRelease();
            model.workflowStep = model.workflowSteps.First.Next;

            model.Online();
            base.saveWorkingRelease(model);

            return View("Online", model);
        }

        [HttpPost]
        public ActionResult Online(AudioModel model)
        {
            return Next(model);
        }

        #endregion

        #region 3.Lists

        /// <summary>
        /// Step III
        /// </summary>
        /// <returns></returns>
        public ActionResult Lists()
        {
            AudioModel model = base.getWorkingRelease();
                model.workflowStep = model.workflowSteps.First.Next.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.isPrepare = false;
            model.Lists();
            base.saveWorkingRelease(model);

            return View("lists", model);
        }

        [HttpPost]
        public ActionResult Lists(AudioModel model)
        {
            return Next(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetChildCategories(int CategoryId)
        {
            IList<Category> childCategories;

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                childCategories = nc.Proxy.GetChildRadioCategories(CategoryId);
            }

            return Json(childCategories);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetCategoryProducts(int CategoryId)
        {
            AudioModel model = base.getWorkingRelease();

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                model.ProductList = nc.Proxy.GetRadioProducts(CategoryId);
            }

            return PartialView("_categoryproducts", model);
        }        

        #endregion

        #region 4.Finalise
        
        /// <summary>
        /// Step IV
        /// </summary>
        /// <returns></returns>
        public ActionResult Finalise()
        {
            AudioModel model = base.getWorkingRelease();
                model.workflowStep = model.workflowSteps.First.Next.Next.Next;

            ActionResult notAllowedAction = base.CanStepHere(model);

            if (notAllowedAction != null)
            {
                return notAllowedAction;
            }

            model.Finalise();
            base.saveWorkingRelease(model); 

            return View("finalise", model);
        }
        [HttpPost]
        public ActionResult Finalise(AudioModel model)
        {
            return Next(model);
        }

        [HttpPost]
        public ActionResult Confirm(AudioModel model)
        {
            return Next(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FileResult Preview(int id)
        {
            string fileExtension;
            string fileName;
            int fileSize;
            int itemCount;
            Stream fileData;
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];

            using (var ncc = new ServiceProxyHelper<FileServiceClient, IFileService>())
            {
                string result = ncc.Proxy.GetTempFileContent(ref id, session.sessionKey, out fileExtension, out fileSize, out itemCount, out fileName, out fileData);
            }

            return File(fileData, MimeExtensionHelper.GetMimeType(fileName), "fileName");
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Models;
using Medianet.Constants;

namespace Medianet.Web.Controllers
{
    [Authorize]
    public class MultimediaController : BaseController<MultiMediaModel>
    {
        public MultimediaController()
            : base(NopCommerce.MultiMedia_Key)
        {
        }

        public ActionResult Index()
        {
            MultiMediaModel model = base.getWorkingRelease();
            model.WidgetList = BaseModel.GetWidgets("multimedia");

            return View("Index",model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Medianet.Web.Models;
using Medianet.Constants;
using Medianet.Web.Models.Curator;
using RestSharp;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using System.IO;
using MigraDoc.DocumentObjectModel.Tables;

namespace Medianet.Web.Controllers
{
    //[Authorize]
    public class ReportController : Controller
    {
        public ActionResult Index()
        {
            ReportModel model = GetModel();

            model.Prepare();
            SaveModel(model);

            return View("index", model);
        }

        [HttpPost]
        public ActionResult PreviousPage()
        {
            ReportModel model = GetModel();

            if (model.PagerIndex > 0)
            {
                model.PagerIndex--;
            }

            model.PageIndex = (model.PagerIndex * NopCommerce.Report_Pager_Size) + 1;
            model.GetReportPage(model.PageIndex);
            SaveModel(model);

            return PartialView("_reportPage", model);
        }

        [HttpPost]
        public ActionResult NextPage()
        {
            ReportModel model = GetModel();

            if ((model.PagerIndex + 1) < (model.TotalPages / NopCommerce.Report_Pager_Size))
            {
                model.PagerIndex++;
            }

            model.PageIndex = (model.PagerIndex * NopCommerce.Report_Pager_Size) + 1;
            model.GetReportPage(model.PageIndex);
            SaveModel(model);

            return PartialView("_reportPage", model);
        }

        [HttpPost]
        public ActionResult GoToPage(int Page)
        {
            ReportModel model = GetModel();

            if (Page >= 0 && Page <= model.TotalPages)
            {
                model.PageIndex = Page;
            }

            model.GetReportPage(model.PageIndex);
            SaveModel(model);

            return PartialView("_reportPage", model);
        }

        [HttpPost]
        public PartialViewResult Details(int ReleaseId, int TransactionId)
        {
            var model = GetReportDetailsModel(ReleaseId, TransactionId);

            SaveModel(model);

            return PartialView("_transactionPopUp", model);
        }

        private ReportModel GetReportDetailsModel(int ReleaseId, int TransactionId)
        {
            ReportModel model = GetModel();

            model.GetTransactionDetail(ReleaseId, TransactionId);

            return model;
        }

        public ActionResult View(int Id)
        {
            ReportModel model = GetModel();

            model.GetReportResult(Id);
            SaveModel(model);

            return View("view", model);
        }

        [HttpGet]
        public JsonResult CuratorReport(int releaseId)
        {
            CuratorReport model = GetCuratorReport(releaseId);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void CancelRelease(int releaseId)
        {
            ReportModel model = GetModel();

            model.CancelRelease(releaseId);
        }

        #region PDF Export 

        [HttpGet]
        public ActionResult Export(int ReleaseId, int TransactionId)
        {
            ReportModel model = GetReportDetailsModel(ReleaseId, TransactionId);
            return File(RenderPDF(model), "application/pdf", string.Format("Transaction_{0}_{1}.pdf", ReleaseId, TransactionId));
        }

        private byte[] RenderPDF(ReportModel model)
        {
            Document document = new Document();
            Section section = document.AddSection();
            section.PageSetup = new PageSetup { MirrorMargins = true, Orientation = Orientation.Landscape };
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(false, PdfFontEmbedding.Always);
            pdfRenderer.Document = document;

            section.AddImage(Server.MapPath("~/Content/img/medianet-logo.jpg"));

            section.AddParagraph("");
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.Font.Size = 16;
            paragraph.Format.Font.Name = "Caption";
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.AddFormattedText("Open Rate Reports", TextFormat.Bold);

            section.AddParagraph("");

            Table table = section.AddTable();
            table.AddColumn("4cm");
            table.AddColumn("20cm");
            Row row = table.AddRow();
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].AddParagraph("Subject/Headline:");
            row.Cells[1].AddParagraph(model.ReleaseDetails.WebDetails != null ? model.ReleaseDetails.WebDetails.Headline : "");
            row = table.AddRow();
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].AddParagraph("List name:");
            row.Cells[1].AddParagraph(model.Transaction.SelectionDescription);

            section.AddParagraph("");

            AddRecipientsToPDF(model, section);

            AddFooterToPDF(section);

            pdfRenderer.RenderDocument();

            using (MemoryStream ms = new MemoryStream())
            {
                pdfRenderer.Save(ms, false);
                byte[] buffer = new byte[ms.Length];
                ms.Seek(0, SeekOrigin.Begin);
                ms.Flush();
                ms.Read(buffer, 0, (int)ms.Length);

                return buffer;
            }
        }

        private void AddRecipientsToPDF(ReportModel model, Section section)
        {
            bool isEmail = model.Transaction.DistributionType == Medianet.Web.MedianetReleaseService.DistributionType.Email;

            Table table = section.AddTable();
            table.Borders.Width = 0.25;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.AddColumn("11cm");
            table.AddColumn("5cm");
            if (isEmail)
                table.AddColumn("1.5cm");
            table.AddColumn("3cm");
            table.AddColumn("4.5cm");

            Row row = table.AddRow();
            row.Height = "0.7cm";

            row.Cells[0].AddParagraph(model.Transaction.ShouldSendResultsToOwner ? "Address" : "Recipient");
            row.Cells[1].AddParagraph("Time delivered");
            if (isEmail)
                row.Cells[2].AddParagraph("Opened");

            row.Cells[isEmail ? 3 : 2].AddParagraph("Status");
            row.Cells[isEmail ? 4 : 3].AddParagraph("Reason");

            for (int i = 0; i < model.Results.Length; ++i)
            {
                var result = model.Results[i];

                row = table.AddRow();
                row.Height = "0.7cm";
                row.Shading.Color = i % 2 == 0 ? Colors.White : Colors.LightGray;
                row.Format.Alignment = ParagraphAlignment.Left;

                row.Cells[0].AddParagraph(model.Transaction.ShouldSendResultsToOwner ? result.Address : result.Recipient);
                row.Cells[1].AddParagraph(result.TransmittedTime.HasValue ? result.TransmittedTime.Value.ToString() : "");
                if (isEmail)
                    row.Cells[2].AddParagraph(result.OpenCount > 0 ? "Yes" : "No");

                row.Cells[isEmail ? 3 : 2].AddParagraph(result.Status.ToString());
                row.Cells[isEmail ? 4 : 3].AddParagraph(result.ErrorMessage);
            }
        }

        private void AddFooterToPDF(Section section)
        {
            Paragraph paragraph = new Paragraph();
            paragraph.AddText("Page ");
            paragraph.AddPageField();
            paragraph.AddText(" of ");
            paragraph.AddNumPagesField();
            section.Footers.Primary.Add(paragraph);
        }

        #endregion

        private ReportModel GetModel()
        {
            SessionModel session = (SessionModel)Session[NopCommerce.Session_Key];
            ReportModel model;

            if (Session[NopCommerce.Report_Key] != null)
            {
                model = (ReportModel)Session[NopCommerce.Report_Key];
            }
            else
            {
                model = new ReportModel();
            }

            model.session = session;

            return model;
        }

        private void SaveModel(ReportModel model)
        {
            Session[NopCommerce.Report_Key] = model;
        }

        public CuratorReport GetCuratorReport(int releaseId)
        {
            var apiAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["CuratorApiAddress"];
            try
            {
                var client = new RestClient(apiAddress);

                var request = new RestRequest("?id_canonical=" + releaseId.ToString(), Method.GET);
                var response = client.Execute<CuratorReport>(request);

                return response.Data;
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return null;
            }
        }
    }
}
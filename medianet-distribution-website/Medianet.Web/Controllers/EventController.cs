﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using Medianet.Web.MedianetGeneralService;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.Models;
using SystemType = Medianet.Web.MedianetCustomerService.SystemType;

namespace Medianet.Web.Controllers
{
    public class EventController : Controller
    {
        private const string Session_Event = "EVENT";

        /// <summary>
        /// Step 1. Initial Event registration page.
        /// </summary>
        [HttpGet]
        public ActionResult Register(string eventId, int scheduleId, string promoCode)
        {
            EventRegisterModel model;

            model = FetchEventDetails(eventId, scheduleId, promoCode);

            return View("Register", model);
        }

        [HttpPost]
        public PartialViewResult AddAttendee(List<EventAttendeeModel> attendees)
        {
            return PartialView("_attendeeRow", attendees);
        }

        [HttpGet]
        public ActionResult ApplyPromoCode(string eventId, int scheduleId, string promoCode)
        {
            var model = FetchEventDetails(eventId, scheduleId, promoCode);

            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.BadRequest, "Invalid model");
            }

            var code = model.PromoCodes.FirstOrDefault(
                                        c => c.PromoCode.Equals(promoCode, StringComparison.CurrentCultureIgnoreCase));

            if (code == null)
            {
                return new HttpNotFoundResult("Invalid event");
            }

            return Json(code, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Step 2. Display both a creditcard payment page and an invoice option
        /// and allow the user to choose which way they want to pay.
        /// </summary>
        [HttpPost]
        public ActionResult Payment(EventRegisterModel model)
        {
            return PaymentSteps(model);
        }

        /// <summary>
        /// Step 2. Redirect after creditcard payment failed.
        /// </summary>
        [HttpGet]
        public ActionResult Payment()
        {
            var model = (EventRegisterModel)Session[Session_Event];

            if (model == null)
            {
                model = new EventRegisterModel();
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Payment could not find details in the session.");
                ModelState.AddModelError(string.Empty, "Details no longer available. Please return to the first step and start again.");
            }

            model.Prepare();
            return PaymentSteps(model);
        }

        /// <summary>
        /// Step 3. Callback method from eway after attempting to pay by creditcard.
        /// </summary>
        [HttpGet]
        public ActionResult Thanks(string debtorNumber, string AccessCode)
        {
            EventRegisterModel model;

            if (string.IsNullOrWhiteSpace(debtorNumber) || string.IsNullOrWhiteSpace(AccessCode))
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Event/Thanks called with missing parameters.");
                EmailHelper.SendErrorEmail("Event/Thanks called with missing parameters.");
                return View("~/Views/Home/500.cshtml", new ContactModel());
            }

            model = FinalisePayment(AccessCode, debtorNumber);

            if (!model.HasFinalised)
            {
                return RedirectToAction("Payment");
            }

            return View("Thanks", model);
        }

        [HttpGet]
        public ActionResult RegisterInterest(string eventId, int scheduleId, string promoCode)
        {
            EventRegisterModel model;

            model = FetchEventDetails(eventId, scheduleId, promoCode);

            return View("RegisterInterest", model);
        }

        private void FetchEventDetails(EventRegisterModel model)
        {
            try
            {
                using (var svc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
                {
                    TrainingCourse course = svc.Proxy.GetTrainingCourse(model.EventId);

                    if (course != null && course.EventType == EventType.Event)
                    {
                        model.EventTitle = course.Title;
                        model.EventSummary = course.Summary.Replace("\n", "<br />");
                        model.EventDescription = course.Description.Replace("\n", "<br />");
                        model.EventSector = course.Sector;
                        model.EventPrice = course.Price;
                        model.PromoCodes = new List<PromoCodeModel>();
                        model.Attendees = ParseAttendees(model.AttendeesString);

                        if (course.PromoCodes != null)
                        {
                            foreach (var code in course.PromoCodes)
                            {
                                var x = new PromoCodeModel();
                                x.PromoCode = code.PromoCode;
                                x.PackageId = code.PackageId;
                                x.Price = code.Price;

                                model.PromoCodes.Add(x);
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(model.PromoCode))
                        {
                            var code = model.PromoCodes.FirstOrDefault(
                                        c => c.PromoCode.Equals(model.PromoCode, StringComparison.CurrentCultureIgnoreCase));

                            if (code == null)
                            {
                                model.PromoCode = string.Empty;
                            }
                            else
                            {
                                model.EventPrice = code.Price;
                            }
                        }

                        if (course.Contact != null)
                        {
                            model.ContactNumber = course.Contact.Number;
                            model.ContactEmail = course.Contact.Email;
                        }

                        var sched = course.Schedules == null
                            ? null
                            : course.Schedules.FirstOrDefault(s => s.Id == model.ScheduleId);

                        if (sched != null)
                        {
                            var seatsRequired = (model.Attendees != null && model.Attendees.Count > 1 ? model.Attendees.Count : 1);
                            var seatsTaken =
                                sched.Enrolments.Count(
                                    e => e.RowStatus == MedianetGeneralService.RowStatusType.Active);

                            model.ScheduleDate = sched.CourseDate;
                            model.ScheduleTime = sched.CourseTime;
                            model.ScheduleLocation = sched.LocationId;
                            model.ScheduleAddress = sched.Location.Description.Replace("\n", "<br />");
                            model.TotalSeats = sched.Seats;
                            model.SeatsRemaining = model.TotalSeats - seatsTaken;
                            model.ScheduleFormHandler = sched.FormHandler;

                            if (string.IsNullOrWhiteSpace(sched.Agenda))
                                model.ScheduleAgenda = new List<string>();
                            else
                                model.ScheduleAgenda = sched.Agenda.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                            if (sched.CourseDate < DateTime.Now)
                                ModelState.AddModelError(string.Empty,
                                    "This event is in the past and is no longer taking registrations.");

                            if (model.SeatsRemaining < seatsRequired)
                                ModelState.AddModelError(string.Empty, string.Format("This event has reached maximum capacity. However you can register your interest for future events <a href='/Event/RegisterInterest?eventId={0}&scheduleId={1}'>here</a>.", model.EventId, model.ScheduleId));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "The event schedule could not be found.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "The event could not be found.");
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("GetTrainingCourse failed.", ex);
                ModelState.AddModelError(string.Empty, "Error fetching event details.");
            }
        }

        private EventRegisterModel FetchEventDetails(string eventId, int scheduleId, string promoCode)
        {
            var model = (EventRegisterModel)Session[Session_Event];

            if (model == null || (model.EventId != eventId && model.ScheduleId != scheduleId))
            {
                model = new EventRegisterModel();
                model.EventId = eventId;
                model.ScheduleId = scheduleId;
            }
            else
            {
                model.Prepare();
            }

            if (!string.IsNullOrWhiteSpace(promoCode))
                model.PromoCode = promoCode;

            FetchEventDetails(model);

            return model;
        }

        private ActionResult PaymentSteps(EventRegisterModel model)
        {
            FetchEventDetails(model);

            if (ModelState.IsValid)
            {
                string debtorNumber;

                debtorNumber = CreateOrUpdateCustomer(model);

                if (ModelState.IsValid)
                {
                    PreparePayment(model, debtorNumber);

                    if (ModelState.IsValid)
                    {
                        return View("Payment", model);
                    }
                }
            }

            return View("Register", model);
        }

        private string CreateOrUpdateCustomer(EventRegisterModel model)
        {
            string debtorNumber = string.Empty;

            if (model.Attendees == null || model.Attendees.Count == 0)
            {
                ModelState.AddModelError(string.Empty, "Please add one or more attendees.");
                return debtorNumber;
            }

            try
            {
                using (var cs = new ServiceProxyHelper<CustomerServiceClient, ICustomerService>())
                {
                    var newCustomer = new NewAccountRequest();

                    newCustomer.FirstName = model.FirstName;
                    newCustomer.LastName = model.Lastname;
                    newCustomer.CompanyName = model.EventId + " - " + model.Company;
                    newCustomer.ABN = String.Empty; //model.ABN;
                    newCustomer.IndustryCode = "H"; // Education & Training
                    newCustomer.ContactPosition = String.Empty; //model.Position;
                    newCustomer.TelephoneNumber = model.Phone;
                    newCustomer.AddressLine1 = model.Address;
                    newCustomer.AddressLine2 = String.Empty;
                    newCustomer.Postcode = model.PostCode;
                    newCustomer.City = model.Suburb;
                    newCustomer.State = model.State;
                    newCustomer.EmailAddress = model.Email;
                    newCustomer.AccountType = CustomerBillingType.Creditcard;
                    newCustomer.Country = model.Country;
                    newCustomer.System = SystemType.Medianet;

                    debtorNumber = cs.Proxy.CreateCustomerForCourse(newCustomer);
                }
            }
            catch (FaultException<MedianetCustomerService.InvalidParameterFault> invalid)
            {
                ModelState.AddModelError(string.Empty, invalid.Message);
            }
            catch (FaultException<MedianetCustomerService.UpdateFault> invalid)
            {
                ModelState.AddModelError(string.Empty, invalid.Message);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("RegisterNewCustomer failed.", ex);
                ModelState.AddModelError(string.Empty, "Error registering customer.");
            }

            return debtorNumber;
        }

        private void PreparePayment(EventRegisterModel model, string debtorNumber)
        {
            var request = new CoursePaymentStartRequest();

            request.ScheduleId = model.ScheduleId;
            request.DebtorNumber = debtorNumber;
            request.RedirectURL = string.Format("{0}://{1}/Event/Thanks?debtorNumber={2}", System.Web.HttpContext.Current.Request.Url.Scheme, System.Web.HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST"), debtorNumber);
            request.IPAddress = string.Empty;
            request.Seats = model.Attendees.Count;
            request.PromoCode = model.PromoCode;

            Session[Session_Event] = model;

            try
            {
                using (var cs = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    model.PaymentResponse = cs.Proxy.InitialiseCoursePayment(request);
                }
            }
            catch (FaultException<MedianetCustomerService.InvalidParameterFault> invalid)
            {
                ModelState.AddModelError(string.Empty, invalid.Message);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("InitialiseCoursePayment failed.", ex);
                ModelState.AddModelError(string.Empty, "Error initialising event payment.");
            }
        }

        private EventRegisterModel FinalisePayment(string accessCode, string debtorNumber)
        {
            EventRegisterModel model;
            string receiptId = string.Empty;
            double totalCost = 0;

            model = (EventRegisterModel)Session[Session_Event];

            if (model == null)
            {
                model = new EventRegisterModel();
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Event/FinalisePayment could not find details in the session for DebtorNumber '" + debtorNumber + "' and AccessCode " + accessCode);
                EmailHelper.SendErrorEmail("Event/FinalisePayment could not find details in the session for DebtorNumber '" + debtorNumber + "' and AccessCode " + accessCode);
            }

            model.Prepare();
            model.HasFinalised = false;

            try
            {
                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    var response = mpc.Proxy.FinaliseCoursePayment(accessCode, debtorNumber);

                    if (response.PaymentSucceeded)
                    {
                        model.HasFinalised = true;
                        receiptId = response.TransactionRecordId.ToString();
                        totalCost = (double)response.Total;
                    }
                    else
                    {
                        model.PaymentResponse = null;
                        //result.Error = response.ResponseMessage;
                        model.PaymentError = response.ResponseMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("FinalisePayment failed.", ex);
                model.Error = ex.Message;
            }

            if (model.HasFinalised)
            {
                RegisterForEvent(model, debtorNumber, receiptId, totalCost);
            }

            return model;
        }

        private void RegisterForEvent(EventRegisterModel model, string debtorNumber, string receiptId, double totalCost)
        {
            try
            {
                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    foreach (var attendee in model.Attendees)
                    {
                        var regoRequest = new CourseRegistrationRequest();

                        regoRequest.DebtorNumber = debtorNumber;
                        regoRequest.FirstName = attendee.FirstName;
                        regoRequest.LastName = attendee.Lastname;
                        regoRequest.CompanyName = attendee.Company;
                        //regoRequest.ABN = model.ABN;
                        regoRequest.ContactPosition = attendee.Position;
                        regoRequest.Address = model.Address;
                        regoRequest.Suburb = model.Suburb;
                        regoRequest.PostCode = model.PostCode;
                        regoRequest.State = model.State;
                        regoRequest.Country = model.Country;
                        regoRequest.TelephoneNumber = attendee.Phone;
                        regoRequest.EmailAddress = attendee.Email;
                        regoRequest.ReceiptId = receiptId;
                        regoRequest.ScheduleId = model.ScheduleId;
                        regoRequest.TotalCost = totalCost;
                        regoRequest.IsInvoice = false;

                        mpc.Proxy.RegisterForCourse(regoRequest);
                    }

                    // We don't need this in the session any more
                    Session[Session_Event] = null;

                    RegisterWithPardot(model);
                }
            }
            catch (FaultException<MedianetCustomerService.InvalidParameterFault> invalid)
            {
                model.Error = invalid.Message;
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("RegisterForCourse failed.", ex);
                model.Error = ex.Message;
            }
        }

        private List<EventAttendeeModel> ParseAttendees(string attendeesString)
        {
            if (string.IsNullOrWhiteSpace(attendeesString))
            {
                return null;
            }

            try
            {
                return (new JavaScriptSerializer().Deserialize<EventAttendeeModel[]>(attendeesString)).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void RegisterWithPardot(EventRegisterModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.ScheduleFormHandler))
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                for (int i = 0; i < model.Attendees.Count; i++)
                {
                    var attendee = model.Attendees[i];
                    var registrationUrl =
                        string.Format(
                            "{0}?CourseName={1}&CourseDate={2}&CourseLocation={3}&FirstName={4}&Lastname={5}" +
                            "&Company={6}&Email={7}&Phone={8}",
                            model.ScheduleFormHandler, HttpUtility.UrlEncode(model.EventTitle),
                            HttpUtility.UrlEncode(model.ScheduleDate.ToString("yyyyMMdd")),
                            HttpUtility.UrlEncode(model.ScheduleLocation),
                            HttpUtility.UrlEncode(attendee.FirstName ?? ""),
                            HttpUtility.UrlEncode(attendee.Lastname ?? ""),
                            HttpUtility.UrlEncode(attendee.Company ?? ""),
                            HttpUtility.UrlEncode(attendee.Email ?? ""), HttpUtility.UrlEncode(attendee.Phone ?? ""));

                    try
                    {
                        HttpWebRequest http = (HttpWebRequest)WebRequest.Create(registrationUrl);
                        http.Method = "GET";
                        HttpWebResponse response = (HttpWebResponse)http.GetResponse();
                        Stream responseStream = response.GetResponseStream();

                        if (responseStream != null)
                        {
                            using (StreamReader sr = new StreamReader(responseStream))
                            {
                                sr.ReadToEnd();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
                            .Error("RegisterWithPardot failed.", ex);
                        EmailHelper.SendErrorEmail("Event/RegisterWithPardot failed for url '" + registrationUrl + ". " + ex.ToString());
                    }
                }
            }
        }
    }
}

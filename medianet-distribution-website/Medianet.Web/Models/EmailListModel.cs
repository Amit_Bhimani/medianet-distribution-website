﻿using System;
using System.IO;
using System.Web;
using FileHelpers;
using System.Linq;
using Medianet.Web.Helpers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Medianet.Web.MedianetListService;
using Medianet.Web.MedianetListEditService;
using FileHelpers.Dynamic;
using System.Data;
using System.ComponentModel;
using log4net;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class EmailListModel : ManageListBase
    {
        public EmailListModel()
        {
            base.distributionType = MedianetListService.DistributionType.Email;
        }

        #region Model

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string eMailAddress { get; set; }
        
        [Display(Name = "Reference Id")]
        public string ReferenceId { get; set; }
        
        [Display(Name = "Company")]
        public string Company { get; set; }
        
        [Display(Name = "Company job title")]
        public string CompanyJobTitle { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "Address line I")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address line II")]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PastedeMailAddresses { get; set; }

        #endregion

        #region Lists

        /// <summary>
        /// 
        /// </summary>
        public string SelectedeMail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SelectedeMailList { get; set; }

        #endregion

        #region Overrides

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        public override void GetRecipient(int ListId, int RecipientId)
        {
            using (var service = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                Medianet.Web.MedianetListService.Recipient[] recipients = service.Proxy.GetServiceListRecipients(ListId, this.session.sessionKey);

                Medianet.Web.MedianetListService.Recipient recipient = recipients.FirstOrDefault(r => r.Id == RecipientId);

                if (recipient == null)
                {
                    modelState.AddModelError("", "Recipient does not exist");
                }
                else
                {
                    this.eMailAddress = recipient.Address;
                    this.ReferenceId = recipient.ReferenceId;
                    this.Company = recipient.Company;
                    this.CompanyJobTitle = recipient.JobTitle;
                    this.FirstName = recipient.FirstName;
                    this.MiddleName = recipient.MiddleName;
                    this.LastName = recipient.LastName;
                    this.Phone = recipient.PhoneNumber;
                    this.AddressLine1 = recipient.AddressLine1;
                    this.AddressLine2 = recipient.AddressLine2;
                    this.RecipientId = RecipientId;
                    this.ListId = ListId;
                }
            }

            this.ListId = ListId;
            this.RecipientId = RecipientId;
            this.LoadListRecipients();
        }

        /// <summary>
        /// Used to add a single recipient to an existing list. 
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="recipient"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public override int AddServiceListRecipient()
        {
            int recipientId = 0;
            this.Error = "";

            Medianet.Web.MedianetListEditService.Recipient recipient = new MedianetListEditService.Recipient();
                recipient.ReferenceId = this.ReferenceId;
                recipient.Company = this.Company;
                recipient.JobTitle = this.CompanyJobTitle;
                recipient.FirstName = this.FirstName;
                recipient.MiddleName = this.MiddleName;
                recipient.LastName = this.LastName;
                recipient.PhoneNumber = this.Phone;
                recipient.AddressLine1 = this.AddressLine1;
                recipient.AddressLine2 = this.AddressLine2;
                recipient.Address = this.eMailAddress;

            try
            {
                using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
                {
                    recipientId = service.Proxy.AddServiceListRecipient(this.ListId.Value, recipient, this.session.sessionKey);
                }
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddServiceListRecipient failed.", e);
                this.Error = e.Message;
            }

            return recipientId;
        }

        /// <summary>
        /// Used to update an existing recipient in an existing list. 
        /// </summary>
        public override void UpdateServiceListRecipient()
        {
            this.Error = "";

            Medianet.Web.MedianetListEditService.Recipient recipient = new MedianetListEditService.Recipient();
                recipient.Id = this.RecipientId.Value;
                recipient.ReferenceId = this.ReferenceId;
                recipient.Company = this.Company;
                recipient.JobTitle = this.CompanyJobTitle;
                recipient.FirstName = this.FirstName;
                recipient.MiddleName = this.MiddleName;
                recipient.LastName = this.LastName;
                recipient.PhoneNumber = this.Phone;
                recipient.AddressLine1 = this.AddressLine1;
                recipient.AddressLine2 = this.AddressLine2;
                recipient.Address = this.eMailAddress;

            try
            {
                using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
                {
                    service.Proxy.UpdateServiceListRecipient(this.ListId.Value, recipient, this.session.sessionKey);
                }
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UpdateServiceListRecipient failed.", e);
                this.Error = e.Message;
            }
        }

        /// <summary>
        /// Generates a stream of comma concatenated rows to export with the recipients
        /// </summary>
        /// <param name="ListId"></param>
        /// <returns></returns>
        public override Stream GetRecipientsForExport(int ListId)
        {
            Medianet.Web.MedianetListService.Recipient[] recipients;

            using (var service = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                recipients = service.Proxy.GetServiceListRecipients(ListId, this.session.sessionKey);
            }

            List<CSVItem> items = new List<CSVItem>();

            items.Add(GenerateHeaderCsvRow());

            foreach (Medianet.Web.MedianetListService.Recipient recipient in recipients)
            {
                CSVItem item = new CSVItem();
                item.ServiceType = "EMAIL";
                item.Address = recipient.Address;
                item.AddressLine1 = recipient.AddressLine1;
                item.AddressLine2 = recipient.AddressLine2;
                item.Company = recipient.Company;
                item.FirstName = recipient.FirstName;
                item.LastName = recipient.LastName;
                item.MiddleName = recipient.MiddleName;
                item.PhoneNumber = recipient.PhoneNumber;
                item.ReferenceId = recipient.ReferenceId;
                item.Title = recipient.JobTitle;

                items.Add(item);
            }

            Stream result = new MemoryStream();
            FileHelperEngine engine = new FileHelperEngine(typeof(CSVItem));

            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);
            engine.WriteStream(tw, items);
            tw.Flush();

            memoryStream.Position = 0;
            return memoryStream;
        }

        #endregion
    }
}
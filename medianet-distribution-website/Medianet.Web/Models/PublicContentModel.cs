﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using System.Web.Script.Serialization;
using log4net;

namespace Medianet.Web.Models
{
    public class DailyDiary
    {
        /// <summary>
        /// 
        /// </summary>
        public string DiaryDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DiaryHeading { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DiarySummary { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string DiaryContent { get; set; }
    }

    public class AgendaItem
    {
        /// <summary>
        /// Caption
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Agenda link
        /// </summary>
        [AllowHtml]
        public string Url { get; set; }

        /// <summary>
        /// Date of ocurrance
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Location or place
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Summary
        /// </summary>
        [AllowHtml]
        public string Details { get; set; }
    }

    public class Topic
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }

    public class Speaker    
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LogoURL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }

    public class Schedule
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CourseDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Seats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Enrolments { get; set; }
    }

    public class TrainingItem
    {
        /// <summary>
        /// 
        /// </summary>
        public string CourseId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Summary { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string Price { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string ContactName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Topic[] Topics { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Speaker[] Speakers { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Schedule[] Schedules { get; set; }
    }

    public class PublicContentModel
    {
        /// <summary>
        /// Gets all the agenda activities for today
        /// </summary>
        /// <returns></returns>
        public static IList<AgendaItem> GetAgendaForToday()
        {
            var apiAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["PublicApiAddress"];

            try
            {
                var client = new RestClient(apiAddress);

                var request = new RestRequest(@"base/Agenda/GetAllEvents", Method.GET);

                IRestResponse<List<AgendaItem>> response = client.Execute<List<AgendaItem>>(request);

                var serializer = new JavaScriptSerializer();

                return serializer.Deserialize<List<AgendaItem>>(response.Content);
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return new List<AgendaItem>();
            }
        }

        /// <summary>
        /// Get active training courses
        /// </summary>
        /// <returns></returns>
        public static IList<TrainingItem> GetTrainingCourses()
        {
            var apiAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["PublicApiAddress"];

            try
            {
                var client = new RestClient(apiAddress);

                var request = new RestRequest(@"base/Training/GetActiveTrainings", Method.GET);

                IRestResponse<List<TrainingItem>> response = client.Execute<List<TrainingItem>>(request);

                var serializer = new JavaScriptSerializer();

                return serializer.Deserialize<List<TrainingItem>>(response.Content);
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return new List<TrainingItem>();
            }
        }

        /// <summary>
        /// Get editorial items
        /// </summary>
        /// <returns></returns>
        public static DailyDiary GetEditorialDiary()
        {
            var apiAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["PublicApiAddress"];

            try
            {
                var client = new RestClient(apiAddress);

                var request = new RestRequest(@"base/Diary/GetDiary", Method.GET);

                IRestResponse<DailyDiary> response = client.Execute<DailyDiary>(request);

                var serializer = new JavaScriptSerializer();

                return serializer.Deserialize<DailyDiary>(response.Content);
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return new DailyDiary();
            }
        }

        public static IList<WidgetItem> GetWidget(string page)
        {
            var apiAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["PublicApiAddress"];

            try
            {
                var client = new RestClient(apiAddress);
                var request = new RestRequest(@"base/DistributionWidget/GetByPage/" + page, Method.GET);

                IRestResponse<List<WidgetItem>> response = client.Execute<List<WidgetItem>>(request);

                var str = response.Content;

                if (str == "[{}]")
                {
                    str = "[]";
                }

                var serializer = new JavaScriptSerializer();

                return serializer.Deserialize<List<WidgetItem>>(str);
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return new List<WidgetItem>();
            }
        }
    }
}
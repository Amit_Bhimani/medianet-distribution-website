﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    public class WidgetItem
    {
        private string _type;
        private string _location;

        public string Type
        {
            get { return _type.ToLower().Replace(" ", ""); }
            set { _type = value; }
        }
        public string Title { get; set; }
        public string Location
        {
            get { return _location.Contains("http") ? _location : "http://" + _location; }
            set { _location = value; }
        }
        public string Content { get; set; }
    }
}
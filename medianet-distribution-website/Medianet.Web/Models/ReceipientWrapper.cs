﻿using System;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;

using Medianet.Web.MedianetListService;
using Medianet.Web.NopCommerceServices;

[Serializable()]
public class ReceipientWrapper : BaseEntity
{
    public ReceipientWrapper(Recipient recipient)
    {
        this.Id = recipient.Id;
        this.item = recipient;
    }

    public Recipient item { get; set; }
}
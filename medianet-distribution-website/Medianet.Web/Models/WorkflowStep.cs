﻿using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class WorkflowStep
    {
        public string Route { get; set; }

        public string MenuName {get;set;}


        public string WidgetPage { get; set; }
        
        public bool isInMenu { get; set; }

        public bool isDefault { get; set; }

        public bool isEnabled { get; set; }

        public Delegate onBeforeAction { get; set; }
        
        public Delegate onAfterAction { get; set; }

        public Delegate onAfterNext { get; set; }
        
        public Delegate onAfterPrevious { get; set; }

        public short Order { get; set; }
    }
}
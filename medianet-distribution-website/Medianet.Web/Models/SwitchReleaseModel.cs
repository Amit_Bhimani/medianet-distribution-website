﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Constants;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class SwitchReleaseModel
    {
        /// <summary>
        /// Release routes
        /// </summary>
        public static string[] ReleaseRoutes = new string[3] { "/wire/prepare", "/audio/prepare", "/sms/prepare"};

        /// <summary>
        /// Release keys
        /// </summary>
        public static string[] ReleaseKeys = new string[3] { NopCommerce.Wire_Key, NopCommerce.Audio_Key, NopCommerce.Sms_Key };

        /// <summary>
        /// From where the request comes
        /// </summary>
        public string ModelKeyFrom { get; set; }

        /// <summary>
        /// To where the request should be redirected
        /// </summary>
        public string ModelKeyTo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsRelease(string key)
        {
            if ( ReleaseKeys.Where(a => a == key).Count() == 1 )
            {
                return true;
            }
            else
                return false;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetRouteFromReleaseKey(string ToKeyRoute)
        {
            int index = 0;

            foreach (string key in ReleaseKeys)
            {
                if ( key == ToKeyRoute )
                {
                    return ReleaseRoutes[index];
                }
                index++;
            }
            return "/";
        }
    }
}
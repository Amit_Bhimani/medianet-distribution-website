﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;
using Medianet.Web.Helpers;
using Medianet.Web.NopCommerceServices;
using System.ComponentModel.DataAnnotations;
using Medianet.Web.MedianetGeneralService;
using Medianet.Constants;
using Medianet.Web.MedianetFileService;
using System.IO;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.MedianetCustomerService;
using log4net;
using System.Text;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class AudioModel : ReleaseModel
    {
        public AudioModel()
            : base()
        {
            this.needtoAddWebAndJournalistsPosting = true;
            this.isAudio = true;
            this.Priority = MedianetReleaseService.ReleasePriorityType.Standard;
            this.Tweet = "News Release:";

            #region eCommerce workflow steps

            this.Steps = new WorkflowStep[]
                {
                    new WorkflowStep() { Route = "prepare"  , MenuName = "Content", WidgetPage = "audio-step1", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Prepare) , onAfterNext = new onAfterCallBack(this.SavePreparePage), Order = 1 },
                    new WorkflowStep() { Route = "online"   , MenuName = "Platforms", WidgetPage = "audio-step2", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Online), onAfterNext = new onAfterCallBack(this.SaveOnlinePage), onAfterPrevious = new onAfterCallBack(this.SaveOnlinePage), Order = 2},
                    new WorkflowStep() { Route = "lists" , MenuName = "Targets", WidgetPage = "audio-step3", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Lists), Order = 3 },
                    new WorkflowStep() { Route = "finalise" , MenuName = "Finalise", WidgetPage = "audio-step4", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Finalise), Order = 4 },
                    new WorkflowStep() { Route = "checkout", WidgetPage = "", Order = 5 },
                    new WorkflowStep() { Route = "thanks", WidgetPage = "thanks", Order = 6 }
                };

            this.PageIndex = 0;
            this.PagerIndex = 0;
            this.TotalPages = 0;


            this.Setup(this.Steps);

            this.PriorityList = new List<SelectListItem>();
            this.SelectedAudioFiles = new List<string>();
            this.WebCategoryList = new List<SelectListItem>();
            this.SecondaryWebCategoryList = new List<SelectListItem>();

            #endregion
        }

        public override void Initialise()
        {
            AddDefaultProducts();
        }

        #region Model

        /// <summary>
        ///
        /// </summary>
        [Required]
        [Display(Name = "Release summary")]
        [AllowHtml]
        public string ReleaseSummary { get; set; }

        /// <summary>
        ///
        /// </summary>
        public IList<string> SelectedAudioFiles { get; set; }

        #endregion

        #region Pagination

        public int PagerIndex { get; set; }

        public int PageIndex { get; set; }

        public int TotalPages { get; set; }

        #endregion

        #region Release workflow steps data bindings

        /// <summary>
        /// Prepares the sms release
        /// </summary>
        public new virtual void Prepare()
        {
            this.isPrepare = true;
            this.isFinalise = false;
            this.isOnline = false;
            this.hasCheckedOut = false;

            #region Priorities

            IEnumerable<Medianet.Web.MedianetReleaseService.ReleasePriorityType> list = (IEnumerable<Medianet.Web.MedianetReleaseService.ReleasePriorityType>)Enum.GetValues(typeof(Medianet.Web.MedianetReleaseService.ReleasePriorityType));
            this.PriorityList = new List<SelectListItem>();

            foreach (Medianet.Web.MedianetReleaseService.ReleasePriorityType priority in list)
            {
                SelectListItem item = new SelectListItem();
                item.Value = priority.ToString();
                item.Text = priority.ToString();

                this.PriorityList.Add(item);
            }

            this.WebCategoryList = GetWebCategories();
            this.SecondaryWebCategoryList = GetWebCategories();

            #endregion

            #region Hours, Minutes, Seconds

            this.Hours = new List<SelectListItem>();
            this.Minutes = new List<SelectListItem>();            

            for (int i = 0; i < 24; i++)
            {
                SelectListItem item = new SelectListItem();
                    item.Value = i.ToString("00");
                    item.Text = i.ToString("00");

                this.Hours.Add(item);
            }

            for (int i = 0; i < 60; i++)
            {
                SelectListItem item = new SelectListItem();
                    item.Value = i.ToString("00");
                    item.Text = i.ToString("00");

                this.Minutes.Add(item);
            }

            #endregion

            #region Audio Files

            this.PageIndex = 1;
            this.LoadAudioFilesPage();

            #endregion

            base.Prepare();
            AddANRProduct();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="pageIndex"></param>
        public void LoadAudioFilesPage()
        {
            int startPos = (this.PageIndex - 1) * NopCommerce.AudioFiles_Page_Size;

            using (var nc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
            {
                this.AudioFiles = nc.Proxy.GetInboundFilesByDebtorNumber(this.session.debtorNumber, MessageType.RadioRelease, startPos, NopCommerce.AudioFiles_Page_Size, InboundFileSortOrderType.Caller, this.session.sessionKey);
            }

            this.TotalPages = (int)Math.Ceiling( (double)this.AudioFiles.TotalCount / NopCommerce.AudioFiles_Page_Size);
        }

        /// <summary>
        /// Returns a wav to play in the browser.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public AudioFileModel Play(int Id)
        {            
            string fileExtension;
            string fileName;
            int fileSize;
            int itemCount;
            Stream fileData;
            byte[] buffer;
            AudioFileModel file = new AudioFileModel();

            using (var nc = new ServiceProxyHelper<FileServiceClient, IFileService>())
            {
                nc.Proxy.GetInboundFileContent(ref Id, this.session.sessionKey, out fileExtension, out fileSize, out itemCount, out fileName, out fileData);

                file.FileName = fileName;
                file.FileExtension = fileExtension;
                file.ContentType = new System.Net.Mime.ContentDisposition
                {
                    FileName = fileName,
                    Inline = false
                };

                buffer = new byte[fileSize];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = fileData.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    file.FileArrayData = ms.ToArray();
                }
                fileData.Close();
            }

            return file;
        }

        /// <summary>
        /// Saves after next on step I
        /// </summary>
        /// <param name="model"></param>
        protected void SavePreparePage(ReleaseModel model)
        {
            AudioModel submitedModel = (AudioModel)model;

            /// changing the priority modifies in 20 % the price.
            bool reCalculateCart = (submitedModel.Priority != this.Priority);

            this.isPrepared = true;

            #region Merge submited model

            this.BillingCode = submitedModel.BillingCode;
            this.SubjectHeadline = submitedModel.SubjectHeadline;
            this.Organisation = submitedModel.Organisation;
            this.CustomerRef = submitedModel.CustomerRef;
            this.Priority = submitedModel.Priority;

            this.DistributeAsFutureRelease = submitedModel.DistributeAsFutureRelease;
            this.DistributeAsEmbargoRelease = submitedModel.DistributeAsEmbargoRelease;
            this.ReleaseSummary = submitedModel.ReleaseSummary;
            this.ScheduleDate = submitedModel.ScheduleDate;
            this.ScheduleTimeHour = submitedModel.ScheduleTimeHour;
            this.ScheduleTimeMinutes = submitedModel.ScheduleTimeMinutes;
            this.EmbargoDate = submitedModel.EmbargoDate;
            this.EmbargoTimeHour = submitedModel.EmbargoTimeHour;
            this.EmbargoTimeMinutes = submitedModel.EmbargoTimeMinutes;

            this.WebCategoryId = submitedModel.WebCategoryId;
            this.SecondaryWebCategoryId = submitedModel.SecondaryWebCategoryId;
            this.isPrepared = true;

            #endregion

            #region Release summary

            int emailWireTextChekSumUpdate = 0;

            if (String.IsNullOrEmpty(this.ReleaseSummary))
            {
                emailWireTextChekSumUpdate = this.EmailWireTextCheckSum;
                this.EmailwireTextTempFileUploadId = 0;
            }
            else
                emailWireTextChekSumUpdate = this.ReleaseSummary.GetHashCode();

            /// Upload to medianet
            if (this.EmailWireTextCheckSum != emailWireTextChekSumUpdate)
            {
                ///// Decrement previous values
                //if ( this.WireRelease != null )
                //{
                //    this.TotalPages -= this.WireRelease.Pages;
                //    this.Words -= this.WireRelease.WordCount;
                //}

                byte[] byteArray = Encoding.UTF8.GetBytes(this.ReleaseSummary);

                MemoryStream stream = new MemoryStream(byteArray);

                this.UploadForConversion(stream, "release_summary.html", AttachmentTypeEnum.Release, true);

                this.EmailWireTextCheckSum = emailWireTextChekSumUpdate;
            }

            #endregion

            if ( reCalculateCart )
            {
                this.UpdateCartAfterOperation();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="Id"></param>
        public void SelectAudioFile(string NameId)
        {
            int index = this.SelectedAudioFiles.IndexOf(NameId);

            if (index == -1)
            {
                this.SelectedAudioFiles.Add(NameId);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="Id"></param>
        public void UnSelectAudioFile(string NameId)
        {
            int index = this.SelectedAudioFiles.IndexOf(NameId);

            if (index != -1)
            {
                this.SelectedAudioFiles.Remove(NameId);
            }
        }

        /// <summary>
        /// Prepares the Social page
        /// </summary>
        public void Online()
        {
            this.isPrepare = false;
            this.isOnline = true;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.OnlineProductList = npc.Proxy.GetOnlineProducts().Where(p => p.DistributionType != NopCommerce.CartDistType_Wire).ToList();
            }
        }

        /// <summary>
        /// In this action we get the stuff not on the online page from the model.
        /// </summary>
        protected void SaveOnlinePage(ReleaseModel model)
        {
            AudioModel submitedModel = (AudioModel)model;

            this.Tweet = submitedModel.Tweet;
        }

        /// <summary>
        /// Prepares the lists selection
        /// </summary>
        public void Lists()
        {
            this.isPrepare = false;
            this.isOnline = false;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.RecommendationList = nc.Proxy.GetShoppingCartRecommendations(session.nopCommerceCustomerId);
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                this.CategoryList = nc.Proxy.GetTopRadioCategories();
            }

            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        /// <summary>
        /// Finalise audio release
        /// </summary>
        public void Finalise()
        {
            this.isPrepare = false;
            this.isOnline = false;
            this.isFinalise = true;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
            }

            this.WebCategoryList = GetWebCategories();
            this.SecondaryWebCategoryList = GetWebCategories();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        #endregion

        #region Shopping Cart

        /// <summary>
        /// Calculates Sms release price
        /// </summary>
        public override void CalculatePrice()
        {
            var request = new MedianetReleaseService.ReleaseQuoteRequest();
            request.MultimediaType = MedianetReleaseService.MultimediaType.Audio;
            request.DebtorNumber = session.debtorNumber;
            request.Priority = this.Priority;
            request.ReleaseDescription = this.SubjectHeadline;
            request.System = MedianetReleaseService.SystemType.Medianet;

            #region Adding products

            request.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

            #endregion

            #region Adding Documents

            request.Documents = GenerateDocuments().ToArray();

            #endregion

            try
            {
                using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    ReleaseQuoteResponse response = rsc.Proxy.QuoteRelease(request, session.sessionKey);

                    #region Refreshing shopping cart items price

                    foreach (TransactionQuoteResponse quoted in response.Transactions)
                    {
                        this.shoppingCart[quoted.SequenceNumber - 1].Price = quoted.SubTotalEx + quoted.GST;
                    }

                    #endregion

                    this.TotalPrice = response.Total;
                    this.QuoteReferenceId = response.QuoteReferenceId;
                }
            }
            catch (Exception e)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
                this.Error = "*** Shopping Cart currently unavailable ****<br><br>Please contact Medianet on 1300 616 813 or <a href='mailto:team@medianet.com.au'>team@medianet.com.au</a> for final pricing.<br><br>Press releases can still be submitted through the portal.<br><br>We apologise for the inconvenience and are working hard to have this resolved as quickly as possible."; //e.Message;
            }
        }

        /// <summary>
        /// Initiates a payment
        /// </summary>
        public override void Payment()
        {
            this.isPrepare = false;
            this.hasCheckedOut = true;

            if (this.PaymentResponse == null)
            {
                using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
                {
                    this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
                }

                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    var paymentRequest = new PaymentStartRequest();
                    paymentRequest.ReleaseQuote = new MedianetReleaseService.ReleaseQuoteRequest();
                    paymentRequest.ReleaseQuote.DebtorNumber = session.debtorNumber;
                    paymentRequest.ReleaseQuote.MultimediaType = MedianetReleaseService.MultimediaType.Audio;
                    paymentRequest.ReleaseQuote.Priority = this.Priority;
                    paymentRequest.ReleaseQuote.ReleaseDescription = this.SubjectHeadline;
                    paymentRequest.ReleaseQuote.BillingCode = CleanBillingCode(this.BillingCode);
                    paymentRequest.ReleaseQuote.CustomerReference = this.CustomerRef;
                    paymentRequest.ReleaseQuote.System = MedianetReleaseService.SystemType.Medianet;
                    paymentRequest.RedirectURL = String.Format("{0}://{1}/audio/Thanks", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST"));
                    #region Adding services

                    paymentRequest.ReleaseQuote.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

                    #endregion

                    #region Adding document

                    paymentRequest.ReleaseQuote.Documents = GenerateDocuments().ToArray();

                    #endregion

                    try
                    {
                        this.PaymentResponse = mpc.Proxy.InitialisePayment(paymentRequest, session.sessionKey);
                        this.QuoteReferenceId = this.PaymentResponse.QuoteReferenceId;
                    }
                    catch (Exception e)
                    {
                        ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                        log.Error(null, e);
                        this.Error = e.Message;
                    }
                }
            }
        }

        /// <summary>
        /// Submits a release
        /// </summary>
        public override void SubmitRelease()
        {
            #region Preparing release

            ReleaseSummary release = new ReleaseSummary();
            release.DebtorNumber = session.debtorNumber;
            release.System = MedianetReleaseService.SystemType.Medianet;
            release.BillingCode = CleanBillingCode(this.BillingCode);
            release.ReleaseDescription = this.SubjectHeadline;
            release.Organisation = this.Organisation;
            release.CustomerReference = this.CustomerRef;
            release.HoldUntilDate = this.DistributeAsFutureRelease == true ? this.HoldUntilDate : null;
            release.EmbargoUntilDate = this.DistributeAsEmbargoRelease == true ? this.EmbargoUntilDate : null;
            release.Priority = this.Priority;
            release.MultimediaType = Medianet.Web.MedianetReleaseService.MultimediaType.Audio;
            release.ReportResultsToSend = session.customerResultType;
            release.EmailFromAddress = NopCommerce.Medianet_Sender;
            release.WebCategoryId = null;
            release.QuoteReferenceId = this.QuoteReferenceId;
            release.TimezoneCode = session.TimeZone.Code;
            release.IsVerified = true;
            release.WebCategoryId = this.WebCategoryId;
            release.TransactionId = this.TransactionId;

            if (session.userReportSendMethod == ReportType.Email || session.userReportSendMethod == ReportType.Both)
            {
                release.ReportEmailAddress = session.customerEmailAddress;
            }

            #endregion

            #region Adding services

            release.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

            if (release.Transactions.Any(t => t.IsSecondaryWebCategory))
            {
                release.SecondaryWebCategoryId = this.SecondaryWebCategoryId;
            }
            #endregion

            #region Adding document

            release.Documents = GenerateDocuments().ToArray();

            #endregion

            try
            {
                using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    rsc.Proxy.AddReleaseFromSummary(release, session.sessionKey);
                }
            }
            catch (Exception e)
            {
                this.Error = e.Message;
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
            }

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                nc.Proxy.CleanCartAndCreateOrders(session.nopCommerceCustomerId, this.TotalPrice);
            }
        }

        #endregion

        #region Produts search and browse and more.

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> PriorityList;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Hours;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Minutes;

        /// <summary>
        /// List get from mediant customer list
        /// </summary>
        [NonSerialized]
        public IList<ProductDTO> CustomList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<NopCommerceServices.Category> CategoryList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<ProductDTO> ProductList;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public InboundFilePage AudioFiles;

        private List<MedianetReleaseService.DocumentSummary> GenerateDocuments()
        {
            int documentIndex = 0;
            var documents = new List<MedianetReleaseService.DocumentSummary>();

            #region Email text wire

            if (this.EmailwireTextTempFileUploadId != 0)
            {
                var emailWiredoc = new MedianetReleaseService.DocumentSummary();
                emailWiredoc.SequenceNumber = documentIndex;
                emailWiredoc.CanDistributeViaEmailBody = true;
                emailWiredoc.CanDistributeViaWire = true;
                emailWiredoc.TempFileId = this.EmailwireTextTempFileUploadId;

                documents.Add(emailWiredoc);
                documentIndex++;
            }

            #endregion

            #region Press Release

            if (this.PressRelease != null)
            {
                var pressRelease = new MedianetReleaseService.DocumentSummary();
                pressRelease.SequenceNumber = documentIndex;
                pressRelease.CanDistributeViaWeb = true;
                pressRelease.TempFileId = this.PressRelease.MedianetAttachmentId;

                documents.Add(pressRelease);
                documentIndex++;
            }

            #endregion

            #region Audio files

            foreach (AttachmentModel attachment in this.Attachments)
            {
                var attach = new MedianetReleaseService.DocumentSummary();
                attach.SequenceNumber = documentIndex;
                attach.CanDistributeViaEmailAttachment = true;
                attach.Description = attachment.Name;
                attach.TempFileId = attachment.MedianetAttachmentId;

                documents.Add(attach);
                documentIndex++;
            }

            #endregion

            #region Inbound Files

            foreach (string inboundFile in this.SelectedAudioFiles)
            {
                int inboundFileId = int.Parse(inboundFile.Split('|')[1].ToString());

                var audioFileSummary = new MedianetReleaseService.DocumentSummary();
                audioFileSummary.SequenceNumber = documentIndex;
                audioFileSummary.CanDistributeViaEmailAttachment = true;
                audioFileSummary.InboundFileId = inboundFileId;

                documents.Add(audioFileSummary);
                documentIndex++;
            }

            #endregion

            return documents;
        }

        #endregion

        #region Workflow engine overrides

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toStep"></param>
        /// <returns></returns>
        public override bool CanTransition()
        {
            bool canTransition = true;

            if (this.workflowStep.Value.Route == "prepare")
            {
                return true;
            }
            else if (this.workflowStep.Value.Route == "online")
            {
                canTransition = this.ValidatePrepare();
            }
            else if (this.workflowStep.Value.Route == "lists")
            {
                canTransition = this.ValidateOnline();
            }
            else if (this.workflowStep.Value.Route == "finalise")
            {
                canTransition = this.ValidateCart();
            }
            else if (this.workflowStep.Value.Route == "payment")
            {
                if (IsTwitterCorrect())
                    canTransition = this.ValidateCanPay();
                else
                    canTransition = false;
            }
            else if (this.workflowStep.Value.Route == "confirm")
            {
                if (IsTwitterCorrect())
                    canTransition = this.ValidateCanConfirm();
                else
                    canTransition = false;
            }
            else if (this.workflowStep.Value.Route == "checkout")
            {
                if (IsTwitterCorrect())
                    canTransition = this.ValidateCanPay();
                else
                    canTransition = false;
            }
            else if (this.workflowStep.Value.Route == "thanks")
            {
                canTransition = this.ValidateCart();
            }

            return canTransition;
        }

        /// <summary>
        /// Validates the prepare step of the workflow
        /// </summary>
        /// <returns></returns>
        protected new internal virtual bool ValidatePrepare()
        {
            if (
                    ( this.Attachments.Count == 0 ) &&
                    ( this.SelectedAudioFiles.Count == 0 )
                )
            {
                modelState.AddModelError("Attachments", "You need to upload one Audio file at least");
            }

            if (this.DistributeAsFutureRelease)
            {
                if (!this.ScheduleDate.HasValue)
                {
                    modelState.AddModelError("ScheduleDate", "If you plan a future release, please select the date");
                }
            }

            if (this.DistributeAsEmbargoRelease)
            {
                if (!this.EmbargoDate.HasValue)
                {
                    modelState.AddModelError("EmbargoDate", "If you wish to embargo this release, please select the date");
                }
            }
            
            return base.ValidatePrepare();
        }

        protected internal virtual bool ValidateOnline()
        {
            // Ignore all the model validation errors for the prepare page
            this.modelState.Clear();

            if (this.IsTwitterPostingAdded())
            {
                if (string.IsNullOrWhiteSpace(this.Tweet))
                    modelState.AddModelError("Tweet", "Please enter your tweet text");
            }

            return this.modelState.IsValid;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        protected internal override bool ValidateCart(bool MustContainIncludedItems= false)
        {
            if (this.shoppingCart.Count == 0)
            {
                this.modelState.AddModelError("shoppingCart", "You need to add at least one product, special, customer contacts or extra recipient");
                return false;
            }

            if (MustContainIncludedItems && shoppingCart.Count(s => s.Included) == 0)
            {
                this.modelState.AddModelError("shoppingCart", "You need to add at least one product, special, customer contacts or extra recipient.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool ValidReleaseContent()
        {
            return (!string.IsNullOrEmpty(SubjectHeadline)) && (!string.IsNullOrEmpty(ReleaseSummary));
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    public enum AttachmentTypeEnum
    {
        Database = 0, Release = 1,Other = 2 , PressRelease = 3
    }

    [Serializable]
    public class AttachmentModel
    {
        public AttachmentModel(AttachmentTypeEnum type, int id, int count, int pages, decimal weight,string name,string path)
        {
            this.AttachmentType = type;
            this.MedianetAttachmentId = id;
            this.WordCount = count;
            this.Path = path;
            this.Pages = pages;
            this.Name= name;
            this.Weight = weight;
        }

        /// <summary>
        /// 1 = Release , 2 = Other attachment
        /// </summary>
        public AttachmentTypeEnum AttachmentType { get; set; }

        /// <summary>
        /// Internal Id pointing to medianet file id when it's uploaded.
        /// </summary>
        public int MedianetAttachmentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int WordCount {get;set;}

        /// <summary>
        /// 
        /// </summary>
        public int Pages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Weight { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }
}
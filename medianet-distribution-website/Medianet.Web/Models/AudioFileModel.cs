﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net.Mime;

namespace Medianet.Web.Models
{
    public class AudioFileModel
    {
        /// <summary>
        ///
        /// </summary>
        public ContentDisposition ContentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileExtension {get;set;} 
        
        /// <summary>
        /// 
        /// </summary>
        public int FileSize {get;set;} 
        
        /// <summary>
        /// 
        /// </summary>
        public int ItemCount {get;set;}
        
        /// <summary>
        /// 
        /// </summary>
        public string FileName {get;set;}        

        /// <summary>
        /// 
        /// </summary>
        public Stream FileData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public byte[] FileArrayData { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileHelpers;

namespace Medianet.Web.Models
{
    [DelimitedRecord(",")]
    public class CSVItem
    {
        public string ServiceType;

        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Address;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string ReferenceId;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Company;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string JobTitle;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string Title;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string FirstName;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string MiddleName;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string LastName;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string PhoneNumber;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine1;

        [FieldOptional, FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string AddressLine2;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Constants;
using Medianet.Web.NopCommerceServices;

namespace Medianet.Web.Models
{
    public class ShoppingCartItemAttributeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsPackage { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsProduct { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsCustomerList { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsReceipient { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsNewsHubPosting { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsTwitterPosting { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsJournalistPosting { get; set; }

        public bool IsDefaultPosting { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int AddressesCount { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string DistributionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAttachmentFee { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSecondaryWebCat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsHighPriorityFee { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAnrFee { get; set; }

        public ShoppingCartItemAttributeModel(ShoppingCartItemDTO item)
        {
            string[] attribs = item.AttributesXml.Split(',');

            this.DistributionType = attribs[1];
            this.AddressesCount = int.Parse(attribs[2]);
            this.IsProduct = (attribs[0] == NopCommerce.CartItem_Service);
            this.IsPackage = (attribs[0] == NopCommerce.CartItem_Package);
            this.IsCustomerList = (attribs[0] == NopCommerce.CartItem_CustomerList);
            this.IsReceipient = (attribs[0] == NopCommerce.CartItem_SingleRecipient);
            this.IsAttachmentFee = (attribs[0] == NopCommerce.CartItem_Attachment);
            this.IsAnrFee = (attribs[0] == NopCommerce.CartItem_ANR);
            this.IsNewsHubPosting = (item.ProductId == NopCommerce.NewsHubPosting_Product_Id || item.HasNewsHubServices);
            this.IsJournalistPosting = (item.ProductId == NopCommerce.JournalistPosting_Product_Id || item.HasJournalistsServices);
            this.IsTwitterPosting = (item.ProductId == NopCommerce.TwitterPosting_Product_Id || item.HasTwitterServices);
            this.IsSecondaryWebCat = item.ProductId == NopCommerce.SecondaryWebCategoryCharge_Product_Id;
            this.IsDefaultPosting = (item.ProductId == NopCommerce.Default_Product_Id);
            this.IsHighPriorityFee = (item.ProductId == NopCommerce.HighPriorityCharge_Product_Id);

            if (this.IsReceipient)
            {
                this.AddressesCount = 1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        public ShoppingCartItemAttributeModel(Product product)
        {
            this.DistributionType = product.DistributionType;
            this.AddressesCount = product.Addresses;
            this.IsProduct = !product.isPackage;
            this.IsPackage = product.isPackage;
            this.IsCustomerList = false;
            this.IsReceipient = false;
            this.IsAttachmentFee = false;
            this.IsAnrFee = false;
            this.IsNewsHubPosting = (product.Id == Medianet.Constants.NopCommerce.NewsHubPosting_Product_Id || product.HasNewsHubServices);
            this.IsJournalistPosting = (product.Id == Medianet.Constants.NopCommerce.JournalistPosting_Product_Id || product.HasJournalistsServices);
            this.IsTwitterPosting = (product.Id == Medianet.Constants.NopCommerce.TwitterPosting_Product_Id || product.HasTwitterServices);
            this.IsSecondaryWebCat = (product.Id == NopCommerce.SecondaryWebCategoryCharge_Product_Id);
            this.IsDefaultPosting = (product.Id == Medianet.Constants.NopCommerce.Default_Product_Id);
            this.IsHighPriorityFee = (product.Id == NopCommerce.HighPriorityCharge_Product_Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductType()
        {
            if (this.IsPackage)
                return "Special";
            else if (this.IsReceipient)
                return "Extra recipient";
            else
                return GetCustomerListProductType(DistributionType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="distributionType"></param>
        /// <returns></returns>
        private string GetCustomerListProductType(string distributionType)
        {
            switch (distributionType)
            {
                case NopCommerce.CartDistType_Email:
                    return "Email";
                case NopCommerce.CartDistType_Wire:
                    return "Newswire";
                case NopCommerce.CartDistType_Sms:
                    return "SMS";
                case NopCommerce.CartDistType_Internet:
                    return "Web";
                case NopCommerce.CartDistType_Twitter:
                    return "Twitter";
                case NopCommerce.CartDistType_AnrFee:
                    return "Audio News Release Charge";
                case NopCommerce.CartDistType_AttachmentFee:
                    return "Attachment Charge";
                case NopCommerce.CartDistType_PriorityFee:
                    return "High Priority Fee";
            }

            return "Other";
        }
    }
}
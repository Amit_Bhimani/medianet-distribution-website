﻿using System;
using System.Collections;
using System.Configuration;
using System.Web;

namespace Medianet.Web.Models
{
    [Serializable]
    public class PaginationModel : ReleaseModel
    {
        public int CurrentPageIndex { get; set; }
        public int StartPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int NumericLinks { get; set; }
        public String QueryParameters { get; set; }

        private static Hashtable SearchConfig = ConfigurationManager.GetSection("MonitoringSearch") as Hashtable;
        public PaginationModel(String baseUrl)
        {
            QueryParameters = baseUrl;
            PageSize = int.Parse(SearchConfig["DefaultSearchResultPageSize"].ToString());
            NumericLinks = int.Parse(SearchConfig["NumericLinksNumber"].ToString());
            CurrentPageIndex = 1;
            StartPage = 1;
            TotalPages = 10;
        }

        public int SessionTotalResults
        {
            get
            {
                if (HttpContext.Current.Session["PaginationTotalResults"] != null)
                {
                    return (int)(HttpContext.Current.Session["PaginationTotalResults"]);
                }
                return 0;
            }
            set
            {
                HttpContext.Current.Session["PaginationTotalResults"] = value;
            }
        }

        public void SetPagination(int newActivepage,int totalResults,int pageSize,bool routeHasPagination)
        {
            TotalPages = (int)(Math.Ceiling((double)totalResults / pageSize));
            PageSize = pageSize;
            if (routeHasPagination)
            {
                if(HttpContext.Current.Session["PaginationStartPage"]!=null)
                {
                    StartPage = int.Parse(HttpContext.Current.Session["PaginationStartPage"].ToString());
                }
            }
            else
            {
                StartPage = newActivepage;
            }
            CurrentPageIndex = newActivepage;

            //Populate the start value of the pager index
            StartPage = newActivepage - (int)Math.Floor((double)NumericLinks / 2);
            if(StartPage<1)
            {
                StartPage = 1;
            }
            else if (StartPage+NumericLinks-1 > TotalPages)
            {
                StartPage = TotalPages - NumericLinks + 1;
            }

            HttpContext.Current.Session["PaginationStartPage"] = StartPage;
        }

    }
}
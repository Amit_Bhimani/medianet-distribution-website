﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Medianet.Web.Helpers.Caching;
using RestSharp;
using log4net;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class BaseModel
    {
        public const string CacheTopPackages = "top-packages";
        public const string CacheTopListsWeek = "top-lists-w";
        public const string CacheTopListsMonth = "top-lists-m";
        public const string CacheTrainingCourses = "training";
        public const string CacheAgenda = "agenda";
        public const string CacheDiary = "diary";

        /// <summary>
        /// 
        /// </summary>
        public SessionModel session { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool isRelease
        {
            get
            {
                #if (!DEBUG)
                    return true;
                #else
                    return false;
                #endif
            }
        }

        public static IList<WidgetItem> GetWidgets(string page)
        {
            try
            {
                return Cacher.Get("widget-" + page, 600, () => GetWidgetFromBase(page));
                //return GetWidgetFromBase(page);
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return new List<WidgetItem>();
            }
        }

        public static IList<WidgetItem> GetWidgetFromBase(string page)
        {
            var apiAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["PublicApiAddress"];

            try
            {
                var client = new RestClient(apiAddress);

                var request = new RestRequest(@"base/DistributionWidget/GetByPage/" + page, Method.GET);

                IRestResponse<List<WidgetItem>> response = client.Execute<List<WidgetItem>>(request);

                var str = response.Content;

                if (str == "[{}]")
                {
                    str = "[]";
                }

                str = str.Replace(@"src=\u0026quot;/media/",
                                  "src=\\u0026quot;" +
                                  System.Web.Configuration.WebConfigurationManager.AppSettings["WidgetMediaAddress"] +
                                  "/media/");

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                return serializer.Deserialize<List<WidgetItem>>(str);
            }
            catch (Exception ex)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                return new List<WidgetItem>();
            }
        }

        #region MVC Model State

        /// <summary>
        /// Access to the controller model state object to add error messages..
        /// </summary>
        public ModelStateDictionary modelState { get; set; }

        #endregion

        /// <summary>
        /// to send an error message before an operation to the view.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// to send a warning message before an operation to the view.
        /// </summary>
        public string Warning { get; set; }
    }
}
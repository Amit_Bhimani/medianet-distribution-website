﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    /// <summary>
    /// Ajax Response Model
    /// </summary>
    public class AjaxResponseModel
    {
        public bool Status { get; set; }
        public bool IsVerified { get; set; }
        public string Message { get; set; }
    }
}
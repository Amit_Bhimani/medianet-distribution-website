﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Medianet.Web.Models
{
    [DataContract]
    public class ReCaptchaResponse
    {
        [DataMember(Name = "success")]
        public string Success { get; set; }

        [DataMember(Name = "challenge_ts")]
        public string ChallengeTs { get; set; }

        [DataMember(Name = "hostname")]
        public string Hostname { get; set; }

        [DataMember(Name = "error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web;
using Medianet.Web.Validators;
using Medianet.Web.Helpers;
using Medianet.Constants;
using log4net;
using Medianet.Web.MedianetChargeBeeService;

namespace Medianet.Web.Models
{
    /// <summary>
    /// Change password model
    /// </summary>
    public class ChangePasswordModel : BaseModel
    {
        public const int MinPasswordLength = 8;
        public const int MaxPasswordLength = 16;
        public const string PasswordError = "Please enter password 8 - 16 characters in length and made up of alpha and numeric characters.";

        [Required]
        [StringLength(MaxPasswordLength, ErrorMessage = PasswordError, MinimumLength = MinPasswordLength)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        [RegularExpression("^((?=.*[a-zA-Z])(?=.*\\d)).+$", ErrorMessage = PasswordError)]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    /// <summary>
    /// Logon Model
    /// </summary>
    public class LogOnModel : BaseModel
    {
        [Required]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    /// <summary>
    /// Workaround for checkbox validation bug in mvc client side validation
    /// </summary>
    public class BooleanRequired : RequiredAttribute, IClientValidatable
    {
        public BooleanRequired()
        {

        }

        public override bool IsValid(object value)
        {
            return value != null && (bool)value == true;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new ModelClientValidationRule[] { new ModelClientValidationRule() { ValidationType = "brequired", ErrorMessage = this.ErrorMessage } };
        }
    }

    /// <summary>
    /// Customer register second step 
    /// </summary>
    [Serializable]
    public class RegisterDetailModel : BaseModel
    {
        #region Model
        
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [AllowedEmail]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        
        [Required]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Company")]
        public string Company { get; set; }

        [ABN(ErrorMessage = @"The field ABN must be a string with a maximum length of 11")]
        [Display(Name = "ABN")]
        public string ABN { get; set; }

        [Required]
        [Display(Name = "Industry")]
        public string Industry { get; set; }
        
        [Required]
        [Display(Name = "Position")]
        public string Position { get; set; }
        
        [Required]
        [Display(Name = "Phone")]
        [RegularExpression(@"^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$", ErrorMessage = "The phone number must be input in this format : 0X23456789 where X can be 2,4,3,7,8")]
        public string Phone { get; set; }
        
        [Required]
        [Display(Name = "Address")]
        [StringLength(50)]
        public string Address { get; set; }
        
        [StringLength(50)]
        public string Address2 { get; set; }
        
        [Required]
        [Display(Name = "Suburb")]
        [StringLength(30)]
        public string Suburb { get; set; }
        
        [Required]
        [StringLength(4)]
        [Display(Name = "Post code")]
        public string PostCode { get; set; }
        
        [Required]
        [Display(Name = "State")]
        public string State { get; set; }
        
        [Required]
        [Display(Name = "Country")]
        public string Country { get; set; }
        
        [BooleanRequired(ErrorMessage = "You must accept Medianet's terms and conditions.")]
        [Display(Name = "I agree to Medianet's Term & Conditions")]
        public bool AgreeConditions { get; set; }
        
        [Required]
        [StringLength(ChangePasswordModel.MaxPasswordLength, ErrorMessage = ChangePasswordModel.PasswordError, MinimumLength = ChangePasswordModel.MinPasswordLength)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression("^((?=.*[a-zA-Z])(?=.*\\d)).+$", ErrorMessage = ChangePasswordModel.PasswordError)]
        public string Password { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


        public Medianet.Web.MedianetCustomerService.CustomerBillingType AccountType { get; set; }

        #endregion

        #region Lists

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Industries;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> States;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Countries;

        #endregion

        #region Prepare

        /// <summary>
        /// Register prepare
        /// </summary>
        public void Prepare()
        {
            #region Industries

            this.Industries = new List<SelectListItem>();
            using (var ncc = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                IList<IndustryCode> industryList = ncc.Proxy.GetIndustryCodes();

                foreach (IndustryCode code in industryList)
                {
                    this.Industries.Add(new SelectListItem() { Text = code.Description.Trim(), Value = code.Code.ToString() });
                }
            }


            #endregion

            #region States

            this.States = new List<SelectListItem>();
            this.States.Add(new SelectListItem() { Text = "New South Wales", Value = "NSW", Selected = true });
            this.States.Add(new SelectListItem() { Text = "Northern Territory", Value = "NT" });
            this.States.Add(new SelectListItem() { Text = "Australian Capital Territory", Value = "ACT" });
            this.States.Add(new SelectListItem() { Text = "Victoria", Value = "VIC" });
            this.States.Add(new SelectListItem() { Text = "Queensland", Value = "QLD" });
            this.States.Add(new SelectListItem() { Text = "Western Australia", Value = "WA" });
            this.States.Add(new SelectListItem() { Text = "South  Australia", Value = "SA" });
            this.States.Add(new SelectListItem() { Text = "Tasmania", Value = "TAS" });

            this.Countries = new List<SelectListItem>();
            this.Countries.Add(new SelectListItem() { Text = "Australia", Value = "Australia", Selected = true });

            #endregion
        }

        #endregion
    }

    /// <summary>
    /// To Edit an account
    /// </summary>
    [Serializable]
    public class EditDetailModel : BaseModel
    {
        #region Model
        
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }
        
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string UserLogonName { get; set; }
        
        [Required]
        [Display(Name = "Address")]
        [StringLength(50)]
        public string Address { get; set; }
        
        [StringLength(50)]
        public string Address2 { get; set; }
        
        [Required]
        [StringLength(4)]
        [Display(Name = "Post code")]
        public string PostCode { get; set; }
        
        [Required]
        [Display(Name = "Suburb")]
        [StringLength(30)]
        public string Suburb { get; set; }
        
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        
        [Required]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }
        
        [ABN(ErrorMessage = @"The field ABN must be a string with a maximum length of 11")]
        [Display(Name = "ABN")]
        public string ABN { get; set; }
        
        [DataType(DataType.Text)]
        [Display(Name = "Company")]
        public string Company { get; set; }
        
        [Required]
        [Display(Name = "Position")]
        public string Position { get; set; }
        
        [Required]
        [Display(Name = "Phone")]
        [RegularExpression(@"^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$", ErrorMessage = "The phone number must be input in this format : 0X23456789 where X can be 2,4,3,7,8")]
        public string Phone { get; set; }
        
        [Required]
        [Display(Name = "Industry")]
        public string Industry { get; set; }
        
        [Required]
        [Display(Name = "State")]
        public string State { get; set; }
        
        public string ErrorMessage { get; set; }
        
        public bool IsUpgrade { get; set; }
        
        public Medianet.Web.MedianetCustomerService.CustomerBillingType AccountType { get; set; }

        #endregion

        #region Lists

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Industries;

        /// <summary>
        /// 
        [NonSerialized]
        public IList<SelectListItem> States;

        [NonSerialized]
        public IList<WidgetItem> WidgetList;

        #endregion

        /// <summary>
        /// Loads the customer information to edit
        /// </summary>
        public void Prepare()
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                Account account = cs.Proxy.GetAccountFromChargeBee(session.MedianetUserId, session.sessionKey);
                this.ABN = account.ABN;
                this.Address = account.CompanyAddress.AddressLine1;
                this.Address2 = account.CompanyAddress.AddressLine2;
                this.PostCode = account.CompanyAddress.Postcode;
                this.State = account.CompanyAddress.State;
                this.Suburb = account.CompanyAddress.City;
                this.Email = account.EmailAddress;
                this.FirstName = account.FirstName;
                this.Lastname = account.LastName;
                this.Phone = account.TelephoneNumber;
                this.Position = account.ContactPosition;
                this.Company = account.CompanyName;
                this.Industry = account.IndustryCode;
                this.UserLogonName = account.UserLogonName;
                this.AccountType = (Medianet.Web.MedianetCustomerService.CustomerBillingType)account.AccountType;
            }

            #region Industries

            this.Industries = new List<SelectListItem>();
            using (var ncc = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                IList<IndustryCode> industryList = ncc.Proxy.GetIndustryCodes();

                foreach (IndustryCode code in industryList)
                {
                    this.Industries.Add(new SelectListItem() { Text = code.Description.Trim(), Value = code.Code.ToString(), Selected = (code.Code == this.Industry) });
                }
            }

            #endregion

            #region States

            this.States = new List<SelectListItem>();
            this.States.Add(new SelectListItem() { Text = "New South Wales", Value = "NSW", Selected = true });
            this.States.Add(new SelectListItem() { Text = "Northern Territory", Value = "NT" });
            this.States.Add(new SelectListItem() { Text = "Australian Capital Territory", Value = "ACT" });
            this.States.Add(new SelectListItem() { Text = "Victoria", Value = "VIC" });
            this.States.Add(new SelectListItem() { Text = "Queensland", Value = "QLD" });
            this.States.Add(new SelectListItem() { Text = "Western Australia", Value = "WA" });
            this.States.Add(new SelectListItem() { Text = "South  Australia", Value = "SA" });
            this.States.Add(new SelectListItem() { Text = "Tasmania", Value = "TAS" });

            #endregion
        }

        /// <summary>
        /// Updates an account 
        /// </summary>
        /// <returns></returns>
        public bool UpdateAccount(SessionModel session)
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                Account account = cs.Proxy.GetAccountFromChargeBee(session.MedianetUserId, session.sessionKey);
                account.ContactPosition = this.Position;
                // No Need to replace with new Value because We are not updating this things.
                //account.EmailAddress = this.Email;

                if (
                        (this.Password != null) &&
                        (this.Password != String.Empty) &&
                        (this.Password == this.ConfirmPassword)
                )
                {
                    account.Password = this.Password;
                }

                //account.CompanyAddress.AddressLine1 = this.Address;
                //account.CompanyAddress.AddressLine2 = this.Address2;
                //account.CompanyAddress.Postcode = this.PostCode;
                //account.CompanyAddress.City = this.Suburb;
                //account.CompanyAddress.State = this.State;
                account.FirstName = this.FirstName;
                account.LastName = this.Lastname;

                if (account.AccountType == CustomerBillingType.Creditcard)
                {
                    account.CompanyName = this.Company;
                    account.ABN = this.ABN;
                }

                account.TelephoneNumber = this.Phone;
                account.IndustryCode = this.Industry;

                try
                {
                    cs.Proxy.UpdateChargeBeeAccount(account, session.sessionKey);
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UpdateAccount failed.", e);
                    this.ErrorMessage = e.Message;
                    return false;
                }
            }
            return true;
        }
    }

    /// <summary>
    /// Customer register first step 
    /// </summary>
    [Serializable]
    public class RegisterModel : BaseModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        [AllowedEmail]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
    }

    /// <summary>
    /// To upgrade an account
    /// </summary>
    [Serializable]
    public class UpgradeModel : BaseModel
    {
        #region Model
        
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "Address")]
        [StringLength(50)]
        public string Address { get; set; }
        
        [StringLength(50)]
        public string Address2 { get; set; }
        
        [Required]
        [Display(Name = "State")]
        public string State { get; set; }
        
        [Required]
        [StringLength(4)]
        [Display(Name = "Post code")]
        public string PostCode { get; set; }
        
        [Required]
        [Display(Name = "Suburb")]
        [StringLength(30)]
        public string Suburb { get; set; }
        
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        
        [Required]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }
        
        [ABN(ErrorMessage = @"The field ABN must be a string with a maximum length of 11")]
        [Display(Name = "ABN")]
        public string ABN { get; set; }
        
        [DataType(DataType.Text)]
        [Display(Name = "Company")]
        public string Company { get; set; }
        
        [Required]
        [Display(Name = "Position")]
        public string Position { get; set; }
        
        [Required]
        [Display(Name = "Phone")]
        [RegularExpression(@"^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$", ErrorMessage = "The phone number must be input in this format : 0X23456789 where X can be 2,4,3,7,8")]
        public string Phone { get; set; }
        
        [Required]
        [Display(Name = "Industry")]
        public string Industry { get; set; }
        
        public string ErrorMessage { get; set; }
        
        public Medianet.Web.MedianetCustomerService.CustomerBillingType AccountType { get; set; }

        #region Billing Address

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Address")]
        [StringLength(255)]
        public string BillingAddress { get; set; }

        /// <summary>
        ///     
        /// </summary>
        public string BillingAddress2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "State")]
        public string BillingState { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [StringLength(4)]
        [Display(Name = "Post code")]
        public string BillingPostCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [Display(Name = "Suburb")]
        [StringLength(30)]
        public string BillingSuburb { get; set; }

        #endregion

        #endregion

        #region Lists

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Industries;

        /// <summary>
        /// 
        [NonSerialized]
        public IList<SelectListItem> States;

        #endregion

        /// <summary>
        /// Loads the customer information to edit
        /// </summary>
        public void Prepare()
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                Account account = cs.Proxy.GetAccountFromChargeBee(session.MedianetUserId, session.sessionKey);
                this.ABN = account.ABN;
                this.Address = account.CompanyAddress.AddressLine1;
                this.Address2 = account.CompanyAddress.AddressLine2;
                this.PostCode = account.CompanyAddress.Postcode;
                this.State = account.CompanyAddress.State;
                this.Suburb = account.CompanyAddress.City;
                this.Email = account.EmailAddress;
                this.FirstName = account.FirstName;
                this.Lastname = account.LastName;
                this.Phone = account.TelephoneNumber;
                this.Position = account.ContactPosition;
                this.Company = account.CompanyName;
                this.Industry = account.IndustryCode;
                this.AccountType = (Medianet.Web.MedianetCustomerService.CustomerBillingType)account.AccountType;

                if (account.BillingAddress != null)
                {
                    this.BillingAddress = account.BillingAddress.AddressLine1;
                    this.BillingAddress2 = account.BillingAddress.AddressLine2;
                    this.BillingPostCode = account.BillingAddress.Postcode;
                    this.BillingState = account.BillingAddress.State;
                    this.BillingSuburb = account.BillingAddress.City;
                }
            }

            #region Industries

            this.Industries = new List<SelectListItem>();
            using (var ncc = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                IList<IndustryCode> industryList = ncc.Proxy.GetIndustryCodes();

                foreach (IndustryCode code in industryList)
                {
                    this.Industries.Add(new SelectListItem() { Text = code.Description.Trim(), Value = code.Code.ToString(), Selected = (code.Code == this.Industry) });
                }
            }


            #endregion

            #region States

            this.States = new List<SelectListItem>();
            this.States.Add(new SelectListItem() { Text = "New South Wales", Value = "NSW", Selected = true });
            this.States.Add(new SelectListItem() { Text = "Northern Territory", Value = "NT" });
            this.States.Add(new SelectListItem() { Text = "Australian Capital Territory", Value = "ACT" });
            this.States.Add(new SelectListItem() { Text = "Victoria", Value = "VIC" });
            this.States.Add(new SelectListItem() { Text = "Queensland", Value = "QLD" });
            this.States.Add(new SelectListItem() { Text = "Western Australia", Value = "WA" });
            this.States.Add(new SelectListItem() { Text = "South  Australia", Value = "SA" });
            this.States.Add(new SelectListItem() { Text = "Tasmania", Value = "TAS" });

            #endregion
        }

        /// <summary>
        /// Upgrades the account type to Invoice
        /// </summary>
        public bool Upgrade()
        {
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                Account account = cs.Proxy.GetAccountFromChargeBee(session.MedianetUserId, session.sessionKey);
                account.ContactPosition = this.Position;
                //account.EmailAddress = this.Email;// No Need to Replace data
                account.CompanyAddress.AddressLine1 = this.Address;
                account.CompanyAddress.AddressLine2 = this.Address2;
                account.CompanyAddress.Postcode = this.PostCode;
                account.CompanyAddress.City = this.Suburb;
                account.CompanyAddress.State = this.State;
                account.FirstName = this.FirstName;
                account.LastName = this.Lastname;
                account.TelephoneNumber = this.Phone;
                account.IndustryCode = this.Industry;

                if (account.BillingAddress == null)
                {
                    account.BillingAddress = new AccountAddress();
                }

                account.BillingAddress.AddressLine1 = this.BillingAddress;
                account.BillingAddress.AddressLine2 = this.BillingAddress2;
                account.BillingAddress.Postcode = this.BillingPostCode;
                account.BillingAddress.City = this.BillingSuburb;
                account.BillingAddress.State = this.BillingState;

                if (account.AccountType == CustomerBillingType.Creditcard)
                {
                    account.CompanyName = this.Company;
                    account.ABN = this.ABN;
                }

                //account.AccountType = CustomerBillingType.Invoiced;// No Need to changes that
                account.IsForUpgrade = true;
                try
                {
                    cs.Proxy.UpdateChargeBeeAccount(account, session.sessionKey);

                    #region Update account in cache

                    DBSession dbSession = cs.Proxy.ValidateSession(session.sessionKey);
                    session.PopulateFromDatabaseChargeBee(dbSession);

                    HttpContext.Current.Session[NopCommerce.Session_Key] = session;

                    #endregion
                }
                catch (Exception e)
                {
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Upgrade failed.", e);
                    this.ErrorMessage = e.Message;
                    return false;
                }
            }

            return true;
        }
      
    }   
}

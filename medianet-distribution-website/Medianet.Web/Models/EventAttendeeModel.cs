﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Medianet.Web.Validators;

namespace Medianet.Web.Models
{
    [Serializable]
    public class EventAttendeeModel
    {
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string Position { get; set; }
        public string Phone { get; set; }
    }
}
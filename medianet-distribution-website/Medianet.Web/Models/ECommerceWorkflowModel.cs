﻿using System;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Security;
using System.Globalization;
using System.Collections.Generic;
using Medianet.Web.NopCommerceServices;
using System.Web;
using System.Web.Routing;
using Medianet.Web.Helpers;
using Medianet.Constants;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.MedianetListService;
using System.Runtime.Serialization;
using System.Collections;
using System.Linq;
using log4net;
using Medianet.Web.MedianetCustomerService;

namespace Medianet.Web.Models
{
    [Serializable()]
    public abstract class ECommerceWorkflowModel : BaseModel
    {        
        public ECommerceWorkflowModel()
        {
            /// this will be set to true when checking out doing the credit card flow.
            this.hasToPay = false;
            this.forcePricing = false;
            this.isPrepared = false;
            this.Error = string.Empty;
        }

        #region Model

        /// <summary>
        /// This flag works to avoid adding default products twice
        /// </summary>
        public bool isPrepared { get; set; }

        /// <summary>
        /// Total shopping cart price
        /// </summary>
        public decimal TotalPrice { get; set; }

        /// <summary>
        /// For Wire 
        /// </summary>
        public AttachmentModel Release { get; set; }

        /// <summary>
        /// When initiated checkout
        /// </summary>
        public bool hasCheckedOut { get; set; }

        /// <summary>
        /// When payment is done 
        /// </summary>
        public bool hasFinalized { get; set; }

        /// <summary>
        /// Submited to pay
        /// </summary>
        public bool hasSubmited { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool hasToPay { get; set; }

        /// <summary>
        /// Payment return access code
        /// </summary>
        public string accessCode { get; set; }
        /// <summary>
        /// Payment return TransactionId
        /// </summary>

        public string TransactionId { get; set; }

        /// <summary>
        /// Payment response from medianet service
        /// </summary>
        public PaymentStartResponse PaymentResponse { get; set; }

        /// <summary>
        /// only true for wire / audio releases.
        /// </summary>
        public bool needtoAddWebAndJournalistsPosting { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool forcePricing { get; set; }

        /// <summary>
        /// Customer Billing Codes
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> BillingCodeList;

        #endregion

        #region Lists

        /// <summary>
        /// Shopping cart collection
        /// </summary>
        public IList<ShoppingCartItemDTO> shoppingCart;

        #endregion

        #region Error handling

        /// <summary>
        /// 
        /// </summary>
        public string PaymentError { get; set; }

        #endregion

        #region Shopping cart

        public bool AddANRProduct()
        {
            bool added = false;
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.AudioNewsReleaseCharge_Product_Id).ToArray();
                if (items.Length == 0)
                {
                    nc.Proxy.AddProductToCart(session.nopCommerceCustomerId, NopCommerce.AudioNewsReleaseCharge_Product_Id, NopCommerce.CartItem_ANR);
                    added = true;
                    
                    this.UpdateCartAfterOperation(nc);
                }

            }
            return added;
            
        }

        public bool AddSWCProduct()
        {
            bool added = false;
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.SecondaryWebCategoryCharge_Product_Id).ToArray();
                if (items.Length == 0)
                {
                    nc.Proxy.AddProductToCart(session.nopCommerceCustomerId, NopCommerce.SecondaryWebCategoryCharge_Product_Id, NopCommerce.CartItem_SWC);
                    added = true;

                    this.UpdateCartAfterOperation(nc);
                }

            }
            return added;
        }

        public bool UpdateAttachmentProductToCart(int attachmentCount)
        {
            bool added = false;
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.AttachmentCharge_Product_Id).ToArray();
                if (attachmentCount == 0)
                {
                    if (items.Length != 0)
                    {
                        nc.Proxy.RemoveProductFromCart(items[0].Id);
                    }
                    return added;
                }

                if (items.Length == 0)
                {
                    nc.Proxy.AddProductToCart(session.nopCommerceCustomerId, NopCommerce.AttachmentCharge_Product_Id,
                                              NopCommerce.CartItem_Attachment);
                    added = true;
                    this.UpdateCartAfterOperation(nc);
                }
                else
                {
                    var attribs = items[0].AttributesXml.Split(Convert.ToChar(","));
                    items[0].AttributesXml = string.Format("{0},{1},{2}", attribs[0], attribs[1],
                                                           attachmentCount.ToString());
                    nc.Proxy.UpdateCartItemAttributes(session.nopCommerceCustomerId, items[0]);
                    this.UpdateCartAfterOperation(nc);
                }

            }
            return added;

        }

        /// <summary>
        /// returns true if the product is in cart enabled or not enabled
        /// </summary>
        /// <param name="poductId"></param>
        /// <returns></returns>
        public bool IsProductInCart(int poductId)
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);

                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == poductId).ToArray();

                return (items.Length > 0);
            }

           
        }

        /// <summary>
        /// Returns true if web posting product is ticked in the cart
        /// </summary>
        /// <returns></returns>
        public bool IsNewsHubPostingAdded()
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);

                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.NewsHubPosting_Product_Id || s.HasNewsHubServices).ToArray();

                if ( items.Length > 0 )
                {
                    ShoppingCartItemDTO webPosting = items.First();

                    return webPosting.Included;
                }
                else
                    return false;
            }
        }

        public bool IsTwitterPostingAdded() {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>()) {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);

                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.TwitterPosting_Product_Id || s.HasTwitterServices).ToArray();

                if (items.Length > 0)
                    return true;
                else
                    return false;
            }
        }
        
        public bool IsJournalistPostingAdded()
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);

                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.JournalistPosting_Product_Id || s.HasJournalistsServices).ToArray();

                if (items.Length > 0)
                {
                    ShoppingCartItemDTO journalistsPosting = items.First();
                    return journalistsPosting.Included;
                }

                return false;
            }
        }

        public bool IsWireListAdded() {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>()) {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);

                foreach (ShoppingCartItemDTO cartItem in this.shoppingCart) {
                    var attributes = new ShoppingCartItemAttributeModel(cartItem);

                    if (attributes.DistributionType == "W")
                        return true;
                }

                return false;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id1"></param>
        /// <param name="Id2"></param>
        /// <returns></returns>
        public ProductInformationModel GetProductsInformation(int Id1, int Id2)
        {
            ProductInformationModel productInformation = new ProductInformationModel();

            using (var rsc = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                productInformation.InformationLists = rsc.Proxy.GetServiceListInfoByIds(new int[2] { Id1, Id2 }, this.session.sessionKey);
            }

            return productInformation;
        }

        /// <summary>
        /// Gets a product long description information.
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <returns></returns>
        public ProductInformationModel GetProductInformation(int Id)
        {
            bool isPackage = false;
            ProductDTO product;
            ProductInformationModel productInformation = new ProductInformationModel();

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                product = npc.Proxy.GetProduct(Id);

                if ( product != null )
                    isPackage = product.IsPackage;
                
                using (var rsc = new ServiceProxyHelper<ListServiceClient, IListService>())
                {
                    if (isPackage)
                    {
                        if (product.ServiceId >= 0)
                            productInformation.InformationLists = rsc.Proxy.GetPackageListInfoByIds (new int[1] { product.ServiceId }, this.session.sessionKey);
                    }
                    else
                    {
                        productInformation.InformationLists = rsc.Proxy.GetServiceListInfoByIds(new int[1] { Id }, this.session.sessionKey);
                    }
                }
            }

            if ( productInformation.InformationLists == null )
            {
                return new ProductInformationModel() { InformationLists = new ListInformation[0] };
            }
            else
                return productInformation;
        }

        /// <summary>
        /// Refreshes the shopping cart and makes a quote request, to price the whole of it.
        /// </summary>
        protected internal void UpdateCartAfterOperation()
        {
            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.UpdateCartAfterOperation(npc);
            }
        }

        /// <summary>
        /// Refreshes the shopping cart and makes a quote request, to price the whole of it, it alse removes the initialized payment if were any
        ///     to begin a new one.
        /// </summary>
        /// <param name="nopCommerceClient"></param>
        protected internal void UpdateCartAfterOperation(ServiceProxyHelper<NopCommerceClient, INopCommerce> nopCommerceClient)
        {
            if ( this.PaymentResponse != null )
            {
                this.PaymentResponse = null;
            }

            this.shoppingCart = nopCommerceClient.Proxy.GetCart(session.nopCommerceCustomerId);

            if (this.shoppingCart.Count > 0)
            {
                // Calculate new prices
                this.CalculatePrice();

                // Updates prices
                ShoppingCartItemDTO[] array = new ShoppingCartItemDTO[this.shoppingCart.Count];
                this.shoppingCart.CopyTo(array, 0);
                nopCommerceClient.Proxy.UpdateCartProducts(session.nopCommerceCustomerId, array);
            }
            else
                this.TotalPrice = 0;
        }

        /// <summary>
        /// Add a receipient to cart 
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="address"></param>
        /// <param name="distributionType"></param>
        public void AddReceipientToCart(SessionModel session, string address, string distributionType)
        {
            address = address.Replace(" ","");

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                bool addedtoCart = nc.Proxy.AddReceipientToCart(session.nopCommerceCustomerId, address, distributionType);

                if (!addedtoCart)
                {
                    //AG:TODO: Move this message to Medianet.Resources
                    throw new Exception("The item is already in the shopping cart");
                }

                this.UpdateCartAfterOperation(nc);
            }
        }

        /// <summary>
        /// Adds a customer list to cart
        /// </summary>
        /// <param name="session"></param>
        /// <param name="ServiceId"></param>
        /// <param name="Name"></param>
        /// <param name="distributionType"></param>
        public void AddCustomerListToCart(SessionModel session, int ServiceId, string Name, int addresses, string distributionType)
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                bool addedtoCart = nc.Proxy.AddCustomerListToCart(session.nopCommerceCustomerId, ServiceId, Name, addresses, distributionType);

                if (!addedtoCart)
                {
                    //AG:TODO: Move this message to Medianet.Resources
                    throw new Exception("The item is already in the shopping cart");
                }

                this.UpdateCartAfterOperation(nc);
            }
        }

        /// <summary>
        /// Add to cart, calculate price, update cart with prices.
        /// </summary>
        /// <param name="thisSession"></param>
        /// <param name="productId"></param>
        /// <param name="isFromDashboard"></param>
        public virtual void AddToCart(SessionModel thisSession, int productId, bool isFromDashboard)
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                ProductDTO productToAdd = nc.Proxy.GetProduct(productId);
                string productType = GetProductType(productId, productToAdd.IsPackage);

                // Adds to cart
                bool addedtoCart = nc.Proxy.AddProductToCart(thisSession.nopCommerceCustomerId, productToAdd.Id, productType);

                if (!addedtoCart)
                {
                    throw new Exception("The item is already in the shopping cart");
                }

                // If they have selected a package containing a NewsHub posting remove any single NewsHub postings they have selected.
                if (productToAdd.IsPackage && productToAdd.HasNewsHubServices)
                {
                    var childProductIds = GetChildProductIds(productToAdd.ChildProductIds);

                    foreach (var c in this.shoppingCart)
                    {
                        bool canRemove = false;

                        if (c.ProductId != Medianet.Constants.NopCommerce.Default_Product_Id)
                        {
                            if (childProductIds.Contains(c.ProductId))
                                canRemove = true;
                            else if (!string.IsNullOrWhiteSpace(c.ChildProductIds))
                            {
                                var ids = GetChildProductIds(c.ChildProductIds);

                                if (ids.Count > 0)
                                {
                                    canRemove = true;

                                    foreach (var x in ids)
                                    {
                                        if (!childProductIds.Contains(x))
                                            canRemove = false;
                                    }
                                }
                            }
                        }

                        if (canRemove)
                            nc.Proxy.RemoveProductFromCart(c.Id);
                    }
                    //var cartItems = this.shoppingCart.Where(p => p.ProductId == NopCommerce.NewsHubPosting_Product_Id).ToList();
                    //foreach (var c in cartItems)
                    //    nc.Proxy.RemoveProductFromCart(c.Id);
                }

                //// If they have selected a package containing a Journalist posting remove any single Journalist postings they have selected.
                //if (productToAdd.IsPackage && productToAdd.HasJournalistsServices)
                //{
                //    var cartItems = this.shoppingCart.Where(p => p.ProductId == NopCommerce.JournalistPosting_Product_Id).ToList();
                //    foreach (var c in cartItems)
                //        nc.Proxy.RemoveProductFromCart(c.Id);
                //}

                //// If they have selected a package containing a Twitter posting remove any single Twitter postings they have selected.
                //if (productToAdd.IsPackage && productToAdd.HasTwitterServices)
                //{
                //    var cartItems = this.shoppingCart.Where(p => p.ProductId == NopCommerce.TwitterPosting_Product_Id).ToList();
                //    foreach (var c in cartItems)
                //        nc.Proxy.RemoveProductFromCart(c.Id);
                //}

                this.UpdateCartAfterOperation(nc);
            }
        }

        private List<int> GetChildProductIds(string productIds)
        {
            var ids = new List<int>();

            if (!string.IsNullOrWhiteSpace(productIds))
            {
                var split = productIds.Split(',');
                
                foreach (var p in split)
                {
                    int num;

                    if (int.TryParse(p, out num))
                        ids.Add(num);
                }
            }

            return ids;
        }

        private string GetProductType(int productId, bool isPackage)
        {
            if (isPackage)
                return NopCommerce.CartItem_Package;

            return NopCommerce.CartItem_Service;
        }

        /// <summary>
        /// Removes an item from the shopping cart, re-calculates the pricing.
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ProductId"></param>
        public virtual void RemoveFromCart(SessionModel session, int ShoppingCartItemId)
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                nc.Proxy.RemoveProductFromCart(ShoppingCartItemId);

                this.UpdateCartAfterOperation(nc);
            }
        }

        /// <summary>
        /// Add to cart, calculate price, update cart with prices.
        /// </summary>
        /// <param name="thisSession"></param>
        /// <param name="productId"></param>
        /// <param name="isFromDashboard"></param>
        public virtual void UpdateCartForPriority(bool isHighPriority)
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.HighPriorityCharge_Product_Id).ToArray();

                if (items.Length == 0 && isHighPriority)
                {
                    // They have selected high priority, so add the fee to the cart.
                    nc.Proxy.AddProductToCart(session.nopCommerceCustomerId, NopCommerce.HighPriorityCharge_Product_Id, NopCommerce.CartItem_Priority);
                    this.UpdateCartAfterOperation(nc);
                }
                else if (items.Length > 0 && !isHighPriority)
                {
                    // We need to remove the high priority fee as they have taken off high priority.
                    foreach (var p in items)
                        nc.Proxy.RemoveProductFromCart(p.Id);

                    this.UpdateCartAfterOperation(nc);
                }
            }
        }

        /// <summary>
        /// Submits the release, finish the transaction
        /// </summary>
        /// <param name="session"></param>
        public virtual void SubmitRelease() { }

        /// <summary>
        /// Calculate the price of the items on the cart
        /// </summary>
        public virtual void CalculatePrice() { }
        
        /// <summary>
        /// Initiates a payment
        /// </summary>
        public virtual void Payment() { }

        /// <summary>
        /// Finalise an initiated CC payment
        /// </summary>
        public virtual void FinalisePayment()
        {
            PaymentEndResponse response;

            try
            {
                this.Error = "";
                this.PaymentError = "";
                this.hasFinalized = false;

                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    response = mpc.Proxy.FinalisePayment(this.accessCode, this.session.sessionKey);
                    
                    if ( !response.PaymentSucceeded )
                    {
                        this.PaymentResponse = null;
                        this.Error = response.ResponseMessage;
                        this.PaymentError = response.ResponseMessage;
                    }
                    else
                    {
                        this.hasFinalized = true;
                        this.TransactionId = response.ResponseCode;// if success return transaction id of chargebee
                    }

                }
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("FinalisePayment failed.", e);
                this.Error = e.Message;
            }
        }

        /// <summary>
        /// Prepares common to all releases.
        /// </summary>
        public virtual void Prepare()
        {
            if (this.session != null && this.session.IsVerified)
            {
                this.BillingCodeList = GetBillingCodes();
            }
        }

        private List<SelectListItem> GetBillingCodes()
        {
            var itemList = new List<SelectListItem>();

            using (var npc = new ServiceProxyHelper<CustomerServiceClient, ICustomerService>())
            {
                var billingList = npc.Proxy.GetCustomerBillingCodes(session.debtorNumber, session.sessionKey).ToList().OrderBy(b => b.SequenceNumber);

                foreach (CustomerBillingCode code in billingList)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = code.Name;
                    item.Text = code.Name;

                    itemList.Add(item);
                }
            }

            return itemList;
        }

        #endregion

        #region Checkout Logic

        /// <summary>
        /// Initiates checkout
        /// </summary>
        /// <returns></returns>
        public ActionResult Checkout()
        {
            //New Logic User Credit Card or Invoice Only Default Credit Card
            /*
                AccountType		    Has Transactions	Total over $1000	Bill By
                -----------		    --------------- 	----------------	--------
                CreditCard	   	    N/A			        N/A			        Creditcard
                Invoiced		    N/A			        N/A			        Invoice
                InvoicedNotApproved	False			    False			    Invoice
                InvoicedNotApproved	False			    True			    Creditcard
                InvoicedNotApproved	True			    N/A			        Creditcard
            */
            if (session.accountType == MedianetCustomerService.CustomerBillingType.Invoiced)
            {
                return this.ConfirmOrderCheckout();
            }
            else //Untill not Approved Works as Credit Cart
            {
                this.hasToPay = true;
                return this.CreditCartCheckout();
            }
            //else /// -> InvoicedNotApproved
            //{
            //    return this.AllowedCartTotalCheckout();
            //}
        }
        
        /// <summary>
        /// Cart validation checkout
        /// </summary>
        /// <returns></returns>
        private ActionResult AllowedCartTotalCheckout()
        {
            if (this.session.isFirstTransaction)
            {
                if (this.TotalPrice > 1000)
                {
                    this.hasToPay = true;
                    return this.CreditCartCheckout();
                }
                else
                    return this.ConfirmOrderCheckout();
            }
            else
            {
                this.hasToPay = true;
                return this.CreditCartCheckout(); 
            }            
        }

        /// <summary>
        /// Goes to credit card payment checkout
        /// </summary>
        /// <returns></returns>
        private ActionResult CreditCartCheckout()
        {
            return new RedirectResult("payment");
        }

        /// <summary>
        /// Goes to confirmation checkout only
        /// </summary>
        /// <returns></returns>
        private ActionResult ConfirmOrderCheckout()
        {         
            return new RedirectResult("confirm");
        }

        /// <summary>
        /// Each release has its own cart validation method
        /// </summary>
        /// <returns></returns>
        protected internal abstract bool ValidateCart(bool MustContainIncludedItems = false);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected internal bool ValidateCanPay()
        {
            bool canPay = this.ValidateCart(true);
            RedirectResult checkout = (RedirectResult)this.Checkout();

            return canPay && (
                                (checkout.Url.ToLower().Contains("confirm") ) ||
                                (checkout.Url.ToLower().Contains("payment") )
                        );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected internal bool ValidateCanConfirm()
        {
            bool canPay = this.ValidateCart();
            RedirectResult checkout = (RedirectResult)this.Checkout();

            return canPay && (checkout.Url.ToLower().Contains("confirm") );
        }

        #endregion

        #region ISerializable
        /*
        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctxt"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("CurrentStep", this.workflowStep.Value.Route);
        }

        /// <summary>
        /// Un-serializationn
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctxt"></param>
        public ECommerceWorkflowModel(SerializationInfo info, StreamingContext ctxt)
        {
            this.Setup(this.Steps);

            string CurrentStep = (string)info.GetValue("CurrentStep", typeof(string));

            LinkedListNode<WorkflowStep> node = this.workflowSteps.First;

            while (node != null)
            {
                if (node.Value.Route.ToLower() == CurrentStep)
                {
                    this.workflowStep = node;
                }
                else
                    node = node.Next;
            }
        }  
         */

        #endregion

        #region Worflow engine

        /// <summary>
        /// 
        /// </summary>
        public WorkflowStep[] Steps { get; set; }

        [NonSerialized]
        private LinkedList<WorkflowStep> m_workflowSteps;

        /// <summary>
        /// Workflow steps
        /// </summary>
        protected internal LinkedList<WorkflowStep> workflowSteps
        {
            get 
            {
                if ( this.m_workflowSteps == null )
                {
                    this.Setup(this.Steps,false);
                }
                return this.m_workflowSteps;
            }
            set {
                m_workflowSteps = value;
            }
        }        
          
        private string m_workflowStep;

        /// <summary>
        /// Current step
        /// </summary>
        protected internal LinkedListNode<WorkflowStep> workflowStep
        {
            get 
            {
                LinkedListNode<WorkflowStep> node = this.workflowSteps.First;                    

                while (node != null)
                {
                    if (node.Value.Route.ToLower() == m_workflowStep)
                    {
                        return node;
                    }
                    else
                        node = node.Next;
                }
                return node;
            }
            set 
            {
                this.m_workflowStep = value.Value.Route.ToLower();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="steps"></param>
        protected void Setup(WorkflowStep[] steps)
        {
            this.Setup(steps, true);
        }

        /// <summary>
        /// Initialize the linked list of workflow step
        /// </summary>
        /// <param name="steps"></param>
        protected void Setup(WorkflowStep[] steps,bool setFirst)
        {
            LinkedListNode<WorkflowStep> currentNode;

            this.workflowSteps = new LinkedList<WorkflowStep>();
            currentNode = null;

            foreach (WorkflowStep step in steps)
            {
                if (currentNode != null)
                {
                    currentNode = this.workflowSteps.AddAfter(currentNode, step);
                }
                else
                    currentNode = this.workflowSteps.AddFirst(step);
            }

            if (setFirst)
            {
                this.workflowStep = this.workflowSteps.First;
            }
        }

        /// <summary>
        /// Returns a step given the name of it
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        protected internal LinkedListNode<WorkflowStep> GetWorkflowStepFromName(string Name)
        {
            if (Name == "next")
            {
                return this.workflowStep.Next;
            }
            else if (Name == "previous")
            {

                return this.workflowStep.Previous;
            }
            else
            {
                LinkedListNode<WorkflowStep> node = this.workflowSteps.First;

                while (node != null)
                {
                    if (node.Value.Route.ToLower() == Name)
                    {
                        return node;
                    }
                    else
                        node = node.Next;
                }
            }

            throw new Exception("Page not found");
        }

        /// <summary>
        /// Returns a step given the route of the {controller}/{action} called
        /// </summary>
        /// <returns></returns>
        protected internal LinkedListNode<WorkflowStep> GetWorkflowStepFromAction()
        {
            #region Route

            HttpContextWrapper context = new HttpContextWrapper(System.Web.HttpContext.Current);
            RouteData route = RouteTable.Routes.GetRouteData((HttpContextBase)context);
            RouteValueDictionary routeParts = route.Values;

            #endregion

            string action = routeParts["action"].ToString().ToLower();

            return GetWorkflowStepFromName(action);
        }

        public virtual bool Current()
        {
            if ( !this.CanTransition() )
            {
                this.workflowStep = this.workflowStep.Previous;
                return false;
            }
                return true;
        }

        /// <summary>
        /// Next step in the workflow
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public virtual bool Next()
        {
            LinkedListNode<WorkflowStep> node = this.workflowStep;

            if (node == null) // This shouldn't happen | Just in case.
            {
                this.workflowStep = this.workflowSteps.First;
            }
            else if (node.Next.Value != null)
            {
                this.workflowStep = node.Next;

                if (!this.CanTransition())
                {
                    this.workflowStep = node;
                }
            }
            else
                this.workflowStep = node;

            /// if not sucess render current node view + model + errors
            if (this.workflowStep == node)
            {
                return false;
            }
            /// if sucess redirect to next node action
            else
                return true;
        }

        /// <summary>
        /// Previous step in the workflow
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public virtual bool Previous()
        {
            LinkedListNode<WorkflowStep> node = this.workflowStep;

            if (node == null) // This shouldn't happen | Just in case.
            {
                this.workflowStep = this.workflowSteps.First;
            }
            else if (node.Previous.Value != null)
            {
                this.workflowStep = node.Previous;
            }
            else
                this.workflowStep = node;

            /// if not sucess render current node view + model + errors
            if (this.workflowStep == node)
            {
                return false;
            }
            /// if sucess redirect to next node action
            else
                return true;
        }

        /// <summary>
        /// Returns if the current workflow step is the firs = Prepare.
        /// </summary>
        /// <returns></returns>
        public bool isFirstStep()
        {
            if (
                ( this.workflowStep != null )                       &&
                ( this.workflowStep == this.workflowSteps.First )
            )
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual bool CanTransition()
        {
            throw new NotImplementedException("Must implement this step in the child object");
        }

        /// <summary>
        /// Each release has its own content validation method
        /// </summary>
        /// <returns></returns>
        public abstract bool ValidReleaseContent();

        #endregion   
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    /// <summary>
    /// Only from premium accounts
    /// </summary>
    public class InvoiceAccountModel
    {
        public string firstName { get; set; }

        public string lastName { get; set; }

        public string company { get; set; }

        public string position { get; set; }

        public string eMail { get; set; }

        public string companyPhone { get; set; }

        public string mobilePhone { get; set; }

        public string abn { get; set; }

        public string companyAddressLine1 { get; set; }

        public string companyAddressLine2 { get; set; }

        public string companyPostCode { get; set; }

        public string companyState { get; set; }

        public string billingAddressLine1 { get; set; }

        public string billingAddressLine2 { get; set; }

        public string billingPostCode { get; set; }

        public string billingState { get; set; }

        public bool notificationPreferenceeMail { get; set; }        
    }
}
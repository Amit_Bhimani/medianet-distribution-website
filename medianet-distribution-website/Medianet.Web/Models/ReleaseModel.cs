﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Medianet.Web.MedianetGeneralService;
using Medianet.Web.NopCommerceServices;
using Medianet.Web.Helpers;
using System.IO;
using Medianet.Web.MedianetFileService;
using log4net;
using Medianet.Constants;
using Medianet.Web.Validators;

namespace Medianet.Web.Models
{
    [Serializable()]
    [ModelBinder(typeof(ReleaseModelBinder))]
    public class ReleaseModel : ECommerceWorkflowModel
    {
        public const string BILLING_CODE_NONE = "_NONE_";

        public ReleaseModel()
        {
            this.needtoAddWebAndJournalistsPosting = false;
            this.isPrepared = false;
            this.isAudio = false;
            this.isWire = false;
            this.isSms = false;
            this.Words = 0;
            this.AttachmentCount = 0;
            this.Pages = 0;
            this.TotalPrice = 0;
            this.Attachments = new List<AttachmentModel>();
            this.shoppingCart = new List<ShoppingCartItemDTO>();
            this.RecommendationList = new List<RelatedProductDTO>();
            this.QuickRecommendationList = new List<RecommendationDTO>();
        }

        public virtual void Initialise()
        {
            // Nothing to do here. Just leave it so other classes can override and implement stuff.
        }

        public virtual void setFromSubmitModel(ReleaseModel Model) { ;}

        #region Delegates

        /// <summary>
        /// To be called when a redirect to a previous step of the wizard happen
        /// </summary>
        public delegate void PrepareCallBack();

        /// <summary>
        /// To be called after a step transition
        /// </summary>
        /// <param name="submitedModel"></param>
        public delegate void onAfterCallBack(ReleaseModel submitedModel);

        /// <summary>
        /// To be called before a step transition
        /// </summary>
        /// <param name="submitedModel"></param>
        public delegate void onBeforeCallBack(ReleaseModel submitedModel);

        #endregion

        #region Workflow overrides

        /// <summary>
        /// Each release has its own cart validation method
        /// </summary>
        /// <returns></returns>
        protected internal override bool ValidateCart(bool MustContainIncludedItems = false)
        {
            throw new NotImplementedException("Must implement this method in each release");
        }

        /// <summary>
        /// This is a common confirm across the different releases.
        /// </summary>
        public virtual void Confirm()
        {
            this.isPrepare = false;
            this.hasCheckedOut = true;

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
            }

            this.TotalPrice = 0;
            foreach (ShoppingCartItemDTO item in this.shoppingCart)
            {
                this.TotalPrice += item.Price;
            }
        }


        public override bool ValidReleaseContent()
        {
            throw new NotImplementedException("Must impelement this is each release");
        }

        #endregion

        #region Model
        
        [Required(ErrorMessage = "The billing code is required.")]
        [Display(Name = "Billing Code")]
        public string BillingCode { get; set; }
        
        [Required(ErrorMessage = "The headline for your press release is required.")]
        [Display(Name = "Subject headline")]
        public string SubjectHeadline { get; set; }
        
        [Required(ErrorMessage = "The organisation your press release is being sent from is a required field.")]
        [Display(Name = "Organisation")]
        public string Organisation { get; set; }
        
        [Display(Name = "Customer Ref")]
        public string CustomerRef { get; set; }
        
        [Required]
        [Display(Name = "Priority")]
        public MedianetReleaseService.ReleasePriorityType Priority { get; set; }

        [Required]
        [Display(Name = "Web category")]
        public int WebCategoryId { get; set; }

        [Display(Name = "Secondary Web category")]
        public int? SecondaryWebCategoryId { get; set; }

        [Display(Name = "Twitter text")]
        public string Tweet { get; set; }
        
        public bool isInitialised { get; set; }
        
        public int Words { get; set; }
        
        public int AttachmentCount { get; set; }
        
        public int Pages { get; set; }
        
        public bool isPrepare { get; set; }
        
        public bool isOnline { get; set; }
        
        public bool isFinalise;
        
        public bool isWire { get; set; }
                
        public bool isSms { get; set; }
        
        public bool isAudio { get; set; }
        
        public IList<AttachmentModel> Attachments { get; set; }
        
        public AttachmentModel PressRelease { get; set; }
        
        [Display(Name = "Wire release text")]
        public AttachmentModel WireRelease { get; set; }
        
        [Display(Name = "Contacts database")]
        public Medianet.Web.MedianetReleaseService.DocumentSummary Database { get; set; }

        /// <summary>
        /// Used to verify if the user chanes the emaiWireText
        /// </summary>
        protected internal int EmailWireTextCheckSum { get; set; }
        
        protected internal int EmailwireTextTempFileUploadId { get; set; }
        
        [Display(Name = "Distribute as future release")]
        public bool DistributeAsFutureRelease { get; set; }

        /// <summary>
        /// Used in submit release
        /// </summary>
        public DateTime? HoldUntilDate
        {
            get
            {
                if (this.ScheduleDate.HasValue)
                {
                    DateTime value = this.ScheduleDate.Value;
                    value = value.AddHours(int.Parse(this.ScheduleTimeHour));
                    value = value.AddMinutes(int.Parse(this.ScheduleTimeMinutes));

                    return value;
                }
                else
                    return null;
            }
        }
        
        [Display(Name = "Schedule date")]
        [ScheduleDate(MaxDaysFromNow = 365 )]
        [DataType(DataType.Date)]
        public DateTime? ScheduleDate { get; set; }

        /// <summary>
        /// Helper for date picker
        /// </summary>
        /// <returns></returns>
        public string ScheduleDateForView()
        {
            if (this.ScheduleDate.HasValue)
            {
                return this.ScheduleDate.Value.ToString("dd/MM/yyyy");
            }
            else
                return "";
        }
        
        public string ScheduleTimeHour { get; set; }
        
        public string ScheduleTimeMinutes { get; set; }
        
        public int QuoteReferenceId { get; set; }
        
        [Display(Name = "Distribute as Embargo release")]
        public bool DistributeAsEmbargoRelease { get; set; }

        /// <summary>
        /// Used in submit release
        /// </summary>
        public DateTime? EmbargoUntilDate
        {
            get
            {
                if (this.EmbargoDate.HasValue)
                {
                    DateTime value = this.EmbargoDate.Value;
                    value = value.AddHours(int.Parse(this.EmbargoTimeHour));
                    value = value.AddMinutes(int.Parse(this.EmbargoTimeMinutes));

                    return value;
                }
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Embargo date")]
        [DataType(DataType.Date)]
        [EmbargoDate(MaxDaysFromNow = 365)]
        public DateTime? EmbargoDate { get; set; }

        /// <summary>
        /// Helper for date picker
        /// </summary>
        /// <returns></returns>
        public string EmbargoDateForView()
        {
            if (this.EmbargoDate.HasValue)
            {
                return this.EmbargoDate.Value.ToString("dd/MM/yyyy");
            }
            else
                return "";
        }

        /// <summary>
        /// 
        /// </summary>
        public string EmbargoTimeHour { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EmbargoTimeMinutes { get; set; }

        public short CurrentStepOrder { get; set; }

        #endregion

        #region Validation Helpers

        protected internal virtual bool ValidatePrepare()
        {
            if (this.DistributeAsFutureRelease && this.DistributeAsEmbargoRelease)
            {
                if (this.HoldUntilDate > this.EmbargoUntilDate)
                    modelState.AddModelError("EmbargoDate", " The Embargo time is before the hold time");
            }

            return this.modelState.IsValid;
        }

        protected bool IsTwitterCorrect()
        {
            if (this.IsTwitterPostingAdded())
            {
                if (string.IsNullOrWhiteSpace(this.Tweet))
                {
                    this.modelState.AddModelError("TwitterCategoryId",
                                                  "To proceed, please click 'Edit' and include your twitter details.");
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region Produts search and browse and more.

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> WebCategoryList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> SecondaryWebCategoryList;

        /// <summary>
        /// This is driven from nop commerce product to product mechanism
        /// </summary>
        [NonSerialized]
        public IList<RelatedProductDTO> RecommendationList;

        /// <summary>
        /// Get form the pluging written for this system
        /// </summary>
        [NonSerialized]
        public List<RecommendationDTO> QuickRecommendationList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<Product> OnlineProductList;
        
        [NonSerialized]
        public IList<Product> InternationalDistributionList;
        
        [NonSerialized]
        public IList<Category> InternationalCategories;

        [NonSerialized]
        public IList<WidgetItem> WidgetList;

        public string FormatedTweet()
        {
            var content = this.Tweet;
            var handleMatch = @"(?<=^|(?<=[^a-zA-Z0-9-_\\.]))@([A-Za-z]+[A-Za-z0-9_]+)";
            var hashtagMatch = @"(?<=^|(?<=[^a-zA-Z0-9-_\\.]))#([A-Za-z]+[A-Za-z0-9_]+)";

            content = Regex.Replace(content, handleMatch, delegate(Match m)
            {
                return string.Format("<a href=\"http://twitter.com/{0}\" target=\"_blank\">{1}</a>", m.ToString().Substring(1), m.ToString());
            }, RegexOptions.IgnoreCase);

            content = Regex.Replace(content, hashtagMatch, delegate(Match m)
            {
                return string.Format("<a href=\"http://twitter.com/search?q=%23{0}\" target=\"_blank\">{1}</a>", m.ToString().Substring(1), m.ToString());
            }, RegexOptions.IgnoreCase);

            return content;
        }

        /// <summary>
        /// Load the recommonedations 
        ///     It is used when the shopping cart changes.
        /// </summary>
        public void ReLoadRecommendations()
        {
            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.RecommendationList = npc.Proxy.GetShoppingCartRecommendations(session.nopCommerceCustomerId);
            }
        }

        protected List<SelectListItem> GetWebCategories()
        {
            var catList = new List<SelectListItem>();

            using (var npc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
            {
                foreach (MedianetGeneralService.WebCategory webCategory in npc.Proxy.GetWebCategories())
                {
                    if (webCategory.ParentId.HasValue)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Value = webCategory.Id.ToString();
                        item.Text = webCategory.Name;

                        catList.Add(item);
                    }
                }
            }

            return catList.OrderBy(w => w.Text).ToList();
        }

        //protected List<SelectListItem> GetTwitterCategories()
        //{
        //    var catList = new List<SelectListItem>();

        //    using (var npc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
        //    {
        //        foreach (TwitterCategory category in npc.Proxy.GetTwitterCategories())
        //        {
        //            if (category.IsVisible && !category.IsDeleted)
        //            {
        //                SelectListItem item = new SelectListItem();
        //                item.Value = category.Id.ToString();
        //                item.Text = category.Name;

        //                catList.Add(item);
        //            }
        //        }
        //    }

        //    return catList;
        //}

        protected List<MedianetReleaseService.TransactionSummary> GenerateTransactionsFromCart(IList<ShoppingCartItemDTO> cart)
        {
            var transList = new List<MedianetReleaseService.TransactionSummary>();
            var hasNewsHub = false;
            var hasTwitter = false;
            var hasJournalist = false;
            var transSeqNo = 1;

            foreach (ShoppingCartItemDTO cartItem in this.shoppingCart)
            {
                if (cartItem.Included)
                {
                    var attributes = new ShoppingCartItemAttributeModel(cartItem);
                    var transaction = new MedianetReleaseService.TransactionSummary();

                    switch (attributes.DistributionType)
                    {
                        case NopCommerce.CartDistType_AttachmentFee:
                            transaction.DistributionType = MedianetReleaseService.DistributionType.Unknown;
                            transaction.IsAttachmentFee = true;
                            break;

                        case NopCommerce.CartDistType_AnrFee:
                            transaction.DistributionType = MedianetReleaseService.DistributionType.Unknown;
                            transaction.IsAnrFee = true;
                            break;

                        case NopCommerce.CartDistType_AWCFee:
                            transaction.DistributionType = MedianetReleaseService.DistributionType.Unknown;
                            if (attributes.IsSecondaryWebCat)
                            {
                                transaction.IsSecondaryWebCategory = true;
                            }
                            break;

                        case NopCommerce.CartDistType_PriorityFee:
                            transaction.DistributionType = MedianetReleaseService.DistributionType.Unknown;
                            transaction.IsHighPriorityFee = true;
                            break;

                        default:
                            transaction.DistributionType = ToEnum<MedianetReleaseService.DistributionType>(attributes.DistributionType);

                            if (transaction.DistributionType == MedianetReleaseService.DistributionType.Twitter || cartItem.HasTwitterServices)
                            {
                                if (transaction.DistributionType == MedianetReleaseService.DistributionType.Twitter)
                                    transaction.ServiceId = NopCommerce.TwitterPosting_Product_Id;

                                transaction.TwitterText = this.Tweet;
                                transaction.TwitterCategoryId = NopCommerce.Default_TwitterCategoryId;
                                hasTwitter = true;
                            }

                            break;
                    }

                    if (cartItem.ProductId == NopCommerce.NewsHubPosting_Product_Id || cartItem.HasNewsHubServices)
                        hasNewsHub = true;

                    if (cartItem.ProductId == NopCommerce.JournalistPosting_Product_Id || cartItem.HasJournalistsServices)
                        hasJournalist = true;

                    if (attributes.IsProduct || attributes.IsCustomerList)
                    {
                        transaction.ServiceId = cartItem.ServiceId;
                    }
                    else if (attributes.IsPackage)
                    {
                        transaction.DistributionType = MedianetReleaseService.DistributionType.Package;
                        transaction.PackageId = cartItem.ServiceId;
                    }
                    else if (attributes.IsReceipient)
                    {
                        transaction.IsSingleRecipient = true;
                        transaction.SingleRecipientAddress = cartItem.Name;
                    }

                    transaction.SequenceNumber = transSeqNo;

                    transList.Add(transaction);
                }
                else
                    cartItem.Price = 0;

                transSeqNo += 1;
            }

            // Make sure twitter/secondarywebcategory are not enabled if we aren't publishing to the web.
            if (hasTwitter && !hasNewsHub)
            {
                transList = transList.Where(t => t.DistributionType != MedianetReleaseService.DistributionType.Twitter).ToList();
            }

            if (!(hasNewsHub || hasJournalist))
            {
                transList = transList.Where(t => t.IsSecondaryWebCategory == false).ToList();
            }
            return transList;
        }

        #endregion

        #region Uploading

        /// <summary>
        /// Checks if the maximum file size allowed for a single releaase has been reached.
        /// </summary>
        /// <param name="weight"></param>
        /// <returns></returns>
        protected internal bool IsMaximumFileSizeAllowed(decimal weight)
        {
            decimal totalWeight = 0;
            decimal maxTotalWeight = 1024 * 4;  // 4mb

            foreach (AttachmentModel attachment in this.Attachments)
            {
                totalWeight += attachment.Weight;
            }

            if (this.Release != null)
            {
                totalWeight += this.Release.Weight;
            }

            if (totalWeight + weight <= (maxTotalWeight))
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Uploads an attachment or email wire text to medianet for conversion.
        /// </summary>
        /// <returns></returns>
        public int UploadForConversion(Stream DataStream, string FileName, AttachmentTypeEnum AttachmentType, bool ConvertToWire)
        {
            string ContentType = MimeExtensionHelper.GetMimeType(FileName);
            int tempFileId = -1;
            int wordCount = 0;
            int pageCount = 0;
            decimal weight = (DataStream.Length / 1024);
            DataStream.Position = 0;

            if (!this.IsMaximumFileSizeAllowed(weight))
            {
                //AG:TODO: Move this messatge to Medianet.Resources
                throw new Exception("The maximum file size allowed for a single release (4mb) has been reached, you can not upload anything else !");
            }

            try
            {
                using (var fsc = new ServiceProxyHelper<FileServiceClient, IFileService>())
                {
                    if (!ConvertToWire)
                    {
                        fsc.Proxy.UploadTempFile(ContentType, ConvertToWire, FileName, session.sessionKey, DataStream, out tempFileId, out wordCount);
                    }
                    else
                        pageCount += fsc.Proxy.UploadTempFileForConversion(ContentType, ConvertToWire, FileName, session.sessionKey, DataStream, out tempFileId, out wordCount);

                    if (AttachmentType == AttachmentTypeEnum.PressRelease)
                    {
                        // If we already have a PressRelease then replace it with the new file.
                        if (this.PressRelease != null)
                        {
                            DecrementCounters(this.PressRelease);
                        }
                        this.PressRelease = new AttachmentModel(AttachmentTypeEnum.Release, tempFileId, wordCount, pageCount, weight, FileName, "");
                    }
                    else if (AttachmentType == AttachmentTypeEnum.Other)
                    {
                        this.Attachments.Add(new AttachmentModel(AttachmentTypeEnum.Other, tempFileId, wordCount, pageCount, weight, FileName, ""));
                        this.AttachmentCount++;
                    }
                    else if (AttachmentType == AttachmentTypeEnum.Release && !ConvertToWire)
                    {
                        // If we already have a Release then replace it with the new file.
                        if (this.Release != null)
                        {
                            DecrementCounters(this.Release);
                        }
                        this.Release = new AttachmentModel(AttachmentTypeEnum.Release, tempFileId, wordCount, pageCount, weight, FileName, "");
                    }
                    else
                    {
                        // If we already have a WireRelease then replace it with the new file.
                        if (this.WireRelease != null)
                        {
                            DecrementCounters(this.WireRelease);
                        }
                        this.WireRelease = new AttachmentModel(AttachmentTypeEnum.Release, tempFileId, wordCount, pageCount, weight, FileName, "");
                        this.EmailwireTextTempFileUploadId = tempFileId;
                    }

                    this.Words += wordCount;
                    this.Pages += pageCount;
                }
            }
            catch (Exception e)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
                throw e;
            }

            return tempFileId;
        }

        public void DeletAttachment(int tempFileId)
        {
            using (var fsc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
            {
                fsc.Proxy.DeleteTempFileById(tempFileId, session.sessionKey);
            }
        }

        public void DeletAttachmentChildren(int tempFileId)
        {
            using (var fsc = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>())
            {
                fsc.Proxy.DeleteTempFileChildrenById(tempFileId, session.sessionKey);
            }
        }

        #endregion

        /// <summary>
        /// This cleans other releases in memory as well the shopping cart.
        /// <param name="ModelKey">all models under keys are deleted except this one</param>
        /// </summary>
        public void Clean(string ModelKey)
        {
            string[] ModelKeys = new string[3] { NopCommerce.Wire_Key, NopCommerce.Sms_Key, NopCommerce.Audio_Key };

            foreach (string modelKey in ModelKeys)
            {
                if (modelKey != ModelKey)
                {
                    System.Web.HttpContext.Current.Session[modelKey] = null;
                }
            }

            if (session != null)
            {
                using (var ncc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
                {
                    ncc.Proxy.CleanCart(session.nopCommerceCustomerId);
                }
            }
        }

        public void DecrementCounters(AttachmentModel attach)
        {
            this.Words -= attach.WordCount;
            this.Pages -= attach.Pages;
        }

        public string CleanBillingCode(string code)
        {
            // The billing code is set to BILLING_CODE_NONE for customers with no billing codes
            // to avoid model validation failures. Set back to null if this is the case.
            if (string.IsNullOrEmpty(code) || code.Equals(BILLING_CODE_NONE))
                return null;
            else
                return code;
        }

        public static T ToEnum<T>(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new Exception("Value cannot be empty for type " + typeof(T).ToString() + ".");

            int intValue = (int)value[0];

            if (!Enum.IsDefined(typeof(T), intValue))
                throw new Exception("Unknown value " + value + " for type " + typeof(T).ToString() + ".");

            return (T)Enum.ToObject(typeof(T), intValue);
        }

        public void AddDefaultProducts()
        {
            if (!this.isPrepared)
            {
                using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
                {
                    var p = nc.Proxy.GetProduct(NopCommerce.Default_Product_Id);

                    if (p.IsPublished)
                    {
                        this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                        ShoppingCartItemDTO[] items = this.shoppingCart.Where(s => s.ProductId == Medianet.Constants.NopCommerce.AudioNewsReleaseCharge_Product_Id).ToArray();
                        if (items.Length == 0)
                        {
                            nc.Proxy.AddProductToCart(session.nopCommerceCustomerId, NopCommerce.Default_Product_Id,
                                NopCommerce.CartItem_Package);

                            this.UpdateCartAfterOperation(nc);
                        }
                    }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.Validators;

namespace Medianet.Web.Models
{
    [Serializable]
    public class EventRegisterModel : BaseModel
    {
        #region Model

        public string EventId { get; set; }
        public string EventTitle { get; set; }
        public string EventSummary { get; set; }
        public string EventDescription { get; set; }
        public string EventSector { get; set; }
        public decimal EventPrice { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public int ScheduleId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        public string ScheduleLocation { get; set; }
        public string ScheduleAddress { get; set; }
        public List<string> ScheduleAgenda { get; set; }
        public string ScheduleFormHandler { get; set; }
        public int TotalSeats { get; set; }
        public int SeatsRemaining { get; set; }

        public string PromoCode { get; set; }
        public List<PromoCodeModel> PromoCodes { get; set; }
        public string AttendeesString { get; set; }
        public List<EventAttendeeModel> Attendees { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Company")]
        public string Company { get; set; }

        //[DataType(DataType.Text)]
        //[Display(Name = "ABN")]
        //public string ABN { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Contact number")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Postcode")]
        public string PostCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [BooleanRequired(ErrorMessage = "You must accept Medianet's terms and conditions.")]
        public bool AgreeConditions { get; set; }

        [NonSerialized]
        public IList<SelectListItem> States;

        [NonSerialized]
        public List<SelectListItem> Countries;

        public PaymentStartResponse PaymentResponse { get; set; }
        public bool HasFinalised { get; set; }
        public string PaymentError { get; set; }

        #endregion

        public EventRegisterModel()
        {
            Prepare();
        }

        public void Prepare()
        {
            this.States = new List<SelectListItem>();
            this.States.Add(new SelectListItem() { Text = "New South Wales", Value = "NSW", Selected = true });
            this.States.Add(new SelectListItem() { Text = "Northern Territory", Value = "NT" });
            this.States.Add(new SelectListItem() { Text = "Australian Capital Territory", Value = "ACT" });
            this.States.Add(new SelectListItem() { Text = "Victoria", Value = "VIC" });
            this.States.Add(new SelectListItem() { Text = "Queensland", Value = "QLD" });
            this.States.Add(new SelectListItem() { Text = "Western Australia", Value = "WA" });
            this.States.Add(new SelectListItem() { Text = "South  Australia", Value = "SA" });
            this.States.Add(new SelectListItem() { Text = "Tasmania", Value = "TAS" });

            this.Countries = new List<SelectListItem>();
            this.Countries.Add(new SelectListItem() { Text = "Australia", Value = "Australia", Selected = true });
        }
    }
}
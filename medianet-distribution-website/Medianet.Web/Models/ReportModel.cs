﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.Helpers;
using Medianet.Constants;
using System.Xml.Serialization;
using System.ServiceModel;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class ReportModel : BaseModel
    {
        public ReportModel()
            : base()
        {
            this.PageIndex = 0;
            this.PagerIndex = 0;
            this.TotalPages = 0;
            this.ReleaseDetails = new Release();
            this.Documents = new Document[0];
            this.Transactions = new TransactionStatistics[0];
        }

        #region Model

        public int PagerIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex {get;set;}

        /// <summary>
        /// 
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TotalResults { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ReleasePage Reports;

        /// <summary>
        /// 
        /// </summary>
        public Release ReleaseDetails;

        /// <summary>
        /// 
        /// </summary>
        public TransactionStatistics[] Transactions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Document[] Documents;

        /// <summary>
        /// 
        /// </summary>
        public Result[] Results;

        /// <summary>
        /// 
        /// </summary>
        public TransactionStatistics Transaction;

        #endregion

        #region Data Binders

        /// <summary>
        /// Load the first page of the results to show + the pagination stats
        /// </summary>
        public void Prepare()
        {
            this.PageIndex = 1;
            this.GetReportPage(1);            
        }

        /// <summary>
        /// Gets a page of results to show
        /// </summary>
        /// <param name="PageIndex"></param>
        public void GetReportPage(int PageIndex)
        {
            int startPos = (this.PageIndex - 1) * NopCommerce.Report_Page_Size;

            using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
            {
               this.Reports = rsc.Proxy.GetReleasesWithResultsToShow(startPos, NopCommerce.Report_Page_Size, ReleaseSortOrderType.CreatedDate, this.session.sessionKey);
            }

            this.TotalPages = (int)Math.Ceiling((double)this.Reports.TotalCount / NopCommerce.Report_Page_Size);
        }

        /// <summary>
        /// Get a report result
        /// </summary>
        /// <param name="ReleaseId"></param>
        /// <returns></returns>
        public void GetReportResult(int ReleaseId)
        {
            using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
            {
                this.ReleaseDetails = rsc.Proxy.GetReleaseById(ReleaseId,this.session.sessionKey);
                this.Transactions = rsc.Proxy.GetReleaseTransactionStatistics(ReleaseId, this.session.sessionKey);
                this.Documents=  rsc.Proxy.GetReleaseDocuments(ReleaseId, this.session.sessionKey);
            }          
        }

        /// <summary>
        /// Results to popup
        /// </summary>
        /// <param name="TransactionId"></param>
        public void GetTransactionDetail(int ReleaseId,int TransactionId)
        {
            using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
            {
                try
                {
                    this.Transaction = this.Transactions.Where(t => t.Id == TransactionId).First();
                    this.Results = rsc.Proxy.GetReleaseResults(ReleaseId, TransactionId, this.session.sessionKey);
                }
                catch (FaultException)
                {
                    this.Results = new Result[0];
                }
            }          
        }

        public void CancelRelease(int releaseId)
        {
            using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
            {
                rsc.Proxy.CancelRelease(releaseId, this.session.sessionKey);
            }
        }

        #endregion
    }
}
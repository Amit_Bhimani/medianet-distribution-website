﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    public class AutosuggestResult
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models.Curator
{
    public class CuratorReport
    {
        public CuratorRating content { get; set; }
        public CuratorRating angle { get; set; }
        public CuratorRating title { get; set; }
        public CuratorRating assets { get; set; }
        public CuratorRating timing { get; set; }
        public CuratorRating heading { get; set; }
        public double rating { get; set; }
        public CuratorNotes notes { get; set; }
    }
}
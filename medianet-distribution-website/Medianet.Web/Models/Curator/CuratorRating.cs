﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models.Curator
{
    public class CuratorRating
    {
        public double rating { get; set; }
        public string comment { get; set; }
    }
}
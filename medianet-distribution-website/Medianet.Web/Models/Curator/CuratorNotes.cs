﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models.Curator
{
    public class CuratorNotes
    {
        public string summary { get; set; }
        public string heading { get; set; }
        public string general { get; set; }
    }
}
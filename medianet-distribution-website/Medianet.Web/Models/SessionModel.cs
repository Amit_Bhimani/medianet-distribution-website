﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Medianet.Web.Helpers;
using Medianet.Constants;
using Medianet.Web.MedianetCustomerService;
using Medianet.Web.NopCommerceServices;
using log4net;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class SessionModel
    {
        #region Model

        /// <summary>
        /// 
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string currentModel { get; set; }

        /// <summary>
        /// Nop Customer Id
        /// </summary>
        public int nopCommerceCustomerId { get; set; }
     
        /// <summary>
        /// Medianet Customer Id
        /// </summary>
        public int mediaNetCustomerId { get; set; }
        
        /// <summary>
        /// Medianet debtor number
        /// </summary>
        public string debtorNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int MedianetUserId { get; set; }

        /// <summary>
        /// Medianet session key
        /// </summary>
        public string sessionKey { get; set; }

        /// <summary>
        /// Usefull to the checkout logic
        /// </summary>
        public bool isFirstTransaction { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Medianet.Web.MedianetCustomerService.CustomerBillingType accountType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MedianetReleaseService.ResultsType customerResultType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Medianet.Web.MedianetCustomerService.ReportType userReportSendMethod { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string customerEmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string userEmailAddress { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public SwitchReleaseModel SwitchRelease { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastRequest { get; set; }

        public DateTime? LastLogon { get; set; }

        public DateTime? TrendWatcherExpiryDate { get; set; }

        public int? SalesRegionId { get; set; }

        public string SalesRegionName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool HasContactsWebAccess { get; set; }

        public bool HasViewedPopupMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool MustUseCreditCard { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Medianet.Web.MedianetCustomerService.Timezone TimeZone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsVerified { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAuthorised { get; set; }
        
        #endregion

        /// <summary>
        /// Saves a file on temp session safe.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string UploadFile(Stream stream,string serverPath,string fileName)
        {
            try
            {
                string path = System.IO.Path.Combine(serverPath, this.mediaNetCustomerId.ToString() + "." + fileName);
                string fileContentType = MimeExtensionHelper.GetMimeType(fileName);                

                var buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    System.IO.File.WriteAllBytes(path, buffer);

                return path;
            }
            catch (Exception e)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UploadFile failed.", e);
                return e.Message;
            }
        }

        /// <summary>
        /// Clean any remain of release saved on session
        /// </summary>
        public void CleanReleases()
        {
            string[] ModelKeys = new string[5] { NopCommerce.Wire_Key, NopCommerce.Sms_Key, NopCommerce.Audio_Key , NopCommerce.Report_Key, NopCommerce.Free_Key};

            foreach (string modelKey in ModelKeys)
            {
                HttpContext.Current.Session[modelKey] = null;
            }

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                nc.Proxy.CleanCart(this.nopCommerceCustomerId);
            }
        }

        public void PopulateFromDatabase(DBSession dbSession)
        {
            this.LastRequest = dbSession.LastAccessedDate;
            this.mediaNetCustomerId = dbSession.UserId;
            this.debtorNumber = dbSession.DebtorNumber;
            this.sessionKey = dbSession.Key;
            this.accountType = dbSession.Customer.AccountType;
            this.customerResultType = (dbSession.Customer.ReportResultsToSend == ResultsType.All) ? MedianetReleaseService.ResultsType.All : MedianetReleaseService.ResultsType.Negative;
            this.isFirstTransaction = !dbSession.Customer.HasTransactions;
            this.userReportSendMethod = dbSession.Customer.ReportSendMethod;
            this.customerEmailAddress = dbSession.User.EmailAddress;
            this.userEmailAddress = dbSession.User.EmailAddress;
            this.MedianetUserId = dbSession.UserId;
            this.HasContactsWebAccess = dbSession.User.HasContactsWebAccess;
            this.FirstName = dbSession.User.FirstName;
            this.LastName = dbSession.User.LastName;
            this.Name = dbSession.Customer.Name;
            this.Company = dbSession.Customer.LogonName;
            this.MustUseCreditCard = dbSession.Customer.MustUseCreditcard;
            this.TimeZone = dbSession.Customer.Timezone;
            this.IsVerified = dbSession.Customer.IsVerified;
            this.HasViewedPopupMessage = dbSession.User.HasViewedDistributionMessage;
            this.LastLogon = dbSession.User.LastLogonDate;
            this.TrendWatcherExpiryDate = dbSession.Customer.TrendWatcherExpiryDate;

            if (dbSession.Customer.SalesRegions != null && dbSession.Customer.SalesRegions.Length > 0)
                this.SalesRegionId = dbSession.Customer.SalesRegions[0].RegionId;
            else
                this.SalesRegionId = null;

            //Setup name as user name if this is an unverfied account
            if (!this.IsVerified)
            {
                this.Name = dbSession.User.FirstName;
            }
            this.IsAuthorised = dbSession.IsAuthorised;
        }
        public void PopulateFromDatabaseChargeBee(Medianet.Web.MedianetChargeBeeService.DBSession dbSession)
        {
            this.LastRequest = dbSession.LastAccessedDate;
            this.mediaNetCustomerId = dbSession.UserId;
            this.debtorNumber = dbSession.DebtorNumber;
            this.sessionKey = dbSession.Key;
            this.accountType = (Medianet.Web.MedianetCustomerService.CustomerBillingType)dbSession.Customer.AccountType;
            this.customerResultType = ((Medianet.Web.MedianetCustomerService.ResultsType)dbSession.Customer.ReportResultsToSend == ResultsType.All) ? MedianetReleaseService.ResultsType.All : MedianetReleaseService.ResultsType.Negative;
            this.isFirstTransaction = !dbSession.Customer.HasTransactions;
            this.userReportSendMethod = (Medianet.Web.MedianetCustomerService.ReportType)dbSession.Customer.ReportSendMethod;
            this.customerEmailAddress = dbSession.User.EmailAddress;
            this.userEmailAddress = dbSession.User.EmailAddress;
            this.MedianetUserId = dbSession.UserId;
            this.HasContactsWebAccess = dbSession.User.HasContactsWebAccess;
            this.FirstName = dbSession.User.FirstName;
            this.LastName = dbSession.User.LastName;
            this.Name = dbSession.User.LogonName;
            this.Company = dbSession.Customer.LogonName;
            this.MustUseCreditCard = dbSession.Customer.MustUseCreditcard;
            var timeZone = dbSession.Customer.Timezone;
            this.TimeZone = new Timezone();
            this.TimeZone.Code = timeZone.Code;
            this.TimeZone.Description = timeZone.Description;
            this.TimeZone.Name = timeZone.Name;

            this.IsVerified = dbSession.Customer.IsVerified;
            this.HasViewedPopupMessage = dbSession.User.HasViewedDistributionMessage;
            this.LastLogon = dbSession.User.LastLogonDate;
            this.TrendWatcherExpiryDate = dbSession.Customer.TrendWatcherExpiryDate;

            if (dbSession.Customer.SalesRegions != null && dbSession.Customer.SalesRegions.Length > 0)
                this.SalesRegionId = dbSession.Customer.SalesRegions[0].RegionId;
            else
                this.SalesRegionId = null;
            this.SalesRegionName = dbSession.Customer.SalesRegionName;
            //Setup name as user name if this is an unverfied account
            if (!this.IsVerified)
            {
                this.Name = dbSession.User.FirstName;
            }
            this.IsAuthorised = dbSession.IsAuthorised;
        }
    }
}
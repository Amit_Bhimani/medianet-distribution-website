﻿using System;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;

using Medianet.Web.MedianetReleaseService;
using Medianet.Web.NopCommerceServices;

public class ExtraReceipient : BaseEntity
{
    /// <summary>
    /// Address
    /// </summary>
    public string address { get; set; }

    /// <summary>
    /// Type : email.
    /// </summary>
    public DistributionType type { get; set; }
}
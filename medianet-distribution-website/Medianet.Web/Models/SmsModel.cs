﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Medianet.Web.MedianetListService;
using Medianet.Web.Helpers;
using Medianet.Web.NopCommerceServices;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.MedianetCustomerService;
using Medianet.Constants;
using log4net;

namespace Medianet.Web.Models
{
    [Serializable()]
    public class SmsModel : ReleaseModel
    {
        public SmsModel()
            : base()
        {
            #region eCommerce workflow steps

            this.Steps = new WorkflowStep[]
                {
                    new WorkflowStep() { Route = "prepare"  , MenuName = "Content", WidgetPage = "sms-step1", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Prepare) , onAfterNext = new onAfterCallBack(this.Save), Order = 1 },
                    new WorkflowStep() { Route = "contacts" , MenuName = "Your Contacts", WidgetPage = "sms-step2", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Contacts), Order = 2},
                    new WorkflowStep() { Route = "finalise" , MenuName = "Finalise", WidgetPage = "sms-step3", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Finalise), Order = 3},
                    new WorkflowStep() { Route = "checkout", WidgetPage = "", Order = 4 },
                    new WorkflowStep() { Route = "thanks", WidgetPage = "thanks", Order = 5 }
                };

            this.isSms = true;
            this.Setup(this.Steps);

            #endregion
        }

        #region Release workflow steps data bindings

        /// <summary>
        /// Prepares the sms release
        /// </summary>
        public new virtual void Prepare()
        {
            this.PriorityList = new List<SelectListItem>();
            this.hasCheckedOut = false;
            this.hasSubmited = false;
            this.isFinalise = false;

            #region Priorities

            IEnumerable<Medianet.Web.MedianetReleaseService.ReleasePriorityType> list = (IEnumerable<Medianet.Web.MedianetReleaseService.ReleasePriorityType>)Enum.GetValues(typeof(Medianet.Web.MedianetReleaseService.ReleasePriorityType));

            foreach (Medianet.Web.MedianetReleaseService.ReleasePriorityType priority in list)
            {
                SelectListItem item = new SelectListItem();
                item.Value = priority.ToString();
                item.Text = priority.ToString();

                this.PriorityList.Add(item);
            }

            #endregion

            #region Hours, Minutes, Seconds

            this.Hours = new List<SelectListItem>();
            this.Minutes = new List<SelectListItem>();

            for (int i = 0; i < 24; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString("00");
                item.Text = i.ToString("00");

                this.Hours.Add(item);
            }

            for (int i = 0; i < 60; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString("00");
                item.Text = i.ToString("00");

                this.Minutes.Add(item);
            }

            #endregion

            base.Prepare();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        /// <summary>
        /// Saves the sms form
        /// </summary>
        /// <param name="submitedModel"></param>
        protected void Save(ReleaseModel model)
        {
            SmsModel submitedModel = (SmsModel)model;

            this.BillingCode = submitedModel.BillingCode;
            this.SubjectHeadline = submitedModel.SubjectHeadline;
            this.CustomerRef = submitedModel.CustomerRef;
            this.Organisation = submitedModel.Organisation;
            this.Priority = submitedModel.Priority;
            this.DistributeAsFutureRelease = submitedModel.DistributeAsFutureRelease;
            this.ScheduleDate = submitedModel.ScheduleDate;
            this.ScheduleTimeHour = submitedModel.ScheduleTimeHour;
            this.ScheduleTimeMinutes = submitedModel.ScheduleTimeMinutes;
            this.Text = submitedModel.Text;
            this.isPrepared = true;
        }

        /// <summary>
        /// Add contacts to the sms release
        /// </summary>
        public void Contacts()
        {
            this.isPrepare = false;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.RecommendationList = nc.Proxy.GetShoppingCartRecommendations(session.nopCommerceCustomerId);
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
            }

            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);

            using (var svc = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                this.CustomList = svc.Proxy.GetServiceListsByDebtorNumber(session.debtorNumber, session.sessionKey);
            }
        }

        /// <summary>
        /// Finalise sms release
        /// </summary>
        public void Finalise()
        {
            this.isPrepare = false;
            this.isFinalise = true;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
            }

            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        /// <summary>
        /// Gets the credit card form
        /// </summary>
        public override void Payment()
        {
            this.isPrepare = false;
            this.hasCheckedOut = true;

            if (this.PaymentResponse == null)
            {

                using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
                {
                    this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
                }

                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    PaymentStartRequest paymentRequest = new PaymentStartRequest();
                    paymentRequest.ReleaseQuote = new MedianetReleaseService.ReleaseQuoteRequest();
                    paymentRequest.ReleaseQuote.DebtorNumber = session.debtorNumber;
                    paymentRequest.ReleaseQuote.MultimediaType = MedianetReleaseService.MultimediaType.None;
                    paymentRequest.ReleaseQuote.Priority = this.Priority;
                    paymentRequest.ReleaseQuote.ReleaseDescription = this.SubjectHeadline;
                    paymentRequest.ReleaseQuote.BillingCode = CleanBillingCode(this.BillingCode);
                    paymentRequest.ReleaseQuote.CustomerReference = this.CustomerRef;
                    paymentRequest.ReleaseQuote.System = Medianet.Web.MedianetReleaseService.SystemType.Medianet;
                    paymentRequest.RedirectURL = String.Format("{0}://{1}/Sms/Thanks", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST"));
                    #region Adding services

                    paymentRequest.ReleaseQuote.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

                    #endregion

                    try
                    {
                        this.PaymentResponse = mpc.Proxy.InitialisePayment(paymentRequest, session.sessionKey);
                        this.QuoteReferenceId = this.PaymentResponse.QuoteReferenceId;
                    }
                    catch (Exception e)
                    {
                        ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                        log.Error(null, e);
                        this.Error = e.Message;
                    }
                }
            }
        }

        /// <summary>
        /// Submits the release to Medianet
        /// </summary>
        public override void SubmitRelease()
        {
            #region Preparing release

            ReleaseSummary release = new ReleaseSummary();
            release.DebtorNumber = session.debtorNumber;
            release.System = MedianetReleaseService.SystemType.Medianet;
            release.BillingCode = CleanBillingCode(this.BillingCode);
            release.ReleaseDescription = this.SubjectHeadline;
            release.Organisation = this.Organisation;
            release.CustomerReference = this.CustomerRef;
            release.HoldUntilDate = this.HoldUntilDate;
            release.Priority = this.Priority;
            release.MultimediaType = Medianet.Web.MedianetReleaseService.MultimediaType.None;
            release.ReportResultsToSend = session.customerResultType;
            release.EmailFromAddress = NopCommerce.Medianet_Sender;
            release.SMSText = this.Text;
            release.WebCategoryId = null;
            release.QuoteReferenceId = this.QuoteReferenceId;
            release.TimezoneCode = session.TimeZone.Code;
            release.IsVerified = true;
            release.TransactionId = this.TransactionId;

            if (session.userReportSendMethod == ReportType.Email || session.userReportSendMethod == ReportType.Both)
            {
                release.ReportEmailAddress = session.customerEmailAddress;
            }

            #endregion

            #region Adding services

            release.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

            #endregion

            try
            {
                using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    rsc.Proxy.AddReleaseFromSummary(release, session.sessionKey);
                }
            }
            catch (Exception e)
            {
                this.Error = e.Message;
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
            }
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                nc.Proxy.CleanCartAndCreateOrders(session.nopCommerceCustomerId, this.TotalPrice);
            }
        }

        #endregion

        #region Release

        /// <summary>
        ///
        /// </summary>
        [Required]
        [AllowHtml]
        [Display(Name = "SMS text")]
        public string Text { get; set; }

        #endregion

        #region Shopping Cart

        /// <summary>
        /// Calculates Sms release price
        /// </summary>
        public override void CalculatePrice()
        {
            MedianetReleaseService.ReleaseQuoteRequest request = new MedianetReleaseService.ReleaseQuoteRequest();
            request.Transactions = new MedianetReleaseService.TransactionSummary[this.shoppingCart.Count];
            request.MultimediaType = MedianetReleaseService.MultimediaType.None;
            request.DebtorNumber = session.debtorNumber;
            request.Priority = this.Priority;
            request.ReleaseDescription = this.SubjectHeadline;
            request.MultimediaType = MedianetReleaseService.MultimediaType.None;
            request.System = MedianetReleaseService.SystemType.Medianet;

            #region Adding products

            request.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

            #endregion

            try
            {
                using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    ReleaseQuoteResponse response = rsc.Proxy.QuoteRelease(request, session.sessionKey);

                    #region Refreshing shopping cart items price

                    foreach (TransactionQuoteResponse quoted in response.Transactions)
                    {
                        this.shoppingCart[quoted.SequenceNumber - 1].Price = quoted.SubTotalEx + quoted.GST;
                    }

                    #endregion

                    this.TotalPrice = response.Total;
                    this.QuoteReferenceId = response.QuoteReferenceId;
                }
            }
            catch (Exception e)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
                this.Error = "*** Shopping Cart currently unavailable ****<br><br>Please contact Medianet on 1300 616 813 or <a href='mailto:team@medianet.com.au'>team@medianet.com.au</a> for final pricing.<br><br>Press releases can still be submitted through the portal.<br><br>We apologise for the inconvenience and are working hard to have this resolved as quickly as possible."; //e.Message;
            }
        }

        #endregion

        #region Products search and browse and more.

        /// <summary>
        /// Category list loaded all at once.
        /// </summary>
        [NonSerialized]
        public IList<CategoryDTO> CategoryList;

        /// <summary>
        /// Category list loaded all at once.
        /// </summary>
        [NonSerialized]
        public IList<NopCommerceServices.Category> TopCategoryList;

        /// <summary>
        /// Nopcommerce has this ?
        /// </summary>
        [NonSerialized]
        public IList<Product> RecentList;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<Product> SpecialProductList;

        /// <summary>
        /// Driven from text search
        /// </summary>
        [NonSerialized]
        public IList<Product> ProductSearchList;

        /// <summary>
        /// List get from mediant customer list
        /// </summary>
        [NonSerialized]
        public IList<ServiceList> CustomList;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<ExtraReceipient> ExtraRecipientList;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> PriorityList;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Hours;

        /// <summary>
        ///
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Minutes;

        #endregion

        #region Workflow engine overrides

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toStep"></param>
        /// <returns></returns>
        public override bool CanTransition()
        {
            bool canTransition = true;

            if (this.workflowStep.Value.Route == "prepare")
            {
                canTransition = true;
            }
            else if (this.workflowStep.Value.Route == "contacts")
            {
                canTransition = this.ValidatePrepare();
            }
            else if (this.workflowStep.Value.Route == "finalise")
            {
                canTransition = this.ValidateCart();
            }
            else if (this.workflowStep.Value.Route == "payment")
            {
                canTransition = this.ValidateCanPay();
            }
            else if (this.workflowStep.Value.Route == "confirm")
            {
                canTransition = this.ValidateCanConfirm();
            }
            else if (this.workflowStep.Value.Route == "checkout")
            {
                canTransition = this.ValidateCanPay();
            }
            else if (this.workflowStep.Value.Route == "thanks")
            {
                canTransition = this.ValidateCart();
            }

            return canTransition;
        }

        /// <summary>
        /// Validates the prepare step of the workflow
        /// </summary>
        /// <returns></returns>
        protected new internal virtual bool ValidatePrepare()
        {
            return base.ValidatePrepare();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        protected internal override bool ValidateCart(bool MustContainIncludedItems = false)
        {
            if (this.shoppingCart.Count == 0)
            {
                this.modelState.AddModelError("shoppingCart", "You need to add at least one product, special, customer contacts or extra recipient");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates that the content for the SMS Release is valid
        /// </summary>
        /// <returns></returns>
        public override bool ValidReleaseContent()
        {
            return (!string.IsNullOrEmpty(SubjectHeadline)) && (!string.IsNullOrEmpty(Text));
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medianet.Web.MedianetListEditService;
using Medianet.Web.Helpers;
using System.IO;
using System.Web.Mvc;
using Medianet.Web.MedianetListService;
using System.ComponentModel.DataAnnotations;
using FileHelpers;
using System.Data;
using System.ComponentModel;
using FileHelpers.Options;
using LumenWorks.Framework.IO.Csv;
using log4net;

namespace Medianet.Web.Models
{
    [Serializable()]
    public abstract class ManageListBase : BaseModel
    {
        #region Model

        [Display(Name = "Name")]
        public string ListName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? RecipientId { get; set; }

        /// <summary>
        /// 
        /// </summary
        public int? ListId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MedianetListService.DistributionType distributionType { get; set; }

        #endregion

        #region Containers

        /// <summary>
        /// To Uploal
        /// </summary>
        public AttachmentModel UploadList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SelectedListRecipient { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string newListName { get; set; }

        #endregion

        #region Lists

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> ListRecipients;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Lists;

        /// <summary>
        /// 
        /// </summary>
        //[NonSerialized]
        //private Medianet.Web.MedianetListEditService.Recipient[] Recipients;

        #endregion

        #region Must inherit

        public MedianetListEditService.Recipient[] UploadForConversion(Stream stream) {
            var recipients = new List<MedianetListEditService.Recipient>();

            using (TextReader reader = new StreamReader(stream)) {
                var recipient = new MedianetListEditService.Recipient();
                var props = TypeDescriptor.GetProperties(recipient);
                var csvReader = new CsvReader(reader,
                    true,
                    CsvReader.DefaultDelimiter,
                    CsvReader.DefaultQuote,
                    CsvReader.DefaultEscape,
                    CsvReader.DefaultComment,
                    ValueTrimmingOptions.All
                );
                string[] headers = csvReader.GetFieldHeaders();

                csvReader.MissingFieldAction = MissingFieldAction.ReplaceByEmpty;

                while (csvReader.ReadNextRecord()) {
                    recipient = DefaultRecipient();

                    foreach (PropertyDescriptor prop in props) {
                        if (headers.Contains(prop.Name)) {
                            prop.SetValue(recipient, csvReader[prop.Name]);
                        }
                    }

                    recipients.Add(recipient);
                }

                return recipients.ToArray();
            }
        }

        private MedianetListEditService.Recipient DefaultRecipient() {
            return new MedianetListEditService.Recipient {
                Address = string.Empty,
                AddressLine1 = string.Empty,
                AddressLine2 = string.Empty,
                Company = string.Empty,
                FirstName = string.Empty,
                JobTitle = string.Empty,
                LastName = string.Empty,
                MiddleName = string.Empty,
                PhoneNumber = string.Empty,
                ReferenceId = string.Empty,
                Title = string.Empty
            };
        }

        public CSVItem GenerateHeaderCsvRow() {
            var item = new CSVItem();
            var props = TypeDescriptor.GetProperties(item);
            var fields = typeof(CSVItem).GetFields();

            foreach (var f in fields) {
                f.SetValue(item, f.Name);
            }

            return item;
        }

        public abstract void GetRecipient(int ListId, int RecipientId);

        public abstract int AddServiceListRecipient();

        public abstract void UpdateServiceListRecipient();

        public abstract Stream GetRecipientsForExport(int ListId);

        #endregion

        /// <summary>
        /// Prepares for index page
        /// </summary>
        public void Index()
        {
            this.Lists = new List<SelectListItem>();
            this.ListRecipients = new List<SelectListItem>();

            using (var service = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                var services = service.Proxy.GetServiceListsByDebtorNumberDistributionType(session.debtorNumber, this.distributionType, 0, 1000, session.sessionKey);
                if (services != null)
                {
                    foreach (var item in services.Services)
                    {
                        this.Lists.Add(new SelectListItem() { Value = item.Id.ToString(), Text = item.SelectionDescription ?? @"Empty value", Selected = false });
                    }
                }
            }
        }

        /// <summary>
        /// Load recipients from a List Id
        /// </summary>
        protected internal void LoadListRecipients()
        {
            Medianet.Web.MedianetListService.Recipient[] recipients;
            this.ListRecipients = new List<SelectListItem>();

            using (var service = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                recipients = service.Proxy.GetServiceListRecipients(ListId.Value, this.session.sessionKey);
            }

            foreach (Medianet.Web.MedianetListService.Recipient recipient in recipients)
            {
                string description = String.Format("{0} {1} {2}", recipient.FirstName, recipient.MiddleName, recipient.LastName);

                if (description.Replace(" ", "") == String.Empty)
                {
                    this.ListRecipients.Add(new SelectListItem() { Value = recipient.Id.ToString(), Text = recipient.Address });                     
                }
                else
                    this.ListRecipients.Add(new SelectListItem() { Value = recipient.Id.ToString(), Text = description });
            }
        }

        /// <summary>
        /// Prepares a list to read its recipients
        /// </summary>
        public void EditListFromSummary(int ListId)
        {
            this.ListId = ListId;
            this.LoadListRecipients();
        }

        /// <summary>
        /// Replace a list recipients with new ones.
        /// </summary>
        public bool UpdateListFromSummary(Stream stream, int ListId)
        {
            MedianetListEditService.Recipient[] recipients = this.UploadForConversion(stream);

            try
            {
                using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
                {
                    service.Proxy.UpdateAllServiceListRecipients(ListId, recipients, session.sessionKey);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("UpdateListFromSummary failed.", ex);
                this.Error = ex.Message;                
            }
            return false;
        }

        /// <summary>
        /// Used to add a new list as well as recipients in one call.
        /// </summary>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        public bool AddServiceListFromSummary(Stream stream, string listName)
        {
            MedianetListEditService.Recipient[] recipients = this.UploadForConversion(stream);

            try
            {
                using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
                {
                    ServiceListUpload uploaded = new ServiceListUpload()
                    {
                        DistributionType = (MedianetListEditService.DistributionType)this.distributionType,
                        Recipients = recipients,
                        SelectionDescription = listName
                    };

                    service.Proxy.AddServiceListFromSummary(uploaded, session.sessionKey);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddServiceListFromSummary failed.", ex);
                this.Error = ex.Message;
            }
            return false;
        }

        /// <summary>
        /// Adds a new list given a collection of addresses
        /// </summary>
        /// <param name="Addresses"></param>
        /// <param name="listName"></param>
        /// <returns></returns>
        public bool AddServiceListFromPaste(string[] Addresses, string listName)
        {
            List<MedianetListEditService.Recipient> recipients = new List<MedianetListEditService.Recipient>();

            foreach (string Address in Addresses)
            {
                MedianetListEditService.Recipient recipient = new MedianetListEditService.Recipient();
                    recipient.Address = Address;

                recipients.Add(recipient);
            }

            try
            {
                using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
                {
                    ServiceListUpload uploaded = new ServiceListUpload()
                    {
                        DistributionType = (MedianetListEditService.DistributionType)this.distributionType,
                        Recipients = recipients.ToArray(),
                        SelectionDescription = listName
                    };

                    service.Proxy.AddServiceListFromSummary(uploaded, session.sessionKey);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("AddServiceListFromPaste failed.", ex);
                this.Error = ex.Message;
            }
            return false;
        }

        /// <summary>
        /// Used to delete an existing list. 
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="sessionKey"></param>
        public void DeleteServiceList(int serviceId)
        {
            using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
            {
                service.Proxy.DeleteServiceList(serviceId, this.session.sessionKey);
            }
        }

        /// <summary>
        /// Used to rename an existing list. 
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="name"></param>
        /// <param name="sessionKey"></param>
        public void RenameServiceList(int ListId, string Name)
        {
            using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
            {
                service.Proxy.RenameServiceList(ListId, Name, this.session.sessionKey);
            }
        }

        /// <summary>
        /// Used to delete a single existing recipient from an existing list. 
        /// </summary>
        /// <param name="serviceId"></param>
        /// <param name="recipientId"></param>
        /// <param name="sessionKey"></param>
        public void DeleteServiceListRecipient(int serviceId, int recipientId)
        {
            using (var service = new ServiceProxyHelper<ListEditServiceClient, IListEditService>())
            {
                service.Proxy.DeleteServiceListRecipient(serviceId, recipientId, this.session.sessionKey);
            }
        }
    }
}
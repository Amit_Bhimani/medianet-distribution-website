﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    [Serializable]
    public class DropdownDataModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
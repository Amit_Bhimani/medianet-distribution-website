﻿using System;
using System.Web;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Medianet.Web.NopCommerceServices;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.MedianetListService;
using Medianet.Constants;
using Medianet.Web.MedianetFileService;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using log4net;
using System.Text.RegularExpressions;
using DistributionType = Medianet.Web.MedianetListService.DistributionType;
using System.Configuration;

namespace Medianet.Web.Models
{

    [Serializable()]
    public class WireModel : ReleaseModel
    {
        public WireModel()
            : base()
        {            
            this.Attachments = new List<AttachmentModel>();
            this.PaymentResponse = null;
            this.needtoAddWebAndJournalistsPosting = true;
            this.forcePricing = true;
            this.isWire = true;
            this.Priority = MedianetReleaseService.ReleasePriorityType.Standard;
            this.Tweet = "News Release:";
            this.DerivedWireText = null;

            #region eCommerce workflow steps

            this.Steps = new WorkflowStep[] 
                { 
                    new WorkflowStep() { Route = "prepare"  , MenuName = "Content", WidgetPage = "wire-step1", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Prepare) , onAfterNext = new onAfterCallBack(this.SaveEmailWireText), Order = 1},
                    new WorkflowStep() { Route = "lists"    , MenuName = "Targets", WidgetPage = "wire-step2", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Lists), Order = 2},
                    new WorkflowStep() { Route = "wire"     , MenuName = "Newswire", WidgetPage = "wire-step3", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Wire ), Order = 3},
                    new WorkflowStep() { Route = "online"   , MenuName = "Global", WidgetPage = "wire-step4", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Online ), onAfterNext = new onAfterCallBack(this.SaveOnlinePage), onAfterPrevious = new onAfterCallBack(this.SaveOnlinePage), Order = 4},
                    new WorkflowStep() { Route = "contacts" , MenuName = "Your Contacts", WidgetPage = "wire-step5", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Contacts), Order = 5},
                    new WorkflowStep() { Route = "finalise" , MenuName = "Finalise", WidgetPage = "wire-step6", isInMenu = true, isDefault = false, onAfterAction = new PrepareCallBack(this.Finalise), Order = 6},
                    new WorkflowStep() { Route = "checkout", WidgetPage = "", Order = 7},
                    new WorkflowStep() { Route = "thanks", WidgetPage = "thanks", Order = 8}
                };

            this.Setup(this.Steps);

            #endregion
        }

        public override void Initialise()
        {
            AddDefaultProducts();
        }

        private int derivedWireFileId;
        public bool resetWireText;

        #region Release workflow steps data bindings
        
        /// <summary>
        /// Prepares all the data necessary to render the step I of the wire
        /// </summary>
        public new virtual void Prepare()
        {            
            this.PriorityList = new List<SelectListItem>();
            this.RecommendationList = new List<RelatedProductDTO>();
            this.QuickRecommendationList = new List<RecommendationDTO>();

            this.isPrepare = true;
            this.isOnline = false;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            this.WebCategoryList = GetWebCategories();
            this.SecondaryWebCategoryList = GetWebCategories();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);

            #region Priorities

            IEnumerable<MedianetReleaseService.ReleasePriorityType> list = (IEnumerable<MedianetReleaseService.ReleasePriorityType>)Enum.GetValues(typeof(Medianet.Web.MedianetReleaseService.ReleasePriorityType));

            foreach (Medianet.Web.MedianetReleaseService.ReleasePriorityType priority in list)
            {
                SelectListItem item = new SelectListItem();
                    item.Value = priority.ToString();
                    item.Text = priority.ToString();

                this.PriorityList.Add(item);
            }

            #endregion

            #region Hours, Minutes, Seconds

            this.Hours = new List<SelectListItem>();
            this.Minutes = new List<SelectListItem>();            

            for (int i = 0; i < 24; i++)
            {
                SelectListItem item = new SelectListItem();
                    item.Value = i.ToString("00");
                    item.Text = i.ToString("00");

                this.Hours.Add(item);
            }

            for (int i = 0; i < 60; i++)
            {
                SelectListItem item = new SelectListItem();
                    item.Value = i.ToString("00");
                    item.Text = i.ToString("00");

                this.Minutes.Add(item);
            }

            #endregion

            this.Sender = string.Empty;

            if ( 
                    ( this.Company == String.Empty ) ||
                    ( this.Company == null )
            )
            {
                this.Company = NopCommerce.Medianet_Company_Sender;
            }            

            base.Prepare();
        }

        /// <summary>
        /// Prepares the Social page
        /// </summary>
        public void Online()
        {
            this.isPrepare = false;
            this.isOnline = true;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
            GetRecommendations();

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.OnlineProductList = npc.Proxy.GetOnlineProducts();
                this.InternationalDistributionList = npc.Proxy.GetInternationalDistribution();
                this.InternationalCategories = npc.Proxy.GetChildCategories(NopCommerce.International_Distribution);
            }
        }

        /// <summary>
        /// Prepares all the data necessary to render the step 3 of the wire
        /// </summary>
        public void Wire()
        {
            this.isPrepare = false;
            this.isOnline = false;
            this.hasCheckedOut = false;
            this.isFinalise = false;
            this.hasSubmited = false;

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.UpdateCartAfterOperation(npc);
                this.CategoryList = npc.Proxy.GetWireCategories();
                this.RecommendationList = npc.Proxy.GetShoppingCartRecommendations(session.nopCommerceCustomerId);
                this.shoppingCart = npc.Proxy.GetCart(base.session.nopCommerceCustomerId);
                this.OnlineProductList = npc.Proxy.GetOnlineProducts();
            }

            GetRecommendations();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        public void GetRecommendations()
        {
            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                var list = npc.Proxy.GetRecommendations(this.WebCategoryId, session.nopCommerceCustomerId)
                    .Where(m => m.DistributionType != NopCommerce.CartDistType_Fax);

                this.QuickRecommendationList = new List<RecommendationDTO>(list);

                if ((!this.QuickRecommendationList.Any() ||
                     (this.QuickRecommendationList.Any() && this.QuickRecommendationList.Count < 3)) &&
                    this.SecondaryWebCategoryId != null)
                {
                    this.QuickRecommendationList.AddRange(
                        npc.Proxy.GetRecommendations(this.SecondaryWebCategoryId.GetValueOrDefault(),
                                                     session.nopCommerceCustomerId)
                           .Take(3 - this.QuickRecommendationList.Count));
                }
            }
        }

        /// <summary>
        /// In this action we upload the email wire text to medianet only if it has changed.
        /// </summary>
        protected void SaveEmailWireText(ReleaseModel model)
        {
            WireModel submitedModel = (WireModel)model;

            // changing the priority modifies in 20 % the price.
            RecalculateCartAfterPrepare = (submitedModel.Priority != this.Priority);

            this.isPrepared = true;

            #region Merge submited model

            this.BillingCode = submitedModel.BillingCode;
            this.SubjectHeadline = submitedModel.SubjectHeadline;
            this.Organisation = submitedModel.Organisation;
            this.CustomerRef = submitedModel.CustomerRef;
            this.Priority = submitedModel.Priority;

            this.DistributeAsFutureRelease = submitedModel.DistributeAsFutureRelease;
            this.DistributeAsEmbargoRelease = submitedModel.DistributeAsEmbargoRelease;

            this.EmailWireText = submitedModel.EmailWireText;
            this.Sender = string.Empty;
            this.Company = (submitedModel.Company == null) ? NopCommerce.Medianet_Company_Sender : submitedModel.Company;
            this.Company = submitedModel.Company;
            this.WebCategoryId = submitedModel.WebCategoryId;
            this.SecondaryWebCategoryId = submitedModel.SecondaryWebCategoryId;
            this.ScheduleDate = submitedModel.ScheduleDate;
            this.ScheduleTimeHour = submitedModel.ScheduleTimeHour;
            this.ScheduleTimeMinutes = submitedModel.ScheduleTimeMinutes;
            this.EmbargoDate = submitedModel.EmbargoDate;
            this.EmbargoTimeHour = submitedModel.EmbargoTimeHour;
            this.EmbargoTimeMinutes = submitedModel.EmbargoTimeMinutes;

            #endregion

            int emailWireTextChekSumUpdate = 0;

            if (String.IsNullOrEmpty(this.EmailWireText))
            {
                emailWireTextChekSumUpdate = this.EmailWireTextCheckSum;
                this.EmailwireTextTempFileUploadId = 0;
            }
            else
                emailWireTextChekSumUpdate = this.EmailWireText.GetHashCode();

            // Upload to medianet
            if (this.EmailWireTextCheckSum != emailWireTextChekSumUpdate)
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(this.EmailWireText);

                MemoryStream stream = new MemoryStream(byteArray);

                this.UploadForConversion(stream, "press_release.html",AttachmentTypeEnum.Release, true);

                this.EmailWireTextCheckSum = emailWireTextChekSumUpdate;
                RecalculateCartAfterPrepare = true;
            }
        }

        /// <summary>
        /// In this action we get the stuff not on the online page from the model.
        /// </summary>
        protected void SaveOnlinePage(ReleaseModel model)
        {
            WireModel submitedModel = (WireModel)model;

            this.Tweet = submitedModel.Tweet;
        }

        /// <summary>
        /// Prepares all the data necessary to render the step III of the wire
        /// </summary>
        public void Lists()
        {
            this.isPrepare = false;
            this.isOnline = false;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.TopCategoryList = nc.Proxy.GetTopCategoryTree();
                this.SpecialProductList = nc.Proxy.GetSpecialProducts();
                this.RecommendationList = nc.Proxy.GetShoppingCartRecommendations(session.nopCommerceCustomerId);
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
                this.RecentList = nc.Proxy.GetRecentProducts(session.nopCommerceCustomerId);
            }

            GetRecommendations();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);

            ServiceListNameFormaterHelper<ProductDTO> helper = new ServiceListNameFormaterHelper<ProductDTO>();
                this.RecentListFormated = helper.GroupServiceLists(RecentList.ToList(), "Name");
        }

        /// <summary>
        /// Autocompletes from a search text box
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public IEnumerable<string> AutoComplete(string criteria)
        {
            IList<string> names = new List<string>();

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                IList<ProductDTO> results = nc.Proxy.SearchProducts(criteria, 10);                

                foreach (ProductDTO product in results)
                {
                    names.Add(product.Name);
                }
            }

            return names;
        }

        /// <summary>
        /// Searchs the products given a criteria
        /// </summary>
        public void ProductSearch(string criteria)
        {
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.ProductAutoCompleteSearch = nc.Proxy.SearchProducts(criteria, 30);    
            }
            ServiceListNameFormaterHelper<ProductDTO> helper = new ServiceListNameFormaterHelper<ProductDTO>();
                this.ProductAutoCompleteSearchFormated = helper.GroupServiceLists((List<ProductDTO>)this.ProductAutoCompleteSearch.ToList(), "Name");
        }

        /// <summary>
        /// Searchs the products given a criteria
        /// </summary>
        public void ProductSearchByOutlet(string criteria)
        {
            List<ServiceList> results;

            using (var nc = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                results = nc.Proxy.SearchAapListsByOutletId(criteria, session.sessionKey)
                    .OrderBy(l => l.SelectionDescription).ThenBy(l => l.SequenceNumber).ToList();
            }

            this.ProductAutoCompleteSearch = new List<ProductDTO>();

            foreach (var r in results)
            {
                this.ProductAutoCompleteSearch.Add(
                    new ProductDTO()
                    {
                        Id = r.Id,
                        ProductId = 0,
                        ServiceId = r.Id,
                        DistributionType = ((char)r.DistributionType).ToString(),
                        Name = r.SelectionDescription,
                        FullDescription = r.SelectionDescription,
                        ShortDescription = r.SelectionDescription,
                        Addresses = r.AddressCount,
                        IsPackage = false,
                        HasEmailServices = (r.DistributionType == DistributionType.Email),
                        HasNewsHubServices = false,
                        HasJournalistsServices = false,
                        HasWireServices = false,
                        HasTwitterServices = false,
                        HasAapOneServices = false,
                        ChildProductIds = string.Empty,
                        IsCustomerList = false,
                        IsExtraRecipient = false,
                        IsPublished = true
                    });
            }

            ServiceListNameFormaterHelper<ProductDTO> helper = new ServiceListNameFormaterHelper<ProductDTO>();
            this.ProductAutoCompleteSearchFormated = helper.GroupServiceLists((List<ProductDTO>)this.ProductAutoCompleteSearch.ToList(), "Name");
        }

        /// <summary>
        /// Prepares all the data necessary to render the step IV of the wire
        /// </summary>
        public void Contacts()
        {
            this.isPrepare = false;
            this.isFinalise = false;
            this.hasCheckedOut = false;
            this.hasSubmited = false;
            
            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.RecommendationList = nc.Proxy.GetShoppingCartRecommendations(session.nopCommerceCustomerId);
                this.shoppingCart = nc.Proxy.GetCart(session.nopCommerceCustomerId);
            }

            GetRecommendations();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);

            using (var svc = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                CustomList = svc.Proxy.GetServiceListsByDebtorNumber(session.debtorNumber, session.sessionKey);

                if (CustomList != null && CustomList.Any())
                    CustomList = CustomList.Where(c => c.DistributionType != DistributionType.Fax).ToArray();
            }
        }

        /// <summary>
        /// Prepares all the data necessary to render the steps V(invasion!) of the wire
        /// </summary>
        public void Finalise()
        {
            this.isPrepare = false;
            this.isFinalise = true;
            this.hasCheckedOut = false;
            this.hasSubmited = false;

            this.RecommendationList = new List<RelatedProductDTO>();
            this.QuickRecommendationList = new List<RecommendationDTO>();

            using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
            }

            if (this.resetWireText)
            {
                this.WireUpdateApplied = false;
                this.ConfirmWireText = false;

                string fileExtension;
                string fileName;
                int fileSize;
                int itemCount;
                Stream fileData;
                int tmpFileRef = this.EmailwireTextTempFileUploadId;

                using (var ncc = new ServiceProxyHelper<FileServiceClient, IFileService>())
                {
                    //MEDI Change
                    if (ConfigurationManager.AppSettings["EnvironmentType"].ToString() != Medianet.Web.Helpers.Constants.environment_local)
                    {
                        string result = ncc.Proxy.GetTempFileDerivedContent(ref tmpFileRef, session.sessionKey, true,
                                                                            out fileExtension, out fileSize, out itemCount,
                                                                            out fileName, out fileData);
                        StreamReader streamReader = new StreamReader(fileData, Encoding.ASCII);
                        string text = streamReader.ReadToEnd();
                        streamReader.Close();
                        this.DerivedWireText = text;
                    }
                    else
                    {
                        this.DerivedWireText = "New Test";
                    }                    
                    derivedWireFileId = tmpFileRef;
                }
            }
            this.WebCategoryList = GetWebCategories();
            this.SecondaryWebCategoryList = GetWebCategories();
            this.WidgetList = GetWidgets(workflowStep.Value.WidgetPage);
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream, Encoding.ASCII);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public void UpdateWireContent(string wireText, bool confirmWire)
        {
            string fileExtension;
            string fileName;
            int fileSize;
            int itemCount;
            Stream fileData;
            int tmpFileId = derivedWireFileId;
            int derivedFileId;
            this.DerivedWireText = wireText;
            this.ConfirmWireText = confirmWire;

            using (var ncc = new ServiceProxyHelper<FileServiceClient, IFileService>())
            {
                // some browsers will use just \n for end of line. we want all lines to \r\n
                var formattedText = Regex.Replace(wireText, "[\r]?\n", "\r\n");

                using (Stream s = GenerateStreamFromString(formattedText))
                {
                    fileData = s;
                    string result = ncc.Proxy.UpdateTempFileDetails(tmpFileId, session.sessionKey, ref fileData, out fileExtension, out derivedFileId, out fileSize, out itemCount, out fileName);
                }
            }
            this.Words = itemCount;
            this.WireUpdateApplied = true;
            this.WebCategoryList = GetWebCategories();
            this.SecondaryWebCategoryList = GetWebCategories();
        }

        #endregion

        #region Release

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage="Your press release copy is required")]
        [AllowHtml]
        [Display(Name = "Email wire text")]        
        public string EmailWireText { get; set; }

        /// <summary>
        /// Used to contain derived wire text on the Finalise step only.
        /// This item should be null/nothing until the finalisation stage.
        /// </summary>
        [AllowHtml]
        public string DerivedWireText { get; set; }

        [Display(Name = "I confirm that the above Wire Release Text is correct.")]
        public bool ConfirmWireText { get; set; }
        public bool WireUpdateApplied { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Sender")]        
        public string Sender { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Company")]
        public string Company { get; set; }


        #endregion
    
        #region Shopping Cart

        /// <summary>
        /// This calculates the shopping cart price, and must be called only on items adding or removal, it stores the outcome in Price property
        /// </summary>
        public override void CalculatePrice()
        {
            // Ugly fix :(
            //if (this.EmailwireTextTempFileUploadId == 0)
            //{
            //    return;
            //}

            var request = new MedianetReleaseService.ReleaseQuoteRequest();
            request.MultimediaType = MedianetReleaseService.MultimediaType.None;
            request.DebtorNumber = session.debtorNumber;
            request.Priority = this.Priority;
            request.ReleaseDescription = this.SubjectHeadline;
            request.System = MedianetReleaseService.SystemType.Medianet;

            #region Adding products

            request.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();

            #endregion

            #region Adding Documents

            request.Documents = GenerateDocuments().ToArray();

            #endregion

            try
            {             
                using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    ReleaseQuoteResponse response = rsc.Proxy.QuoteRelease(request, session.sessionKey);

                    #region Refreshing shopping cart items price

                    if (response.Transactions != null)
                    {
                        foreach (TransactionQuoteResponse quoted in response.Transactions)
                        {
                            this.shoppingCart[quoted.SequenceNumber - 1].Price = quoted.SubTotalEx + quoted.GST;
                        }
                    }

                    #endregion

                    this.TotalPrice = response.Total;
                    this.QuoteReferenceId = response.QuoteReferenceId;
                }
            }
            catch (Exception e)
            {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
                this.Error = "*** Shopping Cart currently unavailable ****<br><br>Please contact Medianet on 1300 616 813 or <a href='mailto:team@medianet.com.au'>team@medianet.com.au</a> for final pricing.<br><br>Press releases can still be submitted through the portal.<br><br>We apologise for the inconvenience and are working hard to have this resolved as quickly as possible."; //e.Message;
            }
        }

        #endregion

        #region Produts search and browse and more.

        /// <summary>
        /// Category list loaded all at once.
        /// </summary>
        [NonSerialized]
        public IList<CategoryDTO> CategoryList;

        /// <summary>
        /// Category list loaded all at once.
        /// </summary>
        [NonSerialized]
        public IList<NopCommerceServices.Category> TopCategoryList;
               
        /// <summary>
        /// Recent list taken from the orders created in nopcommerce when a release is submited
        /// </summary>
        [NonSerialized]
        public IList<ProductDTO> RecentList;

        /// <summary>
        /// Nopcommerce has this ?
        /// </summary>
        [NonSerialized]
        public List<List<ProductDTO>> RecentListFormated;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<Product> SpecialProductList;

        /// <summary>
        /// Driven from text search
        /// </summary>
        [NonSerialized]
        public IList<Product> ProductSearchList;

        /// <summary>
        /// Driven from text search
        /// </summary>
        [NonSerialized]
        public List<List<Product>> ProductSearchListFormated;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<ProductDTO> ProductAutoCompleteSearch;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public List<List<ProductDTO>> ProductAutoCompleteSearchFormated;

        /// <summary>
        /// List get from mediant customer list
        /// </summary>
        [NonSerialized]
        public IList<ServiceList> CustomList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<ExtraReceipient> ExtraRecipientList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> PriorityList;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Hours;

        /// <summary>
        /// 
        /// </summary>
        [NonSerialized]
        public IList<SelectListItem> Minutes;

        [NonSerialized]
        public bool RecalculateCartAfterPrepare;

        #endregion

        #region Workflow engine overrides

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toStep"></param>
        /// <returns></returns>
        public override bool CanTransition()
        {
            bool canTransition = true;

            if (this.workflowStep.Value.Route == "prepare")
            {
                return true;
            }
            else if (this.workflowStep.Value.Route == "lists")
            {
                canTransition = this.ValidatePrepare();
            }
            else if (this.workflowStep.Value.Route == "wire")
            {
                canTransition = true;
            }
            else if (this.workflowStep.Value.Route == "online")
            {
                canTransition = true;
            }
            else if (this.workflowStep.Value.Route == "contacts")
            {
                canTransition = this.ValidateOnline();
            }
            else if (this.workflowStep.Value.Route == "finalise")
            {
                canTransition = this.ValidateCart();
                // Do a second validation just to display a model state error
                this.ValidateCart(true);
            }
            else if (this.workflowStep.Value.Route == "payment")
            {
                if (HasConfirmedWire() && IsTwitterCorrect())
                    canTransition = this.ValidateCanPay();
                else
                    canTransition = false;
            }
            else if (this.workflowStep.Value.Route == "confirm")
            {
                if (HasConfirmedWire() && IsTwitterCorrect())
                    canTransition = this.ValidateCanConfirm();
                else
                    canTransition = false;
            }
            else if (this.workflowStep.Value.Route == "checkout")
            {
                if (HasConfirmedWire() && IsTwitterCorrect())
                    canTransition = this.ValidateCanPay();
                else
                    canTransition = false;
            }
            else if (this.workflowStep.Value.Route == "thanks")
            {
                canTransition = this.ValidateCart();
            }

            return canTransition;
        }

        private bool HasConfirmedWire()
        {
            if (this.IsWireListAdded())
            {
                if (!this.ConfirmWireText && !this.WireUpdateApplied)
                {
                    this.modelState.AddModelError("DerivedWireText",
                                                  "To proceed, please confirm the content of your Wire Release is correct.");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Validates the prepare step of the workflow
        /// </summary>
        /// <returns></returns>
        protected new internal virtual bool ValidatePrepare()
        {
            if ( this.EmailwireTextTempFileUploadId == 0 )
            {
                modelState.AddModelError("EmailWireText", "You didn't fill in the email wire text or the convertion failed, please try again");
            }

            if (this.DistributeAsFutureRelease)
            {
                if (!this.ScheduleDate.HasValue)
                {
                    modelState.AddModelError("ScheduleDate", "If you plan a future release, please select the date");
                }
            }

            if (this.DistributeAsEmbargoRelease)
            {
                if (!this.EmbargoDate.HasValue)
                {
                    modelState.AddModelError("EmbargoDate", "If you wish to embargo this release, please select the date");
                }
            }
            if (ConfigurationManager.AppSettings["EnvironmentType"].ToString() != Medianet.Web.Helpers.Constants.environment_local)
            {
                if (this.Words == 0)
                {
                    modelState.AddModelError("EmailWireText", "You didn't fill in the email wire text or the convertion failed, please try again");
                }
            }
            return base.ValidatePrepare();
        }

        protected internal virtual bool ValidateOnline()
        {
            // Ignore all the model validation errors for the prepare page
            this.modelState.Clear();

            if (this.IsTwitterPostingAdded())
            {
                if (string.IsNullOrWhiteSpace(this.Tweet))
                    modelState.AddModelError("Tweet", "Please enter your tweet text");
            }

            return this.modelState.IsValid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected internal override bool ValidateCart(bool MustContainIncludedItems = false)
        { 
            if (this.shoppingCart.Count == 0 )
            {
                this.modelState.AddModelError("shoppingCart", "You need to add at least one product, special, customer contacts or extra recipient.");
                return false;
            }
            if (MustContainIncludedItems && shoppingCart.Count(s => s.Included) == 0)
            {
                this.modelState.AddModelError("shoppingCart", "You need to add at least one product, special, customer contacts or extra recipient.");
                return false;
            }
            
            return true;
        }
        
        /// <summary>
        /// Gets the credit card form
        /// </summary>
        public override void Payment()
        {
            this.isPrepare = false;
            this.hasCheckedOut = true;

            if (this.PaymentResponse == null)
            {
                int shoppingCartItemCount = this.shoppingCart.Where(s => s.Included).Count();

                using (var npc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
                {
                    this.shoppingCart = npc.Proxy.GetCart(session.nopCommerceCustomerId);
                }

                using (var mpc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    PaymentStartRequest paymentRequest = new PaymentStartRequest();
                    paymentRequest.ReleaseQuote = new MedianetReleaseService.ReleaseQuoteRequest();
                    paymentRequest.ReleaseQuote.DebtorNumber = session.debtorNumber;
                    paymentRequest.ReleaseQuote.MultimediaType = MedianetReleaseService.MultimediaType.None;
                    paymentRequest.ReleaseQuote.Priority = this.Priority;
                    paymentRequest.ReleaseQuote.ReleaseDescription = this.SubjectHeadline;
                    paymentRequest.ReleaseQuote.System = MedianetReleaseService.SystemType.Medianet;
                    paymentRequest.ReleaseQuote.BillingCode = CleanBillingCode(this.BillingCode);
                    paymentRequest.ReleaseQuote.CustomerReference = this.CustomerRef;
                    paymentRequest.RedirectURL = String.Format("{0}://{1}/Wire/Thanks", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST"));
    
                    #region Adding services

                    paymentRequest.ReleaseQuote.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();
                    
                    #endregion

                    #region Adding document

                    paymentRequest.ReleaseQuote.Documents = GenerateDocuments().ToArray();
    
                    #endregion

                    try
                    {
                        this.PaymentResponse = mpc.Proxy.InitialisePayment(paymentRequest, session.sessionKey);
                        this.QuoteReferenceId = this.PaymentResponse.QuoteReferenceId;
                    }
                    catch (Exception e)
                    {
                        ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                        log.Error(null, e);
                        this.Error = e.Message;
                    }
                }
            }
        }

        /// <summary>
        /// Submits the release to Medianet
        /// </summary>
        public override void SubmitRelease()
        {
            int shoppingCartItemCount = this.shoppingCart.Where(s => s.Included).Count();

            #region Preparing release

            var release = new ReleaseSummary();
            release.DebtorNumber = session.debtorNumber;
            release.System = MedianetReleaseService.SystemType.Medianet;
            release.BillingCode = CleanBillingCode(this.BillingCode);
            release.ReleaseDescription = this.SubjectHeadline;
            release.Organisation = this.Organisation;
            release.CustomerReference = this.CustomerRef;
            release.HoldUntilDate = this.DistributeAsFutureRelease ? this.HoldUntilDate : null;
            release.EmbargoUntilDate = this.DistributeAsEmbargoRelease ? this.EmbargoUntilDate : null;
            release.Priority = this.Priority;
            release.MultimediaType = Medianet.Web.MedianetReleaseService.MultimediaType.None;
            release.WebCategoryId = this.WebCategoryId;

            release.ReportResultsToSend = session.customerResultType;
            release.QuoteReferenceId = this.QuoteReferenceId;
            release.TimezoneCode = session.TimeZone.Code;
            release.IsVerified = true;
            release.EmailFromAddress = this.Sender;
            release.EmailFromName = this.Company;
            release.TransactionId = this.TransactionId;

            if (session.userReportSendMethod == ReportType.Email || session.userReportSendMethod == ReportType.Both)
            {
                release.ReportEmailAddress = session.customerEmailAddress;
            }

            #endregion

            #region Adding services

            release.Transactions = GenerateTransactionsFromCart(this.shoppingCart).ToArray();
            if (release.Transactions.Any(t => t.IsSecondaryWebCategory))
            {
                release.SecondaryWebCategoryId = this.SecondaryWebCategoryId;
            }
            #endregion

            #region Adding document

            release.Documents = GenerateDocuments().ToArray();

            #endregion

            try
            {
                using (var rsc = new ServiceProxyHelper<ReleaseServiceClient, IReleaseService>())
                {
                    rsc.Proxy.AddReleaseFromSummary(release, session.sessionKey);
                }
            }
            catch (Exception e)
            {
                this.Error = e.Message;
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, e);
                return;
            }

            using (var nc = new ServiceProxyHelper<NopCommerceClient, INopCommerce>())
            {
                nc.Proxy.CleanCartAndCreateOrders(session.nopCommerceCustomerId, this.TotalPrice);
            }
        }

        private List<MedianetReleaseService.DocumentSummary> GenerateDocuments()
        {
            var docList = new List<MedianetReleaseService.DocumentSummary>();

            var emailWiredoc = new MedianetReleaseService.DocumentSummary();
            emailWiredoc.SequenceNumber = docList.Count() + 1;
            emailWiredoc.CanDistributeViaEmailBody = true;
            emailWiredoc.CanDistributeViaWire = true;
            emailWiredoc.TempFileId = this.EmailwireTextTempFileUploadId;
            docList.Add(emailWiredoc);

            if (this.Release != null)
            {
                var doc = new MedianetReleaseService.DocumentSummary();
                doc.SequenceNumber = docList.Count() + 1;
                doc.CanDistributeViaWeb = true;
                doc.TempFileId = this.Release.MedianetAttachmentId;

                docList.Add(doc);
            }

            foreach (AttachmentModel attachment in this.Attachments)
            {
                var doc = new MedianetReleaseService.DocumentSummary();
                doc.SequenceNumber = docList.Count() + 1;
                doc.CanDistributeViaEmailAttachment = true;
                doc.Description = attachment.Name;
                doc.TempFileId = attachment.MedianetAttachmentId;

                docList.Add(doc);
            }

            return docList;
        }

        /// <summary>
        /// THe email text and subject cannot be empty for a wire release
        /// </summary>
        /// <returns></returns>
        public override bool ValidReleaseContent()
        {
            return (!string.IsNullOrEmpty(SubjectHeadline)) && (!string.IsNullOrEmpty(EmailWireText));
        }

        #endregion
    }


    public enum OnlineProductType
    {
        ImageWire = 10006139
    }
}
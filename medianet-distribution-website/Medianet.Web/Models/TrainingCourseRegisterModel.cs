﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.Validators;

namespace Medianet.Web.Models
{
    [Serializable]
    public class TrainingCourseRegisterModel : BaseModel
    {
        public const string PaymentMethod_Creditcard = "C";
        public const string PaymentMethod_Invoice = "I";

        #region Model

        public string CourseId { get; set; }
        public string CourseTitle { get; set; }
        public string CourseSummary { get; set; }
        public string CourseDescription { get; set; }
        public decimal CoursePrice { get; set; }
        public List<string> CourseTopics { get; set; }
        public int ScheduleId { get; set; }
        public DateTime ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        public string ScheduleLocation { get; set; }
        public string ScheduleAddress { get; set; }
        public string ScheduleAgenda { get; set; }
        public string ScheduleFormHandler { get; set; }
        public int TotalSeats { get; set; }
        public int SeatsRemaining { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Last name")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Company")]
        public string Company { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "ABN")]
        public string ABN { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Position")]
        public string Position { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Contact number")]
        [RegularExpression(@"^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$", ErrorMessage = "The phone number must be input in this format : 0212345678")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Experience Level")]
        public string ExperienceLevel { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Desired Outcome")]
        public string DesiredOutcome { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Topics")]
        public string Topics { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Postcode")]
        public string PostCode { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [BooleanRequired(ErrorMessage = "You must accept Medianet's terms and conditions.")]
        public bool AgreeConditions { get; set; }

        [NonSerialized]
        public IList<SelectListItem> States;

        [NonSerialized]
        public List<SelectListItem> Countries;

        public string PaymentMethod { get; set; }
        public PaymentStartResponse PaymentResponse { get; set; }
        public bool HasFinalised { get; set; }
        public string PaymentError { get; set; }
        
        public string PromoCode { get; set; }
        public List<PromoCodeModel> PromoCodes { get; set; }

        #endregion

        public TrainingCourseRegisterModel()
        {
            Prepare();
        }

        public void Prepare()
        {
            this.PaymentMethod = PaymentMethod_Creditcard;

            this.States = new List<SelectListItem>();
            this.States.Add(new SelectListItem() { Text = "New South Wales", Value = "NSW", Selected = true });
            this.States.Add(new SelectListItem() { Text = "Northern Territory", Value = "NT" });
            this.States.Add(new SelectListItem() { Text = "Australian Capital Territory", Value = "ACT" });
            this.States.Add(new SelectListItem() { Text = "Victoria", Value = "VIC" });
            this.States.Add(new SelectListItem() { Text = "Queensland", Value = "QLD" });
            this.States.Add(new SelectListItem() { Text = "Western Australia", Value = "WA" });
            this.States.Add(new SelectListItem() { Text = "South  Australia", Value = "SA" });
            this.States.Add(new SelectListItem() { Text = "Tasmania", Value = "TAS" });

            this.Countries = new List<SelectListItem>();
            this.Countries.Add(new SelectListItem() { Text = "Australia", Value = "Australia", Selected = true });
        }
    }
}
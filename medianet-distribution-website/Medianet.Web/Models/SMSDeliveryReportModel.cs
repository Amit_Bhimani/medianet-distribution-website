﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    public class SMSDeliveryReportModel
    {
        public MessageMediaVendor vendor_account_id { get; set; }

        public string callback_url { get; set; }

        public string delivery_report_id { get; set; }

        public string source_number { get; set; }

        public DateTime date_received { get; set; }

        public MessageMediaStatus status { get; set; }

        public int delay { get; set; }

        public DateTime submitted_date { get; set; }

        public string original_text { get; set; }

        public string message_id { get; set; }

        public string error_code { get; set; }

        public SMSDeliveryReportMetadataModel metadata { get; set; }
    }

    public class MessageMediaVendor
    {
        public string vendor_id { get; set; }

        public string account_id { get; set; }
    }

    public class SMSDeliveryReportMetadataModel
    {
        public int JobId { get; set; }

        public int TransactionId { get; set; }

        public int? DistributionId { get; set; }
    }

    public enum MessageMediaStatus
    {
        enroute,
        submitted,
        delivered,
        expired,
        rejected,
        failed
    }


    public enum MessageMediaError
    {
        [Description("Processed by the gateway")]
        ProcessedByTheGateway = 101,

        [Description("Being rerouted")]
        BeingRerouted = 102,

        [Description("Held for screening")]
        HeldForScreening = 151,

        [Description("Submitted")]
        submitted = 200,

        [Description("Accepted by downstream provider")]
        AcceptedByDownstreamProvider = 210,

        [Description("Enroute for delivery")]
        EnrouteForDelivery = 211,

        [Description("Submitted. Delivery pending")]
        Submitted_DeliveryPending = 212,

        [Description("Scheduled for delivery")]
        ScheduledForDelivery = 213,

        [Description("Delivered")]
        Delivered = 220,

        [Description("delivered to the handset")]
        DeliveredToTheHandset = 221,

        [Description("Expired (prior to submission)")]
        Expired_PriorToSubmission = 320,

        [Description("Expired (before delivery)")]
        Expired_BeforeDelivery = 401,

        [Description("Usage threshold reached. discarded")]
        Discarded = 301,

        [Description("Destination address blocked. discarded")]
        DestinationAddressBlocked_Discarded = 302,

        [Description("Source address blocked. discarded")]
        SourceAddressBlocked_Discarded = 303,

        [Description("dropped")]
        MessageDropped_ContactSupport = 304,

        [Description("duplicate detection")]
        MessageDiscarded_dueToDuplicateDetection = 305,

        [Description("Message rejected by downstream provider")]
        MessageRejectedByDownstreamProvider = 402,

        [Description("Message skipped by downstream provider")]
        MessageSkippedByDownstreamProvider = 403,

        [Description("Invalid source address.")]
        InvalidSourceAddress = 410,

        [Description("Invalid destination address")]
        InvalidDestinationAddress = 411,

        [Description("Destination address blocked")]
        DestinationAddressBlocked = 412,

        [Description("SMS service unavailable on destination")]
        SMSServiceUnavailableOnDestination = 413,

        [Description("Destination unreachable")]
        DestinationUnreachable = 414,

        [Description("Gateway failure")]
        GatewayFailure = 330,

        [Description("Message discarded")]
        MessageDiscarded = 331,

        [Description("No available route to destination")]
        NoAvailableRouteToDestination = 332,

        [Description("Source address unsupported for this destination")]
        SourceAddressUnsupportedForThisDestination = 333,

        [Description("Message failed; undeliverable")]
        MessageFailed_Undeliverable = 400,

        [Description("Message cancelled or deleted by provider")]
        MessageCancelledOrDeletedByProvider = 405
    }
}
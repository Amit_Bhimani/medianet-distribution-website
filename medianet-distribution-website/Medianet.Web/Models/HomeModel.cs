﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Collections.Generic;
using Medianet.Web.Helpers.Caching;
using Medianet.Web.MedianetListService;
using Medianet.Constants;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetCustomerService;
using Medianet.Web.MedianetGeneralService;
using log4net;
using Medianet.Web.MedianetChargeBeeService;

namespace Medianet.Web.Models
{
    public class HomeModel : BaseModel
    {
        #region Model

        /// <summary>
        /// 
        /// </summary>
        public IList<AgendaItem> AgendaItems { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<TrainingItem> TrainingItems { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public DailyDiary Diary { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MediaStatistics Stats { get; set; }

        public IList<Medianet.Web.MedianetCustomerService.User> Users { get; set; }

        public string AccountManagerName { get; set; }
        public string AccountManagerImageUrl { get; set; }
        public string AccountManagerEmail { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public bool canUpgradeAccount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MedianetPublicSiteUrl { get; set; }

        public List<TopPerformingList> TopListsByWeek;

        public List<TopPerformingList> TopListsByMonth;

        public CustomerStatistic UsageStatistics;

        public List<PackageList> TopValuePackages;
        
        #endregion

        /// <summary>
        /// Get home page content
        /// </summary>
        public void GetHomePageContent()
        {
            #region Stats

            this.Stats = (MediaStatistics)HttpContext.Current.Application[NopCommerce.Application_Global_Stats];

            if ( this.Stats == null )
            {
                this.Stats = new MediaStatistics();
            }

            #endregion

            #region links

            this.MedianetPublicSiteUrl =
                System.Web.Configuration.WebConfigurationManager.AppSettings["MedianetPublicSiteURL"].ToString();

            #endregion
        }

        /// <summary>
        /// Get editorial items
        /// </summary>
        public void GetEditorialDiary()
        {
            this.Diary = Cacher.Get(CacheDiary, 300, PublicContentModel.GetEditorialDiary);
        }

        /// <summary>
        /// Get agenda items
        /// </summary>
        public void GetAgendaItems()
        {
            this.AgendaItems = Cacher.Get(CacheAgenda, 300, PublicContentModel.GetAgendaForToday);
        }

        /// <summary>
        /// Get training items
        /// </summary>
        public void GetTrainingCourses()
        {
            this.TrainingItems = Cacher.Get(CacheTrainingCourses, 300, PublicContentModel.GetTrainingCourses);
        }

        /// <summary>
        /// Get all Users for this customer
        /// </summary>
        public void GetUsers()
        {
            using (var cs = new ServiceProxyHelper<CustomerServiceClient, ICustomerService>())
            {
                this.Users = cs.Proxy.GetAllUsersByCustomer(session.debtorNumber, session.sessionKey);
            }
        }

        /// <summary>
        /// Get the default Sales Region details for this customer
        /// </summary>
        public void GetSalesRegion()
        {
            this.AccountManagerName = string.Empty;
            this.AccountManagerImageUrl = string.Empty;
            this.AccountManagerEmail = string.Empty;

            if (string.IsNullOrWhiteSpace(session.SalesRegionName)) return;
            
            using (var cs = new ServiceProxyHelper<ChargeBeeServiceClient, IChargeBeeService>())
            {
                try
                {
                    var region = cs.Proxy.GetSalesRegion(session.SalesRegionName);

                    if (region != null)
                    {
                        this.AccountManagerName = region.AccountManagerName;
                        this.AccountManagerImageUrl = region.AccountManagerImageUrl;
                        this.AccountManagerEmail = region.SalesEmail;
                    }
                }
                catch (Exception)
                {
                    // Ignore  
                }
            }
            //if (!session.SalesRegionId.HasValue) return;

            //using (var cs = new ServiceProxyHelper<CustomerServiceClient, ICustomerService>())
            //{
            //    try
            //    {
            //        var region = cs.Proxy.GetSalesRegion(session.SalesRegionId.Value);

            //        if (region != null)
            //        {
            //            this.AccountManagerName = region.AccountManagerName;
            //            this.AccountManagerImageUrl = region.AccountManagerImageUrl;
            //            this.AccountManagerEmail = region.SalesEmail;
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        // Ignore  
            //    }
            //}
        }

        /// <summary>
        /// Get the top 5 performing lists
        /// </summary>
        public void GetTopPerformingLists()
        {
            this.TopListsByWeek = Cacher.Get(CacheTopListsWeek, 1800, () => GetTopPerformingListsFromDb(PeriodType.Week));
            this.TopListsByMonth = Cacher.Get(CacheTopListsMonth, 1800, () => GetTopPerformingListsFromDb(PeriodType.Month));
        }

        private List<TopPerformingList> GetTopPerformingListsFromDb(PeriodType period)
        {
            List<TopPerformingList> topLists = null;

            using (var cs = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                try
                {
                    topLists = cs.Proxy.GetTopPerformingLists(period, 5).ToList();
                }
                catch (Exception ex)
                {
                    // Ignore, but log
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error calling GetTopPerformingLists.", ex);
                    return new List<TopPerformingList>();
                }
            }

            return topLists;
        }

        /// <summary>
        /// Get the top 3 top value packages
        /// </summary>
        public void GetTopValuePackages()
        {
            this.TopValuePackages = Cacher.Get(CacheTopPackages, 300, GetTopValuePackagesFromDb);
        }

        private List<PackageList> GetTopValuePackagesFromDb()
        {
            List<PackageList> packages = null;

            using (var cs = new ServiceProxyHelper<ListServiceClient, IListService>())
            {
                try
                {
                    packages = cs.Proxy.GetTopValuePackageLists(session.sessionKey).Take(3).OrderBy(p => p.SelectionDescription).ToList();
                }
                catch (Exception ex)
                {
                    // Ignore, but log
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error calling GetTopValuePackagesFromDb.", ex);
                    return new List<PackageList>();
                }
            }

            return packages;
        }

        public void GetCustomerStatistics()
        {
            using (var cs = new ServiceProxyHelper<CustomerServiceClient, ICustomerService>())
            {
                try
                {
                    this.UsageStatistics = cs.Proxy.GetCustomerStatistics(session.debtorNumber, session.sessionKey);
                }
                catch (Exception ex)
                {
                    // Ignore, but log
                    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error calling GetCustomerStatistics.", ex);
                }
            }
        }
    }
}
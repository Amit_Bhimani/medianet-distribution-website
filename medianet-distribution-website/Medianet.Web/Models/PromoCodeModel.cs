﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Models
{
    [Serializable]
    public class PromoCodeModel
    {
        public string PromoCode { get; set; }
        public int PackageId { get; set; }
        public decimal Price { get; set; }
        public string CourseId { get; set; }
    }
}
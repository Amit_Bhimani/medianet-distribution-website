﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FluentValidation.Mvc;
using Medianet.Web.MedianetCustomerService;
using log4net;
using Medianet.Web.Helpers;
using Medianet.Web.MedianetListService;
using Medianet.Constants;
using Medianet.Web.MedianetGeneralService;

namespace Medianet.Web
{
    /// <summary>
    /// Customised app error handling.
    /// </summary>
    public class log4netExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context) {
            Exception ex = context.Exception;

            ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Error(null, ex);
        }
    }

    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Default
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new log4netExceptionFilter());
            filters.Add(new HandleAuthentication());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="routes"></param>
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region Release Email resources

            routes.MapRoute(
                "Logo_Download",
                "da/da.aspx",
                new { controller = "Download", action = "GenericDownload" }
            );

            routes.MapRoute(
                "Email_Open",
                "SubmitReleases/OpenTracking.aspx",
                new { controller = "Tracking", action = "EmailOpened" }
            );
            
            routes.MapRoute("SMSWebhook", "SMSWebhook/{action}", new { controller = "SMSWebhook", action = "SMSWebhook" });

            #endregion

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                new { controller = "((?!img).*)" } // Needed to exclude paths starting with img/common/ so the Image_Download route handles them.
            );

            #region Home Page

           // routes.MapRoute(
           //     "HomePage",
           //     "{Home}/{Index}",
           //     new { controller = "Home", action = "Index" }
           // );

           // routes.MapRoute(
           //     "HomePage_Terms",
           //     "{Home}/{Terms}",
           //     new { controller = "Home", action = "Terms" }
           // );

           // routes.MapRoute(
           //     "HomePage_Privacy",
           //     "{Home}/{Privacy}",
           //     new { controller = "Home", action = "Privacy" }
           // );

           // routes.MapRoute(
           //     "HomePage_Contact",
           //     "{Home}/{Contact}",
           //     new { controller = "Home", action = "Contact" }
           // );

           // routes.MapRoute(
           //     "HomePage_About",
           //     "{Home}/{About}",
           //     new { controller = "Home", action = "About" }
           // );

           // routes.MapRoute(
           //     "HomePage_Database",
           //     "{Home}/{Database}",
           //     new { controller = "Home", action = "Database" }
           // );

           // routes.MapRoute(
           //     "HomePage_Editorial",
           //     "{Home}/{Editorial}",
           //     new { controller = "Home", action = "Editorial" }
           // );

           // routes.MapRoute(
           //     "HomePage_Calendar",
           //     "{Home}/{Calendar}",
           //     new { controller = "Home", action = "Calendar" }
           // );

           // routes.MapRoute(
           //     "HomePage_Training",
           //     "{Home}/{Training}",
           //     new { controller = "Home", action = "Training" }
           // );

           // #endregion

           // #region Account

           // routes.MapRoute(
           //     "Account_Register",
           //     "{Account}/{Register}",
           //     new { controller = "Account", action = "Register" }
           // );

           // routes.MapRoute(
           //     "Account_Edit",
           //     "{Account}/{Edit}",
           //     new { controller = "Account", action = "Edit" }
           // );

           // routes.MapRoute(
           //     "Account_Register_Details",
           //     "{Account}/{Details}",
           //     new { controller = "Account", action = "Details" }
           // );

           // #endregion

           // #region Audio release

           // routes.MapRoute(
           //     "Audio_Release_prepare",
           //     "{Audio}/",
           //     new { controller = "Audio", action = "Prepare" }
           // );

           // routes.MapRoute(
           //     "Audio_Release_contacts",
           //     "{Audio}/{Contacts}",
           //     new { controller = "Audio", action = "Contacts" }
           // );

           // routes.MapRoute(
           //     "Audio_Release_finalise",
           //     "{Audio}/{Finalise}",
           //     new { controller = "Audio", action = "Finalise" }
           // );

           // #endregion

           // #region Wire, eMail Release

           // routes.MapRoute(
           //     "Wire_Email_Release_prepare",
           //     "{Wire}/",
           //     new { controller = "Wire", action = "Prepare" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_prepareIndex",
           //     "{Wire}/{index}",
           //     new { controller = "Wire", action = "Prepare" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_preparePrepare",
           //     "{Wire}/{prepare}",
           //     new { controller = "Wire", action = "Prepare" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_selectLists",
           //     "{Wire}/{lists}",
           //     new { controller = "Wire", action = "Lists" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_addContacts",
           //     "{Wire}/{contacts}",
           //     new { controller = "Wire", action = "Contacts" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_Finalise",
           //     "{Wire}/{finalise}",
           //     new { controller = "Wire", action = "Finalise" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_Checkout",
           //     "{Wire}/{Checkout}",
           //     new { controller = "Wire", action = "Checkout" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_Confirm",
           //     "{Wire}/{Confirm}",
           //     new { controller = "Wire", action = "Confirm" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_Payment",
           //     "{Wire}/{Payment}",
           //     new { controller = "Wire", action = "Payment" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_Thanks",
           //     "{Wire}/{Thanks}",
           //     new { controller = "Wire", action = "Thanks" }
           // );

           // routes.MapRoute(
           //     "Wire_Email_Release_Preview",
           //     "{Wire}/{finalise}/{id}",
           //     new { controller = "Wire", action = "Preview" }
           // );

           // #endregion

           // #region Video release

           // routes.MapRoute(
           //     "Video_Release_Index",
           //     "{Video}/{Index}",
           //     new { controller = "Video", action = "Index" }
           // );

           // routes.MapRoute(
           //     "Video_Release_Prepare",
           //     "{Video}/{Prepare}",
           //     new { controller = "Video", action = "Prepare" }
           // );

           // #endregion

           // #region Sms release

           // routes.MapRoute(
           //     "Sms_Release_Prepare",
           //     "{Sms}/",
           //     new { controller = "Sms", action = "Prepare" }
           // );

           // routes.MapRoute(
           //     "Sms_Release_Contacts",
           //     "{Sms}/{Contacts}",
           //     new { controller = "Sms", action = "Contacts" }
           // );

           // routes.MapRoute(
           //     "Sms_Release_Finalise",
           //     "{Sms}/{Finalise}",
           //     new { controller = "Sms", action = "Finalise" }
           // );

           // #endregion
          
           // #region Multimedia release

           // routes.MapRoute(
           //     "Multimedia_Release_Index",
           //     "{Multimedia}/{Index}",
           //     new { controller = "Multimedia", action = "Index" }
           // );

           // #endregion

           // #region Results

           // routes.MapRoute(
           //     "Report_Index",
           //     "{Report}/",
           //     new { controller = "Report", action = "Index" }
           // );

           // routes.MapRoute(
           //     "Report_Index_View",
           //     "{Report}/{View}",
           //     new { controller = "Report", action = "View" }
           // );

           // #endregion

           // #region Manage Lists

           // /* ------------------------------------------------------------------------------  eMail */

           // routes.MapRoute(
           //     "ManageEmail_Index",
           //     "{ManageEmail}/",
           //     new { controller = "ManageEmail", action = "Index" }
           // );

           // routes.MapRoute(
           //    "ManageEmail_Download",
           //    "{ManageEMail}/{Download}/{Id}",
           //    new { controller = "ManageEMail", action = "Download" }
           //);

           // /* ------------------------------------------------------------------------------  Sms */

           // routes.MapRoute(
           //     "ManageSms_Index",
           //     "{ManageSms}/",
           //     new { controller = "ManageSms", action = "Index" }
           // );

           // routes.MapRoute(
           //    "ManageSms_Download",
           //    "{ManageSms}/{Download}/{Id}",
           //    new { controller = "ManageSms", action = "Download" }
           //);

            #endregion

            // This needs to go at the end, otherwise RedirectToAction() and Html.ActionLink() calls break.
            routes.Add(
                "Image_Download",
                new Route("img/common/{*filepath}", new ImageRouteHandler())
            );


        }

        /// <summary>
        /// Medianet own app start
        /// </summary>
        private void Medianet_Start() {
            #region List ticker

            try {
                using (var service = new ServiceProxyHelper<ListServiceClient, IListService>()) {
                    Application[NopCommerce.Application_Ticker] = service.Proxy.GetTicker();
                }

            }
            catch (Exception ex) {
                ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                log.Error(null, ex);
                Application[NopCommerce.Application_Ticker] = new ListTicker[0];
            }

            #endregion

            #region Stats

            using (var gs = new ServiceProxyHelper<GeneralServiceClient, IGeneralService>()) {
                Application[NopCommerce.Application_Global_Stats] = gs.Proxy.GetMediaStatistics();
            }

            #endregion

            #region Authentication overrides

            string SkipAuthentication = System.Web.Configuration.WebConfigurationManager.AppSettings["SkipAuthentication"].ToString();

            Application["SkipAuthentication"] = SkipAuthentication.Split(',').ToList();

            #endregion
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            AutoMapperConfigurator.Configure();
        }

        /// <summary>
        ///
        /// </summary>
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            FluentValidationModelValidatorProvider.Configure();

            AutoMapperConfigurator.Configure();

            this.Medianet_Start();
        }
    }
}
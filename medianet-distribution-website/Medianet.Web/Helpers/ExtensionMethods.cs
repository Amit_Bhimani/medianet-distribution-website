﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Medianet.Web.MedianetReleaseService;

namespace Medianet.Web.Helpers
{
    public static class ExtensionMethods
    {
        public static string DisplayReleaseStatus(this HtmlHelper helper, ReleaseStatusType status)
        {
            switch (status)
            {
                case ReleaseStatusType.Invoiced:
                case ReleaseStatusType.Accounts:
                case ReleaseStatusType.Resulted:
                    return "Resulted";
                case ReleaseStatusType.OnHold:
                    return "On Hold";
                case ReleaseStatusType.Embargoed:
                    return "Embargoed";
                case ReleaseStatusType.Cancelled:
                case ReleaseStatusType.Abandoned:
                    return "Cancelled";
                default:
                    return "In Progress";
            }
        }

        public static string DisplayTransactionStatus(this HtmlHelper helper, TransactionStatusType status)
        {
            switch (status)
            {
                case TransactionStatusType.Resulted:
                    return "Resulted";
                case TransactionStatusType.Transmitted:
                    return "In Progress";
                case TransactionStatusType.Cancelled:
                case TransactionStatusType.Failed:
                    return "Failed";
                default:
                    return "Waiting";
            }
        }

        public static Boolean IsInProgress(this HtmlHelper helper, ReleaseStatusType status)
        {
            switch (status)
            {
                case ReleaseStatusType.Converting:
                case ReleaseStatusType.MailMerging:
                case ReleaseStatusType.Processing:
                case ReleaseStatusType.New:
                case ReleaseStatusType.Released:
                case ReleaseStatusType.Transmitted:
                case ReleaseStatusType.OnHold:
                case ReleaseStatusType.Embargoed:
                    return true;
                default:
                    return false;
            }
        }

        public static int? ToNullableInt32(this string s)
        {
            int i;
            if (Int32.TryParse(s, out i)) return i;
            return null;
        }

        public static string Truncate(this string pValue, int length)
        {
            if (pValue.Length > length)
            {
                return pValue.Substring(0, length) + "...";
            }
            else
                return pValue.Trim();
        }

        public static string SafeTrim(this string pValue)
        {
            if (string.IsNullOrEmpty(pValue))
                return string.Empty;
            else
                return pValue.Trim();
        }

        public static List<int> ParseIntCSV(this string pValue)
        {
            int iVal;

            return string.IsNullOrWhiteSpace(pValue)
                ? new List<int>()
                : pValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                               .ToList().Where(pv => int.TryParse(pv, out iVal))
                               .ToList().ConvertAll<int>(pv => int.Parse(pv)).ToList();
        }

        public static string ToOrdinal(this int number)
        {
            string suffix = String.Empty;

            int ones = number % 10;
            int tens = (int)Math.Floor(number / 10M) % 10;

            if (tens == 1)
            {
                suffix = "th";
            }
            else
            {
                switch (ones)
                {
                    case 1:
                        suffix = "st";
                        break;

                    case 2:
                        suffix = "nd";
                        break;

                    case 3:
                        suffix = "rd";
                        break;

                    default:
                        suffix = "th";
                        break;
                }
            }
            return String.Format("{0}{1}", number, suffix);
        }


        public static List<int> ParsePrefixedIntCSV(this IEnumerable<string> values, string pPrefix)
        {
            List<int> result = new List<int>();

            if (values != null)
            {
                int id;

                foreach (string s in values)
                {
                    if (s.StartsWith(pPrefix))
                    {
                        if (int.TryParse(s.Substring(pPrefix.Length), out id))
                        {
                            result.Add(id);
                        }
                    }
                }
            }

            return result;
        }

        public static List<string> ParseStringCSV(this string pValue)
        {
            return string.IsNullOrWhiteSpace(pValue)
                ? new List<string>()
                : pValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static string JoinStrings<T>(this IEnumerable<T> values, string separator)
        {
            var stringValues = values.Select(item => (item == null ? string.Empty : item.ToString()));
            return string.Join(separator, stringValues.ToArray());
        }

        public static IMappingExpression<TSource, TDestination>
                    IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);
            var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType) && x.DestinationType.Equals(destinationType));

            foreach (var property in existingMaps.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }

    }
}
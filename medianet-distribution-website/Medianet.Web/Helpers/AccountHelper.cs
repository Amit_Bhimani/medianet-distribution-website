﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Web.Helpers
{
    public static class AccountHelper
    {
        public static List<SelectListItem> GetIndustryList()
        {
            List<SelectListItem> industries = new List<SelectListItem>();
            var hubHelper = new HubSpotHelper();
            var options = hubHelper.GetCompanyIndustryList();
            foreach (var item in options)
            {
                if (!string.IsNullOrEmpty(item.Value))
                {
                    industries.Add(new SelectListItem() { Text = item.Label.Trim(), Value = item.Value.ToString() });
                }
            }
            return industries;
        }
    }
}
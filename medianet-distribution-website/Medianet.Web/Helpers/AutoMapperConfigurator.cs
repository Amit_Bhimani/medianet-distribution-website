﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Medianet.Web.Helpers
{
    public class AutoMapperConfigurator
    {
        public static bool IsInitialized = false;

        public static void Configure()
        {
            if (!IsInitialized)
            {
                Mapper.Initialize(x => x.AddProfile<AutomapperProfile>());
                Mapper.AssertConfigurationIsValid();
                IsInitialized = true;
            }
        }

    }
}
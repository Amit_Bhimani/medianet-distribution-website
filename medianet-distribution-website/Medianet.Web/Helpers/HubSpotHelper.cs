﻿using Medianet.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Configuration;

namespace Medianet.Web.Helpers
{
    public class HubSpotHelper
    {
        private const string BaseUri = "https://forms.hsforms.com";
        private const string QueryUri = "/submissions/v3/integration/submit/8140723";

        private readonly string ApiKey = ConfigurationManager.AppSettings["HubspotApiKey"].ToString();
        private readonly string ApiBaseUrl = ConfigurationManager.AppSettings["HubspotApiBaseUrl"];
        public void PostRegistration(RegisterDetailModel submitModel, string pageUri)
        {
            var fields = new List<object>();

            if (!string.IsNullOrEmpty(submitModel.Email))
                fields.Add(new { name = "email", value = submitModel.Email });

            if (!string.IsNullOrEmpty(submitModel.FirstName))
                fields.Add(new { name = "firstname", value = submitModel.FirstName });

            if (!string.IsNullOrEmpty(submitModel.Lastname))
                fields.Add(new { name = "lastname", value = submitModel.Lastname });

            if (!string.IsNullOrEmpty(submitModel.Company))
                fields.Add(new { name = "company", value = submitModel.Company });

            //if (!string.IsNullOrEmpty(submitModel.NoOfEmployees))
            //    fields.Add(new { name = "numemployees", value = submitModel.NoOfEmployees });

            if (!string.IsNullOrEmpty(submitModel.Industry))
                fields.Add(new { name = "industry", value = submitModel.Industry });

            if (!string.IsNullOrEmpty(submitModel.Position))
                fields.Add(new { name = "jobtitle", value = submitModel.Position });

            if (!string.IsNullOrEmpty(submitModel.Phone))
                fields.Add(new { name = "phone", value = submitModel.Phone });

            if (!string.IsNullOrEmpty(submitModel.Address) || !string.IsNullOrEmpty(submitModel.Address2))
                fields.Add(new { name = "address", value = ((submitModel.Address ?? "") + " " + (submitModel.Address2 ?? "")).Trim() });

            if (!string.IsNullOrEmpty(submitModel.Suburb))
                fields.Add(new { name = "city", value = submitModel.Suburb });

            if (!string.IsNullOrEmpty(submitModel.PostCode))
                fields.Add(new { name = "zip", value = submitModel.PostCode });

            if (!string.IsNullOrEmpty(submitModel.State))
                fields.Add(new { name = "state", value = submitModel.State });

            if (!string.IsNullOrEmpty(submitModel.Country))
                fields.Add(new { name = "country", value = submitModel.Country });

            var jsonData = new
            {
                fields = fields.ToArray(),
                context = new
                {
                    pageUri = pageUri,
                }
            };

            PostData(jsonData, "b12742d2-f9f6-4eb9-8120-3b3a5d609a37");
        }

        private void PostData(object jsonData, string formGuid)
        {
            using (var client = new HttpClient())
            {
                using (var content = new StringContent(JsonConvert.SerializeObject(jsonData), System.Text.Encoding.UTF8, "application/json"))
                {
                    client.BaseAddress = new Uri(BaseUri);

                    var result = client.PostAsync($"{QueryUri}/{formGuid}", content).Result;
                    string resultContent = string.Empty;

                    if (result.StatusCode == HttpStatusCode.OK || result.StatusCode == HttpStatusCode.Created ||
                        result.StatusCode == HttpStatusCode.Accepted || result.StatusCode == HttpStatusCode.NoContent)
                    {
                        //resultContent = result.Content.ReadAsStringAsync().Result;
                        //Console.WriteLine(resultContent);
                    }
                    else
                    {
                        try
                        {
                            resultContent = result.Content.ReadAsStringAsync().Result;
                        }
                        catch (Exception)
                        {
                            //Ignore. Maybe no content was returned on error
                        }
                        throw new Exception($"HubSpot returned error code {result.StatusCode} ({result.ReasonPhrase}) submitting to form {formGuid}. {resultContent}");
                    }
                }
            }
        }
        public List<Options> GetContactNoOfEmployeeValues()
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/properties/v1/contacts/properties/named/numemployees?hapikey={1}", ApiBaseUrl, ApiKey));
            request.Timeout = 30000;
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "GET";

            var options = new List<Options>();

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                var res = Deserialize<DropdownProperty>(response);
                options = res.Options;
            }

            return options;
        }
        public List<Options> GetCompanyIndustryList()
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/properties/v1/companies/properties/named/industry?hapikey={1}", ApiBaseUrl, ApiKey));
            request.Timeout = 30000;
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "GET";

            var options = new List<Options>();

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                var res = Deserialize<DropdownProperty>(response);
                options = res.Options;
            }

            return options;
        }
        public T Deserialize<T>(HttpWebResponse response)
        {
            string result;

            using (StreamReader Reader = new StreamReader(response.GetResponseStream()))
            {
                result = Reader.ReadToEnd();

                string jsonString = result;
                return JsonConvert.DeserializeObject<T>(jsonString);
            }
        }
        public class DropdownProperty
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("label")]
            public string Label { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("options")]
            public List<Options> Options { get; set; }
        }

        public class Options
        {
            [JsonProperty("value")]
            public string Value { get; set; }
            [JsonProperty("label")]
            public string Label { get; set; }
            [JsonProperty("displayOrder")]
            public int DisplayOrder { get; set; }
            [JsonProperty("hidden")]
            public bool Hidden { get; set; }
        }
    }
}
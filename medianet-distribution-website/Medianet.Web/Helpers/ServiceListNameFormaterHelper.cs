﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Web.Helpers
{
    public class ServiceListNameFormaterHelper<T>  where T : new()
    {
        public List<T> ArrengeDistType(List<T> pair)
        {
            if (pair.Count == 1)
            {
                return pair;
            }
            else
            {
                T item = pair[0];
                string DistributionType = item.GetType().GetProperty("DistributionType").GetValue(item, null).ToString();

                /// Swap
                if (DistributionType == "E") 
                {
                    pair[0] = pair[1];
                    pair[1] = item;
                }
                
                return pair;
            }
        }

        /// <summary>
        /// v.2 : Group services lists in a couple given the same name and different dist.type.
        /// </summary>
        /// <param name="list">A list of services order by Display Order</param>
        /// <param name="key">Field to group by</param>
        /// <returns>A list of inner lists up to 2 services</returns>
        public List<List<T>> GroupServiceLists(List<T> list,string key)
        {
            List<List<T>> tranformedList = new List<List<T>>();
            List<T> innerList = new List<T>();            

            while ( list.Count > 0 )
            {
                T item = list[0];

                string toSearch = item.GetType().GetProperty(key).GetValue(item, null).ToString();

                innerList.Add(item);

                list.Remove(item);

                IEnumerable<T> itemFound = list.Where(a => a.GetType().GetProperty(key).GetValue(a, null).ToString() == toSearch);

                if ( itemFound.Count() > 0 )
                {
                    T secondItem = itemFound.First();

                    innerList.Add(secondItem);
                    list.Remove(secondItem);
                }

                tranformedList.Add(this.ArrengeDistType(innerList));
                innerList = new List<T>();
            }

            return tranformedList;

        }

        /// <summary>
        /// v 1.0 : Group services lists in a couple given the same name and different dist.type.
        /// </summary>
        /// <param name="list">A list of services order by Name,Display Order</param>
        /// <param name="key">Field to group by</param>
        /// <returns>A list of inner lists up to 2 services</returns>
        public List<List<T>> TransformPairList(List<T> list, string key)
        {
            List<List<T>> tranformedList = new List<List<T>>();
            List<T> innerList= new List<T>();

            string lastValue = "";

            foreach(T item in list)
            {
                string value = item.GetType().GetProperty(key).GetValue(item, null).ToString();

                if ( lastValue != "" )
                {
                    if (lastValue == value)
                    {
                        innerList.Add(item);
                    }
                    else
                    {
                        tranformedList.Add(innerList);
                        innerList = new List<T>();
                        innerList.Add(item);
                    }                        
                }
                else
                    innerList.Add(item);

                lastValue = value;
            }
            // remaining ?
            if ( innerList.Count > 0 )
                tranformedList.Add(innerList);

            return tranformedList;
        }
    }
}

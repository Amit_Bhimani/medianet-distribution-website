﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace Medianet.Web.Helpers
{
    public class ImageHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context) {
            var routeValues = context.Request.RequestContext.RouteData.Values;
            string filepath = ConfigurationManager.AppSettings["ImagePath"].ToString();

            filepath = context.Server.MapPath(filepath + (filepath.EndsWith("/") ? "" : "/") + routeValues["filepath"].ToString());

            context.Response.AddHeader("Content-Disposition", "filename=\"" + Path.GetFileName(filepath) + "\"");
            context.Response.ContentType = MimeExtensionHelper.GetMimeType(filepath);
            context.Response.WriteFile(filepath);
        }

        public bool IsReusable {
            get { return false; }
        }
    }
}
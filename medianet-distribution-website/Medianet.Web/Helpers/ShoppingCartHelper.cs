﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using Medianet.Constants;
using Medianet.Web.MedianetReleaseService;
using Medianet.Web.Models;
using System.Web.Mvc;
using Medianet.Web.NopCommerceServices;

namespace Medianet.Web.Helpers
{
    public static class ShoppingCartHelper
    {
        public static string DisableWith(this HtmlHelper html, string parentServices)
        {
            if (!string.IsNullOrWhiteSpace(parentServices))
                return string.Format(" data-disable-with={0}", parentServices);

            return string.Empty;
        }

        public static bool ItemRequiredInfoPopup(this HtmlHelper html, Product item)
        {
            return (item.DistributionType != NopCommerce.CartDistType_Twitter &&
                    item.DistributionType != NopCommerce.CartDistType_Internet);
        }

        public static MvcHtmlString AddToCartText(this HtmlHelper html, bool isPackage, bool isCustomList, bool isRecipient,string distributionType, int productId = 0)
        {
            return MvcHtmlString.Create(String.Format("{0}", ProductType(isPackage, isCustomList, isRecipient, distributionType, productId)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="productPair"></param>
        /// <returns></returns>
        public static MvcHtmlString AddGetProductInfo(this HtmlHelper html, List<Product> productPair)
        {
            if (productPair.Count == 1)
            {
                Product product = productPair.First();

                string productInfo = String.Format("getProductInfo({0},event);return false;", product.Id);

                return MvcHtmlString.Create(productInfo);                
            }
            else
            {
                string productInfo = String.Format("getProductsInfo({0},{1},event);return false;", productPair[0].Id, productPair[1].Id);

                return MvcHtmlString.Create(productInfo);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="productPair"></param>
        /// <returns></returns>
        public static MvcHtmlString AddGetProductInfo(this HtmlHelper html, List<ProductDTO> productPair)
        {
            if (productPair.Count == 1)
            {
                ProductDTO product = productPair.First();

                string productInfo = String.Format("getProductInfo({0},event);return false;", product.Id);

                return MvcHtmlString.Create(productInfo);
            }
            else
            {
                string productInfo = String.Format("getProductsInfo({0},{1},event);return false;", productPair[0].Id, productPair[1].Id);

                return MvcHtmlString.Create(productInfo);
            }
        }

        private static string ProductType(bool isPackage, bool isCustomList, bool isRecipient, string distributionType, int productId)
        {
            if (isPackage)
            {
                return "Package";
            }
            else if (isCustomList)
            {
                return GetCustomerListProductType(distributionType, productId);
            }
            else if (isRecipient)
            {
                return "Recipient";
            }
            else
            {
                return GetCustomerListProductType(distributionType, productId);
            }                       
        }

        private static string GetCustomerListProductType(string distributionType, int productId)
        {
            switch (distributionType)
            {
                case NopCommerce.CartDistType_Email:
                    return "Email";
                case NopCommerce.CartDistType_Wire:
                    return "Newswire";
                case NopCommerce.CartDistType_Sms:
                    return "SMS";
                case NopCommerce.CartDistType_Internet:
                    return "Web";
                case NopCommerce.CartDistType_Twitter:
                    return "Twitter";
                case NopCommerce.CartDistType_AnrFee:
                    return "Audio News Release Charge";
                case NopCommerce.CartDistType_AttachmentFee:
                    return "Attachment Charge";
                case NopCommerce.CartDistType_PriorityFee:
                    return "High Priority Fee";
            }

            return ""; 
        }
    }
}
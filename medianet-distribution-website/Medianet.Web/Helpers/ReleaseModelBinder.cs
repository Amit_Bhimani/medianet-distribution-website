﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace Medianet.Web.Helpers
{
    public class ReleaseModelBinder : DefaultModelBinder
    {
        private CultureInfo _cultureInfo = new CultureInfo("en-AU");

        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            switch (propertyDescriptor.Name)
            {
                case "ScheduleDate":
                    CheckScheduleDate(bindingContext);
                    break;
                case "EmbargoDate":
                    CheckEmbargoDate(bindingContext);
                    break;
            }

            base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }

        private void CheckScheduleDate(ModelBindingContext bindingContext)
        {
            var distributeAsFutureRelease = bindingContext.ValueProvider.GetValue("DistributeAsFutureRelease");
            if (distributeAsFutureRelease == null)
                return;
            
            var scheduleDate = bindingContext.ValueProvider.GetValue("ScheduleDate");
            DateTime temp;

            if (bool.Parse(distributeAsFutureRelease.AttemptedValue))
            {
                if (!DateTime.TryParseExact(scheduleDate == null ? "" : scheduleDate.AttemptedValue, "dd/MM/yyyy", _cultureInfo, DateTimeStyles.None, out temp))
                    bindingContext.ModelState.AddModelError("ScheduleDate",
                        string.Format("The value '{0}' is not valid for Schedule date. Please enter a date within 12 months.", HttpContext.Current.Request["ScheduleDate"]));
            }

        }

        private void CheckEmbargoDate(ModelBindingContext bindingContext)
        {
            var distributeAsEmbargoRelease = bindingContext.ValueProvider.GetValue("DistributeAsEmbargoRelease");
            if (distributeAsEmbargoRelease == null)
                return;

            var embargoDate = bindingContext.ValueProvider.GetValue("EmbargoDate");
            DateTime temp;

            if (bool.Parse(distributeAsEmbargoRelease.AttemptedValue))
            {
                if (!DateTime.TryParseExact(embargoDate == null ? "" : embargoDate.AttemptedValue, "dd/MM/yyyy", _cultureInfo, DateTimeStyles.None, out temp))
                    bindingContext.ModelState.AddModelError("EmbargoDate",
                        string.Format("The value '{0}' is not valid for Embargo date. Please enter a date within 12 months.", HttpContext.Current.Request["EmbargoDate"]));
            }
        }
    }
}
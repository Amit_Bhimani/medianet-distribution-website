﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Helpers
{
    public static class Constants
    {
        public const string session_key_enhanced_search_selected_articles = "enhanced-search-selected-articles";
        public const string session_key_enhanced_search_options = "enhanced-search-options";
        public const string session_key_enhanced_search_table_params = "enhanced-search-table-params";
        public const string session_key_enhanced_search_page_size = "enhanced-search-page-size";

        public const int default_enhanced_search_day_range = -14;

        public const string newscentre_product_internet = "INTERNET";
        public const string newscentre_product_wire = "BREAKINGNEWS";
        public const string newscentre_product_text = "TEXT";

        //sort 
        public const string session_key_enhanced_search_sort_field = "enhanced-search-sort-field";
        public const string session_key_enhanced_search_sort_ascending = "enhanced-search-sort-ascending";

        //download
        public const string session_key_enhanced_download_articles = "enhanced-download-articles";
        public const string session_key_enhanced_download_articles_count = "enhanced-download-articles-count";
        public const string session_key_enhanced_download_articles_page_size = "enhanced-download-articles-page-size";
        public const string session_key_enhanced_download_articles_last_page_number = "enhanced-download-articles-last-page-number";

        public const string environment_local = "Local";
        public const string environment_live = "Live";
        public const string payment_done_closed_message = "Payment Succeed";
    }
}
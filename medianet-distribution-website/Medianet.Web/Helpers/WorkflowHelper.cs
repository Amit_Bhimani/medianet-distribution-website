﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using Medianet.Web.Models;
using System.Web.Mvc;
using Medianet.Web.NopCommerceServices;

namespace Medianet.Web.Helpers
{
    public static class WorkflowHelper
    {
        /// <summary>
        /// Sets the next action
        /// </summary>
        /// <param name="html"></param>
        /// <param name="model">Release in progress</param>
        /// <returns></returns>
        public static MvcHtmlString Next(this HtmlHelper html, ReleaseModel model)
        {
            if ( model.workflowStep.Next != null )
            {
                return MvcHtmlString.Create(
                    String.Format("next_action='{0}';", model.workflowStep.Next.Value.Route)
                    );
            }
            else
                return MvcHtmlString.Create("");
        }

        /// <summary>
        /// Sets the previous action
        /// </summary>
        /// <param name="html"></param>
        /// <param name="model">Release in progress</param>
        /// <returns></returns>
        public static MvcHtmlString Previous(this HtmlHelper html, ReleaseModel model)
        {
            if (model.workflowStep.Previous != null)
            {
                return MvcHtmlString.Create(
                    String.Format("previous_action='{0}';", model.workflowStep.Previous.Value.Route)
                    );
            }
            else
                return MvcHtmlString.Create("");
        }

        /// <summary>
        /// Creates an input hidden used to post the next or previous action.
        /// </summary>
        /// <param name="html"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static MvcHtmlString Action(this HtmlHelper html, short order)
        {
            return MvcHtmlString.Create($"<input type=\"hidden\" id=\"action\" name=\"action\"><input type=\"hidden\" id=\"CurrentStepOrder\" name=\"CurrentStepOrder\" value=\"{order}\">");
        }
    }
}
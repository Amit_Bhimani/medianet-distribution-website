﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Text;
using Medianet.Constants;
using Medianet.Web.Helpers;
using Medianet.Web.Models;
using Profile = AutoMapper.Profile;

namespace Medianet.Web.Helpers
{
    public class AutomapperProfile : Profile
    {
        public const string VIEW_MODEL = "AutomapperProfile";

        public override string ProfileName
        {
            get { return VIEW_MODEL; }
        }

        protected override void Configure()
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medianet.Web.Controllers;
using Medianet.Web.Models;
using Medianet.Constants;
using Medianet.Web.MedianetCustomerService;

namespace Medianet.Web.Helpers
{
    /// <summary>
    /// To handle session verification
    /// </summary>
    public class HandleAuthentication : ActionFilterAttribute
    {       
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            List<string> SkipActions = (List<string>) filterContext.HttpContext.Application["SkipAuthentication"];
            string Action = filterContext.ActionDescriptor.ActionName.ToLower();

            if (SkipActions == null || SkipActions.IndexOf(Action) != -1)
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            //Try auto login
            if (AutoLogin(filterContext))
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            if ( filterContext.HttpContext.Request.IsAjaxRequest() )
            {
                this.HandleAjaxRequest(filterContext);
            }
            else
            {
                this.HandleRequest(filterContext);
            }
        }

        public bool AutoLogin(ActionExecutingContext filterContext)
        {
            SessionModel session = (SessionModel)filterContext.HttpContext.Session[NopCommerce.Session_Key];

            if (session == null)
            {
                //User not login, let's check auto login cookie
                var cookie = filterContext.HttpContext.Request.Cookies[NopCommerce.AutoLogin_Key];
                if (cookie != null)
                {
                    var value = filterContext.HttpContext.Server.UrlDecode(cookie.Value);
                    var seg = value.Split('|');
                    if (seg.Length == 2)
                    {
                        var email = seg[0];
                        var token = seg[1];

                        filterContext.Result =
                            new RedirectResult("/Account/AutoLogOn?email=" + filterContext.HttpContext.Server.UrlEncode(email) + "&token=" + token);

                        return true;
                    }
                }
            }

            return false;
        }

        public void HandleRequest(ActionExecutingContext filterContext)
        {
            SessionModel session = (SessionModel)filterContext.HttpContext.Session[NopCommerce.Session_Key];

            if (session == null)
            {
                filterContext.Result = RedirectToLogon(filterContext);
            }
            else
            {
                if (session.LastRequest.HasValue)
                {
                    if (((TimeSpan)(DateTime.Now - session.LastRequest.Value)).Minutes > NopCommerce.Check_Session_TimeSlice)
                    {
                        using (MedianetChargeBeeService.ChargeBeeServiceClient csc = new MedianetChargeBeeService.ChargeBeeServiceClient())
                        {
                            MedianetChargeBeeService.DBSession dbSession = csc.ValidateSession(session.sessionKey);

                            if (dbSession == null)
                            {
                                filterContext.Result = RedirectToLogon(filterContext);
                            }
                            else
                                session.PopulateFromDatabaseChargeBee(dbSession);
                        }
                    }
                }
                else
                    session.LastRequest = DateTime.Now;

                filterContext.HttpContext.Session[NopCommerce.Session_Key] = session;
            }

            base.OnActionExecuting(filterContext);
        }

        public void HandleAjaxRequest(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller;

            SessionModel session = (SessionModel)filterContext.HttpContext.Session[NopCommerce.Session_Key];

            if (session == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
                controller.TempData["errorMessage"] = "Session expired";
            }

            base.OnActionExecuting(filterContext);
        }

        private RedirectResult RedirectToLogon(ActionExecutingContext filterContext)
        {
            var x = filterContext.HttpContext.Request.QueryString;
            var loginIfRequired = x["loginIfRequired"];

            if (loginIfRequired != null && loginIfRequired.Equals("true", StringComparison.CurrentCultureIgnoreCase))
            {
                // The SavageBull logon page can't handle the returnUrl so in this case go to our login page
                UrlHelper u = new UrlHelper(filterContext.RequestContext);
                return new RedirectResult(u.Action("Logon", "Account") + "?returnUrl=" +
                                          filterContext.HttpContext.Request.FilePath);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(filterContext.HttpContext.Request.FilePath)) 
                    return new RedirectResult(AccountController.InvalidSessionURL);
                else
                    return new RedirectResult(AccountController.InvalidSessionURL + "&returnUrl=" +
                                              filterContext.HttpContext.Request.FilePath);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medianet.Web.Helpers
{
    public class TimezoneHelper
    {
        public static DateTime AdjustDateTime(DateTime date, string destCode)
        {
            var sourceZone = TimeZoneInfo.Local;

            if (sourceZone.Id == destCode)
                return date;

            var destZone = TimeZoneInfo.FindSystemTimeZoneById(destCode);

            // We need to create a new date with the Kind specified as Unspecified. Otherwise the ConvertTime method complains.
            var sourceDate = new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Millisecond, DateTimeKind.Unspecified);

            return TimeZoneInfo.ConvertTime(sourceDate, sourceZone, destZone);
        }
    }
}
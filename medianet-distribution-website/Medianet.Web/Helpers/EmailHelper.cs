﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using log4net;

namespace Medianet.Web.Helpers
{
    public class EmailHelper
    {
        public static void SendEmail(string subject, string body, string toAddresses)
        {
            try
            {
                string smtpHost = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpHost"].ToString();
                int smtPort =
                    int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpPort"].ToString());
                string fromEmailAddress =
                    System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpMailFromAddress"];

                using (var client = new SmtpClient(smtpHost, smtPort))
                {
                    client.Send(fromEmailAddress, toAddresses, subject, body);
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error sending email. " + ex.ToString());
            }
        }

        public static void SendErrorEmail(string body)
        {
            string toAddresses = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorSmtpMailToAddress"];

            SendEmail("Medianet Distribution Error", body, toAddresses);
        }
    }
}
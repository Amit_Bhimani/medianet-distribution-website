﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=152368
  -->
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
    <section name="SessionErrorCodesToQueryString" type="System.Configuration.DictionarySectionHandler" />
    <section name="MonitoringSearch" type="System.Configuration.DictionarySectionHandler" />
  </configSections>
  <log4net debug="true">
    <appender name="RollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="C:\\LogFiles\\Medianet.Distribution\\Errors.txt" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="10MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <appender name="MessageMediaAPILogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="C:\\LogFiles\\Medianet.Distribution\\MessageMediaAPILog.txt" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="10MB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <root>
      <level value="DEBUG" />
      <appender-ref ref="RollingLogFileAppender" />
    </root>
    <logger name="SMSWebhookLogger" additivity="false">
      <level value="ALL" />
      <appender-ref ref="MessageMediaAPILogFileAppender" />
    </logger>
  </log4net>
  <SessionErrorCodesToQueryString>
    <add key="InvalidSession" value="?ec=1&amp;p=distribute" />
    <add key="LoginFailed" value="?ec=2&amp;p=distribute" />
    <add key="AccessDenied" value="?ec=3&amp;p=distribute" />
    <add key="InsuffecientDetails" value="?ec=4&amp;p=distribute" />
    <add key="LoginFailedToContacts" value="?ec=2&amp;p=contacts" />
  </SessionErrorCodesToQueryString>
  <appSettings>
    <add key="webpages:Version" value="2.0.0.0" />
    <add key="PreserveLoginUrl" value="true" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="PublicApiAddress" value="http://umbraco.medianet.com.au" />
    <add key="WidgetMediaAddress" value="//medianet.uat.aap/" />
    <add key="MarketingPage" value="https://www.medianet.com.au/products/media-contacts-database/" />
    <add key="ContactsSite" value="http://contacts-uat.medianet.com.au/Account/LogOnUsingOtherSession" />
    <add key="LoginPageUrl" value="http://localhost:3000/Account/Logon" />
    <add key="SmtpHost" value="mail-relay.aap.com.au" />
    <add key="SmtpPort" value="25" />
    <add key="EnvironmentType" value="Local" />
    <add key="SmtpMailFromAddress" value="team@medianet.com.au" />
    <add key="ErrorSmtpMailToAddress" value="durquhart@mediality.com.au" />
    <add key="AccountManagerInclude" value="durquhart@mediality.com.au" />
    <add key="SkipAuthentication" value="logonusingsession,logon,logonusingothersession,logoff,authenticate,authenticateviaemail,register,submitregister,registerdetails,submitdetails,about,terms,pagenotfound,error,genericdownload,emailopened,document,websitelogo,sendforgotpasswordemail,changepassword,autologon,contact,editorial,calendar,privacy,database,training,thanks,payment,addattendee,applypromocode,registerinterest,smswebhook" />
    <add key="EmailOpenedImage" value="~/Content/img/blank.gif" />
    <add key="ImagePath" value="~/Content/img/" />
    <add key="NotAllowedEmailProviders" value="@isentia,@sentiamedia,@mediamonitors,@brandtology.,@buzznumbers.,gmail.,yahoo.,hotmail.,@aol.,@outlook.,@mail.,@fastmail.,lycos.,@aim.,@gmx.,@icloud.,@live.,@zoho.,@hush.,@hushmail.,@inbox.,@rocketmail.,@ymail.,@windowslive.,@shortmail.,@bigstring.,@mailinator.,@meltwater.,@me.,@mac." />
    <add key="MedianetPublicSiteURL" value="http://medianet.uat.aap/" />
    <add key="MrgPublicSiteUrl" value="http://mediaverse.com.au/" />
    <add key="TrendWatcherUrl" value="//tw.medianet.com.au" />
    <add key="CuratorApiAddress" value="http://tw.medianet.com.au/api-curator/" />
    <add key="ReCaptchaUrl" value="https://www.google.com/recaptcha/api/siteverify" />
    <add key="ReCaptchaSitekey" value="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI" />
    <add key="ReCaptchaSecretkey" value="6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe" />
    <add key="NewRelic.AppName" value="Medianet.Distribution" />
    <add key="HubspotApiBaseUrl" value="https://api.hubapi.com/"/>
    <add key="HubspotApiKey" value="40e52e47-da6d-4185-8256-18ee303e4ba8" />
  </appSettings>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5" />
      </system.Web>
  -->
  <system.web>
    <sessionState mode="InProc" stateConnectionString="tcpip=develweb:42424" cookieless="false" timeout="240" />
    <compilation debug="true" targetFramework="4.6.1">
      <assemblies>
        <add assembly="System.Web.Abstractions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Helpers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Mvc, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.WebPages, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      </assemblies>
    </compilation>
    <authentication mode="Forms">
      <forms loginUrl="http://localhost:3000/Account/Logon" timeout="2880" />
    </authentication>
    <membership>
      <providers>
        <clear />
        <add name="AspNetSqlMembershipProvider" type="System.Web.Security.SqlMembershipProvider" connectionStringName="ApplicationServices" enablePasswordRetrieval="false" enablePasswordReset="true" requiresQuestionAndAnswer="false" requiresUniqueEmail="false" maxInvalidPasswordAttempts="5" minRequiredPasswordLength="6" minRequiredNonalphanumericCharacters="0" passwordAttemptWindow="10" applicationName="/" />
      </providers>
    </membership>
    <profile>
      <providers>
        <clear />
        <add name="AspNetSqlProfileProvider" type="System.Web.Profile.SqlProfileProvider" connectionStringName="ApplicationServices" applicationName="/" />
      </providers>
    </profile>
    <roleManager enabled="false">
      <providers>
        <clear />
        <add name="AspNetSqlRoleProvider" type="System.Web.Security.SqlRoleProvider" connectionStringName="ApplicationServices" applicationName="/" />
        <add name="AspNetWindowsTokenRoleProvider" type="System.Web.Security.WindowsTokenRoleProvider" applicationName="/" />
      </providers>
    </roleManager>
    <pages controlRenderingCompatibilityVersion="4.0">
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>
    <customErrors mode="On">
      <error statusCode="404" redirect="/Home/PageNotFound" />
      <error statusCode="500" redirect="/Home/Error" />
    </customErrors>
  </system.web>
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <modules runAllManagedModulesForAllRequests="true" />
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.serviceModel>
    <behaviors>
      <endpointBehaviors>
        <behavior name="SomeServiceClientEndpointBehavior">
          <dataContractSerializer maxItemsInObjectGraph="2147483647" />
        </behavior>
      </endpointBehaviors>
    </behaviors>
    <bindings>
      <netTcpBinding>
        <binding name="StreamBinding_IFileService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" transactionFlow="false" transferMode="Streamed" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <readerQuotas maxDepth="32" maxStringContentLength="10000000" maxArrayLength="10000000" maxBytesPerRead="10000000" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="None">
            <transport clientCredentialType="Windows" protectionLevel="EncryptAndSign" />
            <message clientCredentialType="Windows" />
          </security>
        </binding>
        <binding name="BufferedBinding_IListService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" transactionFlow="false" transferMode="Buffered" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="10000000" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="None">
            <transport clientCredentialType="Windows" protectionLevel="EncryptAndSign" />
            <message clientCredentialType="Windows" />
          </security>
        </binding>
        <binding name="BufferedBinding_IReleaseService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" transactionFlow="false" transferMode="Buffered" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="None">
            <transport clientCredentialType="Windows" protectionLevel="EncryptAndSign" />
            <message clientCredentialType="Windows" />
          </security>
        </binding>
        <binding name="BufferedBinding_IPaymentService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" transactionFlow="false" transferMode="Buffered" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="None">
            <transport clientCredentialType="Windows" protectionLevel="EncryptAndSign" />
            <message clientCredentialType="Windows" />
          </security>
        </binding>
        <binding name="BufferedBinding_IGeneralService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" transactionFlow="false" transferMode="Buffered" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="None">
            <transport clientCredentialType="Windows" protectionLevel="EncryptAndSign" />
            <message clientCredentialType="Windows" />
          </security>
        </binding>
        <binding name="BufferedBinding_IListEditService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:03:00" transactionFlow="false" transferMode="Buffered" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="None">
            <transport clientCredentialType="Windows" protectionLevel="EncryptAndSign" />
            <message clientCredentialType="Windows" />
          </security>
        </binding>
        <binding name="BufferedBinding_ICustomerService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" maxBufferPoolSize="10000000" maxReceivedMessageSize="10000000">
          <security mode="None" />
        </binding>
        <binding name="BufferedBinding_IGeographicService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" transactionFlow="false" transferMode="Buffered" transactionProtocol="OleTransactions" hostNameComparisonMode="StrongWildcard" listenBacklog="10" maxBufferPoolSize="10000000" maxBufferSize="10000000" maxConnections="10" maxReceivedMessageSize="10000000">
          <security mode="None" />
        </binding>
        <binding name="BufferedBinding_INopCommerce" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" maxBufferPoolSize="10000000" maxReceivedMessageSize="10000000">
          <security mode="None" />
        </binding>
        <binding name="BufferedBinding_ISearchService">
          <security mode="None" />
        </binding>
        <binding name="BufferedBinding_IChargeBeeService" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00"  maxBufferPoolSize="10000000" maxReceivedMessageSize="10000000">
          <security mode="None"/>
        </binding>
      </netTcpBinding>
      <wsHttpBinding>
        <binding name="WSHttpBinding_IAuthentication" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" bypassProxyOnLocal="false" transactionFlow="false" hostNameComparisonMode="StrongWildcard" maxBufferPoolSize="524288" maxReceivedMessageSize="65536" messageEncoding="Text" textEncoding="utf-8" useDefaultWebProxy="true" allowCookies="false">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
          <reliableSession ordered="true" inactivityTimeout="00:10:00" enabled="false" />
          <security mode="Message">
            <transport clientCredentialType="Windows" proxyCredentialType="None" realm="" />
            <message clientCredentialType="Windows" negotiateServiceCredential="true" algorithmSuite="Default" />
          </security>
        </binding>
      </wsHttpBinding>
    </bindings>
    <client>
      <endpoint address="net.tcp://mnuat-sy4:53262/GeographicService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_IGeographicService" contract="MedianetGeneralService.IGeographicService" name="BufferedBinding_IGeographicService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/ListEditService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_IListEditService" contract="MedianetListEditService.IListEditService" name="BufferedBinding_IListEditService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/GeneralService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_IGeneralService" contract="MedianetGeneralService.IGeneralService" name="BufferedBinding_IGeneralService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/FileService" binding="netTcpBinding" bindingConfiguration="StreamBinding_IFileService" contract="MedianetFileService.IFileService" name="StreamBinding_IFileService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/CustomerService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_ICustomerService" contract="MedianetCustomerService.ICustomerService" name="BufferedBinding_ICustomerService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/ReleaseService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_IReleaseService" contract="MedianetReleaseService.IReleaseService" name="BufferedBinding_IReleaseService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/ListService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_IListService" contract="MedianetListService.IListService" name="BufferedBinding_IListService" />
      <endpoint address="net.tcp://mnuat-sy4:51326/SearchService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_ISearchService" contract="SearchService.ISearchService" name="BufferedBinding_ISearchService" />
      <endpoint address="net.tcp://mnuat-sy4:53262/NopCommerce" binding="netTcpBinding" bindingConfiguration="BufferedBinding_INopCommerce" contract="NopCommerceServices.INopCommerce" name="BufferedBinding_INopCommerce" />
      <endpoint address="net.tcp://mnuat-sy4:53262/ChargeBeeService" binding="netTcpBinding" bindingConfiguration="BufferedBinding_IChargeBeeService" contract="MedianetChargeBeeService.IChargeBeeService" name="BufferedBinding_IChargeBeeService" />
    </client>
  </system.serviceModel>
  <system.web>
    <httpRuntime maxRequestLength="262144" executionTimeout="103600" />
    <globalization culture="en-AU" />
  </system.web>
</configuration>
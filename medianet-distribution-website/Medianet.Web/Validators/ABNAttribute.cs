﻿using System.ComponentModel.DataAnnotations;
using Medianet.Web.Models;
using Medianet.Web.MedianetCustomerService;
using System.Web;
using Medianet.Constants;

namespace Medianet.Web.Validators
{
    public class ABNAttribute : ValidationAttribute
    {
        private const int MaxLength = 11;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            CustomerBillingType accountType = validationContext.ObjectInstance is EditDetailModel ? 
                            ((EditDetailModel)validationContext.ObjectInstance).AccountType : (validationContext.ObjectInstance is RegisterDetailModel ? ((RegisterDetailModel)validationContext.ObjectInstance).AccountType : ((UpgradeModel)validationContext.ObjectInstance).AccountType);

            string ABN = value == null ? string.Empty : value.ToString().Replace(" ", "");
            string debtorNumber = "";
            if (((SessionModel)HttpContext.Current.Session[NopCommerce.Session_Key]) != null)
            {
                debtorNumber = ((SessionModel)HttpContext.Current.Session[NopCommerce.Session_Key]).debtorNumber;
            }         
            bool isInternalDebtorNumber = NopCommerce.DEBTOR_NUMBERS_INTERNAL.Contains(debtorNumber);

            if (accountType == CustomerBillingType.Invoiced)
            {
                 if (isInternalDebtorNumber || (!string.IsNullOrEmpty(ABN) && ABN.Length <= MaxLength))
                    return ValidationResult.Success;
            }
            else
            {
               if ((string.IsNullOrEmpty(ABN) || ABN.Length <= MaxLength))
                    return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage);
        }
    }
}
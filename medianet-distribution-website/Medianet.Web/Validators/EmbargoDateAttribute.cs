﻿using Medianet.Constants;
using Medianet.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medianet.Web.Validators
{
    public class EmbargoDateAttribute : ValidationAttribute
    {
        public int MaxDaysFromNow { get; set; }

        private DateTime MaxDate
        {
            get
            {
                return DateTime.Now.AddDays(MaxDaysFromNow);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return $"The value '{HttpContext.Current.Request["EmbargoDate"]} {HttpContext.Current.Request["EmbargoTimeHour"]}:{HttpContext.Current.Request["EmbargoTimeMinutes"]}' is not valid for Embargo date. Please enter a date within 12 months.";
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ReleaseModel model = (ReleaseModel)validationContext.ObjectInstance;

            if (value == null)
            {
                if (model.DistributeAsEmbargoRelease)
                    return new ValidationResult(ErrorMessage);
                else
                    return ValidationResult.Success;
            }

            TimeZoneInfo CustomerTimeZone = TimeZoneInfo.FindSystemTimeZoneById(((SessionModel)HttpContext.Current.Session[NopCommerce.Session_Key]).TimeZone.Description);
            DateTime NowInCustomerTimeZone = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), CustomerTimeZone);

            if (model.DistributeAsEmbargoRelease && (model.EmbargoUntilDate <= NowInCustomerTimeZone || model.EmbargoUntilDate > MaxDate))
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}
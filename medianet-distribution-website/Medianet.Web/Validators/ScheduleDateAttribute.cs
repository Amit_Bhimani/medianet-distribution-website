﻿using Medianet.Constants;
using Medianet.Web.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Medianet.Web.Validators
{
    public class ScheduleDateAttribute : ValidationAttribute
    {
        public int MaxDaysFromNow { get; set; }

        private DateTime MaxDate
        {
            get
            {
                return DateTime.Now.AddDays(MaxDaysFromNow);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return $"The value '{HttpContext.Current.Request["ScheduleDate"]} {HttpContext.Current.Request["ScheduleTimeHour"]}:{HttpContext.Current.Request["ScheduleTimeMinutes"]}' is not valid for Schedule date. Please enter a date within 12 months.";
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ReleaseModel model = (ReleaseModel)validationContext.ObjectInstance;
            
            if (value == null)
            {
                if (model.DistributeAsFutureRelease)
                    return new ValidationResult(ErrorMessage);
                else
                    return ValidationResult.Success;
            }

            TimeZoneInfo CustomerTimeZone = TimeZoneInfo.FindSystemTimeZoneById(((SessionModel)HttpContext.Current.Session[NopCommerce.Session_Key]).TimeZone.Description);
            DateTime NowInCustomerTimeZone = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), CustomerTimeZone);

            if (model.DistributeAsFutureRelease && (model.HoldUntilDate <= NowInCustomerTimeZone || model.HoldUntilDate > MaxDate))
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}
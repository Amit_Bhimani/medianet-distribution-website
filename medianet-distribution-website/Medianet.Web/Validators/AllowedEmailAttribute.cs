﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace Medianet.Web.Validators
{
    public class AllowedEmailAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string email;
            string[] notAllowedList = ConfigurationManager.AppSettings["NotAllowedEmailProviders"].Split(',');

            if (value == null)
            {
                return new ValidationResult("The email account must not be empty.");
            }

            email = (string)value;

            foreach (string s in notAllowedList)
            {
                if (email.IndexOf(s, StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    return new ValidationResult("The email account you have entered is not supported. To complete your sign up please enter your business or corporate email address.");
                }
            }

            return null;
        }
    }
}
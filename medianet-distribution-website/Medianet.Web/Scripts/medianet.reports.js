﻿$(document).ready(function () {
    if (typeof google != 'undefined') {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Status');
            data.addColumn('number', 'Count');
            data.addRows(graphData);

            // Set chart options
            var options = {
                chartArea: {
                    left: 10,
                    top: 10,
                    width: 180,
                    height: 180
                },
                colors: ['#5cb85c', '#777777', '#d9534f'],
                legend: {
                    position: 'none'
                }
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('myChart'));
            chart.draw(data, options);
        }
    }

    fetchReview();
});

function loadReportTransaction(ReleaseId, ResultId)
{
    loadEffectOn('body');
    $('#myModal').html('');

    $.ajax(
    {
        url: '/report/details',
        type: 'POST',
        data: { ReleaseId:ReleaseId, TransactionId:ResultId, __RequestVerificationToken: antiForgetyToken() },
        cache: false,   
        dataType: 'html'
    })
    .success(function (result) 
    {        
        $('#myModal').html(result);
        $('#myModal').show();
        loadEffectOff('body');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('body');
    });    
}

function prevReportPage() 
{
    loadEffectOn('#reports');

    $.ajax(
    {
        url: '/report/previouspage',
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,   
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#reports').html(result);
        loadEffectOff('#reports');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#reports');
    });
}

function nextReportPage() 
{
    loadEffectOn('#reports');

    $.ajax(
    {
        url: '/report/nextpage',
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#reports').html(result);
        loadEffectOff('#reports');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#reports');
    });
}

function goToReportPage(index) 
{
    loadEffectOn('#reports');

    $.ajax(
    {
        url: '/report/gotopage',
        type: 'POST',
        data: { Page: index, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#reports').html(result);
        loadEffectOff('#reports');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#reports');
    });
}

function DisplayReview() {
    data = { header: "Review", body: $('#review').html() };

    $('#myModal').html('');
    $.tmpl("warningInfo", data).appendTo($('#myModal'));
    //$('#myModal').find('#myModalLabel').html("Review");
    $('#myModal').show();
}

function fetchReview() {
    var releaseId = $("#ReleaseId");

    if (releaseId.length === 0) return;

    $.ajax(
    {
        url: '/Report/CuratorReport',
        type: 'GET',
        data: { releaseId: releaseId.val() },
        cache: false,
        dataType: "json"
    })
    .success(function (result) {
        var loadingDiv = $("#curatorLoading");
        var summaryDiv = $("#curatorSummary");

        if (result == null || result.rating == 0) {
            loadingDiv.show();
            loadingDiv.html("Not avaliable yet");
            summaryDiv.hide();
        }
        else {
            loadingDiv.hide();
            summaryDiv.show();
            summaryDiv.children("span").removeClass("hide");
            summaryDiv.find(".star-ratings-sprite-rating").width((result.rating * 100) / 5);
            summaryDiv.find(".rate-number").html(result.rating.toFixed(1));

            var popupDiv = $("#review");
            UpdateCuratorRating(popupDiv, "content", result.content);
            UpdateCuratorRating(popupDiv, "angle", result.angle);
            UpdateCuratorRating(popupDiv, "assets", result.assets);
            UpdateCuratorRating(popupDiv, "timing", result.timing);
            UpdateCuratorRating(popupDiv, "heading", result.heading);
        }
    })
    .error(function (xhr, status) {
        $("#curatorLoading").show();
        $("#curatorSummary").html("Not available");
        $("#curatorSummary").hide();
    });
}

function UpdateCuratorRating(popupDiv, dataType, data) {
    if (data === null) return;

    var curatorTd = popupDiv.find("[data-type='" + dataType + "'");
    curatorTd.find("span .star-ratings-sprite-rating").width((data.rating * 100) / 5);
    curatorTd.find(".rate-number").html(data.rating.toFixed(1));
    curatorTd.find(".comment").html(data.comment);
}

function CancelJob() {
    var releaseId = $("#ReleaseId");

    if (releaseId.length === 0) return;

    ShowOkCancel('Cancel Release', '<p>Please note, Cancelling does not automatically refund a credit card payment. Credit Card refunds will take approximately 2-3 weeks to process.</p><p>Are you sure you want to cancel this Release?</p>', ContinueCancelJob);
}

function ContinueCancelJob() {
    var releaseId = $("#ReleaseId");

    if (releaseId.length === 0) return;

    loadEffectOn('#reports');

    $.ajax(
    {
        url: '/Report/CancelRelease',
        type: 'GET',
        data: { releaseId: releaseId.val() },
        cache: false,
        dataType: "json"
    })
    .success(function (result) {
        loadEffectOff('#reports');

        ShowMessageWithCallback('Cancel', 'Release cancelled.', function() {
            location.reload(true);
        });      
    })
    .error(function (xhr, status) {
        loadEffectOff('#reports');
        ShowMessage('Error', 'Error attempting to cancel Release.');
    });   
}

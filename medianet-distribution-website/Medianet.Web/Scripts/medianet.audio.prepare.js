﻿var player_template = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$(${div}).hide();">×</button><h3 id="myModalLabel">Player</h3></div><embed type="audio/mp3" id="wavplayer" src="/audio/play/${id}"></embed><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$(${div}).hide();">Close</button></div>';


function SelectAudioFile(sender,id,name)
{
    var add = $(sender)[0].checked;

    loadEffectOn('#wordcount_section');

    $.ajax(
    {
        url: '/audio/SelectAudioFile',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken(), Id: id, Name:name, Add: add },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#wordcount_section').html(result);
        loadEffectOff('#wordcount_section');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#wordcount_section');
    });
}

function prevAudioFiles()
{
    loadEffectOn('#audiofiles');

    $.ajax(
    {
        url: '/audio/PreviousPage',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#audiofiles').html(result);
        loadEffectOff('#audiofiles');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#audiofiles');
    });
}

function goToAudiofles(index)
{
    loadEffectOn('#audiofiles');

    $.ajax(
    {
        url: '/audio/GoToPage',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken(), index: index },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#audiofiles').html(result);
        loadEffectOff('#audiofiles');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#audiofiles');
    });
}

function nextAudioFiles()
{
    loadEffectOn('#audiofiles');

    $.ajax(
    {
        url: '/audio/NextPage',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#audiofiles').html(result);
        loadEffectOff('#audiofiles');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#audiofiles');
    });
}

function play(id)
{
    var player = $('#productInfo');

    player.empty();

    $.template("player", player_template);

    data = { id: id, div: "'#productInfo'" };

    $.tmpl("player", data).appendTo(player);
    
    player.show();

    return false;
}

function ShowRenameAudioFile(sender)
{
    var renamePopup = $('#audioFileRename');
    var textbox = renamePopup.find("#newAudioFilename");
    var aTag = $(sender);
    var headerName = $.trim(aTag.html());

    if (headerName.length > 30)
        headerName = headerName.substring(0, 30) + "...";

    renamePopup.find('#myModalLabel').html("Rename: " + headerName);
    textbox.val("");

    // Store the Id of the audio file in the popup.
    renamePopup.data("id", aTag.data("id"));

    renamePopup.show();

    // This is called twice because the first time ever we call it doesn't work.
    renamePopup.position({
        my: "left center",
        at: "right center",
        of: aTag,
        collision: "fit"
    });

    renamePopup.position({
        my: "left center",
        at: "right center",
        of: aTag,
        collision: "fit"
    });

    textbox.focus();

    return false;
}

function HideRenameAudioFile()
{
    //$('#audioFileRename').hide();
    $('#audioFileRename').modal('hide');
}

function RenameAudioFile()
{
    var renamePopup = $('#audioFileRename');
    var textbox = renamePopup.find("#newAudioFilename");
    var newName = $.trim(textbox.val());
    var inboundFileId = parseInt(renamePopup.data("id"));
    
    if (newName.length == 0) {
        window.alert("Name cannot be empty.");
        textbox.focus();
        return false;
    }

    $.ajax(
    {
        url: '/audio/RenameAudioFile',
        type: 'POST',
        data: { Id: inboundFileId, Name: newName, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'json'
    })
    .success(function (result) {
        var aTag = $('#audiofiles').find('[data-id="' + inboundFileId + '"]');

        if (aTag.length > 0) {
            aTag.html(result.Name);
        }
    })
    .error(function (xhr, status) {
        window.alert("An error occurred trying to update the name.");
    });

    HideRenameAudioFile();
}

function ShowDeleteAudioFile(sender, id, fileName) {
    var deletePopup = $('#audioFileDelete');
    var audioFilename = deletePopup.find("#audioFilename");
    var btn = $(sender);
    var headerName = $.trim(fileName);

    audioFilename.html(fileName);

    if (headerName.length > 30)
        headerName = headerName.substring(0, 30) + "...";

    deletePopup.find('#myDeleteLabel').html("Delete: " + headerName);

    // Store the Id of the audio file in the popup.
    deletePopup.data("id", id);

    deletePopup.show();

    // This is called twice because the first time ever we call it doesn't work.
    deletePopup.position({
        my: "center center",
        at: "left center",
        of: btn,
        collision: "fit"
    });

    deletePopup.position({
        my: "center center",
        at: "left center",
        of: btn,
        collision: "fit"
    });

    return false;
}

function HideDeleteAudioFile() {
    $('#audioFileDelete').modal('hide');
}

function DeleteAudioFile() {
    var deletePopup = $('#audioFileDelete');
    var inboundFileId = parseInt(deletePopup.data("id"));

    $.ajax(
    {
        url: '/audio/DeleteAudioFile',
        type: 'POST',
        data: { Id: inboundFileId, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'json'
    })
    .success(function (result) {
        goToAudiofles(1);
    })
    .error(function (xhr, status) {
        window.alert("An error occurred trying to update the name.");
    });

    HideDeleteAudioFile();
}

function RemoveAttachment(id, dom_item)
{
    loadEffectOn('#wordcount_section');

    if (typeof dom_item == 'number') 
    {
        dom_item = $('#' + dom_item.toString());
    }

    $.ajax(
    {
        url: 'deleteattachment',
        data: { id:id , __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        $('#wordcount_section').html(result);
        $(dom_item).remove();
        loadEffectOff('#wordcount_section');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#wordcount_section');
    });   
}

function AddRemovalLink(selector, id) 
{
    var files = $(selector).find('ul[class="qq-upload-list"]');
    var last_file = files.find('li')[files.find('li').length - 1];
    var last_file_handler = $(last_file).find('span[class="qq-upload-file"]');

    var remove_link = $('<a> (X)</a>');
    remove_link.attr('style', 'cursor:pointer;');
    remove_link.bind('click', function () {
        RemoveAttachment(id, last_file);
        return false;
    });

    $(last_file_handler).append(remove_link);
}

function RefreshWordCount() 
{
    loadEffectOn('#wordcount_section');

    $.ajax(
    {
        url: 'WordCount',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#wordcount_section').html(result);
        loadEffectOff('#wordcount_section');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#wordcount_section');
    });
}

function AddAttachmentToList(list,attachment)
{
    var template = '<li class="qq-upload-success" id="${id}"><span class="qq-upload-finished"></span><span class="qq-upload-file">${name}<a style="cursor:pointer;" onclick="RemoveAttachment(${id},${parent});return false;"> (X)</a></span><span style="display:inline">${kb}Mb</span></li>';
    
    $.template("qqFileUploadTemplate", template);

    data = { id:attachment.MedianetAttachmentId, kb: (attachment.Weight / 1024).toFixed(2), name:attachment.Name, parent: attachment.MedianetAttachmentId };

    $.tmpl("qqFileUploadTemplate",data).appendTo(list);
}

function loadRadioCategory(id) 
{
    var selectedTopCategory = $('#' + id);

    button = selectedTopCategory.find('a');

    if (button.length > 0) 
    {
        button = $(button[0]);
    }

    if (button.attr('class') == 'browse-closed') 
    {
        button.attr('class', 'browse-open');
    }
    else
        button.attr('class', 'browse-closed');

    if (selectedTopCategory.find('ul').length > 0) 
    {
        selectedTopCategory.find('ul[class="submenu"]').toggle();
        return;
    }

    loadEffectOn('#custom');

    $.ajax(
    {
        url: 'getchildcategories',
        data: { CategoryId: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        appendSecondCategoryLevel(id, result);
        disableAddToButtonsInCart();
        loadEffectOff('#custom');
    })
    .error(function (xhr, status) 
    {
        loadEffectOff('#custom');
    });
}

function appendSecondCategoryLevel(id, result) 
{
    var categoryNode = $('<ul></ul>');
    categoryNode.attr('class', 'submenu');

    $('#' + id).append(categoryNode);

    var template = '<li id="${Id}" style="cursor:pointer;" onclick="loadProducts(${Id});"><a class="browse-closed"><strong>${Name}</strong></a></li>';

    $.template("productTemplate", template);

    jQuery.each(result, function () 
    {
        $.tmpl("productTemplate", this).appendTo(categoryNode);
    });
}

function loadProducts(id) 
{
    var selectedSecondCategory = $('#' + id);

    button = selectedSecondCategory.find('a');

    if (button.length > 0) 
    {
        button = $(button[0]);
    }

    if (button.attr('class') == 'browse-closed') 
    {
        button.attr('class', 'browse-open');
    }
    else
        button.attr('class', 'browse-closed');

    if (selectedSecondCategory.find('ul').length > 0) 
    {
        selectedSecondCategory.find('ul').toggle();
        return;
    }

    loadEffectOn('#custom');

    $.ajax(
    {
        url: 'GetCategoryProducts',
        data: { CategoryId: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        var categoryNode = $('<ul></ul>');
        categoryNode.attr('style', 'display:block;');

        $('#' + id).append(categoryNode);
        categoryNode.html(result);
        disableAddToButtonsInCart();
        loadEffectOff('#custom');
    })
    .error(function (xhr, status) 
    {
        loadEffectOff('#custom');
    });
}
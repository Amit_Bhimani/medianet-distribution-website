﻿var shoppingCartContainer = '#sidebar';

$(document).ready(function () {
    $("#topListsPeriod ul li a").click(function () {
        var newDiv = $(this).data("div");
        
        // Hide all
        $("#topListsPeriod li a").each(function() {
            var thisDiv = $(this).data("div");
            if (thisDiv != newDiv)
                $("#" + $(this).data("div")).addClass("hide");
        });
        
        $("#" + newDiv).removeClass("hide");
        $("#topListsPeriod .btn").html($(this).html() + " <i class='fa fa-chevron-down'></i>");
        
        //return false;
    });

    $("#HelpSection button").click(function() {
        SendHelpEmail(this);
        return false;
    });

    GetLastReport();
});

function SendHelpEmail(button) {
    $(button).html('<i class="fa fa-spinner"></i> Sending...');
    
    $.ajax(
    {
        url: '/Home/HelpEmail',
        type: 'POST',
        cache: false,
        data: { ToEmail: $("#HelpEmail").val(), Subject: $("#HelpSubject").val(), Message: $("#HelpMessage").val() }
    })
    .success(function (result) {
        var jsonResult = jQuery.parseJSON(result);
        $(button).html('<i class="fa fa-paper-plane"></i> Send Message');

        if (!jsonResult.success) {
            ShowMessage(jsonResult.error, "Error");
        } else {
            // Clear the message
            $("#HelpSubject").val('');
            $("#HelpMessage").val('');
            
            ShowMessage("Email sent", "Information");
        }
    })
    .error(function (xhr, status) {
        ShowMessage("Error sending email. Please try again.", "Error");
    });

    return false;
}

function ShowMessage(message, header) {
    var modalPopup = $("#myModal");

    modalPopup.find(".modal-title").html(header);
    modalPopup.find(".modal-body").html(message);
    modalPopup.modal('show');
}

function loadEffectOn(selector) {
    $(selector).css('opacity', '0.5');
    $(selector).css('filter', 'alpha(opacity=50)');
}

function loadEffectOff(selector) {
    $(selector).css('opacity', '1');
    $(selector).css('filter', 'alpha(opacity=100)');
}

function addToCart(productId, event) {
    $.ajax(
    {
        url: '/Wire/AddProductToCart',
        type: 'POST',
        cache: false,
        data: { ProductId: productId, IsFromDashboard: true, __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) {
        // If we had an error then display it now;
        var error = $(result).find("#errorMsg");
        var str = error.val();

        if (error.length > 0 && str != "") {
                ShowMessage(str, "Error");
        }
        else
            ShowMessage("The item has been added to your cart.", "Success");
    })
    .error(function (xhr, status) {
        //loadEffectOff(shoppingCartContainer);
    });

    if (event != undefined) {
        event.cancelBubble = true;
    }
    else
        window.event.cancelBubble = true;
}

function antiForgetyToken() {
    return $("input[name=__RequestVerificationToken]").val();
}

function GetLastReport() {

    $.ajax(
    {
        url: '/Home/LastReport',
        type: 'GET',
        cache: false
    })
    .success(function (result) {
        $('#lastReport').html(result);
    });

    return false;
}

function ShowMoreUsers() {
    $(".more-users").hide();
    $(".hidden-users").removeClass("hide");

    return false;
}

function HideMoreUsers() {
    $(".hidden-users").addClass("hide");
    $(".more-users").show();

    return false;
}
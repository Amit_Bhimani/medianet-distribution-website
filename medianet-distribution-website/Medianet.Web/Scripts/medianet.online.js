﻿var twitterCatId = "";
var tweetText = "";

function loadCategory(id) {
    loadEffectOn('#browse');

    $.ajax(
    {
        url: '/wire/getchildcategories',
        data: { CategoryId: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) {
        appendSecondCategoryLevel(id, result);
        //disableAddToButtonsInCart();
        loadEffectOff('#browse');
    })
    .error(function (xhr, status) {
        loadEffectOff('#browse');
    });
}

function appendSecondCategoryLevel(id, result) {
    var categoryNode = $('<ul></ul>');
    categoryNode.attr('class', 'submenu');

    $('#' + id).append(categoryNode);

    var template = '<li id="${Id}" style="cursor:pointer;" onclick="loadProducts(${Id});"><a class="browse-closed"><strong>${Name}</strong></a></li>';

    $.template("productTemplate", template);

    jQuery.each(result, function () {
        $.tmpl("productTemplate", this).appendTo(categoryNode);
    });
}

function loadProducts(id)
{
    var selectedSecondCategory = $('#' + id);
    var button = selectedSecondCategory.find('a');

    if (button.length > 0) {
        button = $(button[0]);
    }

    if (button.attr('class') == 'browse-closed') {
        button.attr('class', 'browse-open');
    }
    else
        button.attr('class', 'browse-closed');

    if (selectedSecondCategory.find('ul').length > 0) {
        selectedSecondCategory.find('ul').toggle();
        return;
    }

    loadEffectOn('#browse');

    $.ajax(
    {
        url: '/wire/getcategoryproducts',
        data: { CategoryId: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) {
        var categoryNode = $('<ul></ul>');
        categoryNode.attr('style', 'display:block;');

        $('#' + id).append(categoryNode);
        categoryNode.html(result);
        disableAddToButtonsInCart();
        loadEffectOff('#browse');
    })
    .error(function (xhr, status) {
        loadEffectOff('#browse');
    });
}

function SetTwitter() {
    if ($('input[name="ShouldShowOnTwitter"]')[0].checked) {
        // Enable
        $('#Tweet').removeAttr('disabled');

        // Set old values
        $('#Tweet').val(tweetText);
    }
    else {
        // Disable
        $('#Tweet').attr('disabled', 'disabled');

        // Save old values
        tweetText = $('#Tweet').val();

        // Clear selected values
        $('#Tweet').val('');
    }
}

var tweetRegExp = /^[a-zA-Z0-9~!_=+@\"\-#$%^&*()`\[\]{};':,./<>?| ]*$/;

function limitTweetChars() {
    var limit = 100;
    var i = $('#Tweet').val().length;

    var applyStyle = function (div, length, limit) {
        if (i == limit) {
            $('#counter').css('color', 'red');
        } else {
            $('#counter').css('color', 'white');
        }
    };

    var showCount = function (div, length, limit) {
        div.text('Total: ' + length + "/" + limit);
        applyStyle(div, length, limit);
    };

    showCount($('#counter'), i, limit);

    $('#Tweet').keyup(function (event) {
        //var code = (event.keyCode ? event.keyCode : event.which);
        //var char = String.fromCharCode(code);

        //if (!tweetRegExp.test(char)) {
        //    return false;
        //}

        i = $(this).val().length;

        //if (code != 8 && code != 46) { // backspace and delete
        //    if (i >= (limit-1)) {
        //        if (i >= limit)
        //            $(this).val($(this).val().substring(0, limit));

        //        //showCount($('#counter'), i, limit);
        //        event.preventDefault();
        //        return false;
        //    }
        //}
        if (i > limit) {
            $(this).val($(this).val().substring(0, limit));
            i = limit;
        }

        showCount($('#counter'), i, limit);

        return true;
    });
}

$(document).ready(function () {
    // Save current values
    tweetText = $('#Tweet').val();

    //SetTwitter();
});

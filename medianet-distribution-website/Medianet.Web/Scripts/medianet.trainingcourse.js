﻿function updateTotalPrice() {
	if ($('#PromoCode').val() === '')
		$('#promoCodeApplied').addClass('hidden');
	else
		$('#promoCodeApplied').removeClass('hidden');
}

function applyPromoCode() {
	var courseId = $('#CourseId').val();
	var scheduleId = $('#ScheduleId').val();
	var promoCode = $('#promoCodeToApply').val();

	if (!promoCode || promoCode.length === 0) {
		popCartError("Please enter a promo code.");
		return false;
	}

	loadEffectOn('body');

	$.ajax(
    {
    	url: '/TrainingCourse/ApplyPromoCode',
    	type: 'GET',
    	//contentType: "application/json",
    	data: { courseId: courseId, scheduleId: scheduleId, promoCode: promoCode },
    	dataType: 'json',
    	cache: false
    })
    .success(function (result) {
    	loadEffectOff('body');
    	$('#price').text(displayCurrency(result.Price));
    	$('#PromoCode').val(promoCode);
    	updateTotalPrice();
    })
    .error(function (xhr, status) {
    	loadEffectOff('body');

    	if (xhr.status === 404) {
    		popCartError("Invalid promo code.");
    	} else {
    		popCartError("Error applying promo code.");
    	}
    });
}

function displayCurrency(price) {
    if (Math.round(price) === price)
        return "$" + price;
    else
        return "$" + parseFloat(Math.round(price * 100) / 100).toFixed(2);
}

$(document).ready(function () {
	$('#applyPromoCode').click(applyPromoCode);
	$('#promoCodeToApply').val($('#PromoCode').val());

	updateTotalPrice();
})
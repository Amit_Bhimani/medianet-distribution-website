﻿$(document).ready(function () {
    configureTabs();
    $('input[name=PaymentMethod]').change(configureTabs);
});

function configureTabs() {
    var method = $('input[name=PaymentMethod]:checked').val();

    if (method === 'I') {
        $('#creditcard').hide();
        $('#invoice').show();
    }
    else {
        $('#creditcard').show();
        $('#invoice').hide();

    }
}

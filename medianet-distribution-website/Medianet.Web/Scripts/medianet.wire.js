﻿function loadWireProducts(CategoryId) 
{
    var products_container = $('#' + CategoryId.toString()).find('div[class="accordion-inner"]').find('ul[class="list"]');
    var categorylink       = $('a[name="'+CategoryId+'"]');

    if ( $(products_container).find('li[class="item"]').length > 0 ) 
    {
        if ($('#' + CategoryId.toString()).attr('class') == 'accordion-body collapse in') 
        {
            $('#' + CategoryId.toString()).attr('class', 'accordion-body collapse');
            categorylink.attr("class", "accordion-toggle browse-closed");
        }
        else 
        {
            $('#' + CategoryId.toString()).attr('class', 'accordion-body collapse in');
            categorylink.attr("class", "accordion-toggle browse-open");
        }        

        return;
    }

    loadEffectOn('#wires');

    $.ajax(
    {
        url: '/wire/getwireproducts',
        data: { CategoryId: CategoryId, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        products_container.html(result);
        $('#' + CategoryId.toString()).attr('class', 'accordion-body collapse in');
        categorylink.attr("class", "accordion-toggle browse-open");
        disableAddToButtonsInCart();
        loadEffectOff('#wires');
    })
    .error(function (xhr, status) 
    {
        loadEffectOff('#wires');
    });      
}

﻿shoppingCartContainer = '#shoppingcartSection';

function addExtraEmailRecipientoToCart()
{
    var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( emailRegExp.test( $('#extraeMail').val() ) )
    {
        loadEffectOn(shoppingCartContainer);    

        $.ajax(
        {
            url: 'AddExtraReceipientToCart',        
            type: 'POST',
            cache: false,
            data: { address:$('#extraeMail').val(), distributionType:'E', __RequestVerificationToken: antiForgetyToken() }
        })
        .success(function (result) 
        {
            $(shoppingCartContainer).html(result);
            loadEffectOff(shoppingCartContainer);
            $('#extraeMail').val('');

            // If we had an error then display it now;
            LookForError();
        })
        .error(function(xhr, status) 
        {
            loadEffectOff(shoppingCartContainer);
        });        
    }
    else
        alert('The email is invalid');
}

function addCustomerListtoCart(ServiceId,event)
{
    loadEffectOn(shoppingCartContainer);    

    $.ajax(
    {
        url: 'addcustomListtocart',        
        type: 'POST',
        cache: false,
        data: { ServiceId:ServiceId, __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $(shoppingCartContainer).html(result);
        loadEffectOff(shoppingCartContainer);
        disableAddToButtonsInCart();
        //disableAddToButton(ServiceId);

        // If we had an error then display it now;
        LookForError();
    })
    .error(function(xhr, status) 
    {
        loadEffectOff(shoppingCartContainer);
    });

    if (event != undefined) {
        event.cancelBubble = true;
    }
    else
        window.event.cancelBubble = true;
}
﻿/// <reference path="./plugins/typeahead.bundle.js" />
/// <reference path="./medianet.common.js" />

function search(sender, event)
{
    if ( event.keyCode == 13 )
    {
        loadAutoComplete();
        return false;
    }
    else
        return true;
}

function configAutocomplete() {
    $('#outletSearchbox').typeahead({
        hint: false,
        highlight: true,
        minLength: 3
    },
    {
        limit: 15,
        displayKey: 'Value',
        source: function(query, syncresults, process) {
            var url = '/Wire/AutoSuggestOutlets';

            $.get(url,
                {
                    query: query
                },
                function(data) {
                    process(data);
                });
        }
    }).on('typeahead:selected', function (event, data) {
        $('#outletSearchbox').val(data.Value);
        $('#selectedOutletId').val(data.Key);
        $('#selectedOutletName').val(data.Value);
    });

}

function loadAutoComplete()
{
    var criteria = $('#searchbox').val();

    loadEffectOn('#searchresult');

    $.ajax(
    {
        url: '/wire/loadsearchproducts',
        data: { Criteria:criteria , __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        $('#searchresult').html(result);
        disableAddToButtonsInCart();
        loadEffectOff('#searchresult');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#searchresult');
    });   
}

function outletSearch(sender, event) {
    if (event.keyCode == 13) {
        loadOutletAutoComplete();
        return false;
    }
    else
        return true;
}

function loadOutletAutoComplete() {
    var selectedOutletId = $('#selectedOutletId').val();
    var selectedOutletName = $('#selectedOutletName').val();

    if (selectedOutletId === "" || selectedOutletName !== $('#outletSearchbox').val()) {
        ShowMessage("Error", "Please select an Outlet from the dropdown.");
        return;
    }

    loadEffectOn('#outletSearchResult');

    $.ajax(
    {
        url: '/wire/SearchOutlets',
        data: { outletId: selectedOutletId, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) {
        $('#outletSearchResult').html(result);
        disableAddToButtonsInCart();
        loadEffectOff('#outletSearchResult');
    })
    .error(function (xhr, status) {
        loadEffectOff('#outletSearchResult');
    });
}

function loadProducts(id)
{    
    var selectedSecondCategory = $('#' + id);

    button = selectedSecondCategory.find('a');

    if ( button.length > 0 )
    {
        button = $(button[0]);
    }

    if ( button.attr('class') == 'browse-closed' )
    {
        button.attr('class', 'browse-open');
    }
    else
        button.attr('class', 'browse-closed');

    if (selectedSecondCategory.find('ul').length > 0) 
    {
        selectedSecondCategory.find('ul').toggle();
        return;
    }    
    
    loadEffectOn('#browse');

    $.ajax(
    {
        url: '/wire/getcategoryproducts',
        data: { CategoryId: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        var categoryNode = $('<ul></ul>');
        categoryNode.attr('style', 'display:block;');

        $('#' + id).append(categoryNode);
        categoryNode.html(result);
        disableAddToButtonsInCart();
        loadEffectOff('#browse');
    })
    .error(function (xhr, status) {
        loadEffectOff('#browse');
    });          
}

function appendSecondCategoryLevel(id,result)
{
    var categoryNode = $('<ul></ul>');
        categoryNode.attr('class','submenu');
    
    $('#'+id).append(categoryNode);
        
    var template = '<li id="${Id}" style="cursor:pointer;" onclick="loadProducts(${Id});"><a class="browse-closed"><strong>${Name}</strong></a></li>';

    $.template("productTemplate", template);

    jQuery.each(result, function () 
    {
        $.tmpl("productTemplate",this).appendTo(categoryNode);
    });
}

function loadCategory(id) 
{
    var selectedTopCategory = $('#' + id);

    button = selectedTopCategory.find('a');

    if ( button.length > 0 )
    {
        button = $(button[0]);
    }

    if ( button.attr('class') == 'browse-closed' )
    {
        button.attr('class', 'browse-open');
    }
    else
        button.attr('class', 'browse-closed');

    if (selectedTopCategory.find('ul').length > 0) 
    {
        selectedTopCategory.find('ul[class="submenu"]').toggle();
        return;
    }

    loadEffectOn('#browse');

    $.ajax(
    {
        url: '/wire/getchildcategories',
        data: { CategoryId:id , __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        appendSecondCategoryLevel(id, result);
        //disableAddToButtonsInCart();
        loadEffectOff('#browse');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#browse');
    });        
}
﻿shoppingCartContainer = '#shoppingcartSection';

function addExtraNumberRecipientoToCart()
{
    var emailRegExp = /[0-9]{10}/;
    var val = Trim($('#extraNumber').val());   

    if ( emailRegExp.test( val ) )
    {
        loadEffectOn(shoppingCartContainer);    

        $.ajax(
        {
            url: '/sms/addextrareceipienttocart',
            type: 'POST',
            cache: false,
            data: { address:val, distributionType:'S', __RequestVerificationToken: antiForgetyToken() }
        })
        .success(function (result) 
        {
            $(shoppingCartContainer).html(result);
            loadEffectOff(shoppingCartContainer);
            $('#extraNumber').val('');

            // If we had an error then display it now;
            LookForError();
        })
        .error(function(xhr, status) 
        {
            loadEffectOff(shoppingCartContainer);
        });        
    }
    else
        alert('The number is invalid');
}

function addCustomerListtoCart(ServiceId)
{
    loadEffectOn(shoppingCartContainer);    

    $.ajax(
    {
        url: '/sms/addcustomListtocart',        
        type: 'POST',
        cache: false,
        data: { ServiceId:ServiceId, __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $(shoppingCartContainer).html(result);
        loadEffectOff(shoppingCartContainer);
        disableAddToButtonsInCart();
        //disableAddToButton(ServiceId);

        // If we had an error then display it now;
        LookForError();
    })
    .error(function(xhr, status) 
    {
        loadEffectOff(shoppingCartContainer);
    });
}
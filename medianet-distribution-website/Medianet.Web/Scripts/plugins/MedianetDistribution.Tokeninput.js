﻿/// <reference path="../jquery-1.5.1.js" />
/// <reference path="../bootstrap.js" />
/// <reference path="../Common/MedianetDistribution.js" />
/// <reference path="jquery.tokeninput.js" />

MedianetDistribution.TokenInput = (function () {
    return {
        activeClass: 'token-dropdown-active',
        uiDisabledClass: 'ui-state-disabled',
        GetSubjectsTokenInputResultFormatter: function (item) {
            return '<li>' + item.Name + '</li>';  /*item.Id.toString().match(MedianetDistribution.TokenInput.groupRegex)
                ?'<li class="subjectGroup"><span class="ui-icon ui-icon-folder-collapsed"></span>' + item.Name + '</li>'
                : '<li>' + item.Name + '</li>';*/
        },
        GetSubjectsTokenInputResultRow: function (item) {
            return item.Id.toString().match(MedianetDistribution.TokenInput.groupRegex)
                ? '<li class="subjectGroup"><span class="ui-icon ui-icon-folder-collapsed"></span><p>' + item.Name + '</p></li>'
                : '<li><p>' + item.Name + '</p></li>';
        },
        GetEmployeeSubjectsResultRow: function(item) {
            return item.Id.toString().match(MedianetDistribution.TokenInput.primaryRegex)
                ? '<li class="primarySubject"><i class="icon-star" title="Primary Contact"></i><p>' + item.Name + '</p></li>'
                : '<li class="secondarySubject"><i class="icon-star-empty" title="Non-primary Contact"></i><p>' + item.Name + '</p></li>';
            //: '<li class="secondarySubject"><span class="ui-icon ui-icon-secondary-subject" title="Non-primary Contact"></span><p>' + item.Name + '</p></li>';
        },
        commonTokenInputSettings: {
            tokenValue: "Id",
            propertyToSearch: "Name",
            dropdownHeight: "250px",
            keepOpenOnSelect: true,
            deleteFromSourceOnSelect: true,
            searchDelay: 500,
            minChars: 1,
            preventDuplicates: true,
            theme: "mediapeople",
            noResultsText: "No matches found.",
            showSelectedInDropdown: false,
            searchingText: "Fetching matches from the server...",
            hideOnRemove: false
        },
        tokenInputRegex: /^token\-input\-(.*)$/i,
        groupRegex: /^g\d+$/gi,
        primaryRegex: /^p\d+$/gi,
        /* utility functions starts */
        IsValidDatasource: function(ds) {
            return $.isArray(ds) && ds.length > 0 && ds[0] != null && ds[0].hasOwnProperty('Name') && ds[0].hasOwnProperty('Id');
        },
        GetInputToken: function GetInputToken(id) {
            if (id) { // && $.isArray($("#" + id).tokenInput("get"))) {
                return {
                    wrapper: $("#token-input-" + id + "-token-input-dropdown-mediapeople"),
                    tokensList: $("#token-input-" + id + "-token-input-list-mediapeople"),
                    inputBox: $("#token-input-" + id),
                    height: $("#token-input-" + id + "-token-input-list-mediapeople").height() +
                        parseInt($("#token-input-" + id + "-token-input-list-mediapeople").css('border-top-width')) +
                        parseInt($("#token-input-" + id + "-token-input-list-mediapeople").css('border-bottom-width'))
                };
            }

            return null;
        },
        AddSubjectGroupToTokenInput: function(id) {
            var _data = $("#" + id).data();
            $("#" + id).tokenInput("add", { Id: /*"g" + */ _data.groupFilterId, Name: _data.groupFilterName });

            try {
                $("#" + id).tokenInput("hide");
            } catch(e) {
            }
        },
        ClearSubjectFilterById: function(id) {
            $("#" + id).data({ groupFilterId: '', groupFilterName: '' });
            var ddfilter = $("#" + id).parentsUntil('tr').siblings().find(".dropdown.btnFilter");

            return true;
        },
        RepositionDropdown: function(dropdown) { //hack to position dropdown properly as the tokenInput is increased height. This whole dropdown started with generic and now quite specific to subject groups :(.
            var menu = dropdown.parent().find('div.token-dropdown-menu');

            var isQuickSearchPage = $("form#MonitoringSearchForm").length > 0; //why this hack? because firefox doesnot respect absolutely position items inside a relative td/tr/table. To get around we had to add a wrapper div and hence the top is changed.
            var ti = MedianetDistribution.TokenInput.GetInputToken(dropdown.data('for')); //get TokenInput for positioning groups dropdown properly.

            if (ti && ti.height) {
                var ht = isQuickSearchPage ? ti.height - 27 : ti.height;
                menu.css('top', ht);
            }
        },
        InitFilterDropdowns: function() {
            /* for keeping track of what's "open" */
            var showingDropdown, showingMenu, showingParent;
            var offsetWidth = 4;
            if ($('html').hasClass('ie7') && $("#qSearchWrapper").length) offsetWidth = 6;
            if ($('html').hasClass('ie7') && $("#AdvancedForm").length) offsetWidth = 2;
            /* hides the current menu */
            var hideMenu = function() {
                if (showingDropdown) {
                    if (showingDropdown.siblings(".token-dropdown-menu").find(".tokenSelected").length == 0) showingDropdown.removeClass(MedianetDistribution.TokenInput.activeClass);

                    showingMenu.hide();
                }
            };

            /* recurse through dropdown menus */
            $('.dropdown.btnFilter').each(function() {
                /* track elements: menu, parent */
                var dropdown = $(this);

                var menu = dropdown.parent().find('div.token-dropdown-menu'), parent = dropdown.parent();

                /* function that shows THIS menu */
                var showMenu = function() {
                    //if ie7 and on the quicksearch page
                    MedianetDistribution.TokenInput.RepositionDropdown(dropdown);
                    hideMenu();
                    menu.width(menu.closest('tr').children('td.searchInput:first').width() - offsetWidth);
                    showingDropdown = dropdown.addClass('dropdown-active');
                    showingMenu = menu.show();
                    showingParent = parent;
                    try {
                        $("#" + dropdown.data('for')).tokenInput("hide");
                    } catch(e) {
                    }
                };

                dropdown.on({
                    click: function(e) { /* function to show menu when clicked */
                        //                        if (e) { e.stopPropagation(); e.preventDefault(); }
                        showMenu();
                    },
                    focus: function() { showMenu(); } /* function to show menu when someone tabs to the box */
                });

                //when the anchor for the group is clicked -> filter subjects tokenInput by this group (only if it isn't already the selected or being filtered by)
                menu.children('div.token-dropdown-menu-row').on({
                    click: function() {
                        $("#" + dropdown.data('for')).data({ groupFilterId: $(this).data().groupId, groupFilterName: $(this).data().groupName });
                        //hide the drop down
                        hideMenu();
                        $("[id$=-" + dropdown.data('for') + "]").val(' ').focus().trigger("keydown", { KeyCode: "32" });
                        return false;
                    }
                });
            });

            /* hide when clicked outside */
            $(document.body).on({
                click: function(e) {
                    if (showingParent) {
                        var parentElement = showingParent[0];
                        if (!$.contains(parentElement, e.target) || !parentElement == e.target) {
                            hideMenu();
                        }
                    }
                }
            });
        },
        HideClosestShowingDropdown: function() {
            var tr = $(this).parents('tr:first');

            if (tr.find('div.token-dropdown-menu').is(':visible')) {
                tr.find('.dropdown.btnFilter').removeClass(MedianetDistribution.TokenInput.activeClass);
                tr.find('div.token-dropdown-menu').hide();
            }
        },
        AddTokenItem: function(dropdownId, dataId, dataName) {
            $("#" + dropdownId).tokenInput("add", { Id: dataId, Name: dataName });
        },
        ToggleTokenInput: function(id, enable, container) {
            try {
                var ti = MedianetDistribution.TokenInput.GetInputToken(id);
                var parentTbl = ti.tokensList.parentsUntil(container, 'div').last();

                if (enable != "undefined" && typeof enable === "boolean") {
                    enable ? parentTbl.removeClass(MedianetDistribution.TokenInput.uiDisabledClass) : parentTbl.addClass(MedianetDistribution.TokenInput.uiDisabledClass);

                    $(':input', parentTbl).prop('disabled', !enable);

                    $("#" + id).tokenInput("toggleDisabled", !enable);
                } else {
                    return !parentTbl.hasClass(MedianetDistribution.TokenInput.uiDisabledClass);
                }
            } catch(ex) {
            }
        },
        ConvertToValidGroups: function(ds) {
            return $.map(ds, function(ele, index) {
                return { Id: "g" + ele.Id, Name: ele.Name };
            });
        },
        ConvertToValidPrimarySubject: function(ds) {
            return $.map(ds, function(ele, index) {
                return { Id: "p" + ele.Id, Name: ele.Name };
            });
        }
    };
})();

﻿var finaliseContainer = "#finaliseContent";

function ReloadFinalise() {
    loadEffectOn(finaliseContainer);

    $.ajax(
    {
        url: 'FinalisePartial',
        type: 'GET',
        cache: false,
        data: { __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) {
        $(finaliseContainer).html(result);
        loadEffectOff(finaliseContainer);
    })
    .error(function (xhr, status) {
        loadEffectOff(finaliseContainer);
    });
}

function ValidateWireText() {
    if ($('#ConfirmWireText').is(":checked"))
    {
        if (countWords($("#DerivedWireText").val()) == 0)
        {
            $("#btnConfirmWire").prop("disabled", true);
        } else {
            $("#btnConfirmWire").prop("disabled", false);
        }
    }
    else
        $("#btnConfirmWire").prop("disabled", true);
    $('#modalWireText').modal("show");
}

function confirmWireDetails() {
    $('#modalWireText').modal("hide");
    loadEffectOn(finaliseContainer);
    var conf;
    if ($('#ConfirmWireText').is(":checked"))
        conf = true;
    else
        conf = false;

    $.ajax(
    {
        url: 'ConfirmWireDetails',
        data: {confirmWire: conf,
            wireText: $("#DerivedWireText").val(),
            __RequestVerificationToken: antiForgetyToken()
        },
        cache: false,
        type: 'POST'
    })
    .success(function (result) {
        $(finaliseContainer).html(result);
        loadEffectOff(finaliseContainer);

        ReLoadShoppingCart();
    })
    .error(function (xhr, status) {
        loadEffectOff(finaliseContainer);
    });
}
$("#DerivedWireText").on('change keyup paste', function () {
    if ($('#ConfirmWireText').is(":checked")) {
        if (countWords($("#DerivedWireText").val()) == 0) {
            $("#btnConfirmWire").prop("disabled", true);
        } else {
            $("#btnConfirmWire").prop("disabled", false);
        }
    }
    else {
        $("#btnConfirmWire").prop("disabled", true);
    }
});
function countWords(str) {
    return str
        .split(' ')
        .filter(function (n) { return n != '' })
        .length;
}
$('#ConfirmWireText').click(function () {
    if (this.checked)
    {
        if (countWords($("#DerivedWireText").val()) == 0) {
            $("#btnConfirmWire").prop("disabled", true);
        } else {
            $("#btnConfirmWire").prop("disabled", false);
        }
    }
    else
    {
        $("#btnConfirmWire").prop("disabled", true);
    }
});

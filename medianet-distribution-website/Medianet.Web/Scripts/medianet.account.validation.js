﻿jQuery.validator.addMethod(
    'abn',
    function(value, element) {
        if (!value.length && this.optional(element))
            return true;

        if (value.length != 11 || isNaN(parseInt(value)))
            return false;

        var weighting = [10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19];
        var tally = (parseInt(value.charAt(0)) - 1) * weighting[0];

        for (var i = 1; i < value.length; i++) {
            tally += (parseInt(value.charAt(i)) * weighting[i]);
        }

        return (tally % 89) == 0;
    },
    'This ABN is not valid'
);

jQuery.validator.addMethod(
    'acn',
    function(value, element) {
        if (!value.length && this.optional(element))
            return true;

        if (value.length != 9 || isNaN(parseInt(value)))
            return false;

        var weighting = [8, 7, 6, 5, 4, 3, 2, 1];
        var tally = 0;

        for (var i = 0; i < weighting.length; i++) {
            tally += (parseInt(value.charAt(i)) * weighting[i]);
        }

        var check = 10 - (tally % 10);
        check = check == 10 ? 0 : check;

        return check == value[8];
    },
    'This ACN is not valid'
)
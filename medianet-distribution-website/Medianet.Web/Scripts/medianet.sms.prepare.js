﻿var smsRegExp = /^[a-zA-Z0-9~!_=+@\"\-#$%^&*()`\[\]{};':,./<>?| ]*$/;

function limitSmsChars() 
{
    var limit = 160;
    var i = $('textarea').val().length;

    var applyStyle = function (div, length, limit) 
    {
        if ( i == limit )  
        {
            $('#counter').css('color', 'red');
        } else {
            $('#counter').css('color', 'white');
        }
    };

    var showCount = function (div, length, limit) 
    {
        div.text('Total: ' + length + "/" + limit);
        applyStyle(div, length, limit);
    };

    showCount($('#counter'), i, limit);

    $('textarea').keypress(function (event) 
    {
        var code = (event.keyCode ? event.keyCode : event   .which);
        var char = String.fromCharCode(code);

        if ( !smsRegExp.test(char) )
        {
            return false;
        }

        i = $(this).val().length;
        if ( i == 160 )
        {
            showCount($('#counter'), i, limit);
            return false;
        }
        else
            showCount($('#counter'), i, limit);

        return true;
    });
}

function RefreshWordCount() 
{
    loadEffectOn('#wordcount_section');

    $.ajax(
    {
        url: '/sms/WordCount',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#wordcount_section').html(result);
        loadEffectOff('#wordcount_section');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#wordcount_section');
    });
}


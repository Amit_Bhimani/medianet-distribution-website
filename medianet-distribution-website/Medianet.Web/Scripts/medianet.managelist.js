﻿var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

$(document).ready(function () 
{
    $('#edition :input').attr('disabled', true);
    $('#edition').find('a').attr('disabled', true);

    var allowedExtensions = ['csv'];
    var sizeLimit = 2097152;
    var newUpload = $('#inputNewUploadList')[0];
    var replaceUpload = $('#inputReplaceList')[0];

    var newUploader = new qq.FileUploader(
    {
        element: newUpload,
        allowedExtensions: allowedExtensions,
        sizeLimit: sizeLimit,
        action: 'uploadnewfile',
        multiple: false,
        autoUpload: true,
        debug: false,
        onComplete: function (id, fileName, responseJSON) 
        {
            if (responseJSON.success) 
            {                
                popCartError(jsonResult.error);                
            }
            refreshLists(responseJSON);
        },
        onSubmit: function (id, fileName) {
            newUploader.setParams({ name: TrimSides($('#ListName').val()) });
            return canUploadNewList();
        }
    });

    var replaceUploader = new qq.FileUploader(
    {
        element: replaceUpload,
        allowedExtension: allowedExtensions,
        sizeLimit: sizeLimit,
        action: 'ReplaceList',
        multiple: false,
        autoUpload: true,
        debug: false,
        onComplete: function (id, fileName, responseJSON) 
        {
            if (responseJSON.success) 
            {
                popCartError(jsonResult.error);
            }        
            refreshLists(responseJSON);
        },
        onSubmit: function (id, fileName) {
            replaceUploader.setParams({ ListId: Selected.find("option:selected").val() });
            return canUploadReplaceList();
        }
    });
});

function VerifyEmailAddresses() 
{    
    var addresses       = TrimSides($('#PastedeMailAddresses').val());
    var parsedAddresses = addresses.split(',')
    var count = parsedAddresses.length

    if ( count == 0 ) 
    {
        alert('There are no email addresses input');
        return false;
    }

    for (index = 0; index < count; index++) 
    {
        var address = Trim(parsedAddresses[index]);      

        if (!emailRegExp.test(address)) 
        {
            alert('The email address: ' + address + ' is invalid');
            return false;
        }
    }

    return true;
}

function AddNewFromPaste() 
{
    if (TrimSides($('#ListName').val()) == "") 
    {
        alert('You must input a new name');
        return false;
    }

    if ( !VerifyEmailAddresses() )
    {     
        return false;
    }    

    var name      = TrimSides($('#ListName').val());
    var addresses = TrimSides($('#PastedeMailAddresses').val());    

    loadEffectOn('#content');

    $.ajax(
    {
        url: 'AddPastedList',
        data: { NameList: name, Addresses: addresses, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        var jsonResult = jQuery.parseJSON(result)

        if (!jsonResult.success) 
        {
            popCartError(jsonResult.error);
        }

        refreshLists(jsonResult);
        renameListCancel();
        $('#PastedeMailAddresses').val('');
        loadEffectOff('#content');
    })
    .error(function (xhr, status) 
    {
        renameListCancel();
        loadEffectOff('#content');
    }); 
}

function cancelEditList()
{
    // Deselect the currently selected item in the listbox.
    $("#SelectedListRecipient option").prop("selected", false);

    // Configure the Add/Update/Delete buttons and clear the form.
    onNotEditingRecipient();

    // Clear any validation errors.
    $('.field-validation-error')
    .removeClass('field-validation-error')
    .addClass('field-validation-valid');

    $('.input-validation-error')
    .removeClass('input-validation-error')
    .addClass('valid');

    $('span[class="field-validation-valid"]').html('');
}

function onEditingRecipient()
{
    // Configure the Add/Update/Delete buttons.
    $('#addRecipient').attr('disabled', true);
    $('#UpdateRecipient').removeAttr('disabled');
    $('#deleteRecipient').removeAttr('disabled');
    $('#SelectedListRecipient').removeAttr('disabled');
}

function onNotEditingRecipient() {
    // Clear all in the form except for the ListId.
    $('#edition :input').val('');
    $('#ListId').val(SelectedList.find("option:selected").val());

    // Configure the Add/Update/Delete buttons.
    $('#addRecipient').removeAttr('disabled');
    $('#UpdateRecipient').attr('disabled', true);
    $('#deleteRecipient').attr('disabled', true);
    $('#SelectedListRecipient').removeAttr('disabled');
}

function resetTopBarTool()
{
    //$('#editionTop :input').removeAttr('disabled');
    //$('#editionTop').find('a').removeAttr('disabled');
    $('#SelectedListRecipient').html('');
    $('#edition :input').attr('disabled',true);
    $('#edition').find('a').attr('disabled',true);
}

function downloadList(sender)
{
    if ( SelectedList.find("option:selected").length == 0 )
    {
        alert('You must select a list first!');
        return false;
    }

    var id = SelectedList.find("option:selected").val();

    $(sender).attr('href','download/'+id);

    return true;
}

function editList()
{
    if ( SelectedList.find("option:selected").length == 0 )
    {
        alert('You must select a list first!');
        return false;
    }

    var id = SelectedList.find("option:selected").val();

    loadEffectOn('#content');

    $.ajax(
    {
        url: 'editlist',
        data: { Id: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        var jsonResult = jQuery.parseJSON(result);

        loadRecipients(jsonResult);
        loadEffectOff('#content');
    })
    .error(function (xhr, status) {
        renameListCancel();
        loadEffectOff('#content');
    });  

    $('#edition :input').removeAttr('disabled');
    $('#edition').find('a').removeAttr('disabled');

    onNotEditingRecipient();
}

function loadRecipients(results)
{
    $('#SelectedListRecipient').html('');

    jQuery.each(results.lists, function() 
    {
        var option = $('<option></option>');
            option.attr('value',this.Value);
            option.html(this.Text);

        $('#SelectedListRecipient').append(option);
    });
}

function updateRecipient() 
{
    if (isDisabled("#UpdateRecipient")) 
    {
        return;
    }

    if (TrimSides($('#RecipientId').val()) == '') {
        alert('You must select a recipient first');
        return false;
    }

    $(function() {
    $('#form0').submit(function() 
    {
        loadEffectOn('#edition');   
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            error :function(xhr, status) 
            {
                loadEffectOff('#edition');            
            },
            success: function(result) {
               // The AJAX call succeeded and the server returned a JSON 
               // with a property "s" => we can use this property
               // and set the html of the target div
                $('#edition').html(result);
                $('#SelectedListRecipient').find('option[value="' + $('#RecipientId').val() + '"]')[0].selected = true
               loadEffectOff('#edition');
               onEditingRecipient();
            }
        });
        // it is important to return false in order to 
        // cancel the default submission of the form
        // and perform the AJAX call
        return false;
        });
    });
    
    $('#form0').attr('action','UpdateRecipient');    
    $('#form0').submit();
}

function isDisabled(element) 
{
    if ( $(element).attr('disabled') != undefined ) 
    {
        return true;
    }
    else
        return false;
}

function AddRecipient() 
{
    if (isDisabled("#addRecipient")) 
    {
        return;
    }

    $(function () {
        $('#form0').submit(function () {
            loadEffectOn('#edition');
            $.ajax({
                url: this.action,
                type: this.method,
                data: $(this).serialize(),
                error: function (xhr, status) {
                    loadEffectOff('#edition');
                },
                success: function (result) {
                    // The AJAX call succeeded and the server returned a JSON 
                    // with a property "s" => we can use this property
                    // and set the html of the target div
                    $('#edition').html(result);
                    $('#SelectedListRecipient').find('option[value="' + $('#RecipientId').val() + '"]').attr('selected', true);
                    loadEffectOff('#edition');
                    onEditingRecipient();
                }
            });
            // it is important to return false in order to 
            // cancel the default submission of the form
            // and perform the AJAX call
            return false;
        });
    });
    
    $('#form0').attr('action','AddRecipient');
    $('#form0').validate();
    $('#form0').submit();
}

function deleteRecipient() 
{
    if (isDisabled("#deleteRecipient")) 
    {
        return;
    }

    if (TrimSides($('#RecipientId').val()) == '')
    {
        alert('You must select a recipient first');
        return false;
    }

    var list_id         = $('#ListId').val();
    var recipient_id    = $('#RecipientId').val();

    loadEffectOn('#edition');

    $.ajax(
    {
        url: 'deleterecipient',
        data: { ListId: list_id, RecipientId: recipient_id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) {
        $('#edition').html(result);
        loadEffectOff('#edition');
        onNotEditingRecipient();
    })
    .error(function (xhr, status) {
        loadEffectOff('#form');
    });
}

function editRecipient()
{
    var list_id         = SelectedList.find("option:selected").val();   
    var list_name       = SelectedList.find("option:selected").html();  
    var recipient_id    = $('#SelectedListRecipient').find("option:selected").val();

    if ( $('#SelectedListRecipient').find("option:selected").length == 0 )
    {
        return;
    }

    loadEffectOn('#edition');

    $.ajax(
    {
        url: 'getrecipient',
        data: { RecipientId: recipient_id, ListId: list_id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) {
        $('#edition').html(result);
        $('#addRecipient').attr('disabled', true);
        $('#deleteRecipient').removeAttr('disabled');
        $('#UpdateRecipient').removeAttr('disabled');
        $('#resetRecipient').removeAttr('disabled');
        $('#SelectedListRecipient').find('option[value="' + recipient_id + '"]').attr('selected', true);

        loadEffectOff('#edition');
    })
    .error(function (xhr, status) {
        loadEffectOff('#form');
    });
}

function renameListPrepare()
{
    $('#newListName').show();
    $('#newListNameLabel').show();    
    $('#okRenameListButton').show();
    $('#cancelRenameListButton').show();
    
    $('#downloadFile').hide();
    $('#renameListButton').hide();
    $('#deleteListButton').hide();
    $('#editListButton').hide();
}

function renameListCancel()
{
    $('#newListName').val('');
    $('#newListName').hide();
    $('#newListNameLabel').hide();
    $('#okRenameListButton').hide();
    $('#cancelRenameListButton').hide();
    
    $('#downloadFile').show();
    $('#renameListButton').show();
    $('#deleteListButton').show();
    $('#editListButton').show();
}

function renameList()
{
    if (TrimSides($('#newListName').val()) == "")
    {
        alert('You must input a new name');
        return false;
    }

    var id   = SelectedList.find("option:selected").val();
    var name = TrimSides($('#newListName').val());

    loadEffectOn('#content');

    $.ajax(
    {
        url: 'RenameList',
        data: { Id: id, NewName: name, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        var jsonResult = jQuery.parseJSON(result)

        if (!jsonResult.success) 
        {
            popCartError(jsonResult.error);
        }

        refreshLists(jsonResult, id);
        renameListCancel();
        loadEffectOff('#content');
    })
    .error(function (xhr, status) {
        renameListCancel();
        loadEffectOff('#content');
    });            
}

function deleteList()
{
    if ( SelectedList.find("option:selected").length == 0 )
    {
        alert('You must select a list first!');
        return false;
    }
    
    var id   = SelectedList.find("option:selected").val();
    var name = SelectedList.find("option:selected").html();
    var resp = confirm("Are you sure you want to delete the list " + name + " ? ");
    
    if (resp)
    {
        loadEffectOn('#content');

        $.ajax(
        {
            url: 'deletelist',
            data: { Id: id, __RequestVerificationToken: antiForgetyToken() },
            cache: false,
            type: 'POST'
        })
        .success(function (result) {
            var jsonResult = jQuery.parseJSON(result)
            refreshLists(jsonResult);
            resetTopBarTool();
            loadEffectOff('#content');
        })
        .error(function (xhr, status) {
            loadEffectOff('#content');
        });            
    }
}

function refreshLists(results, id)
{
    Selected.html('');
    SelectedList.html('');

    jQuery.each(results.lists, function() 
    {
        var option = $('<option></option>');
            option.attr('value',this.Value);
            option.html(this.Text);

        var option2 = $('<option></option>');
            option2.attr('value',this.Value);
            option2.html(this.Text);

        Selected.append(option);
        SelectedList.append(option2);
    });

    if(typeof id != 'undefined')
        SelectedList.find('option[value="' + id + '"]').attr('selected', true);
}

function canUploadReplaceList()
{
    if ( Selected.find("option:selected").length == 0 )
    {
        return false;
    }
    else
        return true;
}

function canUploadNewList()
{
    var ListName = TrimSides($('#ListName').val()); 

    if ( ListName == '' )
    {
        alert('Must input the new list name');
        $('#ListName').focus();
        return false;
    }
    else
        return true;
}

function AddRemovalLink(selector,id)
{
    var files             = $(selector).find('ul[class="qq-upload-list"]');
    var last_file         = files.find('li')[files.find('li').length-1];
    var last_file_handler = $(last_file).find('span[class="qq-upload-file"]');
        
    var remove_link = $('<a> (X)</a>'); 
        remove_link.attr('style','cursor:pointer;');
        remove_link.bind( 'click' , function () 
        { 
            RemoveAttachment(id,last_file);
            return false; 
        });

    $(last_file_handler).append(remove_link);
}

function AddFileToList(list,attachment)
{
    var template = '<li class="qq-upload-success" id="${id}"><span class="qq-upload-finished"></span><span class="qq-upload-file">${name}<a style="cursor:pointer;" onclick="RemoveAttachment(${id},${parent});return false;"> (X)</a></span><span style="display:inline">${kb}kB</span></li>';
    
    $.template("qqFileUploadTemplate", template);

    data = { id:attachment.MedianetAttachmentId,
            kb:attachment.Weight,
            name:attachment.Name,
            parent: attachment.MedianetAttachmentId
    };

    $.tmpl("qqFileUploadTemplate",data).appendTo(list);
}

function RemoveAttachment(id,dom_item)
{
    loadEffectOn('#wordcount_section');

    $.ajax(
    {
        url: 'deleteattachment',
        data: { id:id , __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        $('#wordcount_section').html(result);
        $(dom_item).remove();
        loadEffectOff('#wordcount_section');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#wordcount_section');
    });        
}
﻿var attendees = [];

$(document).ready(function () {
    $('#addAttendee').click(showAddAttendeePopup);
    $('#submitStep1').click(validateStep1Form);
    $('#AgreeConditions').change(validateTerms);
    $('#applyPromoCode').click(applyPromoCode);
    $('#promoCodeToApply').val($('#PromoCode').val());

    var attendeesString = $('#AttendeesString').val();
    if (attendeesString === '')
        updateTotalPrice();
    else {
        try {
            attendees = JSON.parse(attendeesString);
            loadAttendees();
        } catch (ex) {
            console.log('Error parsing AttendeesString with value: ' + attendeesString);
        }
    }
});

function updateTotalPrice() {
    var price = $('#price').data("price");
    var totalPrice = price * attendees.length;

    $('#totalPrice').html(displayCurrency(totalPrice));

    if ($('#PromoCode').val() === '')
        $('#promoCodeApplied').addClass('hidden');
    else
        $('#promoCodeApplied').removeClass('hidden');
}

function applyPromoCode() {
    var eventId = $('#EventId').val();
    var scheduleId = $('#ScheduleId').val();
    var promoCode = $('#promoCodeToApply').val();
    
    if (!promoCode || promoCode.length === 0) {
        popCartError("Please enter a promo code.");
        return false;
    }

    loadEffectOn('body');

    $.ajax(
    {
        url: '/Event/ApplyPromoCode',
        type: 'GET',
        //contentType: "application/json",
        data: { eventId: eventId, scheduleId: scheduleId, promoCode: promoCode },
        dataType: 'json',
        cache: false
    })
    .success(function (result) {
        loadEffectOff('body');
        $('#price').html(displayCurrency(result.Price));
        $('#price').data("price", result.Price);
        $('#PromoCode').val(promoCode);
        updateTotalPrice();
    })
    .error(function (xhr, status) {
        loadEffectOff('body');

        if (xhr.status === 404) {
            popCartError("Invalid promo code.");
        } else {
            popCartError("Error applying promo code.");
        }
    });
}

function showAddAttendeePopup() {
    var popup = $('#myModal');

    popup.find("#addFirstName").val("");
    popup.find("#addLastName").val("");
    popup.find("#addEmail").val("");
    popup.find("#addPhone").val("");
    popup.find("#addPosition").val("");
    popup.find("#addCompany").val("");
    popup.find("[data-for='addFirstName']").html("");
    popup.find("[data-for='addLastName']").html("");
    popup.find("[data-for='addEmail']").html("");
    popup.find("[data-for='addPhone']").html("");
    popup.find("[data-for='addPosition']").html("");
    popup.find("[data-for='addCompany']").html("");

    popup.show();
}

function onLostFocus(fieldJs) {
    var popup = $('#myModal');
    var field = $(fieldJs);

    validateField(popup, field);
}

function validateField(popup, field) {
    var fieldVal = field.val();
    var fieldName = field.attr("id");
    var validation = popup.find("[data-for='" + fieldName + "']");

    if (!fieldVal || fieldVal.length === 0) {
        validation.html("<span>This field is required</span>");
        return false;
    } else {
        validation.html("");
        return true;
    }
}

function addAttendee() {
    var valid = true;
    var popup = $('#myModal');

    var firstNameField = popup.find("#addFirstName");
    var lastNameField = popup.find("#addLastName");
    var emailAddressField = popup.find("#addEmail");
    var phoneNumberField = popup.find("#addPhone");
    var positionField = popup.find("#addPosition");
    var companyNameField = popup.find("#addCompany");

    valid = validateField(popup, firstNameField);
    valid = validateField(popup, lastNameField) && valid;
    valid = validateField(popup, emailAddressField) && valid;
    valid = validateField(popup, phoneNumberField) && valid;
    valid = validateField(popup, positionField) && valid;
    valid = validateField(popup, companyNameField) && valid;

    if (!valid) {
        return;
    }

    loadEffectOn('body');

    var attendee = {
        FirstName: firstNameField.val(),
        Lastname: lastNameField.val(),
        Email: emailAddressField.val(),
        Phone: phoneNumberField.val(),
        Position: positionField.val(),
        Company: companyNameField.val()
    }

    attendees.push(attendee);

    loadAttendees();
}

function loadAttendees() {
    $.ajax(
    {
        url: '/Event/AddAttendee',
        type: 'POST',
        contentType: "application/json",
        data: JSON.stringify({ attendees: attendees }),
        cache: false
    })
    .success(function (result) {
        loadEffectOff('body');
        $('#myModal').hide();
        $('#attendeeGrid tbody').html(result);
        updateTotalPrice();
    })
    .error(function (xhr, status) {
        loadEffectOff('body');
    });
}

function removeAttendee(btn) {
    var parent = $(btn).closest("tr");
    var indexPos = -1;
    var firstName = parent.find("[data-for='firstName']").html();
    var lastName = parent.find("[data-for='lastName']").html();
    var emailAddress = parent.find("[data-for='emailAddress']").html();
    var phoneNumber = parent.find("[data-for='phoneNumber']").html();
    var position = parent.find("[data-for='position']").html();
    var companyName = parent.find("[data-for='companyName']").html();

    // Find the attendee in the array
    attendees.forEach(function(item, index) {
        if (item.FirstName === firstName && item.Lastname === lastName &&
            item.Email === emailAddress && item.Phone === phoneNumber
            && item.Position === position && item.Company === companyName) {
            indexPos = index;
        }
    });

    if (indexPos >= 0) {
        // Remove it from the array
        attendees.splice(indexPos, 1);

        // Remove it from the table
        loadAttendees();
    } else {
        popCartError("Error removing attendee. Please load the page and start again.");
    }
}

function validateStep1Form() {
    if (attendees.length === 0) {
        popCartError("Please add at least one attendee.");
        return false;
    }

    if (!validateTerms()) {
        return false;
    }

    $('#AttendeesString').val(JSON.stringify(attendees));
    return true;
}

function validateTerms() {
    var agree = $('#AgreeConditions');
    var validator = $("span[data-valmsg-for='AgreeConditions']");

    if (agree.is(':checked')) {
        validator.html('');
        validator.removeClass('field-validation-error');
        validator.addClass('field-validation-valid');
        return true;
    } else {
        var msg = agree.data('val-brequired');

        validator.html(msg);
        validator.removeClass('field-validation-valid');
        validator.addClass('field-validation-error');
        return false;
    }
}

function displayCurrency(price) {
    if (Math.round(price) === price)
        return "$" + price;
    else
        return "$" + parseFloat(Math.round(price * 100) / 100).toFixed(2);
}

var medianet = {
    addTabs : function() {
        $('.nav-tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            
        });  
    },
    init : function() {
        this.addTabs();
    }
}

$(document).ready(function() {
    medianet.init();
    $("[rel=tooltip]").tooltip();
});
$('table tr:nth-child(2n+1)').addClass('odd');

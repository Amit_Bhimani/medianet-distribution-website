﻿function RemoveAttachment(id,dom_item)
{
    loadEffectOn('#form');

    if (typeof dom_item == 'number') 
    {
        dom_item = $('#' + dom_item.toString());
    }

    $.ajax(
    {
        url: 'deleteattachment',
        data: { id: id, __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        type: 'POST'
    })
    .success(function (result) 
    {
        RefreshWordCount();
        ReLoadShoppingCart();
        $(dom_item).remove();
        loadEffectOff('#form');
    })
    .error(function (xhr, status) 
    {
        loadEffectOff('#form');
    });        
}

function AddRemovalLink(selector,id)
{
    var files             = $(selector).find('ul[class="qq-upload-list"]');
    var last_file         = files.find('li')[files.find('li').length-1];
    var last_file_handler = $(last_file).find('span[class="qq-upload-file"]');
        
    var remove_link = $('<a> (X)</a>'); 
        remove_link.attr('style','cursor:pointer;');
        remove_link.bind( 'click' , function () 
        { 
            RemoveAttachment(id,last_file);
            return false; 
        });

    var preview_link = $('<a> (Preview)</a>');
        preview_link.attr('style', 'cursor:pointer;');
        preview_link.attr('target', '_blank');
        preview_link.attr('href', 'Preview/'+id);

    $(last_file_handler).append(preview_link);    
    $(last_file_handler).append(remove_link);
}

function RefreshWordCount() 
{
    loadEffectOn('#wordcount_section');

    $.ajax(
    {
        url: 'WordCount',        
        type: 'POST',
        data: { __RequestVerificationToken: antiForgetyToken() },
        cache: false,
        dataType: 'html'
    })
    .success(function (result) 
    {
        $('#wordcount_section').html(result);
        loadEffectOff('#wordcount_section');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#wordcount_section');
    });
}

function AddAttachmentToList(list,attachment)
{
    var template = '<li class="qq-upload-success" id="${id}"><span class="qq-upload-finished"></span><span class="qq-upload-file">${name}<a target="_blank" href="Preview/${id}"> (Preview)</a><a style="cursor:pointer;" onclick="RemoveAttachment(${id},${parent});return false;"> (X)</a></span><span style="display:inline">${kb}kB</span></li>';
    
    $.template("qqFileUploadTemplate", template);

    data = { id:attachment.MedianetAttachmentId, kb:attachment.Weight, name:attachment.Name, parent: attachment.MedianetAttachmentId };

    $.tmpl("qqFileUploadTemplate",data).appendTo(list);
}

$(document).ready(function () {
    var releaseUpload = $('#inputUploadRelease')[0];
    var attachmentUpload = $('#inputUploadAttachment')[0];

    var releaseUploader = new qq.FileUploader({
        element: releaseUpload,
        allowedExtensions: ['doc', 'docx', 'pdf', 'txt', 'htm', 'html', 'jpg', 'jpeg', 'png'],
        sizeLimit: 2097152,
        action: '/Wire/UploadRelease',
        multiple: false,
        autoUpload: true,
        debug: true,
        onSubmit: function (id, fileName) {
            fileUploadInProgress = true;
        },
        onComplete: function (id, fileName, responseJSON) {
            fileUploadInProgress = false;
            AddRemovalLink('#inputUploadRelease', responseJSON.id);
            RefreshWordCount();
            ReLoadShoppingCart();
        }
    });

    var attachmentUploader = new qq.FileUploader({
        element: attachmentUpload,
        allowedExtensions: ['wav', 'doc', 'docx', 'pdf', 'txt', 'htm', 'html', 'jpg', 'jpeg', 'png', 'xls', 'csv', 'gif', 'mp3', 'mov', 'wmv', 'mp4'],
        sizeLimit: 2097152,
        action: '/Wire/UploadAttachment',
        multiple: false,
        autoUpload: true,
        debug: true,
        multiple: true,
        onSubmit: function (id, fileName) {
            fileUploadInProgress = true;
        },
        onComplete: function (id, fileName, responseJSON) {
            fileUploadInProgress = false;
            AddRemovalLink('#inputUploadAttachment', responseJSON.id);
            RefreshWordCount();
            ReLoadShoppingCart();
        }
    });
});
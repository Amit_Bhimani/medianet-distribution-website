﻿/// <reference path="../jquery-1.5.1.js" />
/// <reference path="../bootstrap.js" />

// Define the trim function on strings for browsers that don't support it.
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

$.urlParam = function(name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return results[1] || 0;
};

var TitlesList = [{ Id: "Dr", Name: "Dr" }, { Id: "Prof", Name: "Prof" },
                        { Id: "Journalist", Name: "Journalist" }, { Id: "Hon", Name: "Hon" },
                        { Id: "Mrs", Name: "Mrs" }, { Id: "Father", Name: "Father" },
                        { Id: "Mr", Name: "Mr" }, { Id: "Ms", Name: "Ms" }];

var UserStatusList = [{ Id: "A", Name: "Active" }, { Id: "D", Name: "Deleted" }, { Id: "I", Name: "Inactive" }];

// Center a jQuery object in the window.
jQuery.fn.center = function() {
    this.css("position", "absolute");
    this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
    this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
    return this;
};

// Check if an element is scrolled to the bottom.
jQuery.fn.isScrollBottom = function() {
    var scrollDiff = this[0].scrollHeight - this.scrollTop();
    var height = this.outerHeight();

    return (scrollDiff == height);
};

var MedianetDistribution = (function () {
    var timeout = null;
    
    function showPopup(message, header, alertDialogCallback) {
        var popup = $('#alertModal');

        MedianetDistribution.HideWaiting(); // Just in case this is showing.

        if (message == '')
            message = "Unknown error";

        if (header == '')
            header = "Unknown error";

        $(".alertOk").off('click');
        $(".alertOk").on('click', function () {
            popup.modal('hide');
            if (alertDialogCallback) alertDialogCallback(true);
        });

        popup.find('.modal-header h3').html(header);
        popup.find('.alertModalMessage').html(message);
        popup.modal('show');
    }

    function showConfirm(message, header, confirmDialogCallback, confirmDialogData) {
        var popup = $('#confirmModal');

        MedianetDistribution.HideWaiting(); // Just in case this is showing.
        $(".confirmModalNo").off('click');
        $(".confirmModalNo").on('click', function () {
            popup.modal('hide');
            if (confirmDialogCallback) confirmDialogCallback(false, confirmDialogData);
        });

        $(".confirmModalYes").off('click');
        $(".confirmModalYes").on('click', function () {
            popup.modal('hide');
            if (confirmDialogCallback) confirmDialogCallback(true, confirmDialogData);
        });

        popup.find('.modal-header h3').html(header);
        popup.find('.confirmModalMessage').html(message);
        popup.modal('show');
    }

    return {
        easeOut: "easeOutQuart",
        visuallyHiddenClass: "visuallyhidden",
        errorList: "",
        /* JqueryGlobalHandlers */
        RegisterGlobalRedirectOnInvalidSession: function() {
            $.ajaxSetup({
                statusCode: {
                    401: function() {
                        window.location = "/";
                    }
                }
            });
        },
        ShowConfirm: function (message, heading, confirmDialogCallback, confirmDialogData) {
            showConfirm(message, heading, confirmDialogCallback, confirmDialogData);
        },
        ShowAlert: function (message, heading, alertDialogCallback) {
            showPopup(message, heading, alertDialogCallback);
        },
        ShowError: function(message, status) {
            if (!(typeof status === "undefined") && status == 401) {
                // Their session must be dead. Send them o the logon page.
                window.location = "/";
            } else {
                showPopup(message, "Internal Error");
            }
        },
        ShowWaiting: function() {
            // Make the waiting popup display after 1 second.
            timeout = setTimeout("$('#waitingModal').modal('show');", 500);
        },
        HideWaiting: function() {
            // Cancel the display of the waiting popup.
            if (timeout != null) {
                clearTimeout(timeout);
                timeout = null;
            }

            $("#waitingModal").modal('hide');
        },
        ClearErrorList: function() {
            MedianetDistribution.errorList = "";
        },
        BuildErrorList: function(errorMessage) {
            var errPrefix = "<span><i class='icon-remove-sign'></i>";
            var errSuffix = "</span><br />";
            MedianetDistribution.errorList = MedianetDistribution.errorList + errPrefix + " " + errorMessage + errSuffix;
        },
        HaveErrors: function() {
            return (MedianetDistribution.errorList != "" ? true : false);
        }
    };
})();

$(document).ready(function () {
});


﻿if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisp */) {
        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function")
            throw new TypeError();

        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

(function ($) {
    $.cleditor.defaultOptions.updateTextArea = function (html) {
        // Normalize to xhtml/html5 common standards and only keep allowed tags.
        var ary = html.split("<");
        console.log(ary);
        var end = -1;
        for (var i = 0; i < ary.length; i++) {
            if (ary[i].lastIndexOf("!--[if ", 7) === 0) { // handle Microsoft <!--[if ... <![endif]-->
                ary[i] = "";
                var found = false;
                for (i++; i < ary.length; i++) {
                    if (ary[i].lastIndexOf("![endif]-->", 11) === 0) { found = true; }
                    ary[i] = "";
                    if (found) break;
                }
                if (i >= ary.length) break;
            }
            end = ary[i].indexOf(">");
            if (end == -1) continue;
            ary[i] = ary[i].substring(0, end).toLowerCase() + ary[i].substring(end);
            var search = ["strong>", "em>", "strike>", "u>", "br>"];
            var replace = ["b>", "i>", "del>", "ins>", "br/>"];
            for (var j = 0; j < search.length; j++) {
                var pos = ary[i].lastIndexOf(search[j], search[j].length + 1);
                if (pos == 0 || (pos == 1 && ary[i].charAt(0) == '/')) {
                    ary[i] = (pos == 1 ? "/" : "") + replace[j] + ary[i].substring(search[j].length + pos);
                }
            }
            var spellcheckerRE = /^\/?span[^\/>]*\/?>/m;
            var cleanupRE = /^(\/?)(br|b|del|ins|i|li|ol|p|ul)[^a-zA-Z\/>]*[^\/>]*(\/?)>/m;
            if (spellcheckerRE.test(ary[i])) {
                //ary[i] = ary[i].replace(spellcheckerRE, "");
                ary[i] = "<" + ary[i];
            } else if (cleanupRE.test(ary[i])) {
                ary[i] = ary[i].replace(cleanupRE, "<$1$2$3>");
                //ary[i] = ary[i].replace(/^<p>/, "");
                //ary[i] = ary[i].replace(/^<\/?p\/?>/, "<br/>");
            } else {
                ary[i] = end + 1 < ary[i].length ? ary[i].substring(end + 1) : "";
            }
            ary[i] = ary[i].replace(/&nbsp;/gm, " ");
            ary[i] = ary[i].replace(/\n\n/gm, "\n");
        }
        ary = ary.filter(function (v) { return v !== ''; });
        console.log(ary);
        html = ary.join("");
        // Trim leading whitespace.
        var trimRE = /^(\s+|&nbsp;|<br\/?>|<p>(&nbsp;)*<\/p>)+/m;
        if (trimRE.test(html)) {
            html = html.replace(trimRE, "");
        }
        // Test if there is any actual non-whitespace text content.
        var body = document.getElementsByTagName("body")[0];
        var div = document.createElement("div");
        div.style.display = "none";
        body.appendChild(div);
        div.innerHTML = html;
        var text = div.innerText || div.textContent;
        body.removeChild(div);
        var trimRE = /\S/m;
        if (!trimRE.test(text)) html = "";
        return html;
    };
})(jQuery);
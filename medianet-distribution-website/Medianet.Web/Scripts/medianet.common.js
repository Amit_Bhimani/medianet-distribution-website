﻿// Note: medianet.common.js and medianet.common.new.js should be identical. I alternate between using the two files
// to avoid cached versions of the file breaking functionality after releasing new functionality.
// Ideally we'd use bundling to avoid this problem.

var fileUploadInProgress = false;
var shoppingCartContainer = '#shoppingcartSection';
var productInfo = '#productInfo';
//var error_template = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$(productInfo).hide();">×</button><h3 id="myModalLabel">Error:</h3>  </div><div class="modal-body">${error}</div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$(productInfo).hide();">Close</button></div>';
var popup_template = '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$(productInfo).hide();">×</button><h3 id="myModalLabel">${header}</h3></div><div class="modal-body">{{html body}}</div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$(productInfo).hide();">Close</button></div>';
var preview_release = "<div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" onclick=\"$('#productInfo').hide();\">×</button><h3 id=\"myModalLabel\">Preview</h3></div><div class=\"modal-body\" id=\"content\">${text}</div><div class=\"modal-footer\"><button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\" onclick=\"$('#productInfo').hide();\">Close</button></div>";
var next_action = 'next';
var previous_action = 'previous';

$(document).ready(function () {
    //$.template("errorInfo", error_template);
    $.template("warningInfo", popup_template);

    // Prevent the tooltips from being tabbed to.
    $(".help").attr('tabindex', '-1');

    // If we had an error then display it now;
    LookForError();
});

function LookForError() {
    var hiddenError = $("#errorMsg");
    var hiddenWarning = $("#warningMsg");
    var found = false;

    if (hiddenError.length > 0) {
        var str = hiddenError.val();
        if (str != "") {
            found = true;
            popCartError(str);
        }
    }

    if (hiddenWarning.length > 0) {
        var str = hiddenWarning.val();
        if (str != "") {
            found = true;
            ShowWarning(str);
        }
    }

    return found;
}

function ChangePriority() 
{
    loadEffectOn(shoppingCartContainer);

    $.ajax(
    {
        url: 'UpdateCartForPriority',
        type: 'POST',
        cache: false,
        data: { priority: $("#Priority").val() }
    })
    .success(function (result) {
        $(shoppingCartContainer).html(result);

        loadEffectOff(shoppingCartContainer);

        // If we had an error then display it now. Otherwise show a warning when high priority is selected.
        if (!LookForError()) {
            if ($("#Priority").val() == "High") {
                data = { header: "Warning:", body: "Warning:  High priority broadcasts will carry additional costs." };

                $('#productInfo').html('');
                $.tmpl("warningInfo", data).appendTo($('#productInfo'));
                $('#productInfo').show();
            }
        }
    })
    .error(function (xhr, status) {
        loadEffectOff(shoppingCartContainer);
        popCartError('Error updating shopping cart. Prices may not be accurate.');
    });
}

function showPackageMessage(id) {
    loadEffectOn('body');

    $.ajax(
    {
        url: 'GetProductInfoJson',
        type: 'GET',
        cache: false,
        data: { id: id }
    })
    .success(function (result) {
        loadEffectOff('body');
        ShowMessage("Information", result.comment);
    })
    .error(function (xhr, status) {
        loadEffectOff('body');
        popCartError('Error fetching package description');
    });
}

function antiForgetyToken() 
{
    return $("input[name=__RequestVerificationToken]").val();
}

function loadEffectOn(selector) 
{
    $(selector).css('opacity', '0.5');
    $(selector).css('filter', 'alpha(opacity=50)');
}

function loadEffectOff(selector) 
{
    $(selector).css('opacity', '1');
    $(selector).css('filter', 'alpha(opacity=100)');
}

function setWireStep(selector) 
{
    var cssClass = $(selector).attr('class').toString();

    $(selector).attr('class', cssClass+' active');
}

function setSmsStep(selector) {
    var cssClass = $(selector).attr('class').toString();
    $(selector).attr('class', cssClass + ' active');
}

function goNext() 
{
    if (!fileUploadInProgress) {
        if (
                ($('#extraeMail').length > 0) ||
                ($('#extraNumber').length > 0)
        ) {
            if (
                 (TrimSides($('#extraeMail').val()) != "") ||
                 (TrimSides($('#extraNumber').val()) != "")
            ) {
                popCartError("You haven't confirmed your extra recipient - use the plus button next to the email or sms to add it to the cart.");
                return false;
            }
        }

        $('#action').val(next_action);
        $('#form').submit();
        return false;
    }
    else {
        alert("Please wait, there's a file upload in progress");
        return false;
    }
}

function releaseMenuClick(action, order) {
    var currentOrder = +$("#CurrentStepOrder").val();
    if (order >= currentOrder) {
        $('#action').val(action);

        $('#form').submit();
    }
    else{
        previous_action = action;
        goPrevious();
    }
}
function goContacts() {
    $('#form').attr('action', 'lists');
    $('#action').val('lists');
    $('#form').submit();
}

function goPrevious() 
{
    if (!fileUploadInProgress) 
    {
        $('#form').attr('action', 'previous');
        
        $('#action').val(previous_action);
        $('#form').submit();
    }
    else
        alert("Please wait, there's a file upload in progress");
}

function goFinalise() 
{
    $('#action').val('finalise');
    $('#form').attr('action', 'finalise');
    $('#form').submit();
}

function ReLoadShoppingCart() 
{
    loadEffectOn(shoppingCartContainer);

    $.ajax(
    {
        url: 'ShoppingCart',
        type: 'POST',
        cache: false,
        data: { __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $(shoppingCartContainer).html(result);
        loadEffectOff(shoppingCartContainer);

        // If we had an error then display it now;
        LookForError();
    })
    .error(function (xhr, status) 
    {
        loadEffectOff(shoppingCartContainer);
    });
}

function ReloadRecommendations() 
{
    loadEffectOn('#promo-box');

    $.ajax(
    {
        url: 'getrecommendations',
        type: 'POST',
        cache: false,
        data: { __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $('#promo-box').html(result);
        disableAddToButtonsInCart();
        loadEffectOff('#promo-box');
    })
    .error(function (xhr, status) 
    {
        loadEffectOff('#promo-box');
    });   
}

function ReloadShoppingCartRecommendations()
{
    loadEffectOn('#cartRecommendations');    

    $.ajax(
    {
        url: 'shoppingcartrecommendations',        
        type: 'POST',
        cache: false,
        data: { __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $('#cartRecommendations').html(result);
        loadEffectOff('#cartRecommendations');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('#cartRecommendations');
    });    
}

function shouldBeDisabled(item, allChildren) {
    var ids = getDisableWithIds(item);

    if (ids.length > 0) {
        for (var i = 0; i < ids.length; i++) {
            if ($.inArray(ids[i], allChildren) < 0)
                return false;
        }

        return true;
    }

    return ($.inArray(item.attr('name'), allChildren) >= 0);
}

function getDisableWithIds(item) {
    var data = item.data("disable-with");
    var ids = [];

    if (typeof data != "undefined") {
        var children = String(data);

        if (children != "" && children != "undefined") {
            ids = children.split(",");
        }
    }
    
    return ids;
}

function disableAddToButtonsInCart() {
    var items = $(".cart-items div");
    var allButtons = $('a.cart');
    var allChildren = [];
    var i = 0;
    
    // Loop through all items in the cart to collect the child product ids
    for (i = 0; i < items.length; i++) {
        var id = String($(items[i]).data("id"));
        var ids = getDisableWithIds($(items[i]));

        if (id != "") {
            allChildren.push(id);
        }

        for (var j = 0; j < ids.length; j++) {
            // Save all the ids in the cart for later
            allChildren.push(ids[j]);
        }
    }

    for (i = 0; i < allButtons.length; i++) {
        var button = $(allButtons[i]);

        if (shouldBeDisabled(button, allChildren))
            button.addClass('btn-inactive');
        else
            button.removeClass('btn-inactive');
    }
}

function addToCart(productId, event) 
{
    var item = $('a[name="' + productId + '"]');

    if (item.length > 1) 
    {
        item = $(item[0]);
    }
   
    if ( item.attr('class').indexOf('btn-inactive') != -1 ) 
    {
        return false;
    }    

    loadEffectOn(shoppingCartContainer);

    $.ajax(
    {
        url: 'addproducttocart',
        type: 'POST',
        cache: false,
        data: { productId: productId, __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $(shoppingCartContainer).html(result);
        
        loadEffectOff(shoppingCartContainer);
        ReloadRecommendations();

        disableAddToButtonsInCart();

        // If we had an error then display it now;
        LookForError();
    })
    .error(function (xhr, status) {
        loadEffectOff(shoppingCartContainer);
    });

    if (event != undefined) 
    {
        event.cancelBubble = true;
    }
    else
        window.event.cancelBubble = true;
}

function AddSWCProductToCart(productId) {

    if ($("#SecondaryWebCategoryId").val() == '') {
            removeSWCFromCart();
        return;
    }
    
    loadEffectOn(shoppingCartContainer);

    $.ajax(
    {
        url: 'AddSwcProductToCart',
        type: 'POST',
        cache: false,
        data: { __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) {
        $(shoppingCartContainer).html(result);

        loadEffectOff(shoppingCartContainer);

        ReloadRecommendations();
        disableAddToButtonsInCart();

        // If we had an error then display it now;
        LookForError();
    })
    .error(function (xhr, status) {
        loadEffectOff(shoppingCartContainer);
    });

    if (event != undefined) {
        event.cancelBubble = true;
    }
    else
        window.event.cancelBubble = true;
}

function getProductsInfo(id1, id2,event) 
{
    loadEffectOn('body');

    $.ajax(
    {
        url: 'getproductsinfo',
        type: 'POST',
        cache: false,
        data: { Id1: id1, Id2:id2, __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) {
        $('#productInfo').html(result);
        $('#productInfo').show();
        loadEffectOff('body');
    })
    .error(function (xhr, status) {
        loadEffectOff('body');
    });

    if (event != undefined) 
    {
        event.cancelBubble = true;
    }
    else
        window.event.cancelBubble = true;
}

function getProductInfo(id,event)
{        
    loadEffectOn('body');

    $.ajax(
    {
        url: 'getproductinfo',        
        type: 'POST',
        cache: false,
        data: { Id:id , __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {        
        $('#productInfo').html(result);
        $('#productInfo').show();
        loadEffectOff('body');
    })
    .error(function(xhr, status) 
    {
        loadEffectOff('body');
    });

    if (event != undefined) 
    {
        event.cancelBubble = true;
    }
    else
        window.event.cancelBubble = true;
}

function removeSWCFromCart(productId)
{
    loadEffectOn(shoppingCartContainer);

    $.ajax(
    {
        url: 'RemoveSwcFromCart',
        type: 'POST',
        cache: false,
        data: {__RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) 
    {
        $(shoppingCartContainer).html(result);
        disableAddToButtonsInCart();
        loadEffectOff(shoppingCartContainer);
        ReloadRecommendations();

        // If we had an error then display it now;
        LookForError();

        // If given a callback method to call then call now.
        if (callbackMethod) callbackMethod();
    })
    .error(function (xhr, status) {
        loadEffectOff(shoppingCartContainer);
    }); 
}

function removeFromCart(itemId, productId, serviceId, callbackMethod) {
    loadEffectOn(shoppingCartContainer);

    $.ajax(
    {
        url: 'RemoveFromCart',
        type: 'POST',
        cache: false,
        data: { ShoppingCartItem: itemId, __RequestVerificationToken: antiForgetyToken() }
    })
    .success(function (result) {
        $(shoppingCartContainer).html(result);
        disableAddToButtonsInCart();
        loadEffectOff(shoppingCartContainer);
        ReloadRecommendations();

        // If we had an error then display it now;
        LookForError();

        // If given a callback method to call then call now.
        if (callbackMethod) callbackMethod();
    })
    .error(function (xhr, status) {
        loadEffectOff(shoppingCartContainer);
    });
}

function popCartError(error)
{
    if ( error != "" )
    {                
        data = { error:error };

        var html =  "<div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\" onclick=\"$(productInfo).hide();\">×</button>";
            html += "<h3 id=\"myModalLabel\">Error:</h3></div><div class=\"modal-body\">" + error + "</div>";
            html += "<div class=\"modal-footer\"><button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\" onclick=\"$(productInfo).hide();\">Close</button></div>";

        $('#productInfo').html(html);
        $('#productInfo').show();
    }
}

function ShowWarning(msg) {
    ShowMessage("Warning", msg);
}

function ShowMessage(header, msg) {
    var data = { header: header, body: msg };

    $('#productInfo').html('');
    $.tmpl("warningInfo", data).appendTo($('#productInfo'));
    $('#productInfo').show();
}

function ShowMessageWithCallback(header, msg, callback) {
    var popup = $('#productInfo');
    var html = "<div class=\"modal-header\"><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>";
    html += "<h3 id=\"myModalLabel\">" + header + "</h3></div><div class=\"modal-body\">" + msg + "</div>";
    html += "<div class=\"modal-footer\"><button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button></div>";

    popup.html(html);
    popup.show();
    popup.find('button').click(function () {
        popup.hide();
        callback();
    });
}

function ShowOkCancel(header, msg, callback) {
    var popup = $('#productInfo');
    var html = "<div class=\"modal-header\"><button type=\"button\" class=\"close\" aria-hidden=\"true\" onclick=\"$(productInfo).hide();\">×</button>";
    html += "<h3 id=\"myModalLabel\">" + header + "</h3></div><div class=\"modal-body\">" + msg + "</div>";
    html += "<div class=\"modal-footer\"><button class=\"btn\" id=\"okButton\" aria-hidden=\"true\">Ok</button><button class=\"btn\" aria-hidden=\"true\" onclick=\"$(productInfo).hide();\">Cancel</button></div>";

    popup.html(html);
    popup.show();
    popup.find('#okButton').click(function () {
        popup.hide();
        callback();
    });
}

function Trim(val)
{    
    if ( val != null )
    {
        return val.replace(/\s+/g,'');
    }
    else
    {
        return '';
    }
}

function TrimSides(val) 
{
    if (val != null) 
    {
        return val.replace(/^\s+/g, '').replace(/\s+$/g, '');
    }
    else 
    {
        return '';
    }    
}

function Continue() 
{
    $('#form').attr('action', 'Continue');
    $('#form').submit();
}

function GoBack() 
{
    $('#form').attr('action', 'GoBack');
    $('#form').submit();
}

function copyAddress() 
{
    $('#BillingAddress').val($('#Address').val());
    $('#BillingAddress2').val($('#Address2').val());
    $('#BillingPostCode').val($('#PostCode').val());
    $('#BillingState').val($('#State').val());
    $('#BillingSuburb').val($('#Suburb').val());
}

function PreviewWire(html) {
    var modal = $('#productInfo');
    var data = { text: "" };

    $.template("preview_wire", preview_release);

    modal.html('');
    $.tmpl("preview_wire", data).appendTo(modal);
    modal.find("#content").append(html);
    modal.find("#content").find("a").prop("target", "_blank");
    modal.show();
}

function UnsetScheduling(fldName) {
    if (fldName === undefined ||fldName == null || fldName.length == 0) {
        fldName = "Schedule"
    }

    $('#' + fldName + 'TimeHour').attr('disabled', 'disabled');
    $('#' + fldName + 'Date').attr('disabled', 'disabled');
    $('#' + fldName + 'TimeMinutes').attr('disabled', 'disabled');

    $('#' + fldName + 'Date').val('');
    $('#' + fldName + 'TimeHour')[0].selectedIndex = 0;
    $('#' + fldName + 'TimeMinutes')[0].selectedIndex = 0;
}

function setScheduling(fldName) {
    if (fldName === undefined || fldName == null || fldName.length == 0) {
        fldName = "Schedule"
    }

    $('#' + fldName + 'TimeHour').removeAttr('disabled');
    $('#' + fldName + 'Date').removeAttr('disabled');
    $('#' + fldName + 'TimeMinutes').removeAttr('disabled');
}

Following are DB Changes Needed to Reflect in Application.

1) `alter table mn_User add IndustryCode nvarchar(max)`

2) `alter table mn_User add RelevantSubjects nvarchar(max)`

3) `alter table mn_custom_pricing alter column CostPerUnit decimal(18,2)`

4) `alter table mn_job add TransactionId nvarchar(max)`

5) `alter table mn_job add PaymentMethod varchar(50)`

6) `insert into mn_environment(UserId,Application,Type,Value,Description)
values(1,'OPR','AfterChargeBeeDebtorNumber','20000','The DebtorNumber Start from. After ChargeBee integration. This is used to determine Where to start new DebtorNumber.')`


Note : In WCF Please Check for Following Path medianet-wcf-service-a9561b2ff194/Medianet.ServiceTemplate/Medianet_Upgrade_Account.html, if it is excluded from project than need to add manually into project.
